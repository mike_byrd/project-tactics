﻿using System.Collections;
using System.Collections.Generic;
using System;
using MCB;

public class Command
{
  public CommandTypes type;
  public object data;
  public List<Tile> targets;

  public Command(CommandTypes type)
  {
    this.type = type;
  }

  public Command(CommandTypes type, object data)
  {
    this.type = type;
    this.data = data;
  }

  public Command(CommandTypes type, object data, List<Tile> targets)
  {
    this.type = type;
    this.data = data;
    this.targets = targets;
  }
}

