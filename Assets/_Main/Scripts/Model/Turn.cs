﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public class Turn
{
    public Unit actor;
    public bool hasUnitMoved;
    public bool hasUnitActed;
    public bool lockMove;
    public Ability ability;
    public List<Tile> targets;
    public PlanOfAttack plan;
    Tile startTile;
    Direction startDir;

    public void Change(Unit current)
    {
        actor = current;
        hasUnitMoved = false;
        hasUnitActed = false;
        lockMove = false;
        startTile = actor.CurrentTile;
        startDir = actor.Facing;
        plan = null;
    }

    public void UndoMove()
    {
        hasUnitMoved = false;
        startTile.Place(actor);
        actor.Facing = startDir;
        actor.Match();
    }
}