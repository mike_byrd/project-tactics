﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;

public class LevelData : ScriptableObject
{
    public List<Point> playerStartingTiles;
    public List<Point> enemyStartingTiles;
    public List<Vector3> tiles;
}