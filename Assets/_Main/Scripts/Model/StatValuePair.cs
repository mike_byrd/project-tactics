﻿[System.Serializable]
public struct StatValuePair
{
  public StatTypes stat;
  public int value;
}
