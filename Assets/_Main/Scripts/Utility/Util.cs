﻿using System;
using System.Collections.Generic;
using System.Linq;
using MCB;
using UnityEngine;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

public class Util
{
    public static string GetDisplayName(JobType jobType)
    {
        switch (jobType)
        {
            case JobType.Soldier:
            case JobType.Archer:
            case JobType.Thief:
                return jobType.ToString();
            case JobType.WhiteMage:
                return "White Mage";
            case JobType.BlackMage:
                return "Black Mage";
        }

        Debug.LogWarning("GetDisplayName: Invalid JobType");
        return "";
    }

    public static GameObject InstantiatePrefab(string name)
    {
        GameObject prefab = Resources.Load<GameObject>(name);
        if (prefab == null)
        {
            Debug.LogError("No Prefab for name: " + name);
            return new GameObject(name);
        }

        GameObject instance = Object.Instantiate(prefab);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    public static GameObject InstantiatePrefab(GameObject prefab)
    {
        GameObject instance = Object.Instantiate(prefab);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    public static GameObject InstantiatePrefab(GameObject prefab, Transform parent,
        bool instantiateInWorldSpace = false)
    {
        GameObject instance = Object.Instantiate(prefab, parent, instantiateInWorldSpace);
        instance.name = instance.name.Replace("(Clone)", "");
        return instance;
    }

    public static Vector3 DirectionToDegrees(Direction direction)
    {
        Vector3 rotation = new Vector3();

        switch (direction)
        {
            case Direction.North:
                rotation.y = 0;
                break;
            case Direction.East:
                rotation.y = 90f;
                break;
            case Direction.South:
                rotation.y = 180f;
                break;
            case Direction.West:
                rotation.y = -90f;
                break;
            case Direction.NorthEast:
                rotation.y = 45f;
                break;
            case Direction.SouthEast:
                rotation.y = 135f;
                break;
            case Direction.SouthWest:
                rotation.y = 225f;
                break;
            case Direction.NorthWest:
                rotation.y = -45f;
                break;
        }

        return rotation;
    }

    public static IEnumerable<T> GetEnumValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }

    public static bool IsPointerOverUI()
    {
        EventSystem eventSystem = EventSystem.current;
        if (!eventSystem)
        {
            return false;
        }

        return eventSystem.IsPointerOverGameObject();
    }

    public static bool IsMouseOverGameWindow => !(0 > Input.mousePosition.x || 0 > Input.mousePosition.y ||
                                                  Screen.width < Input.mousePosition.x ||
                                                  Screen.height < Input.mousePosition.y);

    /// <summary>
    /// Gets all the UI elements under the mouse.
    /// </summary>
    /// <returns></returns>
    public static List<RaycastResult> RaycastMouse()
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current)
        {
            pointerId = -1,
        };

        pointerData.position = Input.mousePosition;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerData, results);

        return results;
    }

    public static int GetAnyMouseButtonDown()
    {
        if (Input.GetMouseButtonDown(0)) return 0; // Left click
        if (Input.GetMouseButtonDown(1)) return 1; // Right click
        if (Input.GetMouseButtonDown(2)) return 2; // Middle click
        return -1;
    }
}