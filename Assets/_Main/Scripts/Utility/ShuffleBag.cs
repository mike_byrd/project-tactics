﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShuffleBag<T> {
  List<T> valuesInBag;
  int drawnValueCount;

  public ShuffleBag() {
    valuesInBag = new List<T>();
    drawnValueCount = 0;
  }

  public ShuffleBag(T[] values, bool isRandom = false) {
    valuesInBag = new List<T>();
    drawnValueCount = 0;

    for (int i = 0; i < values.Length; i++) {
      AddValue(values[i], isRandom);
    }
  }

  public void Shuffle() {
    //Move the values to a new bag in a random order.
    List<T> newValues = new List<T>();

    int randomIndex;
    while (valuesInBag.Count > 0) {
      randomIndex = Random.Range(0, valuesInBag.Count);
      newValues.Add(valuesInBag[randomIndex]);
      valuesInBag.RemoveAt(randomIndex);
    }

    valuesInBag = newValues;
    drawnValueCount = 0;
  }

  public void AddValue(T value, bool isRandom = false) {
    if (isRandom) {
      valuesInBag.Insert(Random.Range(0, valuesInBag.Count), value);
    } else {
      valuesInBag.Add(value);
    }
  }

  public T DrawValue() {
    if (drawnValueCount == valuesInBag.Count) {
      Shuffle();
    }

    T drawnValue = valuesInBag[drawnValueCount];
    drawnValueCount++;
    return drawnValue;
  }
}