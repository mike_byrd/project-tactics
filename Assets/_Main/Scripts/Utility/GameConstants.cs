﻿using UnityEngine;

namespace ProjectTactics
{
    public static class K
    {
        public static class TileColor
        {
            public static Color UnselectedClearColor => new Color(0.1f, 1f, 0.0f, .0f);
            public static Color UnselectedWhiteColor => new Color(1f, 1f, 1f, 1f);
            public static Color SelectedColor => new Color(0.1f, 1f, 0.0f, .2f);
            public static Color SelectedSolidColor => new Color(0.1f, 1f, 0.0f, .6f);
        }
        
        public static class ZoneName
        {
            public const string TownCenter = "town-center";
        }
    }
}

public static class GameConstants
{
    // Inputs.
    public const string INPUT_FIRE1 = "Fire1";
    public const string INPUT_FIRE2 = "Fire2";
    public const string INPUT_FIRE3 = "Fire3";
    public const string INPUT_UP = "Up";
    public const string INPUT_DOWN = "Down";
    public const string INPUT_LEFT = "Left";
    public const string INPUT_RIGHT = "Right";
    public const string INPUT_ACTION_BASIC_ATTACK = "Basic Attack";
    public const string INPUT_ACTION_ABILITY1 = "Ability 1";
    public const string INPUT_ACTION_ABILITY2 = "Ability 2";
    public const string INPUT_ACTION_ABILITY3 = "Ability 3";
    public const string INPUT_ACTION_ABILITY4 = "Ability 4";
    public const string INPUT_ACTION_ABILITY5 = "Ability 5";
    public const string INPUT_ACTION_ABILITY6 = "Ability 6";
    public const string INPUT_ACTION_ABILITY7 = "Ability 7";
    public const string INPUT_ACTION_ABILITY8 = "Ability 8";
}