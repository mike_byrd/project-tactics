using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class RenderTextureTarget : MonoBehaviour
    {
        public Camera renderCamera;
        public Transform container;
        public RenderTexture Texture { get; private set; }

        public GameObject Subject { get; private set; }
        public bool IsLoaded { get; private set; }

        public Unit LoadForUnit(UnitData unitData)
        {
            Unload();
            IsLoaded = true;

            Texture = RenderTexture.GetTemporary(512, 512, 24, RenderTextureFormat.ARGB32);
            renderCamera.targetTexture = Texture;
            renderCamera.gameObject.SetActive(true);

            Unit clone = UnitFactory.Create(unitData, container);
            clone.Hud.Hide();
            Subject = clone.gameObject;
            return clone;
        }

        public Unit LoadForUnit(Unit unit)
        {
            Unload();
            IsLoaded = true;

            Texture = RenderTexture.GetTemporary(512, 512, 24, RenderTextureFormat.ARGB32);
            renderCamera.targetTexture = Texture;
            renderCamera.gameObject.SetActive(true);

            Unit clone = Instantiate(unit, container);
            clone.Hud.Hide();
            clone.DisplayName = unit.DisplayName;
            clone.transform.localPosition = Vector3.zero;
            clone.transform.localRotation = Quaternion.identity;

            Subject = clone.gameObject;
            return clone;
        }

        public void Unload()
        {
            if (!IsLoaded)
            {
                return;
            }

            RenderTexture.ReleaseTemporary(Texture);
            Texture = null;

            if (renderCamera)
            {
                renderCamera.targetTexture = null;
                renderCamera.gameObject.SetActive(false);
            }

            if (Subject)
            {
                Destroy(Subject);
                Subject = null;
            }

            IsLoaded = false;
        }
    }
}
