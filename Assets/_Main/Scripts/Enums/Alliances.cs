﻿using UnityEngine;
using System.Collections;

public enum Alliances
{
	None = 0,
	Neutral = 1 << 0,
	Ally = 1 << 1,
	Enemy = 1 << 2
}