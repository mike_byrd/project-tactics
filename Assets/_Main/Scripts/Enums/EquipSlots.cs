﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This enum will be a bit-mask which can specify one or more combinations of 
/// locations to equip an item. For example, there might be one-handed as well 
/// as two-handed weapons, so the item will need to indicate how many of these 
/// slots to occupy.
/// </summary>
[System.Flags]
public enum EquipSlots
{
	None = 0,
	Primary = 1 << 0, 	// usually a weapon (sword etc)
	Secondary = 1 << 1,	// usually a shield, but could be another sword (dual-wield) or occupied by two-handed weapon
	Head = 1 << 2,		// helmet, hat, etc
	Body = 1 << 3,		// body armor, robe, etc
	Accessory = 1 << 4	// ring, belt, etc
}