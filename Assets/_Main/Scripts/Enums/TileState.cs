﻿public enum TileState
{
  None,
  BattleHighlighted,
  WorldHighlighted,
  Selected,
  OutOfSight,
  Enemy,
  Ally
}
