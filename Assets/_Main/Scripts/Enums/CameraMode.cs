﻿public enum CameraMode
{
  FollowingUnit,
  PreparingForPan,
  Panning
}
