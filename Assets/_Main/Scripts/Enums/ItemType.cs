﻿public enum ItemType
{
  None,
  Sword,
  Bow,
  Shield,
  Staff,
  Dagger,

  //Armor
  Helmet
}