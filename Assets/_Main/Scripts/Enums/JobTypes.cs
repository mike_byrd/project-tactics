﻿public enum JobType
{
  None,
  // Human Jobs
  Soldier,
  Archer,
  Thief,
  WhiteMage,
  BlackMage
}