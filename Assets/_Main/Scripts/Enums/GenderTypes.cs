﻿public enum GenderType {
  None,
  Male,
  Female,
  Animal
}