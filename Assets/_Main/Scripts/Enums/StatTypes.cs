﻿using UnityEngine;
using System.Collections;

public enum StatTypes
{
    LVL, // Level
    EXP, // Experience

    HP, // Hit Points
    MHP, // Max Hit Points
    AP, // Action Points
    MAP, // Max Action Points
    MP, // Move Points
    MMP, // Max Move Points
    RNG, // Range

    ATK, // Physical Attack
    DEF, // Physical Defense
    MAT, // Magic Attack
    MDF, // Magic Defense

    JMP, // Jump Height
    CRT, // Critical Hit Rate
    SPD, // Speed - for turn order
    Count
}