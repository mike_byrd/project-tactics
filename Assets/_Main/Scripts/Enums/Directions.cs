﻿using UnityEngine;
using System.Collections;

namespace MCB
{
    public enum Direction
    {
        North,      // 180
        NorthEast,  // 45
        East,       // 90
        SouthEast,  // 135
        South,      // 0
        SouthWest,  // 225
        West,        // -90
        NorthWest   // -45
    }
}