using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectTactics
{
    public class ZoneChangePoint : MonoBehaviour
    {
        public static event Action<string> Clicked;

        [SerializeField] private string zoneName;

        private void OnMouseUpAsButton()
        {
            if (!Util.IsPointerOverUI())
            {
                Clicked?.Invoke(zoneName);
            }
        }
    }

}