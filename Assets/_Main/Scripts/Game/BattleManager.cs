using System;
using System.Collections;
using System.Collections.Generic;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class BattleManager : MonoBehaviour
    {
        private static BattleManager _i;

        private static string _battleId;
        private static string _hostId;
        private static PartyData _party;
        private static List<UnitData> _allies;
        private static List<UnitData> _enemies;
        private static string[] _turnOrder;
        private static int _currentTurn;

        public static LocationData LocationData { get; private set; }

        private void Awake()
        {
            if (!_i)
            {
                _i = this;
            }
            else if (_i != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }

        public static void SetupBattleData(CreateBattleResult result)
        {
            _allies = new List<UnitData>();
            _enemies = new List<UnitData>();

            _battleId = result.battleId;
            _hostId = result.hostId;
            _party = new PartyData(result.playerParty);
            _turnOrder = result.turnOrder;
            _currentTurn = result.currentTurn;

            LocationData = new LocationData(result.locationData);

            foreach (UnitDataModel enemy in result.enemyUnits)
            {
                _enemies.Add(new UnitData(enemy));
            }
        }

        public static UnitData[] GetEnemyUnits()
        {
            return _enemies.ToArray();
        }
    }
}
