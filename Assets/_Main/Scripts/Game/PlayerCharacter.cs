﻿using System;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class PlayerCharacter : MonoBehaviour
    {
        public static event Action<PlayerCharacter> AddedToWorld;
        public static event Action<PlayerCharacter> RemovedFromWorld;
        public Unit unit { get; private set; }

        private void Awake()
        {
            unit = GetComponent<Unit>();
        }
    }
}
