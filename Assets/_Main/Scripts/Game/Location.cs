using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class Location : MonoBehaviour
    {
        [SerializeField] private WorldGrid worldGrid;
        public WorldGrid Grid => worldGrid;

        [SerializeField] private bool highlightTileOnHover;
        public bool HighlightTileOnHover => highlightTileOnHover;

        public void Initialize(LocationData locationData)
        {
            worldGrid.Initialize(locationData);
        }
    }
}
