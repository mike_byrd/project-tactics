﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;
using UnityEngine.Serialization;

namespace ProjectTactics
{
    public class UnitModelVisual : MonoBehaviour
    {
        public GameObject Hair { get; private set; }
        public Transform Head { get; set; }
        public Transform RightHand { get; set; }
        public Transform LeftHand { get; set; }

        public Animator Anim { get; private set; }
        public BoxCollider Collider { get; private set; }

        //private const string HeadLocation = "RigPelvis/RigSpine1/RigSpine2/RigRibcage/RigNeck/RigHead/Dummy Prop Head";

        public void Init(GameObject bodyModel, GameObject hairModel = null)
        {
            if (!bodyModel)
            {
                Debug.LogWarning("Can't assign null model");
                return;
            }

            // Attach the Model object to the Unit object
            Transform jumper = transform.Find("Jumper");
            bodyModel.transform.SetParent(jumper);

            Anim = bodyModel.GetComponent<Animator>();
            Head = bodyModel.transform.FindDeepChild("Dummy Prop Head");
            RightHand = bodyModel.transform.FindDeepChild("Dummy Prop Right");
            LeftHand = bodyModel.transform.FindDeepChild("Dummy Prop Left");

            Collider = GetComponent<BoxCollider>();

            if (hairModel)
            {
                SetHair(hairModel);
            }
        }

        public void  SetHair(GameObject newHair, bool destroyOldHair = true)
        {
            if (!newHair)
            {
                return;
            }

            if (Hair && destroyOldHair)
            {
                Destroy(Hair);
            }

            Hair = newHair;
            Hair.transform.SetParent(Head, false);
        }

        public void HideHair()
        {
            Hair.gameObject.SetActive(false);
        }

        public void ShowHair()
        {
            Hair.gameObject.SetActive(true);
        }

    }
}
