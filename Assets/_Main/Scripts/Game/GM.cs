using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    /// <summary>
    /// Game Manager
    /// </summary>
    public class GM : MonoBehaviour
    {
        public static GM Instance { get; private set; }
        public static LocationData BattleLocationData { get; set; }
        public static List<UnitData> Enemies { get; set; }

        // TODO: Move these to a more permanent place. Maybe load from resources into a database like UnitFactory.
        [SerializeField] private Material tileMaterialDefault;
        [SerializeField] private Material tileMaterialUnwalkable;
        [SerializeField] private Material tileMaterialAlly;
        [SerializeField] private Material tileMaterialEnemy;
        [SerializeField] private Material tileMaterialTravel;
        public static Material TileMaterialDefault => Instance.tileMaterialDefault;
        public static Material TileMaterialUnwalkable => Instance.tileMaterialUnwalkable;
        public static Material TileMaterialAlly => Instance.tileMaterialAlly;
        public static Material TileMaterialEnemy => Instance.tileMaterialEnemy;
        public static Material TileMaterialTravel => Instance.tileMaterialTravel;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
        }

        public static void ExitGame()
        {
#if UNITY_EDITOR
            if (Application.isEditor && UnityEditor.EditorApplication.isPlaying)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
#else
            Application.Quit();
#endif
        }

#if !PRODUCTION
        public static int ServerSelectionIndex = 0;
#endif
    }
}