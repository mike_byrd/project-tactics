﻿using System;
using MCB.Common;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MCB
{
    public class TileSurface : MonoBehaviour
    {
        public static event Action<Tile> MouseDown;
        public static event Action<Tile> MouseClicked;
        public static event Action<Tile> MouseEntered;
        public static event Action<Tile> MouseUp;

        public const float MinHeight = 0.125f;
        public const float MaxHeight = 5f;

        public Point GridPosition { get; private set; }
        public float Height { get; private set; }

        public Tile Tile { get; private set; }

        private void Awake()
        {
            Tile = GetComponentInParent<Tile>();
        }

        public void SetGridPosition(Point gridPosition)
        {
            GridPosition = gridPosition;
            transform.localPosition = new Vector3(gridPosition.x, Height, gridPosition.y);
        }

        public void SetHeight(float height)
        {
            Height = height;
            var t = transform;
            var oldPosition = t.position;
            t.localPosition = new Vector3(oldPosition.x, height, oldPosition.z);
        }

        // Note: Clicking too fast will result in a double click which will prevent this function from firing...
        private void OnMouseUpAsButton()
        {
            if (Util.IsPointerOverUI())
            {
                return;
            }

            MouseClicked?.Invoke(Tile);
        }

        private void OnMouseDown()
        {
            if (Util.IsPointerOverUI())
            {
                return;
            }

            MouseDown?.Invoke(Tile);
        }

        private void OnMouseEnter()
        {
            if (Util.IsPointerOverUI())
            {
                return;
            }

            MouseEntered?.Invoke(Tile);
        }

        private void OnMouseUp()
        {
            if (Util.IsPointerOverUI())
            {
                return;
            }

            MouseUp?.Invoke(Tile);
        }
    }
}