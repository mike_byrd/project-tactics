using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.AI;
using MCB.Cameras;
using MCB.Common;
using MCB.MapEditor;
using ProjectTactics.DataModels;
using UnityEngine;
using Tile = MCB.Tile;
using TileData = ProjectTactics.DataModels.TileData;

namespace ProjectTactics
{
    public class WorldGrid : MonoBehaviour
    {
        public event Action MouseEntered;
        public event Action MouseExited;

        [SerializeField] private Transform tileContainer;
        [SerializeField] private GameObject[] environmentObjects;

        private Tile _tilePrefab;
        private Astar _pathfinding;

        private Point _min;
        private Point _max;
        bool _isMouseOnBoard;
        private Camera _mainCamera;

        public Point Min => _min;
        public Point Max => _max;

        Point[] directions = new Point[4]
        {
            new Point(0, 1),
            new Point(0, -1),
            new Point(1, 0),
            new Point(-1, 0)
        };

        public Dictionary<Point, Tile> Tiles { get; set; }
        public Point TravelPosition { get; set; }
        public List<Point> PlayerStartingTiles { get; set; }
        public List<Point> EnemyStartingTiles { get; set; }
        public bool IsShowingTacticalView { get; private set; } = true;

        private void Awake()
        {
            Tiles = new Dictionary<Point, Tile>();
            PlayerStartingTiles = new List<Point>();
            EnemyStartingTiles = new List<Point>();
        }

        private void Start()
        {
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            LayerMask mask = 1 << LayerMask.NameToLayer("Tile");
            bool tileHit = Physics.Raycast(ray, out RaycastHit hit, 1000, mask);
            if (tileHit && !_isMouseOnBoard)
            {
                _isMouseOnBoard = true;
                MouseEntered?.Invoke();
            }
            else if (!tileHit && _isMouseOnBoard)
            {
                _isMouseOnBoard = false;
                MouseExited?.Invoke();
            }
        }

        public void Initialize(LocationData locationData)
        {
            if (locationData == null)
            {
                Debug.Log("World has no grid data: " + name);
                return;
            }

            _tilePrefab = Resources.Load<Tile>("Grid/Tile");
            if (!_tilePrefab)
            {
                Debug.LogError($"Failed to load tile prefab.");
                return;
            }

            _min = new Point(int.MaxValue, int.MaxValue);
            _max = new Point(int.MinValue, int.MinValue);

            GenerateGrid(locationData.Width, locationData.Height);
            UpdateTiles(locationData.Tiles);
            InitializePathfinding(locationData.Width, locationData.Height);

            // TODO: Allow defaulting this in the settings.
            HideTacticalView();
        }

        public void ToggleTacticalView()
        {
            SetTacticalViewVisible(!IsShowingTacticalView);
        }

        public void ShowTacticalView()
        {
            SetTacticalViewVisible(true);
        }

        public void HideTacticalView()
        {
            SetTacticalViewVisible(false);
        }

        private void SetTacticalViewVisible(bool visible)
        {
            foreach (KeyValuePair<Point, Tile> kvp in Tiles)
            {
                kvp.Value.SetVisible(visible);
            }

            foreach (GameObject envObject in environmentObjects)
            {
                envObject.SetActive(!visible);
            }

            IsShowingTacticalView = visible;
        }

        public void Place(Unit unit, Point position)
        {
            Tile tile = GetTile(position);
            if (!tile)
            {
                Debug.LogError($"Invalid tile position: {position}");
                return;
            }

            unit.Movement.SetWorldGrid(this, tile);
        }

        private void InitializePathfinding(int width, int height)
        {
            Node[,] nodes = new Node[width, height];
            foreach (var kvp in Tiles)
            {
                Tile t = kvp.Value;
                Node n = new Node(t.GridPosition.ToNumericVector2(), t.IsWalkable, t.Height);
                nodes[kvp.Key.x, kvp.Key.y] = n;
            }

            _pathfinding = new Astar(nodes);
        }

        private void GenerateGrid(int width, int height)
        {
            Clear();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Tile t = Instantiate(_tilePrefab, tileContainer).GetComponent<Tile>();
                    t.transform.SetParent(transform);
                    t.Load(new Point(x, y));
                    Tiles.Add(t.GridPosition, t);

                    _min.x = Mathf.Min(_min.x, t.GridPosition.x);
                    _min.y = Mathf.Min(_min.y, t.GridPosition.y);
                    _max.x = Mathf.Max(_max.x, t.GridPosition.x);
                    _max.y = Mathf.Max(_max.y, t.GridPosition.y);
                }
            }
        }

        private void UpdateTiles(TileData[] worldTiles)
        {
            foreach (TileData td in worldTiles)
            {
                Tile t = Tiles[td.Position];
                t.UpdateTileData(td);

                switch (td.Type)
                {
                    case TileType.StartAlly:
                        PlayerStartingTiles.Add(td.Position);
                        break;
                    case TileType.StartEnemy:
                        EnemyStartingTiles.Add(td.Position);
                        break;
                    case TileType.Travel:
                        TravelPosition = td.Position;
                        break;
                }
            }
        }

        private void Clear()
        {
            Tile[] temp = tileContainer.GetComponentsInChildren<Tile>();
            foreach (var child in temp)
            {
                Destroy(child.gameObject);
            }

            Tiles.Clear();
            PlayerStartingTiles.Clear();
            EnemyStartingTiles.Clear();
        }

        public Tile GetTile(Vector2 position)
        {
            return GetTile(position.x, position.y);
        }

        public Tile GetTile(Point position)
        {
            return GetTile(position.x, position.y);
        }

        public Tile GetTile(float x, float y)
        {
            return GetTile((int) x, (int) y);
        }

        public Tile GetTile(int x, int y)
        {
            Point p = new Point(x, y);
            return Tiles.TryGetValue(p, out Tile tile) ? tile : null;
        }

        public List<Tile> FindPath(Tile from, Tile to, float jumpHeight, bool allowDiagonal = false)
        {
            var nodes = _pathfinding.FindPath(
                from.GridPosition.ToNumericVector2(),
                to.GridPosition.ToNumericVector2(),
                jumpHeight, allowDiagonal);
            if (nodes == null)
            {
                Debug.Log($"No path found to {to.GridPosition} from {from.GridPosition}");
                return null;
            }

            List<Tile> path = new List<Tile>(nodes.Count);

            foreach (Node node in nodes)
            {
                path.Add(GetTile(node.Position.X, node.Position.Y));
            }

            return path;
        }

        public bool ContainsTileAt(Point p)
        {
            return Tiles.ContainsKey(p);
        }

        public bool IsPointInBounds(Point p)
        {
            return p.x >= Min.x && p.x <= Max.x && p.y >= Min.y && p.y <= Max.y;
        }

        public void HighlightTiles(List<Tile> tiles)
        {
            for (int i = tiles.Count - 1; i >= 0; --i)
            {
                tiles[i].state = TileState.BattleHighlighted;
            }
        }

        public void SelectTiles(List<Tile> tiles)
        {
            for (int i = tiles.Count - 1; i >= 0; --i)
            {
                tiles[i].state = TileState.Selected;
            }
        }

        public void SelectTile(Tile tile)
        {
            tile.state = TileState.Selected;
        }

        public void DeselectTile(Tile tile)
        {
            tile.state = TileState.None;
        }

        public void DeselectTiles(List<Tile> tiles)
        {
            for (int i = tiles.Count - 1; i >= 0; --i)
            {
                tiles[i].state = TileState.None;
            }
        }

        public void HighlightTile(Tile tile)
        {
            tile.state = TileState.BattleHighlighted;
        }

        void ClearSearch()
        {
            foreach (Tile t in Tiles.Values)
            {
                t.prev = null;
                t.distance = int.MaxValue;
            }
        }

        void SwapReference(ref Queue<Tile> a, ref Queue<Tile> b)
        {
            Queue<Tile> temp = a;
            a = b;
            b = temp;
        }

        void SwapReference(ref Queue<Point> a, ref Queue<Point> b)
        {
            Queue<Point> temp = a;
            a = b;
            b = temp;
        }

        public List<Tile> Search(Tile start, Func<Tile, Tile, bool> addTile, bool ignoreStartTile = false)
        {
            List<Tile> retValue = new List<Tile>();
            if (!ignoreStartTile)
            {
                retValue.Add(start);
            }

            ClearSearch();
            Queue<Tile> checkNext = new Queue<Tile>();
            Queue<Tile> checkNow = new Queue<Tile>();

            start.distance = 0;
            checkNow.Enqueue(start);

            while (checkNow.Count > 0)
            {
                Tile t = checkNow.Dequeue();
                for (int i = 0; i < 4; ++i)
                {
                    Tile next = GetTile(t.GridPosition + directions[i]);
                    if (next == null || next.distance <= t.distance + 1)
                        continue;

                    if (addTile(t, next))
                    {
                        next.distance = t.distance + 1;
                        next.prev = t;
                        checkNext.Enqueue(next);
                        retValue.Add(next);
                    }
                }

                if (checkNow.Count == 0)
                {
                    SwapReference(ref checkNow, ref checkNext);
                }
            }

            return retValue;
        }

        public List<Tile> Search(Point start, Func<Point, bool> addPos)
        {
            List<Point> searchPositions = new List<Point>();
            List<Tile> retValues = new List<Tile>();

            Queue<Point> checkNext = new Queue<Point>();
            Queue<Point> checkNow = new Queue<Point>();

            checkNow.Enqueue(start);

            int attempts = 0;

            HashSet<Point> uniquePoints = new HashSet<Point>();

            while (checkNow.Count > 0)
            {
                if (attempts++ >= 1000)
                {
                    Debug.Log("Max Attempts");
                    break;
                }

                Point currentPos = checkNow.Dequeue();
                for (int i = 0; i < 4; ++i)
                {
                    Point nextPos = currentPos + directions[i];
                    if (!IsPointInBounds(nextPos) || uniquePoints.Contains(nextPos))
                    {
                        continue;
                    }

                    Tile nextTile;
                    if (addPos(nextPos))
                    {
                        checkNext.Enqueue(nextPos);
                        searchPositions.Add(nextPos);
                        uniquePoints.Add(nextPos);

                        nextTile = GetTile(nextPos);
                        if (nextTile)
                        {
                            retValues.Add(nextTile);
                        }
                    }
                }

                if (checkNow.Count == 0)
                {
                    SwapReference(ref checkNow, ref checkNext);
                }
            }

            return retValues;
        }
    }
}