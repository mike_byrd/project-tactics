using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectTactics
{
    public class TileDisplayEventTrigger : MonoBehaviour
    {
        public static event Action MouseEntered;

        private void OnMouseEnter()
        {
            MouseEntered?.Invoke();
        }
    }
}
