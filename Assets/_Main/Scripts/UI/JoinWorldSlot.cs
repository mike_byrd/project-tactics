using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class JoinWorldSlot : MonoBehaviour
    {
        public static event Action<JoinWorldSlot> Clicked;

        [SerializeField] private Button button;
        [SerializeField] private Image background;

        [SerializeField] private TMP_Text titleLabel;
        [SerializeField] private TMP_Text playerCountLabel;
        [SerializeField] private TMP_Text descriptionLabel;

        public string WorldID { get; private set; }

        public void OnButtonClicked()
        {
            Clicked?.Invoke(this);
        }

        public void UpdateUI(string id, string roomName, int playerCount)
        {
            WorldID = id;
            titleLabel.text = roomName;
            playerCountLabel.text = $"{playerCount} / {NetworkManager.MaxPlayers}";
        }

        public void SetSelected(bool selected)
        {
            if (selected)
            {
                background.color = K.TileColor.SelectedColor;
            }
            else
            {
                background.color = K.TileColor.UnselectedClearColor;
            }
        }
    }
}
