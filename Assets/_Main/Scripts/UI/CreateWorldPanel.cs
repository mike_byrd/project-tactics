using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using MCB.UI;
using ProjectTactics;
using TMPro;
using UnityEngine;

namespace MCB
{
    public class CreateWorldPanel : UIView
    {
        public static event Action<string> CreateClicked;
        public static event Action BackClicked;

        [SerializeField] private TMP_InputField roomInput;

        private void Start()
        {
            roomInput.text = $"{NetworkManager.LocalPlayer.PlayerData.Username}'s Room";
        }

        public void OnCreateClicked()
        {
            if (string.IsNullOrEmpty(roomInput.text))
            {
                DBG.LogWarning("Room name can't be empty.");
                return;
            }

            CreateClicked?.Invoke(roomInput.text);
        }

        public void OnBackClicked()
        {
            BackClicked?.Invoke();
        }
    }
}
