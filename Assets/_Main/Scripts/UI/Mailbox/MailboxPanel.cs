using System;
using System.Collections.Generic;
using MCB.Extensions;
using MCB.UI;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public enum MailType
    {
        None,
        PartyInvite
    }

    public class Mail
    {
        public MailType type;
        public string senderId;
        public string senderName;
        public string body;
        public DateTime timestamp;
    }

    public class MailboxPanel : UIPanel
    {
        public static event Action CloseClicked;

        [SerializeField] private MailboxSlot slotClone;
        [SerializeField] private MailboxSlot emptySlot;
        [SerializeField] private Transform container;

        [SerializeField] Scrollbar scrollbar;
        [SerializeField] private GameObject staticBackground;
        [SerializeField] private GameObject scrollBackground;
        [SerializeField] private RectTransform viewport;

        private List<MailboxSlot> _slots = new List<MailboxSlot>();
        private bool _isShowingStaticBackground = false;

        private void Start()
        {
            slotClone.Hide();
        }

        private void Update()
        {
            if (!_isShowingStaticBackground)
            {
                if (scrollbar.gameObject.activeSelf)
                {
                    SetStaticBackgroundVisible(true);
                }
            }
            else
            {
                if (!scrollbar.gameObject.activeSelf)
                {
                    SetStaticBackgroundVisible(false);
                }
            }
        }

        public void UpdateUI(List<Mail> mail)
        {
            int count = 0;
            foreach (Mail m in mail)
            {
                if (count < _slots.Count)
                {
                    _slots[count].UpdateUI(m);
                    _slots[count].Show();
                }
                else
                {
                    MailboxSlot slot = Instantiate(slotClone, container);
                    slot.UpdateUI(m);
                    slot.Show();
                    _slots.Add(slot);
                }
                count++;
            }

            for (int i = count; i < _slots.Count; i++)
            {
                _slots[i].Hide();
            }

            if (mail.Count <= 0)
            {
                emptySlot.Show();
            }
            else
            {
                emptySlot.Hide();
            }
        }

        public void OnCloseClicked()
        {
            CloseClicked?.Invoke();
        }

        private void SetStaticBackgroundVisible(bool visible)
        {
            staticBackground.SetActive(visible);
            scrollBackground.SetActive(!visible);
            _isShowingStaticBackground = visible;

            viewport.SetBottom(visible ? 20 : 0);

            Debug.Log($"Static background showing: {visible}");
        }
    }
}
