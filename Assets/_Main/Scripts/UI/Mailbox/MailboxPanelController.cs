using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class MailboxPanelController : UIController
    {
        public MailboxPanel mailboxPanel;
        public GameMenuButton mailButton;

        private void Start()
        {
            mailboxPanel.Init(this);
            ChangeState<MailboxPanelStateHidden>();
        }

        private void OnEnable()
        {
            NetworkManager.MailReceived += OnMailReceived;
        }

        private void OnDisable()
        {
            NetworkManager.MailReceived -= OnMailReceived;
        }

        private void OnMailReceived(List<Mail> mail)
        {
            mailButton.ShowBadge(mail.Count);
        }

        public override void Dismiss()
        {
            ChangeState<MailboxPanelStateHidden>();
        }
    }
}
