using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class MailboxPanelState : State
    {
        protected MailboxPanelController _owner;

        private void Awake()
        {
            _owner = GetComponent<MailboxPanelController>();
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void AddListeners()
        {
            base.AddListeners();
            _owner.mailButton.Button.onClick.AddListener(OnGameMenuButtonClicked);
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            _owner.mailButton.Button.onClick.RemoveListener(OnGameMenuButtonClicked);
        }

        protected virtual void OnGameMenuButtonClicked()
        {

        }


    }
}
