using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class MailboxPanelStateHidden : MailboxPanelState
    {
        public override void Enter()
        {
            base.Enter();
            _owner.mailboxPanel.Hide();
            _owner.mailButton.SetSelected(false);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.M) && !UIModalView.IsModalViewShowing)
            {
                _owner.ChangeState<MailboxPanelStateRoot>();
            }
        }

        protected override void OnGameMenuButtonClicked()
        {
            _owner.ChangeState<MailboxPanelStateRoot>();
        }
    }
}
