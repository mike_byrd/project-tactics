using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class MailboxPanelStateRoot : MailboxPanelState
    {
        public override void Enter()
        {
            base.Enter();

            _owner.mailboxPanel.UpdateUI(NetworkManager.GetMail());
            _owner.mailboxPanel.Show(true);
            _owner.mailButton.SetSelected(true);
        }

        protected override void AddListeners()
        {
            base.AddListeners();

            NetworkManager.MailReceived += OnMailReceived;
            MailboxSlot.PartyInvitationResponded += OnPartyInvitationResponded;

            MailboxPanel.CloseClicked += OnCloseClicked;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();

            NetworkManager.MailReceived -= OnMailReceived;
            MailboxSlot.PartyInvitationResponded -= OnPartyInvitationResponded;

            MailboxPanel.CloseClicked -= OnCloseClicked;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.M) && !UIModalView.IsModalViewShowing)
            {
                _owner.ChangeState<MailboxPanelStateHidden>();
            }
        }

        private void OnCloseClicked()
        {
            _owner.ChangeState<MailboxPanelStateHidden>();
        }

        private void OnMailReceived(List<Mail> mail)
        {
            _owner.mailboxPanel.UpdateUI(mail);
        }

        private void OnPartyInvitationResponded(string senderId, bool response)
        {
            NetworkManager.RespondToPartyInvite(senderId, response);
            if (NetworkManager.GetMail().Count == 0)
            {
                // This was the only mail so close the mailbox.
                _owner.ChangeState<MailboxPanelStateHidden>();
            }
        }

        protected override void OnGameMenuButtonClicked()
        {
            _owner.ChangeState<MailboxPanelStateHidden>();
        }
    }
}
