using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using ProjectTactics;
using TMPro;
using UnityEngine;

namespace ProjectTactics
{
    public class MailboxSlot : UIView
    {
        public static event Action<string, bool> PartyInvitationResponded;

        public TMP_Text senderLabel;
        public TMP_Text timestampLabel;
        public TMP_Text bodyLabel;

        public Transform partyInviteButtons;
        private Mail _mail;

        public void UpdateUI(Mail mail)
        {
            _mail = mail;
            senderLabel.text = mail.senderName;
            timestampLabel.text = mail.timestamp.ToShortTimeString();

            switch (mail.type)
            {
                case MailType.PartyInvite:
                    bodyLabel.text = $"{mail.senderName} has invited you to a party!";
                    break;
                default:
                    DBG.LogError("Invalid mail type.");
                    return;
            }
        }

        public void OnAcceptPartyInviteClicked()
        {
            DBG.Log($"You accepted {_mail.senderName}'s party invitation.");
            PartyInvitationResponded?.Invoke(_mail.senderId, true);
        }

        public void OnDeclinePartyInviteClicked()
        {
            DBG.Log($"You declined {_mail.senderName}'s party invitation.");
            PartyInvitationResponded?.Invoke(_mail.senderId, false);
        }
    }
}
