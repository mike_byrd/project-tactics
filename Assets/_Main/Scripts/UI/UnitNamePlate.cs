using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using TMPro;
using UnityEngine;

namespace ProjectTactics
{
    public class UnitNamePlate : UIView
    {
        [SerializeField] private string decorationsFolderPath = "Cosmetics/Decorations/Name Plates";

        [SerializeField] private RectTransform decorationContainer;
        [SerializeField] private TMP_Text unitNameLabel;
        [SerializeField] private TMP_Text playerNameLabel;
        [SerializeField] private Unit unit;

        private GameObject _decoration;

        public void UpdateUI(string decorationId = null)
        {
            unitNameLabel.text = unit.DisplayName;
            if (unit.Owner != null)
            {
                playerNameLabel.text = unit.Owner.PlayerData.Username;
            }

            if (decorationId != null)
            {
                SetDecoration(decorationId);
            }
        }

        public void SetDecoration(string decorationId)
        {
            if (decorationId == null)
            {
                Debug.LogError("decorationId cannot be null.");
                return;
            }

            GameObject decoration = Resources.Load<GameObject>($"{decorationsFolderPath}/{decorationId}");
            if (!decoration)
            {
                Debug.LogError($"Could not load decoration: {decorationId}");
                return;
            }

            if (_decoration)
            {
                Destroy(_decoration);
            }

            _decoration = Instantiate(decoration, decorationContainer);
        }
    }
}
