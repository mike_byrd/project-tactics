using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class PlayerListPanelController : UIController
    {
        public GameMenuButton playerListButton;
        public PlayerListPanel playerListPanel;

        private void Start()
        {
            playerListPanel.Init(this);
            ChangeState<PlayerListPanelStateHidden>();
        }

        public override void Dismiss()
        {
            ChangeState<PlayerListPanelStateHidden>();
        }
    }
}
