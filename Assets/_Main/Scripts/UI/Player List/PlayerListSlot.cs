using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class PlayerListSlot : UIView, IPointerClickHandler
    {
        public static event Action<PlayerListSlot, PointerEventData.InputButton> Clicked;

        public TMP_Text unitNameLabel;
        public TMP_Text unitLevelLabel;
        public TMP_Text playerUsernameLabel;
        public TMP_Text statusLabel;

        public Sprite otherPlayerSprite;
        public Sprite localPlayerSprite;
        public Image background;

        public Unit Unit { get; private set; }

        public void UpdateUI(Unit unit, string status)
        {
            Unit = unit;

            unitNameLabel.text = unit.DisplayName;
            unitLevelLabel.text = unit.Level.ToString();
            playerUsernameLabel.text = unit.Owner.PlayerData.Username;
            statusLabel.text = status;

            background.sprite = unit.Owner.PlayerData.ID == NetworkManager.LocalPlayer.PlayerData.ID ?
                localPlayerSprite :
                otherPlayerSprite;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            // We have to bubble up this event so that the focus trigger in the PlayerListPanel works correctly.
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.pointerDownHandler);

            // The click event should be fired after bubbling up the click event so the context menu appears on top of
            // the player list panel.
            Clicked?.Invoke(this, eventData.button);
        }
    }
}
