using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;

namespace ProjectTactics
{
    public class PlayerListPanel : UIPanel
    {
        public static event Action CloseClicked;

        [SerializeField] private PlayerListSlot slotPrefab;
        [SerializeField] private Transform container;
        public TMP_Text playerCountLabel;
        public TMP_Text pageLabel;
        public UIView pagination;

        private List<PlayerListSlot> _slots = new List<PlayerListSlot>();
        private int _currentPageIndex;

        private void Start()
        {
            slotPrefab.Hide();
        }

        public void UpdateUI(PlayerHandle[] players)
        {
            pagination.Hide();
            playerCountLabel.text = players.Length.ToString();

            int count;
            for (count = 0; count < players.Length; count++)
            {
                Unit unit = players[count].Unit;
                if (count < _slots.Count)
                {
                    _slots[count].UpdateUI(unit, "Town Center"); // TODO: update to the correct status.
                    _slots[count].Show();
                    continue;
                }

                unit = players[count].Unit;

                PlayerListSlot slot = Instantiate(slotPrefab, container);
                _slots.Add(slot);
                _slots[count].UpdateUI(unit, "Town Center"); // TODO: update to the correct status.
                _slots[count].Show();
            }

            // Hide remaining slots.
            for (int i = count; i < _slots.Count; i++)
            {
                _slots[i].Hide();
            }
        }

        public override void Hide()
        {
            base.Hide();
            ContextMenu.Dismiss(gameObject);
        }

        public void OnCloseClicked()
        {
            CloseClicked?.Invoke();
        }

        public void OnPreviousClicked()
        {

        }

        public void OnNextClicked()
        {

        }
    }
}
