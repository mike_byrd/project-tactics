using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class PlayerListPanelStateHidden : PlayerListPanelState
    {
        public override void Enter()
        {
            base.Enter();
            _owner.playerListPanel.Hide();
            _owner.playerListButton.SetSelected(false);
        }

        protected override void OnGameMenuButtonClicked()
        {
            _owner.ChangeState<PlayerListPanelStateRoot>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P) && !UIModalView.IsModalViewShowing)
            {
                _owner.ChangeState<PlayerListPanelStateRoot>();
            }
        }
    }
}