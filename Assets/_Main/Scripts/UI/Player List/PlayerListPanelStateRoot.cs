using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTactics
{
    public class PlayerListPanelStateRoot : PlayerListPanelState
    {
        public override void Enter()
        {
            base.Enter();
            _owner.playerListPanel.UpdateUI(NetworkManager.CurrentWorld.GetPlayers());
            _owner.playerListPanel.Show(true);
            _owner.playerListButton.SetSelected(true);
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            PlayerListSlot.Clicked += PlayerListSlotOnClicked;
            NetworkManager.PlayerJoined += OnPlayerJoined;
            NetworkManager.PlayerLeft += OnPlayerLeft;

            PlayerListPanel.CloseClicked += OnCloseClicked;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            PlayerListSlot.Clicked -= PlayerListSlotOnClicked;
            NetworkManager.PlayerJoined -= OnPlayerJoined;
            NetworkManager.PlayerLeft -= OnPlayerLeft;

            PlayerListPanel.CloseClicked -= OnCloseClicked;
        }

        private void OnCloseClicked()
        {
            _owner.ChangeState<PlayerListPanelStateHidden>();
        }

        protected override void OnGameMenuButtonClicked()
        {
            _owner.ChangeState<PlayerListPanelStateHidden>();
        }

        protected void OnPlayerJoined(PlayerHandle player)
        {
            _owner.playerListPanel.UpdateUI(NetworkManager.CurrentWorld.GetPlayers());
        }

        protected void OnPlayerLeft(PlayerHandle player)
        {
            _owner.playerListPanel.UpdateUI(NetworkManager.CurrentWorld.GetPlayers());
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.P) && !UIModalView.IsModalViewShowing)
            {
                _owner.ChangeState<PlayerListPanelStateHidden>();
            }
        }

        private void PlayerListSlotOnClicked(PlayerListSlot slot, PointerEventData.InputButton mouseButton)
        {
            switch (mouseButton)
            {
                case PointerEventData.InputButton.Right:
                    ContextMenu.Show(slot.Unit, Input.mousePosition, _owner.playerListPanel.gameObject);
                    break;
            }
        }
    }
}
