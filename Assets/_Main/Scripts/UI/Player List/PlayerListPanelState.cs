using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class PlayerListPanelState : State
    {
        protected PlayerListPanelController _owner;

        private void Awake()
        {
            _owner = GetComponent<PlayerListPanelController>();
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void AddListeners()
        {
            base.AddListeners();
            _owner.playerListButton.Button.onClick.AddListener(OnGameMenuButtonClicked);
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            _owner.playerListButton.Button.onClick.RemoveListener(OnGameMenuButtonClicked);
        }

        protected virtual void OnGameMenuButtonClicked()
        {

        }
    }
}
