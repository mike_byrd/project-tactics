using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class GameMenuButton : UIView
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image background;
        [SerializeField] private UIView badge;
        [SerializeField] private TMP_Text badgeLabel;

        [SerializeField] private Color selectedColor;
        [SerializeField] private string tooltipText;

        public Button Button { get; private set; }

        private RectTransform _rectTransform;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            Button = GetComponent<Button>();
        }

        public void ShowBadge(int totalMail)
        {
            if (totalMail < 1)
            {
                badge.Hide();
                return;
            }

            badgeLabel.text = totalMail > 99 ? "99+" : totalMail.ToString();
            badge.Show();
        }

        public void SetSelected(bool selected)
        {
            background.color = selected ? selectedColor : Color.white;
        }

        public void OnPointerEnter()
        {
            InfoCardController.instance.ShowSimpleInfo(_rectTransform, tooltipText);
        }

        public void OnPointerExit()
        {
            InfoCardController.instance.HideCard();
        }
    }
}