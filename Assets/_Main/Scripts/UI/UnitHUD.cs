using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class UnitHUD : UIView
    {
        [SerializeField] private HealthBar healthBar;
        [SerializeField] private Image partyIcon;
        [SerializeField] private Sprite hostSprite;
        [SerializeField] private Sprite memberSprite;
        [SerializeField] private Canvas canvas;

        public UnitNamePlate namePlate;
        public HealthBar HealthBar => healthBar;

        public void Initialize(Camera uiCamera)
        {
            canvas.worldCamera = uiCamera;
            healthBar.gameObject.SetActive(false);
            namePlate.UpdateUI();
            namePlate.Hide();
        }

        public void InitializeHealthBar(Alliances alliances)
        {
            HealthBar.InitializeHealthBar(alliances);
        }

        public void ShowPartyIcon(bool isHost)
        {
            partyIcon.sprite = isHost ? hostSprite : memberSprite;
            partyIcon.gameObject.SetActive(true);
        }

        public void HidePartyIcon()
        {
            partyIcon.gameObject.SetActive(false);
        }
    }
}
