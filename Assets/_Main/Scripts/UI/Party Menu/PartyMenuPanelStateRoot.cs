using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTactics
{
    public class PartyMenuPanelStateRoot : PartyMenuPanelState
    {
        public static event Action<UnitData> UnitInspected;

        public override void Enter()
        {
            base.Enter();
            _owner.partyMenu.UpdateUI(NetworkManager.Party);
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            PartyMenuSlot.Clicked += PartyMenuSlotOnClicked;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            PartyMenuSlot.Clicked -= PartyMenuSlotOnClicked;
        }

        protected override void OnPartyUpdated(PartyData party)
        {
            if (party.units.Count <= 1)
            {
                DBG.Log("Entered a solo party");
                _owner.ChangeState<PartyMenuPanelStateHidden>();
                return;
            }

            _owner.partyMenu.UpdateUI(party);
        }

        private void PartyMenuSlotOnClicked(PartyMenuSlot slot, PointerEventData.InputButton mouseButton)
        {
            switch (mouseButton)
            {
                case PointerEventData.InputButton.Right:
                    var entries = new List<ContextMenuEntry>
                    {
                        new ContextMenuEntry { title = "Inspect", callback = () => InspectUnit(slot.UnitData), enabled = true}
                    };

                    bool isMyLeaderUnit = slot.Player.PlayerID == NetworkManager.LocalPlayer.PlayerID &&
                                          slot.UnitData.ID == slot.Player.LeaderID;
                    if (isMyLeaderUnit)
                    {
                        entries.Add(new ContextMenuEntry { title = "Leave Party", callback = () => RemoveFromParty(slot), enabled = true});
                    }
                    else
                    {
                        bool isHost = NetworkManager.LocalPlayer.PlayerID == NetworkManager.Party.hostId;
                        bool isMyUnit = NetworkManager.LocalPlayer.PlayerData.Inventory.HasUnit(slot.UnitData.ID);
                        if (isHost || isMyUnit)
                        {
                            entries.Add(new ContextMenuEntry { title = "Kick From Party", callback = () => RemoveFromParty(slot), enabled = true});
                        }
                    }

                    ContextMenu.Show(Input.mousePosition, entries, slot.UnitData.Name, _owner.partyMenu.gameObject);
                    break;
            }
        }

        private void InspectUnit(UnitData unitData)
        {
            UnitInspected?.Invoke(unitData);
        }

        private void RemoveFromParty(PartyMenuSlot slot)
        {
            // Determine if another player is being removed or if only their extra unit is.
            if (slot.Player.LeaderID == slot.UnitData.ID)
            {
                // Remove the player, which in turn removes their units.
                NetworkManager.RemovePlayerFromParty(slot.Player.PlayerID);
            }
            else
            {
                NetworkManager.RemoveUnitFromParty(slot.UnitData.ID);
            }
            ContextMenu.Dismiss(_owner.partyMenu.gameObject);
        }
    }
}
