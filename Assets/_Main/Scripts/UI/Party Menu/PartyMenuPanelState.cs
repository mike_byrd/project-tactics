using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using UnityEngine;

namespace ProjectTactics
{
    public class PartyMenuPanelState : State
    {
        protected PartyMenuPanelController _owner;

        private void Awake()
        {
            _owner = GetComponent<PartyMenuPanelController>();
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            NetworkManager.PartyUpdated += OnPartyUpdated;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            NetworkManager.PartyUpdated -= OnPartyUpdated;
        }

        protected virtual void OnPartyUpdated(PartyData party)
        {

        }
    }
}
