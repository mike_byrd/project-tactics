using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class PartyMenuPanelStateHidden : PartyMenuPanelState
    {
        public override void Enter()
        {
            base.Enter();
            _owner.partyMenu.Hide();
        }

        protected override void OnPartyUpdated(PartyData party)
        {
            if (party.units.Count <= 1)
            {
                DBG.Log("Entered a solo party");
                return;
            }

            _owner.ChangeState<PartyMenuPanelStateRoot>();
        }
    }
}
