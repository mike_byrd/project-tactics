using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class PartyMenuPanelController : UIController
    {
        public PartyMenuPanel partyMenu;

        private void Start()
        {
            partyMenu.Init(this);
            ChangeState<PartyMenuPanelStateHidden>();
        }
    }
}
