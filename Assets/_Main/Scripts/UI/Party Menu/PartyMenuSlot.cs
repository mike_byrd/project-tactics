using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class PartyMenuSlot : UIView, IPointerClickHandler
    {
        public static event Action<PartyMenuSlot, PointerEventData.InputButton> Clicked;

        [SerializeField] private TMP_Text unitNameLabel;
        [SerializeField] private Image hostIcon;
        [SerializeField] private Image background;

        [SerializeField] private Sprite otherPlayerSprite;
        [SerializeField] private Sprite localPlayerSprite;

        [SerializeField] private Sprite hostIconSprite;
        [SerializeField] private Sprite memberIconSprite;

        public PlayerHandle Player { get; private set; }
        public UnitData UnitData { get; private set; }

        public void UpdateUI(PlayerHandle player, UnitData unitData, bool isHost)
        {
            Player = player;
            UnitData = unitData;

            unitNameLabel.text = unitData.Name;

            background.sprite = player.PlayerID == NetworkManager.LocalPlayer.PlayerData.ID ?
                localPlayerSprite :
                otherPlayerSprite;

            if (unitData.ID == player.LeaderID)
            {
                hostIcon.sprite = isHost ? hostIconSprite : memberIconSprite;
                hostIcon.gameObject.SetActive(true);
            }
            else
            {
                hostIcon.gameObject.SetActive(false);
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Clicked?.Invoke(this, eventData.button);
        }
    }
}
