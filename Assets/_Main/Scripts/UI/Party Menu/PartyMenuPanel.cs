using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class PartyMenuPanel : UIPanel
    {
        [SerializeField] private PartyMenuSlot slotClone;
        [SerializeField] private UIView container;

        private List<PartyMenuSlot> _slots = new List<PartyMenuSlot>();

        private void Start()
        {
            slotClone.Hide();
        }

        public void UpdateUI(PartyData party)
        {
            int count = 0;

            // Sort units
            List<UnitData> sorted = new List<UnitData>(party.units.Count);
            foreach (PlayerHandle playerHandle in party.members)
            {
                foreach (KeyValuePair<string, UnitData> kvp in party.units)
                {
                    if (playerHandle.PlayerData.Inventory.HasUnit(kvp.Value.ID))
                    {
                        sorted.Add(kvp.Value);
                    }
                }
            }

            foreach (UnitData unit in sorted)
            {
                PlayerHandle owner = party.GetUnitOwner(unit.ID);
                UnitData unitData = owner.PlayerData.Inventory.GetUnit(unit.ID);

                bool isHost = party.hostId == owner.PlayerID && unitData.ID == owner.LeaderID;
                if (count < _slots.Count)
                {
                    _slots[count].UpdateUI(owner, unitData, isHost);
                    _slots[count].Show();
                }
                else
                {
                    PartyMenuSlot slot = Instantiate(slotClone, container.transform);
                    _slots.Add(slot);
                    _slots[count].UpdateUI(owner, unitData, isHost);
                    slot.Show();
                }
                count++;
            }

            for (int i = count; i < _slots.Count; i++)
            {
                _slots[i].Hide();
            }

            Show(true);
        }

        public void OnPartyButtonClicked()
        {
            container.Toggle();
        }

        public override void Hide()
        {
            base.Hide();
            ContextMenu.Dismiss(gameObject);
        }
    }
}
