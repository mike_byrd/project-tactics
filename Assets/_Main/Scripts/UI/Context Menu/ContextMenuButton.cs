using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class ContextMenuButton : UIView
    {
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text label;

        [SerializeField] private Sprite enabledSprite;
        [SerializeField] private Sprite disabledSprite;

        private Action _callback;
        public bool Enabled { get; private set; }

        public void Setup(ContextMenuEntry context)
        {
            label.text = context.title;
            _callback = context.callback;

            Enabled = context.enabled;
            image.sprite = context.enabled ? enabledSprite : disabledSprite;
        }

        public void OnClick()
        {
            if (!Enabled)
            {
                return;
            }

            _callback?.Invoke();
        }
    }
}
