using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTactics
{
    public class ContextMenuPanel : UIView
    {
        [SerializeField] private ContextMenuButton buttonClone;
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Transform container;
        [SerializeField] private TMP_Text titleLabel;
        [SerializeField] private RectTransform canvasRect;

        private List<ContextMenuButton> _slots;

        private void Start()
        {
            buttonClone.Hide();
        }

        private void Update()
        {
            int mouseButtonId = Util.GetAnyMouseButtonDown();
            if (mouseButtonId > -1)
            {
                List<RaycastResult> results = Util.RaycastMouse();

                bool clickedOnContext = false;
                foreach (RaycastResult result in results)
                {
                    if (result.gameObject == gameObject)
                    {
                        clickedOnContext = true;
                        break;
                    }

                    // If this is the context menu, don't dismiss.
                    if (gameObject == result.gameObject)
                    {
                        clickedOnContext = true;
                        break;
                    }

                    // We don't want to dismiss the menu if we right clicked on a contextual item.
                    bool isContextualItem = result.gameObject.layer == LayerMask.NameToLayer("Context");
                    if (isContextualItem && mouseButtonId == 1)
                    {
                        clickedOnContext = true;
                        break;
                    }
                }

                if (!clickedOnContext)
                {
                    // Dismiss this menu if we clicked outside of it.
                    Hide();
                }
            }
        }

        public void Show(Vector3 screenPosition, List<ContextMenuEntry> menuEntries, string title)
        {
            Setup(menuEntries, title);

            Vector3 menuPosition = screenPosition / canvasRect.localScale.x;
            float rightDifference = canvasRect.sizeDelta.x - (menuPosition.x + rectTransform.sizeDelta.x);
            if (rightDifference < 0)
            {
                menuPosition.x += rightDifference;
            }

            float bottomDifference = menuPosition.y - rectTransform.sizeDelta.y;
            if (bottomDifference < 0)
            {
                menuPosition.y -= bottomDifference;
            }

            rectTransform.anchoredPosition = menuPosition;
            Show(true);
        }

        private void Setup(List<ContextMenuEntry> menuEntries, string title)
        {
            titleLabel.text = title;

            Clear();
            foreach (ContextMenuEntry entry in menuEntries)
            {
                ContextMenuButton newButton = Instantiate(buttonClone, container);
                newButton.Show();
                newButton.Setup(entry);
                _slots.Add(newButton);
            }
        }

        private void Clear()
        {
            if (_slots == null)
            {
                _slots = new List<ContextMenuButton>();
            }

            foreach (ContextMenuButton button in _slots)
            {
                Destroy(button.gameObject);
            }

            _slots.Clear();
        }
    }
}
