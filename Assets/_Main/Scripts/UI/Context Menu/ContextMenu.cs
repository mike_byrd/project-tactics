using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Extensions;
using MCB.Networking;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTactics
{
    public struct ContextMenuEntry
    {
        public string title;
        public Action callback;
        public bool enabled;
    }

    public class ContextMenu : MonoBehaviour
    {
        public static event Action<Unit> UnitInspected;

        [SerializeField] private ContextMenuPanel panel;

        private static ContextMenu _i;
        private static GameObject _context;

        private void Awake()
        {
            if (!_i)
            {
                _i = this;
            }
            else if (_i != this)
            {
                Destroy(gameObject);
            }
        }

        public static void Show(Vector3 screenPosition, List<ContextMenuEntry> menuEntries, string title, GameObject context = null)
        {
            if (!_i)
            {
                return;
            }

            _context = context;
            _i.panel.Show(screenPosition, menuEntries, title);
        }

        public static void Show(Unit unit, Vector3 screenPosition, GameObject context = null)
        {
            var contextEntries = new List<ContextMenuEntry>
            {
                new ContextMenuEntry { title = "Inspect", callback = () => InspectUnit(unit), enabled = true}
            };

            bool canInvite = NetworkManager.CanInviteToParty(unit.Owner.PlayerData.ID);
            bool canRemove = NetworkManager.CanRemoveFromParty(unit.Owner.PlayerData.ID);
            if (unit.Owner != NetworkManager.LocalPlayer)
            {
                if (canInvite)
                {
                    contextEntries.Add(new ContextMenuEntry { title = "Invite to Party", callback = () => InviteToParty(unit), enabled = true});
                }
                else if (canRemove)
                {
                    contextEntries.Add(new ContextMenuEntry { title = "Kick From Party", callback = () => RemoveFromParty(unit), enabled = true});
                }

                contextEntries.Add(new ContextMenuEntry { title = "Send Message", callback = () => DBG.Log($"Send Message to {unit.DisplayName}"), enabled = false});
            }
            else
            {
                if (canRemove)
                {
                    contextEntries.Add(new ContextMenuEntry { title = "Leave Party", callback = () => RemoveFromParty(unit), enabled = true});
                }
            }

            Show(screenPosition, contextEntries, unit.DisplayName, context);
        }

        private static void InspectUnit(Unit unit)
        {
            UnitInspected?.Invoke(unit);
            Dismiss(_context);
        }

        private static void InviteToParty(Unit unit)
        {
            NetworkManager.SendPartyInvite(unit.Owner.PlayerData.ID);
            Dismiss(_context);
        }

        private static void RemoveFromParty(Unit unit)
        {
            NetworkManager.RemovePlayerFromParty(unit.Owner.PlayerData.ID);
            Dismiss(_context);
        }

        public static void Dismiss(GameObject context)
        {
            if (!_i)
            {
                return;
            }

            if (_context == context)
            {
                Hide();
            }
        }

        private static void Hide()
        {
            _context = null;
            _i.panel.Hide();
        }
    }
}
