using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class MenuBar : MonoBehaviour
    {
        public event Action SettingsClicked;

        public StatPanel mainStatPanel;
        public GameMenuButton mailButton;

        public void UpdateMailButton(int totalMail)
        {
            mailButton.ShowBadge(totalMail);
        }

        public void OnSettingsButtonClicked()
        {
            SettingsClicked?.Invoke();
        }
    }
}
