using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;

namespace MCB
{
    public class JoinWorldPanel : UIView
    {
        public static event Action<string> JoinClicked;
        public static event Action BackClicked;

        [SerializeField] private Transform joinWorldSlotContainer;
        [SerializeField] private JoinWorldSlot slotPrefab;

        private List<JoinWorldSlot> _joinWorldSlots;
        private JoinWorldSlot _selectedSlot;

        private void OnEnable()
        {
            JoinWorldSlot.Clicked += OnJoinWorldSlotClicked;
        }

        private void OnDisable()
        {
            JoinWorldSlot.Clicked -= OnJoinWorldSlotClicked;
        }

        private void OnJoinWorldSlotClicked(JoinWorldSlot slot)
        {
            SetSlotSelected(slot);
        }

        private void Awake()
        {
            _joinWorldSlots = new List<JoinWorldSlot>();
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
            _selectedSlot = null;
        }

        public void UpdateUI(GetWorldsResult[] worlds)
        {
            ClearSlots();

            foreach (GetWorldsResult world in worlds)
            {
                JoinWorldSlot slot = Instantiate(slotPrefab, joinWorldSlotContainer);
                slot.UpdateUI(world.id, world.name, world.playerCount);

                _joinWorldSlots.Add(slot);
            }
        }

        public void OnJoinClicked()
        {
            if (!_selectedSlot)
            {
                return;
            }

            JoinClicked?.Invoke(_selectedSlot.WorldID);
        }

        public void OnBackClicked()
        {
            BackClicked?.Invoke();
        }

        private void SetSlotSelected(JoinWorldSlot selectedSlot)
        {
            _selectedSlot = selectedSlot;
            foreach (JoinWorldSlot slot in _joinWorldSlots)
            {
                if (slot == selectedSlot)
                {
                    slot.SetSelected(true);
                }
                else
                {
                    slot.SetSelected(false);
                }
            }
        }

        private void ClearSlots()
        {
            var slots = joinWorldSlotContainer.GetComponentsInChildren<JoinWorldSlot>();
            foreach (JoinWorldSlot slot in slots)
            {
                Destroy(slot.gameObject);
            }

            _joinWorldSlots.Clear();
        }
    }
}
