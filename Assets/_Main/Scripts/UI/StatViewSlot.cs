using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class StatViewSlot : UIView
    {
        [SerializeField] private TMP_Text statLabel;
        [SerializeField] private TMP_Text statValue;
        [SerializeField] private Image background;

        public void UpdateUI(StatTypes stat, Unit unit, bool hideBackground = false)
        {
            switch (stat)
            {
                case StatTypes.HP:
                case StatTypes.MHP:
                    statLabel.text = "HP";
                    statValue.text = $"{unit.Stats[StatTypes.HP]} / {unit.Stats[StatTypes.MHP]}";
                    break;
                case StatTypes.AP:
                case StatTypes.MAP:
                    statLabel.text = "AP";
                    statValue.text = unit.Stats[StatTypes.MAP].ToString();
                    break;
                case StatTypes.MP:
                case StatTypes.MMP:
                    statLabel.text = "MP";
                    statValue.text = unit.Stats[StatTypes.MMP].ToString();
                    break;
                case StatTypes.EXP:
                    statLabel.text = "EXP";
                    statValue.text = $"{unit.Stats[StatTypes.EXP]} / {Rank.ExperienceForLevel(unit.Level + 1)}";
                    break;
                default:
                    statLabel.text = stat.ToString();
                    statValue.text = unit.Stats[stat].ToString();
                    break;
            }

            background.enabled = !hideBackground;
        }
    }
}
