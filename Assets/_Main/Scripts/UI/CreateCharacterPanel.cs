﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectTactics;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class CreateCharacterPanel : MonoBehaviour
    {
        public static event Action BackButtonClicked;
        public static event Action CreateCharacterButtonClicked;
        public static event Action<GenderType> GenderButtonClicked;
        public static event Action<JobType> JobButtonClicked;

        [SerializeField] private CharacterPropertySlot[] propertySlots;
        [SerializeField] private CharacterCreationJobButton[] jobButtons;
        [SerializeField] private Button maleButton;
        [SerializeField] private Button femaleButton;
        [SerializeField] private Button createCharacterButton;

        [SerializeField] private TMP_InputField nameInputField;
        [SerializeField] private UnitModelVisual characterModel;

        private UnitData unitData;

        private JobType currentJob;
        private GenderType currentGender;

        private int currentHairIndex;
        private int currentSkinIndex;

        private CharacterCreationJobButton currentJobButton;

        private void Start()
        {
            currentJobButton = jobButtons[0];
            currentJobButton.SetSelected(true);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void UpdateUI(UnitData unitData)
        {
            this.unitData = unitData;
            if (unitData == null)
            {
                return;
            }

            SetCreateButtonActive(true);

            GenderType gender = unitData.Gender;
            UpdateGenderButtons(gender);
        }

        private void UpdateGenderButtons(GenderType gender)
        {
            if (gender == GenderType.Male)
            {
                maleButton.image.color = K.TileColor.SelectedColor;
                femaleButton.image.color = K.TileColor.UnselectedWhiteColor;
            }
            else
            {
                maleButton.image.color = K.TileColor.UnselectedWhiteColor;
                femaleButton.image.color = K.TileColor.SelectedSolidColor;
            }
        }

        public void OnBackButtonClicked()
        {
            if (currentJobButton)
            {
                currentJobButton.SetSelected(false);
            }

            currentJobButton = jobButtons[0];
            currentJobButton.SetSelected(true);


            BackButtonClicked?.Invoke();
        }

        public void OnMaleButtonClicked()
        {
            GenderButtonClicked?.Invoke(GenderType.Male);
            UpdateGenderButtons(GenderType.Male);
        }

        public void OnFemaleButtonClicked()
        {
            GenderButtonClicked?.Invoke(GenderType.Female);
            UpdateGenderButtons(GenderType.Female);
        }

        public void OnJobButtonClicked(Button button)
        {
            var jobButton = button.GetComponent<CharacterCreationJobButton>();
            if (currentJobButton)
            {
                currentJobButton.SetSelected(false);
            }

            currentJobButton = jobButton;
            jobButton.SetSelected(true);
            JobButtonClicked?.Invoke(jobButton.Job);
        }

        public void OnCreateCharacterButtonClicked()
        {
            if (string.IsNullOrWhiteSpace(nameInputField.text))
            {
                DBG.Log("Name cannot be empty");
                return;
            }

            unitData.Name = nameInputField.text;
            CreateCharacterButtonClicked?.Invoke();
        }

        public void ResetPropertySlots()
        {
            foreach (var property in propertySlots)
            {
                property.UpdateUI("1");
            }
        }

        public void SetCreateButtonActive(bool active)
        {
            createCharacterButton.interactable = active;
        }
    }
}
