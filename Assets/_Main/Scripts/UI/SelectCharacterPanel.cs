﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class SelectCharacterPanel : MonoBehaviour
    {
        private const int MaxCharacterSlots = 6;
        public static event Action NewCharacterButtonClicked;
        public static event Action<UnitData> SelectionChanged;
        public static event Action CreateClicked;
        public static event Action JoinClicked;
        public static event Action<string> BackClicked;
        public static event Action<string> EnterWorld;

        [SerializeField] private Button createSessionButton;
        [SerializeField] private Button joinSessionButton;
        [SerializeField] private Button backButton;
        [SerializeField] private CreateWorldPanel createWorldPanel;
        [SerializeField] private JoinWorldPanel joinWorldPanel;
        [SerializeField] private Transform characterSlotContainer;
        [SerializeField] private CharacterSelectSlot[] characterSlots;

        private CharacterSelectSlot _selectedSlot;

        private bool _disableInteraction;

        private void OnEnable()
        {
            CharacterSelectSlot.OnClicked += CharacterSelectSlotOnClicked;
        }

        private void OnDisable()
        {
            CharacterSelectSlot.OnClicked -= CharacterSelectSlotOnClicked;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            characterSlotContainer.gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void SetSessionButtonsEnabled(bool enabled)
        {
            createSessionButton.interactable = enabled;
            joinSessionButton.interactable = enabled;
        }

        private void CharacterSelectSlotOnClicked(CharacterSelectSlot slotClicked)
        {
            if (_selectedSlot == slotClicked)
            {
                return;
            }

            if (slotClicked.isNewCharacterSlot)
            {
                NewCharacterButtonClicked?.Invoke();
                return;
            }

            if (slotClicked.currentDataModel == null)
            {
                return;
            }

            if (_selectedSlot)
            {
                _selectedSlot.SetSelected(false);
            }

            _selectedSlot = slotClicked;
            _selectedSlot.SetSelected(true);

            SetSessionButtonsEnabled(true);

            SelectionChanged?.Invoke(_selectedSlot.currentDataModel);
        }

        public void OnCreateClicked()
        {
            // EnterWorld?.Invoke("create");
            characterSlotContainer.gameObject.SetActive(false);
            CreateClicked?.Invoke();
        }

        public void OnJoinClicked()
        {
            // EnterWorld?.Invoke("join");
            characterSlotContainer.gameObject.SetActive(false);
            JoinClicked?.Invoke();
        }

        public void OnRootBackClicked() { BackClicked?.Invoke("root"); }
        public void OnJoinBackClicked() { BackClicked?.Invoke("join"); }
        public void OnCreateBackClicked() { BackClicked?.Invoke("create"); }

        public void UpdateUI(UnitData[] ownerUnitDataModels)
        {
            if (ownerUnitDataModels.Length == 0)
            {
                SetupForNewCharacter();
            }
            else
            {
                ShowAvailableCharacters(ownerUnitDataModels);
            }
        }

        private void SetupForNewCharacter()
        {
            characterSlots[0].UpdateUI(null, true);
            for (int i = 1; i < characterSlots.Length; i++)
            {
                characterSlots[i].UpdateUI(null);
            }

            SetSessionButtonsEnabled(false);
        }

        private void ShowAvailableCharacters(UnitData[] ownerUnitDataModels)
        {
            UnitData dataModel;
            for (int i = 0; i < MaxCharacterSlots; i++)
            {
                if (i < ownerUnitDataModels.Length)
                {
                    dataModel = ownerUnitDataModels[i];
                    characterSlots[i].UpdateUI(dataModel);

                    if (i == 0)
                    {
                        characterSlots[i].SetSelected(true);
                        SetSessionButtonsEnabled(true);
                        _selectedSlot = characterSlots[i];
                        SelectionChanged?.Invoke(dataModel);
                    }
                }
                else
                {
                    characterSlots[i].SetEmpty(true);
                }
            }
        }

        public void ShowCharacterSlots()
        {
            characterSlotContainer.gameObject.SetActive(true);
            backButton.gameObject.SetActive(true);
            createWorldPanel.Hide();
            joinWorldPanel.Hide();
        }

        public void ShowCreateRoomPanel()
        {
            createWorldPanel.Show();
            joinWorldPanel.Hide();
            characterSlotContainer.gameObject.SetActive(false);
            backButton.gameObject.SetActive(false);
        }

        public void ShowJoinRoomPanel()
        {
            joinWorldPanel.Show();
            createWorldPanel.Hide();
            characterSlotContainer.gameObject.SetActive(false);
            backButton.gameObject.SetActive(false);
        }

        private void LoadCharacterModel(UnitDataModel dataModel)
        {
//            JobType job = GetJobFromString(dataModel.job);
//            GenderType gender = GetGenderFromString(dataModel.gender);
//            int baseMaterialIndex = dataModel.modelVisual.baseMaterial;
//            int hairPropIndex = dataModel.modelVisual.hairProp;
//
//            var baseMaterial = UnitFactory.GetSkinTones(job, gender)[baseMaterialIndex];
//            var hairProp = UnitFactory.GetHairStyles(gender)[hairPropIndex];
//
//            owner.characterModel.name = dataModel.name;
//            owner.characterModel.SetBase(baseMaterial);
//            owner.characterModel.SetHair(hairProp);
        }

        public void UpdateJoinPanelUI(GetWorldsResult[] worlds)
        {
            joinWorldPanel.UpdateUI(worlds);
        }
    }
}
