using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ProjectTactics
{
    public class UnitManagerPanelStateRoot : UnitManagerPanelState
    {
        private bool _waitingForUnits;

        public override void Enter()
        {
            base.Enter();

            UnitData[] units = NetworkManager.LocalPlayer.PlayerData.Inventory.GetUnits();
            LoadUnits(units);

            UnitManagerSlot unitSlot = _owner.unitManagerPanel.GetUnitSlot(_owner.CurrentSlotIndex);
            _owner.SetCurrentSelectedSlot(unitSlot);
            _owner.unitManagerButton.SetSelected(true);

//            // TODO: in the future we may want to revisit this, but for now using the cached units is fine.
//            // If coming from the unit details screen we actually shouldn't need to fetch the units from the server.
//            if (_owner.PreviousState as UnitManagerPanelStateHidden)
//            {
//                NetworkManager.GetUnitInventory(LoadUnits);
//                _waitingForUnits = true;
//            }
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            UnitManagerSlot.PointerDown += UnitManagerSlotOnPointerDown;
            UnitManagerSlot.DoubleClicked += UnitManagerSlotOnDoubleClicked;
            UnitManagerSlot.PointerClicked += UnitManagerSlotOnPointerClicked;

            UnitManagerPanel.ViewDetailsClicked += OnViewDetailsClicked;
            UnitManagerPanel.CloseClicked += OnCloseClicked;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            UnitManagerSlot.PointerDown -= UnitManagerSlotOnPointerDown;
            UnitManagerSlot.PointerClicked -= UnitManagerSlotOnPointerClicked;
            UnitManagerSlot.DoubleClicked -= UnitManagerSlotOnDoubleClicked;

            UnitManagerPanel.ViewDetailsClicked -= OnViewDetailsClicked;
            UnitManagerPanel.CloseClicked -= OnCloseClicked;
        }

        private void LoadUnits(UnitData[] unitDataList)
        {
            _waitingForUnits = false;

            if (!enabled)
            {
                // In case for whatever reason we leave this state before getting the data from the server.
                return;
            }

            List<Unit> units = new List<Unit>(unitDataList.Length);
            int count = 0;
            foreach (UnitData unitData in unitDataList)
            {
                Unit unit = RenderTextureManager.LoadForUnit(unitData, count);
                units.Add(unit);
                count++;
            }

            List<RenderTextureTarget> targets = RenderTextureManager.GetRenderTextureTargets();
            _owner.unitManagerPanel.UpdateUI(units, targets, _owner.CurrentSlotIndex);
            _owner.unitManagerPanel.Show(true);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.U) && !UIModalView.IsModalViewShowing)
            {
                if (_waitingForUnits)
                {
                    return;
                }

                _owner.ChangeState<UnitManagerPanelStateHidden>();
            }
        }

        private void OnCloseClicked()
        {
            _owner.ChangeState<UnitManagerPanelStateHidden>();
        }

        protected override void OnUnitManagementClicked()
        {
            if (_waitingForUnits)
            {
                return;
            }

            _owner.ChangeState<UnitManagerPanelStateHidden>();
        }

        private void UnitManagerSlotOnPointerDown(UnitManagerSlot slot, PointerEventData.InputButton button)
        {
            _owner.SetCurrentSelectedSlot(slot);
        }

        private void UnitManagerSlotOnPointerClicked(UnitManagerSlot slot, PointerEventData.InputButton button)
        {
            if (button == PointerEventData.InputButton.Right)
            {
                var contextEntries = new List<ContextMenuEntry>
                {
                    new ContextMenuEntry { title = "View Details", callback = () => ViewDetails(slot), enabled = true}
                };

                if (slot.Unit.ID != NetworkManager.LocalPlayer.PlayerData.LeaderData.ID)
                {
                    if (NetworkManager.Party.HasUnit(slot.Unit.ID))
                    {
                        contextEntries.Add(new ContextMenuEntry { title = "Kick from Party", callback = () => RemoveUnitFromParty(slot.Unit.ID), enabled = true});
                    }
                    else
                    {
                        contextEntries.Add(new ContextMenuEntry { title = "Add to Party", callback = () => AddUnitToParty(slot.Unit.ID), enabled = true});
                    }
                }

                ContextMenu.Show(Input.mousePosition, contextEntries, slot.Unit.DisplayName, _owner.unitManagerPanel.gameObject);
            }
        }

        private void AddUnitToParty(string unitId)
        {
            NetworkManager.AddUnitToParty(unitId);
            ContextMenu.Dismiss(_owner.unitManagerPanel.gameObject);
        }

        private void RemoveUnitFromParty(string unitId)
        {
            NetworkManager.RemoveUnitFromParty(unitId);
            ContextMenu.Dismiss(_owner.unitManagerPanel.gameObject);
        }

        private void ViewDetails(UnitManagerSlot slot)
        {
            _owner.SetCurrentSelectedSlot(slot);
            _owner.ChangeState<UnitManagerPanelStateViewDetails>();
        }

        private void UnitManagerSlotOnDoubleClicked(UnitManagerSlot slot, PointerEventData.InputButton button)
        {
            _owner.SetCurrentSelectedSlot(slot);
            _owner.ChangeState<UnitManagerPanelStateViewDetails>();
        }

        private void OnViewDetailsClicked()
        {
            _owner.ChangeState<UnitManagerPanelStateViewDetails>();
        }
    }

}
