using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using UnityEngine;

namespace ProjectTactics
{
    public class UnitManagerPanelStateHidden : UnitManagerPanelState
    {
        public override void Enter()
        {
            base.Enter();
            _owner.unitManagerPanel.Hide();
            _owner.viewDetailsPanel.Hide();
            _owner.unitManagerButton.SetSelected(false);

            _owner.UnloadAll();
            ContextMenu.Dismiss(_owner.unitManagerPanel.gameObject);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.U) && !UIModalView.IsModalViewShowing)
            {
                _owner.ChangeState<UnitManagerPanelStateRoot>();
            }
        }

        protected override void OnUnitManagementClicked()
        {
            _owner.ChangeState<UnitManagerPanelStateRoot>();
        }
    }
}
