using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class UnitManagerPanelStateViewDetails : UnitManagerPanelState
    {
        public override void Enter()
        {
            base.Enter();
            RenderTextureTarget rtt = RenderTextureManager.GetRenderTextureTarget(_owner.CurrentSlotIndex);
            _owner.viewDetailsPanel.UpdateUI(_owner.CurrentUnit, rtt.Texture);
            _owner.viewDetailsPanel.Show(true);
        }

        protected override void AddListeners()
        {
            base.AddListeners();
            UnitManagerViewDetailsPanel.DoneButtonClicked += OnDoneButtonClicked;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            UnitManagerViewDetailsPanel.DoneButtonClicked -= OnDoneButtonClicked;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.U))
            {
                _owner.ChangeState<UnitManagerPanelStateHidden>();
            }
        }

        private void OnDoneButtonClicked()
        {
            _owner.ChangeToPreviousState();
        }
    }
}
