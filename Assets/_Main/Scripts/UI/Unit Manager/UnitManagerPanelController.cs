using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class UnitManagerPanelController : UIController
    {
        public GameMenuButton unitManagerButton;
        public UnitManagerPanel unitManagerPanel;
        public UnitManagerViewDetailsPanel viewDetailsPanel;

        public Unit CurrentUnit { get; private set; }
        public int CurrentSlotIndex { get; set; }

        private UnitManagerSlot _currentSelectedSlot;

        private void Start()
        {
            unitManagerPanel.Init(this);
            viewDetailsPanel.Init(this);
            ChangeState<UnitManagerPanelStateHidden>();
        }

        public void SetCurrentSelectedSlot(UnitManagerSlot slot)
        {
            if (_currentSelectedSlot && _currentSelectedSlot != slot)
            {
                _currentSelectedSlot.SetSelected(false);
            }

            _currentSelectedSlot = slot;
            _currentSelectedSlot.SetSelected(true);

            CurrentUnit = slot.Unit;
            CurrentSlotIndex = slot.SlotIndex;
            unitManagerPanel.UpdateStatView(CurrentUnit);
        }

        private void OnDestroy()
        {
            UnloadAll();
        }

        public void UnloadAll()
        {
            unitManagerPanel.DestroyUnits();
            RenderTextureManager.UnloadAll();

            CurrentSlotIndex = 0;
            CurrentUnit = null;
        }

        public override void Dismiss()
        {
            if (viewDetailsPanel.IsVisible)
            {
                viewDetailsPanel.Hide();
                ChangeState<UnitManagerPanelStateRoot>();
                return;
            }

            ChangeState<UnitManagerPanelStateHidden>();
        }
    }
}
