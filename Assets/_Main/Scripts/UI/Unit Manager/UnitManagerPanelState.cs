using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class UnitManagerPanelState : State
    {
        protected UnitManagerPanelController _owner;

        protected virtual void Awake()
        {
            _owner = GetComponent<UnitManagerPanelController>();
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void AddListeners()
        {
            base.AddListeners();
            _owner.unitManagerButton.Button.onClick.AddListener(OnUnitManagementClicked);
        }

        /// <summary>
        /// Base function call required.
        /// </summary>
        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            _owner.unitManagerButton.Button.onClick.RemoveListener(OnUnitManagementClicked);
        }

        protected virtual void OnUnitManagementClicked()
        {
            _owner.ChangeState<UnitManagerPanelStateHidden>();
        }
    }
}
