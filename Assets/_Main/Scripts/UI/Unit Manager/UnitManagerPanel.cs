using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class UnitManagerPanel : UIPanel
    {
        public static event Action ViewDetailsClicked;
        public static event Action CloseClicked;

        [SerializeField] private UnitManagerSlot slotClone;
        [SerializeField] private Transform slotContainer;
        [SerializeField] private StatView statView;

        [SerializeField] private TMP_Text unitNameLabel;
        [SerializeField] private TMP_Text unitLevelLabel;

        private List<UnitManagerSlot> _slots = new List<UnitManagerSlot>();

        private void Start()
        {
            slotClone.Hide();
        }

        public void UpdateUI(List<Unit> units, List<RenderTextureTarget> renderTextureTargets, int selectedIndex)
        {
            int count= 0;
            foreach (Unit u in units)
            {
                if (count < _slots.Count)
                {
                    _slots[count].UpdateUI(u, renderTextureTargets[count].Texture, count);
                    _slots[count].Show();
                }
                else
                {
                    UnitManagerSlot slot = Instantiate(slotClone, slotContainer);
                    slot.UpdateUI(u, renderTextureTargets[count].Texture, count);
                    slot.Show();
                    _slots.Add(slot);
                }
                count++;
            }

            for (int i = count; i < _slots.Count; i++)
            {
                _slots[i].Hide();
            }

            statView.UpdateUI(units[selectedIndex]);
        }

        public UnitManagerSlot GetUnitSlot(int index)
        {
            if (index >= _slots.Count)
            {
                return null;
            }

            return _slots[index];
        }

        public void OnViewDetailsClicked()
        {
            ViewDetailsClicked?.Invoke();
        }

        public void DestroyUnits()
        {
            foreach (UnitManagerSlot slot in _slots)
            {
                slot.DestroyUnit();
            }
        }

        public void UpdateStatView(Unit unit)
        {
            unitNameLabel.text = unit.DisplayName;
            unitLevelLabel.text = $"Lvl {unit.Level.ToString()}";

            statView.UpdateUI(unit);
        }

        public void OnCloseClicked()
        {
            CloseClicked?.Invoke();
        }
    }

}
