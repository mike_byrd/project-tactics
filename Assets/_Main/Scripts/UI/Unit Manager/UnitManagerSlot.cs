using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.Rendering;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class UnitManagerSlot : UIView, IPointerDownHandler, IPointerClickHandler
    {
        public static event Action<UnitManagerSlot, PointerEventData.InputButton> PointerDown;
        public static event Action<UnitManagerSlot, PointerEventData.InputButton> PointerClicked;
        public static event Action<UnitManagerSlot, PointerEventData.InputButton> DoubleClicked;

        [SerializeField] private TMP_Text unitNameLabel;
        [SerializeField] private TMP_Text jobLevelLabel;
        [SerializeField] private RawImage unitView;
        [SerializeField] private Image highlight;

        [SerializeField] private float doubleClickTime = 0.5f;

        public Unit Unit { get; private set; }
        public int SlotIndex { get; private set; }

        private int _clickCount = 0;
        private bool _waitForDoubleClick = false;
        private float _timeSinceFirstClick = 0;

        public void UpdateUI(Unit unit, RenderTexture renderTexture, int slotIndex)
        {
            DestroyUnit();

            Unit = unit;
            unitNameLabel.text = Unit.DisplayName;
            unitView.texture = renderTexture;
            jobLevelLabel.text = $"{Util.GetDisplayName(unit.Job.jobType)} Lvl {unit.Level}";

            SlotIndex = slotIndex;
        }

        public void DestroyUnit()
        {
            if (Unit)
            {
                Destroy(Unit.gameObject);
            }
        }

        private void Update()
        {
            if (_waitForDoubleClick)
            {
                _timeSinceFirstClick += Time.deltaTime;
                if (_timeSinceFirstClick > doubleClickTime)
                {
                    _waitForDoubleClick = false;
                    _timeSinceFirstClick = 0;
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            ExecuteEvents.ExecuteHierarchy(transform.parent.gameObject, eventData, ExecuteEvents.pointerDownHandler);

            // The click event should be fired after bubbling up the click event so the context menu appears on top of
            // the player list panel.
            PointerDown?.Invoke(this, eventData.button);

            if (eventData.button != PointerEventData.InputButton.Left) return;
            if (_waitForDoubleClick)
            {
                _timeSinceFirstClick = 0;
                DoubleClicked?.Invoke(this, eventData.button);
            }

            _waitForDoubleClick = true;
        }

        public void SetSelected(bool selected)
        {
            highlight.gameObject.SetActive(selected);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            PointerClicked?.Invoke(this, eventData.button);
        }
    }
}
