using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class UnitManagerViewDetailsPanel : UIPanel
    {
        public static event Action DoneButtonClicked;

        [SerializeField] private TMP_Text unitNameLabel;
        [SerializeField] private TMP_Text jobLabel;
        [SerializeField] private RawImage unitView;
        [SerializeField] private StatView[] statViews;
        [SerializeField] private StatTypes[] backgroundStats;

        [SerializeField] private Transform abilityPanel;
        [SerializeField] private Transform inventoryPanel;
        [SerializeField] private Button closeButton;
        [SerializeField] private Image background;

        public bool IsRestrictedView { get; private set; }

        private RenderTextureTarget _target;

        public void UpdateUI(Unit unit, RenderTexture renderTexture, bool restrictedView = false)
        {
            unitNameLabel.text = unit.DisplayName;
            unitView.texture = renderTexture;
            jobLabel.text = Util.GetDisplayName(unit.Job.jobType);

            foreach (StatView statView in statViews)
            {
                statView.UpdateUI(unit, backgroundStats);
            }

            IsRestrictedView = restrictedView;
            inventoryPanel.gameObject.SetActive(!restrictedView);
            abilityPanel.gameObject.SetActive(!restrictedView);
            background.gameObject.SetActive(!restrictedView);
            jobLabel.gameObject.SetActive(!restrictedView);
            closeButton.gameObject.SetActive(restrictedView);
        }

        public void OnDoneButtonClicked()
        {
            DoneButtonClicked?.Invoke();
        }

        public void InspectUnit(Unit unit, RenderTextureTarget textureTarget)
        {
            _target = textureTarget;
            UpdateUI(unit, textureTarget.Texture, true);
            Show(true);
        }

        public override void Hide()
        {
            base.Hide();
            if (_target)
            {
                _target.Unload();
                _target = null;
            }
        }
    }
}
