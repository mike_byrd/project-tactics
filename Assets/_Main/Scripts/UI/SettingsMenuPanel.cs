using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class SettingsMenuPanel : UIModalView
    {
        public static event Action OptionsClicked;
        public static event Action LeaveWorldClicked;
        public static event Action LogoutClicked;
        public static event Action ExitGameClicked;

        public void OnOptionsClicked()
        {
            OptionsClicked?.Invoke();
        }

        public void OnLeaveWorldClicked()
        {
            LeaveWorldClicked?.Invoke();
        }

        public void OnLogoutClicked()
        {
            LogoutClicked?.Invoke();
        }

        public void OnExitGameClicked()
        {
            ExitGameClicked?.Invoke();
        }

        public void OnCloseClicked()
        {
            Hide();
        }
    }
}
