using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MCB.UI
{
    public class UIFocusTrigger : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private UIView target;

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!target)
            {
                Debug.LogError("UIFocusTriggers require targets.");
                return;
            }

            UIView.SetCurrentFocus(target);
        }
    }
}
