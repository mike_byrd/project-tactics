﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CharacterPropertySlot : MonoBehaviour
{
    public static event Action<CharacterPropertySlot> LeftClicked;
    public static event Action<CharacterPropertySlot> RightClicked;

    [SerializeField] private string propertyName;
    [SerializeField] private Button leftButton;
    [SerializeField] private Button rightButton;
    [SerializeField] private TMP_Text currentIndexLabel;

    public string PropertyName => propertyName;

    private int currentIndex;

    private void Awake()
    {
        leftButton.onClick.AddListener(() =>
        {
            LeftClicked?.Invoke(this);
        });
            
        rightButton.onClick.AddListener(() =>
        {
            RightClicked?.Invoke(this);
        });
    }

    public void UpdateUI(string labelText)
    {
        currentIndexLabel.text = labelText;
    }
}
