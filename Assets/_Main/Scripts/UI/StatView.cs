using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.UI;
using TMPro;
using UnityEngine;

namespace ProjectTactics
{
    [Serializable]
    public class UIStat
    {
        public StatTypes stat;
        public string text;
    }

    public class StatView : UIView
    {
        [SerializeField] private Transform container;
        [SerializeField] private StatViewSlot slotClone;
        [SerializeField] private StatTypes[] stats;

        private List<StatViewSlot> _slots = new List<StatViewSlot>();

        private void Start()
        {
            slotClone.Hide();
        }

        public void UpdateUI(Unit unit, StatTypes[] backgroundStats = null)
        {
            int count= 0;
            foreach (StatTypes stat in stats)
            {
                bool hideStatBackground = IsBackgroundStat(stat, backgroundStats);
                if (count < _slots.Count)
                {
                    _slots[count].UpdateUI(stat, unit, hideStatBackground);
                    _slots[count].Show();
                }
                else
                {
                    StatViewSlot slot = Instantiate(slotClone, container);
                    slot.UpdateUI(stat, unit, hideStatBackground);
                    slot.Show();
                    _slots.Add(slot);
                }
                count++;
            }

            for (int i = count; i < _slots.Count; i++)
            {
                _slots[i].Hide();
            }
        }

        private static bool IsBackgroundStat(StatTypes stat, StatTypes[] backgroundStats)
        {
            if (backgroundStats == null)
            {
                return false;
            }

            foreach (StatTypes s in backgroundStats)
            {
                if (s == stat)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
