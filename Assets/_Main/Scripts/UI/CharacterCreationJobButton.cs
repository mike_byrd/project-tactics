﻿using System.Collections;
using System.Collections.Generic;
using ProjectTactics;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class CharacterCreationJobButton : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private TMP_Text label;
        [SerializeField] private JobType job;

        public JobType Job => job;

        public void SetSelected(bool selected)
        {
            if (selected)
            {
                button.image.color = K.TileColor.SelectedSolidColor;
            }
            else
            {
                button.image.color = K.TileColor.UnselectedWhiteColor;
            }
        }
    }
}
