﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using UnityEngine;
using UnityEngine.UI;

namespace ProjectTactics
{
    public class ZoneChangeMenuPanel : UIModalView
    {
        public static event Action BattleClicked;
        public static event Action CloseClicked;

        public void OnBattleClicked()
        {
            BattleClicked?.Invoke();
        }

        public void OnCloseClicked()
        {
            CloseClicked?.Invoke();
        }
    }
}
