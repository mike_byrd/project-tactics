﻿using System;
using System.Collections;
using System.Collections.Generic;
using ProjectTactics;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class CharacterSelectSlot : MonoBehaviour
    {
        public static event Action<CharacterSelectSlot> OnClicked;

        [SerializeField] private Transform content;
        [SerializeField] private Image background;
        [SerializeField] private TMP_Text nameLabel;
        [SerializeField] private TMP_Text jobLabel;
        [SerializeField] private TMP_Text levelLabel;
        [SerializeField] private TMP_Text emptyLabel;

        [SerializeField] private Button button;
        [SerializeField] private TMP_Text buttonLabel;

        public UnitData currentDataModel { get; private set; }
        public bool isNewCharacterSlot { get; private set; }

        private void Awake()
        {
            button.onClick.AddListener(OnButtonClicked);
        }

        private void OnButtonClicked()
        {
            OnClicked?.Invoke(this);
        }

        public void UpdateUI(UnitData unitData, bool isNewCharacter = false)
        {
            currentDataModel = unitData;
            isNewCharacterSlot = isNewCharacter;

            if (isNewCharacter)
            {
                ShowNewCharacterButton();
            }
            else
            {
                HideNewCharacterButton();
                if (unitData == null)
                {
                    SetEmpty(true);
                    return;
                }

                SetEmpty(false);
                nameLabel.text = unitData.Name;
                jobLabel.text = unitData.Job.ToString();
                levelLabel.text = $"Level {unitData.Stats.Level}";
            }
        }

        public void SetSelected(bool selected)
        {
            if (selected)
            {
                background.color = K.TileColor.SelectedColor;
            }
            else
            {
                background.color = K.TileColor.UnselectedClearColor;
            }
        }

        public void SetEmpty(bool empty)
        {
            if (empty)
            {
                emptyLabel.gameObject.SetActive(true);
                content.gameObject.SetActive(false);
            }
            else
            {
                emptyLabel.gameObject.SetActive(false);
                content.gameObject.SetActive(true);
            }
        }

        public void ShowNewCharacterButton()
        {
            button.image.enabled = true;
            buttonLabel.text = "New Character";
        }

        public void HideNewCharacterButton()
        {
            button.image.enabled = false;
            buttonLabel.text = "";
        }
    }
}