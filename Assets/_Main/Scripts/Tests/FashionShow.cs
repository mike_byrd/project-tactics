﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FashionShow : MonoBehaviour
{
  public GameObject[] characterPrefabs;

  private void Start()
  {
    GameObject models = new GameObject("Models");
    for(int i = characterPrefabs.Length - 1; i >= 0; i--) {
      GameObject obj = Instantiate(characterPrefabs[i], models.transform);
      obj.transform.Translate(Vector3.right * i);
    }
  }
}
