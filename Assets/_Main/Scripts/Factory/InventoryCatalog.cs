﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventoryCatalog : ScriptableObject
{
  public GameObject primaryWeapon;
  public GameObject secondaryWeapon;
  public GameObject helmet;
  public GameObject body;
  public GameObject accessory;
}
