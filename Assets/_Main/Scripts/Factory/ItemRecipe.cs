﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemRecipe : ScriptableObject
{
  public string itemName;
  public string itemModelName;
  public string itemModelVariantName;

  [EnumFlag("Equip Slots")]
  public EquipSlots equipSlots;
  public ItemType itemType;

  public StatValuePair[] statModifiers;
}
