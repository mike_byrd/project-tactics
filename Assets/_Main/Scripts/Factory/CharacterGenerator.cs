﻿using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

public class CharacterGenerator : MonoBehaviour
{
    public GameObject characterPrefab;
    public SkinnedMeshRenderer characterBaseMeshRenderer;

    public GameObject characterHead;
    public GameObject characterBack;
    public GameObject characterRightArm;
    public GameObject characterLeftArm;

    public Material[] baseMaterials;

    public Unit character;

    void Awake()
    {
        NameGenerator.Initialize();
    }

    private void Start()
    {
        GenerateCharacter("Soldier");
    }

    public void GenerateCharacter(string jobType)
    {
        if (character)
        {
            Destroy(character);
        }

        switch (jobType)
        {
            case "Soldier":
                character = UnitFactory.Create(JobType.Soldier);
                break;
            case "Archer":
                character = UnitFactory.Create(JobType.Archer);
                break;
            case "Thief":
                character = UnitFactory.Create(JobType.Thief);
                break;
            case "White Mage":
                character = UnitFactory.Create(JobType.WhiteMage);
                break;
            case "Black Mage":
                character = UnitFactory.Create(JobType.BlackMage);
                break;
            default:
                break;
        }
    }
}