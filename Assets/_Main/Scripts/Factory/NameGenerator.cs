﻿using UnityEngine;
using System.Collections;

public class NameGenerator
{
    private ShuffleBag<string> maleNames;
    private ShuffleBag<string> femaleNames;
    private ShuffleBag<string> animalNames;

    private static NameGenerator instance;

    public static void Initialize()
    {
        if (instance == null)
        {
            instance = new NameGenerator();
        }
        else
        {
            return;
        }

        try
        {
            //Load male names
            TextAsset nameList = Resources.Load<TextAsset>("Names/Male names") as TextAsset;
            string[] names = nameList.text.Split('\n');
            instance.maleNames = new ShuffleBag<string>(names, true);

            //Load female names
            nameList = Resources.Load<TextAsset>("Names/Female names") as TextAsset;
            names = nameList.text.Split('\n');
            instance.femaleNames = new ShuffleBag<string>(names, true);

            //Load animal names
            nameList = Resources.Load<TextAsset>("Names/Animal names") as TextAsset;
            names = nameList.text.Split('\n');
            instance.animalNames = new ShuffleBag<string>(names, true);
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
            throw;
        }
    }

    public static string GetRandomName(GenderType gender)
    {
        if (instance == null)
        {
            Debug.LogWarning("NameGenerator has not been initialized.");
            return "";
        }

        string name = "Unknown";
        switch (gender)
        {
            case GenderType.None:
                break;
            case GenderType.Male:
                name = instance.maleNames.DrawValue();
                break;
            case GenderType.Female:
                name = instance.femaleNames.DrawValue();
                break;
            case GenderType.Animal:
                name = instance.animalNames.DrawValue();
                break;
            default:
                //If no gender is specified, get any name
                int rand = Random.Range(1, 4);
                if (rand == (int) GenderType.Male)
                {
                    name = instance.maleNames.DrawValue();
                }
                else if (rand == (int) GenderType.Female)
                {
                    name = instance.femaleNames.DrawValue();
                }
                else
                {
                    name = instance.animalNames.DrawValue();
                }

                break;
        }

        return name.TrimEnd();
    }
}