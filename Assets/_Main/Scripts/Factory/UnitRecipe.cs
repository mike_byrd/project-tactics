﻿using UnityEngine;
using System.Collections;

public class UnitRecipe : ScriptableObject 
{
	public string model;
	public JobType job;
	public string attack;
	public string abilityCatalog;
	public string strategy;
	public Locomotions locomotion;
	public Alliances alliance;
}