﻿using System;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics.DataModels;
using Random = UnityEngine.Random;

public static class UnitFactory
{
    private static Dictionary<JobType, GameObject[]> _maleSkinTones;
    private static Dictionary<JobType, GameObject[]> _femaleSkinTones;
    private static Dictionary<GenderType, GameObject[]> _hairStyles;

    private const string ModelPathMale = "Units/Base Characters/Male";
    private const string ModelPathFemale = "Units/Base Characters/Female";

    private static bool _initialized;

    #region Public

    public static void Initialize()
    {
        if (_initialized)
        {
            return;
        }

        _initialized = true;
        _maleSkinTones = new Dictionary<JobType, GameObject[]>();
        _femaleSkinTones = new Dictionary<JobType, GameObject[]>();
        _hairStyles = new Dictionary<GenderType, GameObject[]>();

        IEnumerable<JobType> jobValues = Util.GetEnumValues<JobType>();
        foreach (JobType job in jobValues)
        {
            if (job == JobType.None) continue;

            GameObject[] skinTones = Resources.LoadAll<GameObject>($"{ModelPathMale}/{Util.GetDisplayName(job)}");
            _maleSkinTones[job] = skinTones;

            skinTones = Resources.LoadAll<GameObject>($"{ModelPathFemale}/{Util.GetDisplayName(job)}");
            _femaleSkinTones[job] = skinTones;
        }

        GameObject[] hairStyles = Resources.LoadAll<GameObject>("Hair/Male");
        UnitFactory._hairStyles[GenderType.Male] = hairStyles;

        hairStyles = Resources.LoadAll<GameObject>("Hair/Female");
        UnitFactory._hairStyles[GenderType.Female] = hairStyles;
    }

    public static Unit Create(UnitData data)
    {
        Unit unit = Create(data.Name, data.Gender, data.Job, data.BaseModelId, data.HairModelId, data.Stats, data.Alliances);
        unit.ID = data.ID;
        return unit;
    }

    public static Unit Create(UnitData data, Transform parent)
    {
        Unit unit = Create(data.Name, data.Gender, data.Job, data.BaseModelId, data.HairModelId, data.Stats, data.Alliances);
        unit.ID = data.ID;
        unit.transform.SetParent(parent, false);
        return unit;
    }

    public static Unit Create(string name, GenderType gender, JobType job, int baseModelId, int hairModelId,
        StatsData stats, Alliances alliances)
    {
        Initialize();

        Unit unit;
        try
        {
            // Create the Unit Object
            GameObject unitObject = Util.InstantiatePrefab("Units/Unit");
            Stats unitStats;
            if (stats != null)
            {
                unitStats = AddStats(unitObject, stats);
            }
            else
            {
                unitStats = AddStats(unitObject);
            }
            AddLocomotion(unitObject, Locomotions.Walk);
            unitObject.AddComponent<Status>();
            AddJob(unitObject, job);
            AddRank(unitObject, unitStats[StatTypes.LVL]);

            GameObject modelVisualPrefab = GetSkinTone(job, gender, baseModelId);
            GameObject modelVisual = Util.InstantiatePrefab(modelVisualPrefab);

            GameObject hairModelPrefab = GetHairstyle(gender, hairModelId);
            GameObject hair = Util.InstantiatePrefab(hairModelPrefab);

            unit = unitObject.GetComponent<Unit>();
            unit.ModelVisual.Init(modelVisual, hair);

            unit.DisplayName = name;
            unit.gender = gender;

            unit.Hud.InitializeHealthBar(alliances);

            AddEquipment(unit, job);

            // TODO: Add Custom Attack types later.
            if (job == JobType.Archer)
            {
                AddAttack(unit.gameObject, "Common/Archer Basic Attack");
            }
            else
            {
                AddAttack(unit.gameObject, "Common/Basic Attack");
            }

            string abilityCatalogName = Util.GetDisplayName(job) + " Catalog";
            AddAbilityCatalog(unit.gameObject, abilityCatalogName);
            AddAlliance(unit.gameObject, alliances);

            // TODO: add ai attack pattern
            AddAttackPattern(unit.gameObject, "");
        }
        catch (Exception e)
        {
            Debug.LogError(e);
            return null;
        }

        return unit;
    }

    public static Unit Create(string name, GenderType gender, JobType job, int baseModelId, int hairModelId,
        int level = 1, Alliances alliances = Alliances.Ally)
    {
        return Create(name, gender, job, baseModelId, hairModelId, null, alliances);
    }

    public static Unit Create(JobType job, int level = 1, Alliances alliances = Alliances.Ally)
    {
        GenderType gender = Random.Range(0, 2) == 1 ? GenderType.Male : GenderType.Female;
        string name = NameGenerator.GetRandomName(gender);

        int totalSkinTones = gender == GenderType.Male ? _maleSkinTones.Count : _femaleSkinTones.Count;
        int skinIndex = Random.Range(0, totalSkinTones);

        int totalHairStyles = _hairStyles[gender].Length;
        int hairIndex = Random.Range(0, totalHairStyles);

        return Create(name, gender, job, skinIndex, hairIndex, level, alliances);
    }

    public static GameObject Create(string name, int level)
    {
        UnitRecipe recipe = Resources.Load<UnitRecipe>("Unit Recipes/" + name);
        if (recipe == null)
        {
            Debug.LogError("No Unit Recipe for name: " + name);
            return null;
        }

        return Create(recipe, level);
    }

    public static GameObject Create(UnitRecipe recipe, int level)
    {
        GameObject obj = Util.InstantiatePrefab("Units/" + recipe.model);
        obj.name = recipe.name;
        obj.AddComponent<Unit>();
        AddStats(obj);
        AddLocomotion(obj, recipe.locomotion);
        obj.AddComponent<Status>();
        obj.AddComponent<Equipment>();
        AddJob(obj, recipe.job);
        AddRank(obj, level);
        obj.AddComponent<Health>();
        obj.AddComponent<ActionPoints>();
        AddAttack(obj, recipe.attack);
        AddAbilityCatalog(obj, recipe.abilityCatalog);
        AddAlliance(obj, recipe.alliance);
        AddAttackPattern(obj, recipe.strategy);

        // Get random base material
        GenderType gender = Random.Range(0, 2) == 1 ? GenderType.Male : GenderType.Female;
        string name = NameGenerator.GetRandomName(gender);
        obj.name = name;

        string materialAssetPath =
            string.Format("Materials/Costumes/{0}/{1}", Util.GetDisplayName(recipe.job), gender.ToString());
        Material[] baseMaterials = Resources.LoadAll<Material>(materialAssetPath);
        int randomMatIndex = Random.Range(0, baseMaterials.Length);

        SkinnedMeshRenderer meshRenderer = obj.GetComponentInChildren<SkinnedMeshRenderer>();
        meshRenderer.material = baseMaterials[randomMatIndex];

        // Add canvas with healthbar.
        GameObject canvas = Util.InstantiatePrefab("Units/Unit Canvas");
        canvas.transform.SetParent(obj.transform, true);

        return obj;
    }

    public static GameObject[] GetSkinTones(JobType job, GenderType gender)
    {
        switch (gender)
        {
            case GenderType.Male:
                return _maleSkinTones[job];
            case GenderType.Female:
                return _femaleSkinTones[job];
            default:
                return null;
        }
    }

    public static GameObject GetSkinTone(JobType job, GenderType gender, int index)
    {
        var skins = GetSkinTones(job, gender);
        if (index < 0 || index >= skins.Length)
        {
            return null;
        }

        return skins[index];
    }

    public static GameObject[] GetHairstyles(GenderType gender)
    {
        switch (gender)
        {
            case GenderType.Male:
            case GenderType.Female:
                return _hairStyles[gender];
            default:
                return null;
        }
    }

    public static GameObject GetHairstyle(GenderType gender, int index)
    {
        GameObject[] hair = GetHairstyles(gender);
        if (index < 0 || index >= hair.Length)
        {
            return null;
        }

        return hair[index];
    }

    #endregion

    #region Private

    private static void AddEquipment(Unit unit, JobType job)
    {
        Equipment equipment = unit.GetComponent<Equipment>();

        InventoryCatalog inventory =
            Resources.Load<InventoryCatalog>("Inventory Catalogs/" + Util.GetDisplayName(job) + " Inventory");

        if (inventory.primaryWeapon)
        {
            Equippable primary = Util.InstantiatePrefab(inventory.primaryWeapon, unit.ModelVisual.transform)
                .GetComponent<Equippable>();
            equipment.Equip(primary, primary.defaultSlots);
        }

        if (inventory.secondaryWeapon)
        {
            Equippable secondary = Util.InstantiatePrefab(inventory.secondaryWeapon, unit.ModelVisual.transform)
                .GetComponent<Equippable>();
            equipment.Equip(secondary, secondary.defaultSlots);
        }

        if (inventory.helmet)
        {
            Equippable helmet = Util.InstantiatePrefab(inventory.helmet, unit.ModelVisual.transform)
                .GetComponent<Equippable>();
            //helmet.SetAllianceColor(alliances);
            equipment.Equip(helmet, helmet.defaultSlots);
        }
    }

    static Stats AddStats(GameObject obj)
    {
        Stats s = obj.GetComponent<Stats>();
        s.SetValue(StatTypes.LVL, 1, false);
        return s;
    }

    static Stats AddStats(GameObject obj, StatsData stats)
    {
        Stats s = obj.GetComponent<Stats>();
        s.SetValue(StatTypes.LVL, (int)stats.Level, false);
        s.SetValue(StatTypes.EXP, (int)stats.Exp, false);
        s.SetValue(StatTypes.HP, (int)stats.HP, false);
        s.SetValue(StatTypes.MHP, (int)stats.MaxHP, false);
        s.SetValue(StatTypes.AP, (int)stats.AP, false);
        s.SetValue(StatTypes.MAP, (int)stats.MaxAP, false);
        s.SetValue(StatTypes.MP, (int)stats.MP, false);
        s.SetValue(StatTypes.MMP, (int)stats.MaxMP, false);
//        s.SetValue(StatTypes.SP, (int)stats.SP, false); // TODO: Add later
//        s.SetValue(StatTypes.MSP, (int)stats.MaxSP, false);
        s.SetValue(StatTypes.ATK, (int)stats.Attack, false);
        s.SetValue(StatTypes.DEF, (int)stats.Defense, false);
        s.SetValue(StatTypes.MAT, (int)stats.MagicAttack, false);
        s.SetValue(StatTypes.MDF, (int)stats.MagicDefense, false);
        s.SetValue(StatTypes.RNG, (int)stats.Range, false);
        s.SetValue(StatTypes.JMP, (int)stats.JumpHeight, false);
        s.SetValue(StatTypes.CRT, (int)stats.CriticalHitRate, false);
        s.SetValue(StatTypes.SPD, (int)stats.Speed, false);
        return s;
    }

    static void AddJob(GameObject obj, JobType jobType, bool loadDefaultStats = false)
    {
        GameObject instance = Util.InstantiatePrefab("Jobs/" + Util.GetDisplayName(jobType));
        instance.transform.SetParent(obj.transform);
        Job job = instance.GetComponent<Job>();

        job.jobType = jobType;

        if (loadDefaultStats)
        {
            job.Employ();
            job.LoadDefaultStats();
        }
    }

    static void AddLocomotion(GameObject obj, Locomotions type)
    {
        switch (type)
        {
            case Locomotions.Walk:
                obj.AddComponent<WalkMovement>();
                break;
            case Locomotions.Fly:
                obj.AddComponent<FlyMovement>();
                break;
            case Locomotions.Teleport:
                obj.AddComponent<TeleportMovement>();
                break;
        }
    }

    static void AddAlliance(GameObject obj, Alliances type)
    {
        Alliance alliance = obj.AddComponent<Alliance>();
        alliance.type = type;
    }

    static void AddRank(GameObject obj, int level)
    {
        Rank rank = obj.AddComponent<Rank>();
        rank.Init(level);
    }

    static void AddAttack(GameObject obj, string name)
    {
        GameObject instance = Util.InstantiatePrefab("Abilities/" + name);
        instance.transform.SetParent(obj.transform);
    }

    static void AddAbilityCatalog(GameObject obj, string name)
    {
        GameObject main = new GameObject("Ability Catalog");
        main.transform.SetParent(obj.transform);
        main.AddComponent<AbilityCatalog>();

        AbilityCatalogRecipe recipe = Resources.Load<AbilityCatalogRecipe>("Ability Catalog Recipes/" + name);
        if (recipe == null)
        {
            Debug.LogError("No Ability Catalog Recipe Found: " + name);
            return;
        }

        for (int i = 0; i < recipe.categories.Length; ++i)
        {
            GameObject category = new GameObject(recipe.categories[i].name);
            category.transform.SetParent(main.transform);

            for (int j = 0; j < recipe.categories[i].entries.Length; ++j)
            {
                string abilityName = string.Format("Abilities/{0}/{1}", recipe.categories[i].name,
                    recipe.categories[i].entries[j]);
                GameObject ability = Util.InstantiatePrefab(abilityName);
                ability.transform.SetParent(category.transform);
            }
        }
    }

    static void AddAttackPattern(GameObject obj, string name)
    {
        Driver driver = obj.AddComponent<Driver>();
        if (string.IsNullOrEmpty(name))
        {
            driver.normal = Drivers.Human;
        }
        else
        {
            driver.normal = Drivers.Computer;
            GameObject instance = Util.InstantiatePrefab("Attack Pattern/" + name + " Attack Pattern");
            instance.transform.SetParent(obj.transform);
        }
    }

    #endregion
}