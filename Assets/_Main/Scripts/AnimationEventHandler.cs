﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles all animation events from the animation timeline. 
/// NOTE: Animation Event keys should not be on the last frame of an animation 
/// or else the event might fail to fire for some reason.
/// </summary>
public class AnimationEventHandler : MonoBehaviour
{
  public const string AnimationEventNotification = "AnimationEventHandler.AnimationEventNotification";

  public void OnAnimationEventTriggered(string animationName)
  {
    this.PostNotification(AnimationEventNotification, animationName);
  }
}
