using MCB;
using MCB.Common;

namespace ProjectTactics.DataModels
{
    public class PlayerHandle
    {
        public bool IsMe { get; set; }
        public PlayerData PlayerData { get; set; }
        public PlayerCharacter Character { get; set; }
        public Unit Unit { get; set; }
        public string LeaderID => PlayerData.LeaderData.ID;
        public string PlayerID => PlayerData.ID;
    }

    public class PlayerData
    {
        public string ID;
        public string Username { get; set; }
        public InventoryData Inventory { get; private set; }
        public Point Position { get; set; }
        public UnitData LeaderData { get; private set; }
        public string NameplateDecorationId { get; private set; }

        public PlayerData(PlayerDataModel dataModel)
        {
            ID = dataModel.id;
            Username = dataModel.username;
            Inventory = new InventoryData(dataModel.inventory);
            Position = dataModel.position;
            LeaderData = Inventory.GetUnit(dataModel.leaderId);
            NameplateDecorationId = dataModel.nameplateDecoration;
        }

        public PlayerDataModel ToDataModel()
        {
            var units = Inventory.GetUnits();
            var unitDataModels = new UnitDataModel[units.Length];
            for (int i = 0; i < units.Length; i++)
            {
                unitDataModels[i] = units[i].ToDataModel();
            }

            return new PlayerDataModel
            {
                inventory = new InventoryDataModel
                {
                    units = unitDataModels
                },
                username = Username
            };
        }

        public bool IsLeadUnit(string unitId)
        {
            return unitId != null && unitId == LeaderData.ID;
        }
    }
}
