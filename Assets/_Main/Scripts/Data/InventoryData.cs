using System;
using System.Collections.Generic;
using System.Linq;
using BestHTTP.JSON.LitJson;
using UnityEngine;

namespace ProjectTactics.DataModels
{
    public class InventoryData
    {
        private List<UnitData> _units;
        private Dictionary<string, List<string>> _decorations;

        public InventoryData(InventoryDataModel dataModel)
        {
            _units = new List<UnitData>();
            _decorations = new Dictionary<string, List<string>>();

            UpdateUnits(dataModel.units);
            UpdateDecorations(dataModel.cosmetics);
        }

        public void UpdateUnits(IEnumerable<UnitDataModel> unitRecords)
        {
            _units.Clear();
            if (unitRecords == null)
            {
                return;
            }

            AddUnits(unitRecords);
        }

        public void UpdateDecorations(Dictionary<object, object> cosmetics)
        {
            _decorations.Clear();
            try
            {
                var decorations = cosmetics["decorations"] as JsonData;
                var nameplates = decorations["nameplates"];

                for (int i = 0; i < nameplates.Count; i++)
                {
                    string np = (string)nameplates[i];
                    if (_decorations.ContainsKey("nameplates"))
                    {
                        _decorations["nameplates"].Add(np);
                    }
                    else
                    {
                        _decorations.Add("nameplates", new List<string> { np });
                    }
                    Debug.Log("Nameplate: " + np);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public UnitData[] GetUnits()
        {
            return _units.ToArray();
        }

        public UnitData GetUnit(string unitId)
        {
            foreach (UnitData unitData in _units)
            {
                if (unitData.ID == unitId)
                {
                    return unitData;
                }
            }

            return null;
        }

        public UnitData GetUnitAtIndex(int index)
        {
            return index < 0 || index >= _units.Count ? null : _units[index];
        }

        public void AddUnit(UnitData unit)
        {
            if (_units.Contains(unit))
            {
                return;
            }

            _units.Add(unit);
        }

        public void AddUnits(IEnumerable<UnitDataModel> unitRecords)
        {
            foreach (UnitDataModel record in unitRecords)
            {
                AddUnit(new UnitData(record));
            }
        }

        public void RemoveUnit(UnitData unit)
        {
            _units.Remove(unit);
        }

        public bool HasUnit(string unitId)
        {
            foreach (UnitData unitData in _units)
            {
                if (unitData.ID == unitId)
                {
                    return true;
                }
            }

            return false;
        }
    }

}
