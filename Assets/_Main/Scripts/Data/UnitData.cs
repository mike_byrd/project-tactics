using System;
using UnityEngine;

namespace ProjectTactics.DataModels
{
    public class StatsData
    {
        public float Level { get; set; }
        public float Exp { get; set; }
        public float HP { get; set; }
        public float MaxHP { get; set; }
        public float AP { get; set; }
        public float MaxAP { get; set; }
        public float MP { get; set; }
        public float MaxMP { get; set; }
        public float SP { get; set; }
        public float MaxSP { get; set; }
        public float Attack { get; set; }
        public float Defense { get; set; }
        public float MagicAttack { get; set; }
        public float MagicDefense { get; set; }
        public float Range { get; set; }
        public float JumpHeight { get; set; }
        public float CriticalHitRate { get; set; }
        public float Speed { get; set; }

        public StatsDataModel ToDataModel()
        {
            return new StatsDataModel
            {
                level = Level,
                exp = Exp,
                maxHp = MaxHP,
                maxAp = MaxAP,
                maxSp = MaxSP,
                attack = Attack,
                defense = Defense,
                magicAttack = MagicAttack,
                magicDefense = MagicDefense,
                range = Range,
                jumpHeight = JumpHeight,
                criticalHitRate = CriticalHitRate,
                speed = Speed
            };
        }
    }

    public class UnitData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public JobType Job { get; set; }
        public GenderType Gender { get; set; }
        public Alliances Alliances { get; set; }
        public int BaseModelId { get; set; }
        public int HairModelId { get; set; }
        public StatsData Stats { get; set; }

        public UnitData(UnitDataModel dataModel)
        {
            ID = dataModel.id;
            Name = dataModel.name;
            Job = (JobType) Enum.Parse(typeof(JobType), dataModel.job);
            Gender = (GenderType) Enum.Parse(typeof(GenderType), dataModel.gender);
            BaseModelId = dataModel.baseModelId;
            HairModelId = dataModel.hairModelId;

            Stats = new StatsData
            {
                Level = dataModel.stats.level,
                Exp = dataModel.stats.exp,
                HP = dataModel.stats.maxHp,
                MaxHP = dataModel.stats.maxHp,
                AP = dataModel.stats.maxAp,
                MaxAP = dataModel.stats.maxAp,
                MP = dataModel.stats.maxMp,
                MaxMP = dataModel.stats.maxMp,
                SP = dataModel.stats.maxSp,
                MaxSP = dataModel.stats.maxSp,
                Attack = dataModel.stats.attack,
                Defense = dataModel.stats.defense,
                MagicAttack = dataModel.stats.magicAttack,
                MagicDefense = dataModel.stats.magicDefense,
                JumpHeight = dataModel.stats.jumpHeight,
                CriticalHitRate = dataModel.stats.criticalHitRate,
                Speed = dataModel.stats.speed
            };

            switch (dataModel.alliance)
            {
                case "Ally":
                    Alliances = Alliances.Ally;
                    break;
                case "Enemy":
                    Alliances = Alliances.Enemy;
                    break;
                case "Neutral":
                    Alliances = Alliances.Neutral;
                    break;
                default:
                    Debug.LogWarning($"Invalid Alliance data: {dataModel.alliance}");
                    Alliances = Alliances.Neutral;
                    break;
            }
        }

        public UnitDataModel ToDataModel()
        {
            return new UnitDataModel
            {
                gender = Gender.ToString(),
                job = Job.ToString(),
                name = Name,
                baseModelId = BaseModelId,
                hairModelId = HairModelId,
                stats = Stats.ToDataModel(),
                alliance = Alliances.ToString()
            };
        }
    }
}