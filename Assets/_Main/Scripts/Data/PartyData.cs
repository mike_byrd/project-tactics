using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class PartyData
    {
        public string hostId;
        public List<PlayerHandle> members;
        public Dictionary<string, UnitData> units;

        public PartyData(PartyDataModel party)
        {
            Update(party);
        }

        public void Update(PartyDataModel party)
        {
            hostId = party.hostId;
            members = new List<PlayerHandle>(party.memberIds.Length);
            foreach (string memberId in party.memberIds)
            {
                members.Add(NetworkManager.CurrentWorld.GetPlayer(memberId));
            }

            units = new Dictionary<string, UnitData>(party.units.Length);
            foreach (UnitDataModel unitDataModel in party.units)
            {
                units[unitDataModel.id] = new UnitData(unitDataModel);
            }
        }

        public bool HasPlayer(string playerId)
        {
            return GetPlayer(playerId) != null;
        }

        public void AddPlayer(PlayerHandle player)
        {
            if (!HasPlayer(player.PlayerData.ID))
            {
                members.Add(player);
            }
        }

        public void RemovePlayer(PlayerHandle player)
        {
            List<string> toRemove = new List<string>();
            foreach (KeyValuePair<string,UnitData> kvp in units)
            {
                if (player.PlayerData.Inventory.HasUnit(kvp.Value.ID))
                {
                    toRemove.Add(kvp.Key);
                }
            }

            foreach (string key in toRemove)
            {
                units.Remove(key);
            }

            members.Remove(player);
        }

        public PlayerHandle GetPlayer(string playerId)
        {
            foreach (PlayerHandle member in members)
            {
                if (member.PlayerData.ID == playerId)
                {
                    return member;
                }
            }

            return null;
        }


        public bool HasUnit(string unitId)
        {
            return units.ContainsKey(unitId);
        }

        public void AddUnit(UnitData unitData)
        {
            if (!units.ContainsKey(unitData.ID))
            {
                units.Add(unitData.ID, unitData);
            }
        }

        public void RemoveUnit(UnitData unit)
        {
            units.Remove(unit.ID);
        }

        public PlayerHandle GetUnitOwner(string unitId)
        {
            foreach (PlayerHandle member in members)
            {
                if (member.PlayerData.Inventory.HasUnit(unitId))
                {
                    return member;
                }
            }

            return null;
        }
    }
}
