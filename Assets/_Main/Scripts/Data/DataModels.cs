using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;

namespace ProjectTactics.DataModels
{
    [Serializable]
    public class PlayerDataModel
    {
        public string id;
        public string username;
        public InventoryDataModel inventory;
        public Point position;
        public string leaderId;
        public string nameplateDecoration;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class InventoryDataModel
    {
        public UnitDataModel[] units;
        public Dictionary<object, object> cosmetics;

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class CosmeticsDataModel
    {
        public Dictionary<object, object> decorations;
    }

    [Serializable]
    public class UnitDataModel
    {
        public string id;
        public string name;
        public string job;
        public string gender;
        public string alliance;
        public int level;
        public int baseModelId;
        public int hairModelId;

        public StatsDataModel stats;

        public UnitDataModel()
        {
            stats = new StatsDataModel();
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }

    [Serializable]
    public class StatsDataModel
    {
        public float level;
        public float exp;
        public float maxHp;
        public float maxAp;
        public float maxMp;
        public float maxSp; // Special Points. Do not refresh. Intended for special moves. TODO: unused for now.
        public float attack;
        public float defense;
        public float magicAttack;
        public float magicDefense;
        public float range; // TODO: unused for now.
        public float jumpHeight;
        public float criticalHitRate;
        public float speed;
    }

    [Serializable]
    public class JobDataModel
    {
        public string job;
        public StatsDataModel baseStats;
        public StatsDataModel growthStats;

        // TODO: add ability catalogs.
    }

    [Serializable]
    public class LocationDataModel
    {
        public string id;
        public string name;
        public int width;
        public int height;
        public Point travelPosition;
        public TileDataModel[] tiles;
    }

    [Serializable]
    public class TileDataModel
    {
        public Point position;
        public bool isWalkable;
        public int height;
        public int type;
    }

    [Serializable]
    public class LoginUserResult
    {
        public string authToken;
        public PlayerDataModel player;
    }

    [Serializable]
    public class AddUnitResult
    {
        public UnitDataModel unitAdded;
        public UnitDataModel[] units;
    }

    [Serializable]
    public class UnitInventoryResult
    {
        public UnitDataModel[] units;
    }

    [Serializable]
    public class EnterWorldResult
    {
        public string id;
        public string name;
        public LocationDataModel locationData;
        public PlayerDataModel[] players;
        public PartyDataModel party;
    }

    [Serializable]
    public class GetWorldsResult
    {
        public string id;
        public string name;
        public int playerCount;
    }

    [Serializable]
    public class CreateBattleResult
    {
        public string battleId;
        public string hostId;
        public LocationDataModel locationData;
        public PartyDataModel playerParty;
        public UnitDataModel[] enemyUnits;
        public string[] turnOrder;
        public int currentTurn;
    }

    [Serializable]
    public class PartyDataModel
    {
        public string hostId;
        public string[] memberIds;
        public UnitDataModel[] units;
    }

    [Serializable]
    public class PartyInviteResult
    {
        public string sender;
        public long timestamp;
    }

    [Serializable]
    public class PartyUpdateResult
    {
        public PartyDataModel party;
    }


    [Serializable]
    public class PartyJoinResult
    {
        public string playerId;
    }

    [Serializable]
    public class PartyLeaveResult
    {
        public string playerId;
        public string hostId;
    }
}