using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using MCB.MapEditor;
using UnityEngine;

namespace ProjectTactics.DataModels
{
    public class LocationData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Point TravelPosition { get; set; }
        public TileData[] Tiles { get; set; }

        public LocationData(LocationDataModel dataModel)
        {
            ID = dataModel.id;
            Name = dataModel.name;
            Width = dataModel.width;
            Height = dataModel.height;
            TravelPosition = dataModel.travelPosition;
            Tiles = new TileData[dataModel.tiles.Length];

            for (int i = 0; i < Tiles.Length; i++)
            {
                Tiles[i] = new TileData(dataModel.tiles[i]);
            }
        }

//        public LocationData(string id, string name, LocationDataModel dataModel, PlayerDataModel[] players)
//        {
//            ID = id;
//            Name = name;
//            Width = dataModel.width;
//            Height = dataModel.height;
//            TravelPosition = dataModel.travelPosition;
//            Tiles = new TileData[dataModel.tiles.Length];
//
//            for (int i = 0; i < Tiles.Length; i++)
//            {
//                Tiles[i] = new TileData(dataModel.tiles[i]);
//            }
//
//            this.players = new Dictionary<string, PlayerHandle>(players.Length);
//            foreach (var playerDataModel in players)
//            {
//                var handle = new PlayerHandle
//                {
//                    PlayerData = new PlayerData(playerDataModel),
//                    Unit = null,
//                    Character = null
//                };
//
//                this.players.Add(playerDataModel.id, handle);
//            }
//        }

//        public void RemovePlayer(string playerId)
//        {
//            PlayerHandle player = players[playerId];
//            if (player == null)
//            {
//                DBG.Log($"RemovePlayer: Player {playerId} does not exist.");
//                return;
//            }
//
//            Object.Destroy(player.Unit.gameObject);
//            players.Remove(playerId);
//            DBG.Log($"Player {player.PlayerData.Username} has been removed from this world.");
//        }
    }

    public class TileData
    {
        public Point Position { get; set; }
        public int Height { get; set; }
        public bool IsWalkable { get; set; }
        public TileType Type { get; set; }

        public TileData(TileDataModel dataModel)
        {
            Position = dataModel.position;
            Height = dataModel.height;
            IsWalkable = dataModel.isWalkable;
            Type = (TileType)dataModel.type;
        }
    }
}
