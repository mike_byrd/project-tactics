﻿using UnityEngine;
using System.Collections;

public static class TransformDeepChildExtension
{
    public static Transform FindDeepChild(this Transform parent, string childName)
    {
        var result = parent.Find(childName);
        if (result != null)
            return result;
        foreach (Transform child in parent)
        {
            result = child.FindDeepChild(childName);
            if (result != null)
                return result;
        }

        return null;
    }

    public static Transform GetLastChild(this Transform parent)
    {
        return parent.GetChild(parent.childCount - 1);
    }
}