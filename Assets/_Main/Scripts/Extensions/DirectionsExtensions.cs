﻿using UnityEngine;
using System.Collections;
using MCB;
using MCB.Common;

public static class DirectionsExtensions
{
    public static Direction GetDirection (this Tile t1, Tile t2)
    {
        int yDist = t2.GridPosition.y - t1.GridPosition.y;
        int xDist = t2.GridPosition.x - t1.GridPosition.x;

        if (yDist > 0 && yDist == xDist)
        {
            return Direction.NorthEast;
        }

        if (yDist < 0 && yDist == xDist)
        {
            return Direction.SouthWest;
        }

        if (xDist < 0 && xDist == (yDist * -1))
        {
            return Direction.NorthWest;
        }

        if (xDist > 0 && xDist == (yDist * -1))
        {
            return Direction.SouthEast;
        }

        int yDistAbs = Mathf.Abs(t2.GridPosition.y - t1.GridPosition.y);
        int xDistAbs = Mathf.Abs(t2.GridPosition.x - t1.GridPosition.x);

        // If the target is farther along the y-axis than the x-axis, only check the y direction
        if(yDistAbs >= xDistAbs) {
            if(t1.GridPosition.y < t2.GridPosition.y) {
                return Direction.North;
            } else {
                return Direction.South;
            }
        } else {
            if(t1.GridPosition.x < t2.GridPosition.x) {
                return Direction.East;
            } else {
                return Direction.West;
            }
        }
    }

    public static Direction GetDirection(this Point p1, Point p2)
    {
        int yDist = Mathf.Abs(p1.y - p2.y);
        int xDist = Mathf.Abs(p1.x - p2.x);

        // If the target is farther along the y-axis than the x-axis, only check the y direction
        if (yDist >= xDist)
        {
            if (p1.y < p2.y)
            {
                return Direction.North;
            }
            else
            {
                return Direction.South;
            }
        }
        else
        {
            if (p1.x < p2.x)
            {
                return Direction.East;
            }
            else
            {
                return Direction.West;
            }
        }
    }

    public static Vector3 ToEuler(this Direction d)
    {
        return new Vector3(0, (int) d * 45, 0);
    }

    public static Direction GetDirection(this Point p)
    {
        if (p.y > 0)
            return Direction.North;
        if (p.x > 0)
            return Direction.East;
        if (p.y < 0)
            return Direction.South;
        return Direction.West;
    }

    public static Point GetNormal(this Direction dir)
    {
        switch (dir)
        {
            case Direction.North:
                return new Point(0, 1);
            case Direction.East:
                return new Point(1, 0);
            case Direction.South:
                return new Point(0, -1);
            default: // Directions.West:
                return new Point(-1, 0);
        }
    }
}