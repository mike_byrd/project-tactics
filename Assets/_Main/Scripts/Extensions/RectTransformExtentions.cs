﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.Extensions
{
    public enum AnchorType
    {
        TopLeft,
        TopCenter,
        TopRight,
        MidLeft,
        MidCenter,
        MidRight,
        BotLeft,
        BotCenter,
        BotRight,
        Custom
    }

    public static class RectTransformExtensions
    {
        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }

        public static void MoveToTarget(this RectTransform source, RectTransform target,
            AnchorType anchorType = AnchorType.TopLeft, float offsetX = 0, float offsetY = 0)
        {
            switch (anchorType)
            {
                case AnchorType.TopLeft:
                    Vector3 newPosition = target.position;
                    source.localPosition = newPosition;
                    break;
            }

//            if (target == null)
//            {
//                source.SetParent(null);
//                source.anchoredPosition = Vector2.zero;
//                return;
//            }
//
//            float sourceWidth = source.sizeDelta.x / 100;
//            float screenWidth = Screen.width / 100f;
//            source.pivot = new Vector2(.5f, .5f);
//
//            switch (anchorType)
//            {
//                case AnchorType.TopLeft:
//                    source.anchorMin = new Vector2(0, 1);
//                    source.anchorMax = new Vector2(0, 1);
//                    //source.pivot = new Vector2(0, 1);
//
//                    //source.anchoredPosition = new Vector2(target.rect.width + offsetX, offsetY);
//                    Vector3 newPosition = target.position;
//                    Vector2 size = target.sizeDelta;
//                    newPosition.x += size.x / 2;
//                    newPosition.y += size.y / 2;
//                    source.position = newPosition;
//
////                    if (source.transform.position.x + sourceWidth > screenWidth)
////                    {
////                        MoveToTarget(source, target, AnchorType.TopRight);
////                    }
//
//                    break;
//                case AnchorType.TopRight:
//                    source.anchorMin = new Vector2(1, 1);
//                    source.anchorMax = new Vector2(1, 1);
////                    source.pivot = new Vector2(1, 1);
//
//                    source.anchoredPosition = new Vector2(-(target.rect.width + offsetX), offsetY);
//                    break;
//                case AnchorType.MidCenter:
//                    source.anchorMin = new Vector2(0.5f, .5f);
//                    source.anchorMax = new Vector2(0.5f, .5f);
////                    source.pivot = new Vector2(0.5f, .5f);
//                    source.position = target.position;
//                    break;
//                case AnchorType.MidLeft:
//                    source.anchorMin = new Vector2(0f, .5f);
//                    source.anchorMax = new Vector2(0f, .5f);
////                    source.pivot = new Vector2(0f, .5f);
//
//                    source.position = target.position;
//                    source.anchoredPosition = new Vector2(target.rect.width + offsetX, offsetY);
//                    break;
//                case AnchorType.MidRight:
//                    source.anchorMin = new Vector2(1f, .5f);
//                    source.anchorMax = new Vector2(1f, .5f);
////                    source.pivot = new Vector2(0f, .5f);
//
//                    source.position = target.position;
//                    source.anchoredPosition = new Vector2(0, 0);
//                    break;
//                case AnchorType.BotLeft:
//                    source.anchorMin = new Vector2(0, 0);
//                    source.anchorMax = new Vector2(0, 0);
////                    source.pivot = new Vector2(0, 0);
//                    source.anchoredPosition = target.position;
//                    break;
//                case AnchorType.BotRight:
//                    source.anchorMin = new Vector2(1, 0);
//                    source.anchorMax = new Vector2(1, 0);
////                    source.pivot = new Vector2(1, 0);
//                    source.position = new Vector3(target.position.x + offsetX, target.position.y + offsetY);
//                    break;
//                case AnchorType.BotCenter:
//                    source.anchorMin = new Vector2(0, 0);
//                    source.anchorMax = new Vector2(0.5f, 0.5f);
////                    source.pivot = new Vector2(.5f, 0);
//                    source.position = new Vector3(target.position.x + offsetX, target.position.y + offsetY);
//                    break;
//            }
        }
    }
}