﻿using UnityEngine;
using System.Collections;
using MCB;

public static class FacingsExtensions
{
	public static Facings GetFacing (this Unit attacker, Unit target)
	{
		Vector2 targetDirection = target.Facing.GetNormal();
		Vector2 approachDirection = ((Vector2)(target.CurrentTile.GridPosition - attacker.CurrentTile.GridPosition)).normalized;
		float dot = Vector2.Dot( approachDirection, targetDirection );
		if (dot >= 0.45f)
			return Facings.Back;
		if (dot <= -0.45f)
			return Facings.Front;
		return Facings.Side;
	}
}
