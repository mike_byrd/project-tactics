﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class BoardCreator : MonoBehaviour
{
    #region Fields / Properties

    [SerializeField] GameObject tileViewPrefab;
    [SerializeField] GameObject tileSelectionIndicatorPrefab;
    [SerializeField] int width = 10;
    [SerializeField] int depth = 10;

    [SerializeField] int height = 8;

    //[SerializeField] Vector2 growthRate;
    [SerializeField] Rect currentRect;
    [SerializeField] Point markerPosition;
    [SerializeField] LevelData levelData;
    [SerializeField] string LevelName = "New Level";
    [SerializeField] List<Point> playerStartingTiles;
    [SerializeField] List<Point> enemyStartingTiles;
    Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();

    Transform marker
    {
        get
        {
            if (_marker == null)
            {
                GameObject instance = Instantiate(tileSelectionIndicatorPrefab) as GameObject;
                _marker = instance.transform;
            }

            return _marker;
        }
    }

    Transform _marker;

    #endregion

    private void Awake()
    {
        currentRect = new Rect();
    }

    #region Public

    public void Grow()
    {
        GrowSingle(markerPosition);
    }

    public void Shrink()
    {
        ShrinkSingle(markerPosition);
    }

    public void GrowArea()
    {
        currentRect = new Rect(0, 0, currentRect.width, currentRect.height);
        GrowRect(currentRect);
    }

    public void GrowRandomArea()
    {
        Rect r = RandomRect();
        GrowRect(r);
    }

    public void ShrinkArea()
    {
        currentRect = new Rect(0, 0, currentRect.width, currentRect.height);
        ShrinkRect(currentRect);
    }

    public void ShrinkRandomArea()
    {
        Rect r = RandomRect();
        ShrinkRect(r);
    }

    public void UpdateMarker()
    {
        Tile t = tiles.ContainsKey(markerPosition) ? tiles[markerPosition] : null;
        marker.localPosition = t != null ? t.Center : new Vector3(markerPosition.x, 0, markerPosition.y);
    }

    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        tiles.Clear();
    }

    public void Save()
    {
#if UNITY_EDITOR
        string filePath = Application.dataPath + "/Resources/Levels";
        if (!Directory.Exists(filePath))
        {
            CreateSaveDirectory();
        }

        Tile[] currentTiles = transform.GetComponentsInChildren<Tile>();
        LevelData board = ScriptableObject.CreateInstance<LevelData>();
        board.tiles = new List<Vector3>(currentTiles.Length);
        foreach (Tile t in currentTiles)
        {
            board.tiles.Add(new Vector3(t.GridPosition.x, t.Height, t.GridPosition.y));
        }

        board.playerStartingTiles = new List<Point>(playerStartingTiles);
        board.enemyStartingTiles = new List<Point>(enemyStartingTiles);

        // Don't allow emty level names.
        LevelName = (LevelName == string.Empty) ? name : LevelName;
        string fileName = string.Format("Assets/Resources/Levels/{1}.asset", filePath, LevelName);
        AssetDatabase.CreateAsset(board, fileName);

        levelData = AssetDatabase.LoadAssetAtPath<LevelData>(fileName);
#endif
    }

    void CreateSaveDirectory()
    {
#if UNITY_EDITOR
        string filePath = Application.dataPath + "/Resources";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");
        filePath += "/Levels";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Levels");
        AssetDatabase.Refresh();
#endif
    }

    public void Load()
    {
        Clear();
        if (levelData == null)
            return;

        foreach (Vector3 v in levelData.tiles)
        {
            Tile t = Create();
            t.Load(v);
            tiles.Add(t.GridPosition, t);
        }
    }

    #endregion

    #region Private

    Rect RandomRect()
    {
        int x = UnityEngine.Random.Range(0, width);
        int y = UnityEngine.Random.Range(0, depth);
        int w = UnityEngine.Random.Range(1, width - x + 1);
        int h = UnityEngine.Random.Range(1, depth - y + 1);
        return new Rect(x, y, w, h);
    }

    void GrowRect(Rect rect)
    {
        for (int y = (int) rect.yMin; y < (int) rect.yMax; ++y)
        {
            for (int x = (int) rect.xMin; x < (int) rect.xMax; ++x)
            {
                Point p = new Point(x, y);
                GrowSingle(p);
            }
        }
    }

    void ShrinkRect(Rect rect)
    {
        for (int y = (int) rect.yMin; y < (int) rect.yMax; ++y)
        {
            for (int x = (int) rect.xMin; x < (int) rect.xMax; ++x)
            {
                Point p = new Point(x, y);
                ShrinkSingle(p);
            }
        }
    }

    Tile Create()
    {
        GameObject instance = Instantiate(tileViewPrefab) as GameObject;
        instance.transform.parent = transform;
        return instance.GetComponent<Tile>();
    }

    Tile GetOrCreate(Point p)
    {
        if (tiles.ContainsKey(p))
            return tiles[p];

        Tile t = Create();
        t.Load(p, 0);
        tiles.Add(p, t);

        return t;
    }

    void GrowSingle(Point p)
    {
        Tile t = GetOrCreate(p);
        if (t.Height < height)
        {
            t.Grow();
        }
    }

    void ShrinkSingle(Point p)
    {
        if (!tiles.ContainsKey(p))
            return;

        Tile t = tiles[p];
        t.Shrink();

        if (t.Height <= 0)
        {
            tiles.Remove(p);
            DestroyImmediate(t.gameObject);
        }
    }

    #endregion
}