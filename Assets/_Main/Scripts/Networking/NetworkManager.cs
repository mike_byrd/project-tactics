using System;
using System.Collections;
using System.Collections.Generic;
using BestHTTP.SocketIO3;
using BestHTTP.SocketIO3.Events;
using MCB.Common;
using Newtonsoft.Json;
using ProjectTactics;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MCB.Networking
{
    public class NetworkManager : MonoBehaviour
    {
        // Instance
        private static NetworkManager _i;

        public static event Action<bool> ConnectionStatusChanged;
        public static event Action<PlayerHandle> PlayerJoined;
        public static event Action<PlayerHandle> PlayerLeft;
        public static event Action<PlayerHandle, Point> PlayerMoved;
        public static event Action<CreateBattleResult> BattleRequestComplete;
        public static event Action<List<Mail>> MailReceived;

        public static event Action<PartyData> PartyUpdated;

        public const int MaxPlayers = 6;
        public const int MaxPartySize = 6;

        public static bool IsConnected { get; private set; }

        private World _currentWorld;

        public static World CurrentWorld
        {
            get => _i._currentWorld;
            private set => _i._currentWorld = value;
        }

        private PlayerHandle _localPlayer;

        public static PlayerHandle LocalPlayer
        {
            get => _i._localPlayer;
            set => _i._localPlayer = value;
        }

        private PartyData _party;

        public static PartyData Party
        {
            get => _i._party;
            set => _i._party = value;
        }

        private SocketManager _sm;
        private string _sessionToken;

        private List<Mail> _partyInvites;

        private void Awake()
        {
            if (!_i)
            {
                _i = this;
            }
            else if (_i != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            ResetDefaults();
        }

        private static void ResetDefaults()
        {
            _i._partyInvites = new List<Mail>();
        }

        public static void ConnectToServer(string uri)
        {
            if (_i._sm != null)
            {
                _i._sm.Socket.Off();
                _i._sm.Close();
            }

            _i._sm = new SocketManager(new Uri(uri), new SocketOptions());

            DBG.Log($"Connecting to server at: {uri}...");

            _i._sm.Socket.On<Error>(SocketIOEventTypes.Error, OnError);
            _i._sm.Socket.On<ConnectResponse>(SocketIOEventTypes.Connect, OnConnectToServer);
            _i._sm.Socket.On(SocketIOEventTypes.Disconnect, OnDisconnectFromServer);

            _i._sm.Socket.On<string>("message", OnMessage);
            _i._sm.Socket.On<string>("leaveWorld", OnLeaveWorld);

            _i._sm.Socket.On<PlayerDataModel>("world:joined", OnPlayerJoined);
            _i._sm.Socket.On<string, string>("world:playerLeft", OnPlayerLeftWorld);
            _i._sm.Socket.On("loggedOut", DisconnectFromServer);

            _i._sm.Socket.On<string, Point>("player:worldMove", OnWorldMove);
            _i._sm.Socket.On<CreateBattleResult>("player:battleCreated", OnBattleCreated);

            // Party
            _i._sm.Socket.On<PartyInviteResult>("party:inviteReceived", OnPartyInviteReceived);
            _i._sm.Socket.On<string>("party:inviteDeclined", OnPartyInviteDeclined);
            _i._sm.Socket.On<PartyUpdateResult>("party:updated", OnPartyUpdated);
            _i._sm.Socket.On<PartyJoinResult>("party:playerJoined", OnPlayerJoinedParty);
            _i._sm.Socket.On<string>("party:playerLeft", OnPlayerLeftParty);

            // Ex: Receiving Json
            // _sm.Socket.On<Dictionary<string, object>>("message", OnJsonReceived);
        }

        public static void DisconnectFromServer()
        {
            if (_i._sm == null)
            {
                return;
            }

            _i._sm.Socket.Disconnect();
        }

        #region Player Routes

        public static void RegisterWithUsername(string username, string password, Action<LoginUserResult> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement<LoginUserResult>(result =>
            {
                _i._sessionToken = result.authToken;
                callback?.Invoke(result);
            }).Emit("player:registerWithUsername", username, password);
        }

        public static void LoginWithUsername(string username, string password, Action<LoginUserResult> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement<LoginUserResult>(result =>
            {
                _i._sessionToken = result.authToken;
                callback?.Invoke(result);
            }).Emit("player:loginWithUsername", username, password);
        }

        public void RegisterWithEmail(string username, string email, string password, Action<LoginUserResult> callback)
        {
            _sm.Socket.ExpectAcknowledgement<LoginUserResult>(result =>
            {
                _sessionToken = result.authToken;
                callback?.Invoke(result);
            }).Emit("player:registerWithEmail", username, email, password);
        }

        public void LoginWithEmail(string email, string password, Action<LoginUserResult> callback)
        {
            _sm.Socket.ExpectAcknowledgement<LoginUserResult>(result =>
            {
                _sessionToken = result.authToken;
                callback?.Invoke(result);
            }).Emit("player:loginWithEmail", email, password);
        }

        public static void Logout()
        {
            _i._sm.Socket.Emit("player:logout", _i._sessionToken);
        }

        public static void CreateStartingUnit(UnitDataModel unitData, Action callback)
        {
            _i._sm.Socket.ExpectAcknowledgement(callback).Emit("player:createStartingUnit", unitData, _i._sessionToken);
        }

        public static void GetUnitInventory(Action<UnitData[]> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement<UnitInventoryResult>(result =>
            {
                LocalPlayer.PlayerData.Inventory.UpdateUnits(result.units);
                callback?.Invoke(LocalPlayer.PlayerData.Inventory.GetUnits());
            }).Emit("player:getUnits", _i._sessionToken);
        }

        #endregion

        #region World Routes

        public static void CreateWorld(string roomName, string selectedUnitId, Action<EnterWorldResult> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement((EnterWorldResult result) =>
            {
                OnCreateWorld(result);
                callback(result);
            }).Emit("room:createWorld", roomName, selectedUnitId, _i._sessionToken);
        }

        public static void JoinWorld(string worldID, string selectedUnitId, Action<EnterWorldResult> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement((EnterWorldResult result) =>
            {
                OnJoinWorld(result);
                callback(result);
            }).Emit("room:joinWorld", worldID, selectedUnitId, _i._sessionToken);
        }

        public static void LeaveWorld()
        {
            //_i._sm.Socket.Emit("world:leave", LocalPlayer.PlayerID, _i._sessionToken);
        }

        public static void SendPartyInvite(string playerId)
        {
            _i._sm.Socket.Emit("player:sendPartyInvite", playerId, _i._sessionToken);
        }

        public static void RemovePlayerFromParty(string playerId)
        {
            _i._sm.Socket.Emit("player:removePlayerFromParty", playerId, _i._sessionToken);
        }

        public static void RespondToPartyInvite(string senderId, bool response)
        {
            _i._sm.Socket.Emit("player:respondToPartyInvite",
                LocalPlayer.PlayerData.ID, senderId, response, _i._sessionToken);
            Mail mail = GetMail(senderId);
            _i._partyInvites.Remove(mail);
            MailReceived?.Invoke(_i._partyInvites);
        }

        private static void OnPartyInviteDeclined(string recipientId)
        {
            PlayerHandle recipient = CurrentWorld.GetPlayer(recipientId);
            if (recipient != null)
            {
                DBG.Log($"{recipient.Unit.DisplayName} declined your invitation.");
            }
        }

        public static void AddUnitToParty(string unitId)
        {
            _i._sm.Socket.Emit("party:addUnit", unitId, _i._sessionToken);
        }

        public static void RemoveUnitFromParty(string unitId)
        {
            _i._sm.Socket.Emit("party:removeUnit", unitId, _i._sessionToken);
        }

        private static void OnPartyUpdated(PartyUpdateResult result)
        {
            Party.Update(result.party);
            PartyUpdated?.Invoke(_i._party);
        }

        private static void OnPlayerJoinedParty(PartyJoinResult result)
        {
            PlayerHandle player = CurrentWorld.GetPlayer(result.playerId);
            Party.AddPlayer(player);
        }

        private static void OnPlayerLeftParty(string playerId)
        {
            Party.RemovePlayer(CurrentWorld.GetPlayer(playerId));
            PartyUpdated?.Invoke(Party);
        }

        private static void OnPartyInviteReceived(PartyInviteResult result)
        {
            PlayerHandle sender = CurrentWorld.GetPlayer(result.sender);
            DBG.Log($"{sender.PlayerData.Username} invited you to party!");

            if (_i._partyInvites.Find(m => m.senderId == result.sender) != null)
            {
                return;
            }

            _i._partyInvites.Add(new Mail
            {
                body = "",
                senderId = result.sender,
                senderName = sender.PlayerData.Username,
                type = MailType.PartyInvite,
                timestamp = DateTimeOffset.FromUnixTimeMilliseconds(result.timestamp).LocalDateTime
            });

            MailReceived?.Invoke(_i._partyInvites);
        }

        private static void OnCreateWorld(EnterWorldResult result)
        {
            Debug.Log("Creating new world");
            CurrentWorld = new World(result);
        }

        private static void OnJoinWorld(EnterWorldResult result)
        {
            Debug.Log("Joining new world");
            CurrentWorld = new World(result);
        }

        public static void GetWorlds(Action<GetWorldsResult[]> callback)
        {
            _i._sm.Socket.ExpectAcknowledgement(callback).Emit("room:getWorlds");
        }

        public static void RequestMove(string playerID, Point position)
        {
            _i._sm.Socket.Emit("player:requestMove", playerID, position, _i._sessionToken);
        }

        public static void RequestBattle(string playerID, string destination)
        {
            _i._sm.Socket.Emit("player:requestBattle", playerID, destination, _i._sessionToken);
        }

        #endregion

        #region Admin Handlers

        private static void OnError(Error error)
        {
            DBG.LogError($"Connection error: {error.message}");
        }

        private static void OnConnectToServer(ConnectResponse response)
        {
            DBG.Log($"Connected successfully: {response.sid}");

            IsConnected = true;
            ConnectionStatusChanged?.Invoke(true);
        }

        private static void OnDisconnectFromServer()
        {
            DBG.Log("Server has disconnected");
            ConnectionStatusChanged?.Invoke(false);

            // Destroy the server so things get cleaned up on the next login scene
            Destroy(_i.gameObject);

            IsConnected = false;
            SceneManager.LoadScene("Login Scene");
        }

        private static void OnMessage(string message)
        {
            DBG.Log(message);
        }

        #endregion

        #region Player Handlers

        #endregion

        #region World Handlers

        private static void OnPlayerJoined(PlayerDataModel playerDataModel)
        {
            PlayerHandle player = CurrentWorld.AddPlayer(playerDataModel);
            DBG.Log($"{player.PlayerData.Username} has Joined {CurrentWorld.Name}!");

            PlayerJoined?.Invoke(player);
        }

        private static void OnPlayerLeftWorld(string playerId, string worldId)
        {
            PlayerHandle player = CurrentWorld.RemovePlayer(playerId);
            DBG.Log($"{player.PlayerData.Username} has left {CurrentWorld.Name}.");

            PlayerLeft?.Invoke(player);
        }

        private static void OnWorldMove(string playerID, Point position)
        {
            PlayerHandle player = CurrentWorld.GetPlayer(playerID);
            PlayerMoved?.Invoke(player, position);
        }

        private static void OnBattleCreated(CreateBattleResult result)
        {
            PlayerHandle player = CurrentWorld.GetPlayer(result.hostId);
            Debug.Log($"{player.PlayerData.Username} has started a battle.");
            BattleRequestComplete?.Invoke(result);
        }

        public static void OnLeaveWorld(string worldID)
        {
            DBG.Log($"Leaving world: {worldID}");
        }

        #endregion

        private void OnDestroy()
        {
            if (_sm == null) return;

            _sm.Close();
            _sm = null;
        }

        public static void LeaveBattle()
        {
            _i._sm.Socket.ExpectAcknowledgement<EnterWorldResult>(OnLeftBattle).Emit("player:leaveBattle");
        }

        private static void OnLeftBattle(EnterWorldResult result)
        {
            Debug.Log("I Left Battle");
//            GM.TownCenterLocationData = new LocationData(result.id, result.name, result.locationData, result.players);
            SceneManager.LoadScene("Game World Scene");
        }

        public static List<Mail> GetMail()
        {
            return _i._partyInvites;
        }

        public static Mail GetMail(string senderId)
        {
            return _i._partyInvites.Find(m => m.senderId == senderId);
        }

        public static bool CanInviteToParty(string playerId)
        {
            if (Party == null || LocalPlayer == null)
            {
                return false;
            }

            return LocalPlayer.PlayerData.ID != playerId &&
                   Party.hostId == LocalPlayer.PlayerData.ID &&
                   Party.units.Count < MaxPartySize &&
                   !Party.HasPlayer(playerId);
        }

        public static bool CanRemoveFromParty(string playerId)
        {
            if (Party == null || LocalPlayer == null)
            {
                return false;
            }

            bool isMe = LocalPlayer.PlayerData.ID == playerId;
            if (isMe && Party.units.Count == 1)
            {
                // This is a solo party so the player isn't allowed to leave it.
                return false;
            }

            return isMe || Party.hostId == LocalPlayer.PlayerData.ID && Party.HasPlayer(playerId);
        }
    }
}