using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class World
    {
        private readonly Dictionary<string, PlayerHandle> _players;
        private readonly Dictionary<string, LocationData> _locations;

        public string ID { get; private set; }
        public string Name { get; private set; }

        public World(EnterWorldResult result)
        {
            ID = result.id;
            Name = result.name;

            _players = new Dictionary<string, PlayerHandle>();
            foreach (var playerDataModel in result.players)
            {
                var handle = new PlayerHandle
                {
                    PlayerData = new PlayerData(playerDataModel),
                    Unit = null,
                    Character = null
                };

                AddPlayer(handle);
            }

            _locations = new Dictionary<string, LocationData>();
            _locations.Add(result.locationData.id, new LocationData(result.locationData));
        }

        public PlayerHandle AddPlayer(PlayerDataModel playerModel)
        {
            if (playerModel == null)
            {
                DBG.LogError("Trying to add null player.");
                return null;
            }

            var handle = new PlayerHandle
            {
                PlayerData = new PlayerData(playerModel),
                Unit = null,
                Character = null
            };

            AddPlayer(handle);
            return handle;
        }

        public void AddPlayer(PlayerHandle player)
        {
            if (player == null)
            {
                DBG.LogError("Trying to add null player.");
                return;
            }

            if (_players.ContainsKey(player.PlayerData.ID))
            {
                DBG.LogWarning("Player already exists.");
                return;
            }

            _players.Add(player.PlayerData.ID, player);
        }

        public PlayerHandle RemovePlayer(string playerID)
        {
            if (_players.TryGetValue(playerID, out PlayerHandle player))
            {
                _players.Remove(playerID);
                return player;
            }

            DBG.LogError("Trying to remove null player.");
            return null;
        }

        public PlayerHandle GetPlayer(string playerId)
        {
            if (_players.TryGetValue(playerId, out PlayerHandle player))
            {
                return player;
            }

            DBG.LogError($"Player does not exist.");
            return null;
        }

        public PlayerHandle[] GetPlayers()
        {
            PlayerHandle[] players = new PlayerHandle[_players.Count];
            int count = 0;
            foreach (KeyValuePair<string,PlayerHandle> kvp in _players)
            {
                players[count] = kvp.Value;
                count++;
            }

            return players;
        }

        public LocationData GetLocation(string locationId)
        {
            if (_locations.TryGetValue(locationId, out LocationData location))
            {
                return location;
            }

            DBG.LogError($"Location does not exist.");
            return null;
        }
    }
}
