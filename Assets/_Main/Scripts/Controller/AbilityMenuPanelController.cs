﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.EventSystems;
using MCB;

public class AbilityMenuPanelController : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] Button endButton;
    AbilityMenuEntry[] abilityEntries;

    private void Awake()
    {
        abilityEntries = panel.GetComponentsInChildren<AbilityMenuEntry>();
        InitializeAbilityButtonListeners();
    }

    public void SetupForTownCenter()
    {
        SetEndButtonVisible(false);
    }

    private void InitializeAbilityButtonListeners()
    {
        AbilityMenuEntry entry;
        Button abilityButton;
        for (int i = 0; i < abilityEntries.Length; i++)
        {
            entry = abilityEntries[i];
            abilityButton = abilityEntries[i].GetComponent<Button>();
            AddAbilityListeners(abilityButton);

            entry.entryIndex = i;
        }
    }

    void AddAbilityListeners(Button abilityButton)
    {
        abilityButton.onClick.AddListener(delegate { OnAbilityClicked(abilityButton); });

        EventTrigger trigger = abilityButton.GetComponent<EventTrigger>();
        EventTrigger.Entry eventEntry = new EventTrigger.Entry();
        eventEntry.eventID = EventTriggerType.PointerEnter;
        eventEntry.callback.AddListener(delegate { OnAbilityMouseEnter(abilityButton); });
        trigger.triggers.Add(eventEntry);

        eventEntry = new EventTrigger.Entry();
        eventEntry.eventID = EventTriggerType.PointerExit;
        eventEntry.callback.AddListener(delegate { OnAbilityMouseExit(abilityButton); });
        trigger.triggers.Add(eventEntry);
    }

    public void UpdateUI(AbilityCatalog catalog)
    {
        for (int i = 0; i < abilityEntries.Length; i++)
        {
            abilityEntries[i].Reset();
        }

        Ability basicAttack = catalog.transform.parent.GetComponentInChildren<Ability>();
        abilityEntries[0].AssignAbility(basicAttack);
        abilityEntries[1].AssignAbility(basicAttack);

        GameObject container;
        int abilityCap = 1; //NOTE: Start this at 1 since basic attack takes up the first 1 slots.
        for (int categoryIndex = 0; categoryIndex < catalog.CategoryCount(); categoryIndex++)
        {
            container = catalog.GetCategory(categoryIndex);
            Ability ability;
            int abilityCount = catalog.AbilityCount(container);
            for (int abilityIndex = 0; abilityIndex < abilityCount; abilityIndex++)
            {
                if (abilityCap > abilityEntries.Length - 1)
                {
                    break;
                }

                ability = catalog.GetAbility(categoryIndex, abilityIndex);
                //AbilityMagicCost cost = ability.GetComponent<AbilityMagicCost>();
                abilityEntries[abilityCap].AssignAbility(ability);

                abilityCap++;
            }
        }

        SetVisible(true);
    }

    public void UpdateLockedAbilities()
    {
        AbilityMenuEntry entry;
        for (int i = 0; i < abilityEntries.Length; i++)
        {
            entry = abilityEntries[i];
            entry.UpdateLockedState();
        }
    }

    public void Hide()
    {
        SetVisible(false);
    }

    public Ability GetAbilityAt(int index)
    {
        if (index < 0 || index >= abilityEntries.Length)
        {
            return null;
        }

        return abilityEntries[index].ability;
    }

    void SetVisible(bool visible)
    {
        RectTransform rectTransform = panel.GetComponent<RectTransform>();
        float endPosition;
        if (visible)
        {
            endPosition = 0;
        }
        else
        {
            endPosition = rectTransform.sizeDelta.x;
        }

        rectTransform.DOAnchorPosX(endPosition, .5f, true);
    }

    public void EndTurnClicked()
    {
        this.PostNotification(AbilityMenuEntry.ON_END_TURN);
    }

    public void SetEndButtonVisible(bool visible)
    {
        endButton.gameObject.SetActive(visible);
        InfoCardController.instance.HideCard();
    }

    public void OnEndButtonEnter(RectTransform endTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(endTransform, "End Turn <b>(space)</b>");
    }

    public void OnEndButtonExit(RectTransform endTransform)
    {
        InfoCardController.instance.HideCard();
    }

    public void OnAbilityClicked(Button abilityButton)
    {
        AbilityMenuEntry abilityEntry = abilityButton.GetComponent<AbilityMenuEntry>();
        this.PostNotification(AbilityMenuEntry.ON_SELECT, abilityEntry);
    }

    public void OnAbilityMouseEnter(Button abilityButton)
    {
        AbilityMenuEntry abilityEntry = abilityButton.GetComponent<AbilityMenuEntry>();
        this.PostNotification(AbilityMenuEntry.ON_HOVER, abilityEntry);

        if (abilityEntry.ability)
        {
            InfoCardController.instance.ShowDetailedInfo(abilityEntry);
        }
    }

    public void OnAbilityMouseExit(Button abilityButton)
    {
        AbilityMenuEntry abilityEntry = abilityButton.GetComponent<AbilityMenuEntry>();
        this.PostNotification(AbilityMenuEntry.ON_EXIT, abilityEntry);

        InfoCardController.instance.HideCard();
    }
}