﻿using System.Collections;
using System.Collections.Generic;
using MCB.Extensions;
using UnityEngine;
using UnityEngine.UI;

public class InfoCardController : MonoBehaviour
{
    [SerializeField] private Canvas canvas;
    [SerializeField] SimpleInfoCard simpleInfoCard;
    [SerializeField] AbilityInfoCard abilityInfoCard;

    [SerializeField] private float simpleOffset = 5;
    [SerializeField] private float detailedOffset = 10;

    public static InfoCardController instance { get; private set; }

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(instance.gameObject);
        }
    }

    public void ShowSimpleInfo(RectTransform target, string info)
    {
        simpleInfoCard.SetVisible(true);
        simpleInfoCard.UpdateInfo(info);

        // This solved the issue where the size delta would be incorrect after hovering between different targets.
        LayoutRebuilder.ForceRebuildLayoutImmediate(abilityInfoCard.rectTransform);

        RectTransform targetRectTransform = target.GetComponent<RectTransform>();
        RectTransform parentRect = canvas.GetComponent<RectTransform>();

        float canvasScale = parentRect.localScale.x;
        float halfHeight = targetRectTransform.sizeDelta.y / 2;
        Vector3 menuPosition = targetRectTransform.position;
        menuPosition.y += (halfHeight + simpleOffset) * canvasScale;
        menuPosition /= canvasScale;
        float rightDifference = parentRect.sizeDelta.x - (menuPosition.x + simpleInfoCard.rectTransform.sizeDelta.x);
        if (rightDifference < 0)
        {
            menuPosition.x += rightDifference;
            Debug.Log("Menu off right: " + rightDifference);
        }

        simpleInfoCard.rectTransform.anchoredPosition = menuPosition;
    }

    public void ShowDetailedInfo(AbilityMenuEntry target)
    {
        abilityInfoCard.SetVisible(true);
        abilityInfoCard.UpdateInfo(target);

        // This solved the issue where the size delta would be incorrect after hovering between different targets.
        LayoutRebuilder.ForceRebuildLayoutImmediate(abilityInfoCard.rectTransform);

        RectTransform targetRectTransform = target.GetComponent<RectTransform>();
        RectTransform parentRect = canvas.GetComponent<RectTransform>();

        float canvasScale = parentRect.localScale.x;
        float halfWidth = targetRectTransform.sizeDelta.x / 2;
        float halfHeight = targetRectTransform.sizeDelta.y / 2;
        Vector3 menuPosition = targetRectTransform.position;
        menuPosition.x -= halfWidth * canvasScale;
        menuPosition.y += (halfHeight + detailedOffset) * canvasScale;
        menuPosition /= canvasScale;
        float rightDifference = parentRect.sizeDelta.x - (menuPosition.x + abilityInfoCard.rectTransform.sizeDelta.x);
        if (rightDifference < 0)
        {
            menuPosition.x += rightDifference;
        }

        abilityInfoCard.rectTransform.anchoredPosition = menuPosition;
    }

    public void HideCard()
    {
        simpleInfoCard.SetVisible(false);
        abilityInfoCard.SetVisible(false);
    }
}