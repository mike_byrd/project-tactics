using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class RenderTextureManager : MonoBehaviour
    {
        private static RenderTextureManager _i;
        public List<RenderTextureTarget> renderTextureTargets;

        private void Awake()
        {
            if (!_i)
            {
                _i = this;
            } else if (_i != this)
            {
                Destroy(_i.gameObject);
            }
        }

        public static Unit LoadForUnit(Unit unit, out RenderTextureTarget displayTextureTarget)
        {
            Unit clone = LoadForUnit(unit, 0);
            displayTextureTarget = _i.renderTextureTargets[0];
            return clone;
        }

        public static Unit LoadForUnit(UnitData unitData, out RenderTextureTarget displayTextureTarget)
        {
            Unit clone = LoadForUnit(unitData, 0);
            displayTextureTarget = _i.renderTextureTargets[0];
            return clone;
        }

        public static Unit LoadForUnit(UnitData unitData, int renderTextureIndex)
        {
            RenderTextureTarget rtt = _i.renderTextureTargets[renderTextureIndex];
            Unit clone = rtt.LoadForUnit(unitData);
            return clone;
        }

        public static Unit LoadForUnit(Unit unit, int renderTextureIndex)
        {
            RenderTextureTarget rtt = _i.renderTextureTargets[renderTextureIndex];
            Unit clone = rtt.LoadForUnit(unit);
            return clone;
        }

        public static List<RenderTextureTarget> GetRenderTextureTargets()
        {
            return _i.renderTextureTargets;
        }

        public static RenderTextureTarget GetRenderTextureTarget(int index)
        {
            if (!_i || index < 0 || index >= _i.renderTextureTargets.Count)
            {
                return null;
            }

            return _i.renderTextureTargets[index];
        }

        public static void UnloadAll()
        {
            foreach (RenderTextureTarget renderTextureTarget in _i.renderTextureTargets)
            {
                renderTextureTarget.Unload();
            }
        }
    }
}
