﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public class TurnOrderController : MonoBehaviour
{
  #region Constants
  const int turnActivation = 0;
  const int turnCost = 0; //500
  const int moveCost = 0; //300
  const int actionCost = 0; //200
  #endregion

  #region Notifications
  public const string RoundBeganNotification = "TurnOrderController.roundBegan";
  public const string TurnCheckNotification = "TurnOrderController.turnCheck";
  public const string TurnBeganNotification = "TurnOrderController.TurnBeganNotification";
  public const string TurnCompletedNotification = "TurnOrderController.turnCompleted";
  public const string RoundEndedNotification = "TurnOrderController.roundEnded";
  #endregion

  #region Public
  public IEnumerator Round()
  {
    BattleScene bc = GetComponent<BattleScene>(); ;

    int counter = 0;

    while (true) {
      counter++;
      if(counter >= 100) {
        Debug.Log("Max reached");
        break;
      }
      this.PostNotification(RoundBeganNotification);

      List<Unit> units = new List<Unit>(bc.units);
      List<Unit> allyUnits = bc.GetTeam(Alliances.Ally);
      List<Unit> enemyUnits = bc.GetTeam(Alliances.Enemy);
      List<Unit> sortedUnits = SortUnits(allyUnits, enemyUnits);

      for (int i = sortedUnits.Count - 1; i >= 0; --i) {
        if (CanTakeTurn(sortedUnits[i])) {
          bc.currentTurn.Change(sortedUnits[i]);
          sortedUnits[i].PostNotification(TurnBeganNotification);

          yield return sortedUnits[i];

          sortedUnits[i].PostNotification(TurnCompletedNotification);
        }
      }

      this.PostNotification(RoundEndedNotification);
    }
  }
  #endregion

  #region Private
  bool CanTakeTurn(Unit target)
  {
    BaseException exc = new BaseException(GetInitiative(target) >= turnActivation);
    target.PostNotification(TurnCheckNotification, exc);
    return exc.toggle;
  }

  List<Unit> SortUnits(List<Unit> ally, List<Unit> enemy)
  {
    int totalUnits = ally.Count + enemy.Count;

    ally.Sort((a, b) => GetInitiative(a).CompareTo(GetInitiative(b)));
    int allyInitiative = GetAverageInitiative(ally);

    enemy.Sort((a, b) => GetInitiative(a).CompareTo(GetInitiative(b)));
    int enemyInitiative = GetAverageInitiative(enemy);

    int largestTeamSize = Mathf.Max(ally.Count, enemy.Count);

    List<Unit> sortedUnits = new List<Unit>(totalUnits);
    if(allyInitiative >= enemyInitiative) {      
      for(int i = 0; i < largestTeamSize; i++) {
        if(i < ally.Count) {
          sortedUnits.Add(ally[i]);
        }

        if(i < enemy.Count) {
          sortedUnits.Add(enemy[i]);
        }
      }
    } else {
      for(int i = 0; i < largestTeamSize; i++) {
        if(i < enemy.Count) {
          sortedUnits.Add(enemy[i]);
        }
        if(i < ally.Count) {
          sortedUnits.Add(ally[i]);
        }        
      }
    } 

    return sortedUnits;
  }

  int GetInitiative(Unit target)
  {
    return target.GetComponent<Stats>()[StatTypes.SPD];
  }

  int GetAverageInitiative(List<Unit> units)
  {
    int total = 0;
    for(int i = 0; i < units.Count; i++) {
      total += units[i].GetComponent<Stats>()[StatTypes.SPD];
    }

    return total / units.Count;

  }
  #endregion
}