﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MCB;
using MCB.Cameras;
using MCB.Common;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;
using UnityEngine.UI;

public class BattleScene : StateMachine
{
    public Location location;
    public Transform tileSelectionIndicator;
    public Point pos;

    public Tile SelectedTile
    {
        get { return location.Grid.GetTile(pos); }
    }

    public AbilityMenuPanelController abilityMenuPanelController;
    public StatPanelController statPanelController;
    public HitSuccessIndicator hitSuccessIndicator;
    public BattleMessageController battleMessageController;
    public FacingIndicator facingIndicator;
    public Turn currentTurn = new Turn();
    public List<Unit> units = new List<Unit>();
    public IEnumerator round;
    public ComputerPlayer cpu;
    public Image cursorAbilityImage;
    public Tile currentHoveredTile;
    public Unit CurrentActor => currentTurn.actor;

    public SettingsMenuPanel settingsMenuPanel;

    Queue<Command> _commands;
    Command _currentCommand;
    Command _previousCommand;

    public static BattleScene instance { get; private set; }

    public bool isExecutingCommand { get; private set; }

    bool _isTurnEnding = false;
    bool _canAddMoveCommand = true;

    public List<Tile> movementTiles;
    public List<Tile> waypoints;

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            _commands = new Queue<Command>();
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start()
    {
        DOTween.Init();
        Random.InitState(System.Guid.NewGuid().GetHashCode());
        NameGenerator.Initialize();
        UnitFactory.Initialize();

        ChangeState<InitBattleState>();
    }

    void OnEnable()
    {
        this.AddObserver(OnAnimationEvent, AnimationEventHandler.AnimationEventNotification);
        this.AddObserver(OnCurrentActorDisplaced, DisplacementAbilityEffect.OnCurrentActorDisplaced);
        this.AddObserver(OnCurrentActorDisplaced, TeleportAbilityEffect.OnCurrentActorTeleported);
        TileSurface.MouseEntered += OnTileHover;
    }

    void OnDisable()
    {
        this.RemoveObserver(OnAnimationEvent, AnimationEventHandler.AnimationEventNotification);
        this.RemoveObserver(OnCurrentActorDisplaced, DisplacementAbilityEffect.OnCurrentActorDisplaced);
        this.RemoveObserver(OnCurrentActorDisplaced, TeleportAbilityEffect.OnCurrentActorTeleported);
        TileSurface.MouseEntered -= OnTileHover;
    }

    public void Spawn(UnitData unitData, Point position, Alliances alliances)
    {
        Unit unit = UnitFactory.Create(unitData);
        unit.Hud.HealthBar.gameObject.SetActive(true);

        location.Grid.Place(unit, position);
        unit.Movement.allowDiagonal = false;

        //TODO: add alliance to unitData?
        Alliance alliance = unit.GetComponent<Alliance>();
        if (alliances == Alliances.Enemy)
        {
            alliance.type = Alliances.Enemy;
            unit.Facing = Direction.South;
        }
        else
        {
            unit.Facing = Direction.North;
        }

        Debug.Log($"{unit.DisplayName} is facing {unit.Facing}");
        unit.Movement.inBattle = true;
        unit.Match();

        units.Add(unit);
    }

    public void SetUnitCollidersEnabled(bool enabled)
    {
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i] == currentTurn.actor)
            {
                units[i].SetColliderEnabled(false);
            }
            else
            {
                units[i].SetColliderEnabled(enabled);
            }
        }
    }

    public List<Unit> GetTeam(Alliances alliance)
    {
        List<Unit> team = new List<Unit>();
        foreach (Unit unit in units)
        {
            if (unit.GetComponent<Alliance>().type == alliance)
            {
                team.Add(unit);
            }
        }

        return team;
    }

    void OnAnimationEvent(object sender, object args)
    {
        isExecutingCommand = false;
        ApplyAbility();
    }

    public void AddCommandToQueue(Command command)
    {
        _commands.Enqueue(command);
        if (command.type == CommandTypes.EndTurn)
        {
            _isTurnEnding = true;
        }
        else if (command.type == CommandTypes.Movement)
        {
            _canAddMoveCommand = false;
        }
    }

    public bool CanAddMoveCommand()
    {
        return _canAddMoveCommand;
    }

    public bool TurnIsEnding()
    {
        return _isTurnEnding;
    }

    void Update()
    {
        while (_commands.Count > 0 && !isExecutingCommand)
        {
            _previousCommand = _currentCommand;
            _currentCommand = _commands.Dequeue();
            if (_currentCommand.type == CommandTypes.Ability)
            {
                StartCoroutine(Animate());
            }
            else if (_currentCommand.type == CommandTypes.Movement)
            {
                Tile tile = _currentCommand.data as Tile;
                SelectTile(tile.GridPosition);
                StartCoroutine(MoveSequence());
            }
            else if (_currentCommand.type == CommandTypes.EndTurn)
            {
                StartCoroutine(EndTurn());
            }
        }

        float width = cursorAbilityImage.rectTransform.sizeDelta.x;
        float height = cursorAbilityImage.rectTransform.sizeDelta.y;
        cursorAbilityImage.transform.position = Input.mousePosition + new Vector3(width, -height);

        if (Input.GetKeyDown(KeyCode.T))
        {
            location.Grid.ToggleTacticalView();
        }
    }

    IEnumerator Animate()
    {
        isExecutingCommand = true;

        yield return new WaitForSeconds(.25f);

        // Turn to face the first target in the list only if not targetting self.
        if (_currentCommand.targets.Count > 0 && _currentCommand.targets[0] != currentTurn.actor.CurrentTile)
        {
            Direction dir = currentTurn.actor.CurrentTile.GetDirection(_currentCommand.targets[0]);
            currentTurn.actor.Turn(dir);
        }

        // play animations, etc
        if (currentTurn.actor.Anim)
        {
            Ability ability = _currentCommand.data as Ability;
            if (ability.name.Contains("Basic Attack"))
            {
                PlayBasicAttackAnimation();
            }
            else
            {
                PlayAbilityAnimation();
            }
        }

        yield return new WaitUntil(() => !isExecutingCommand);

        // Wait a little bit so the process doesn't seem so jarring.
        yield return new WaitForSeconds(.25f);
    }

    IEnumerator EndTurn()
    {
        isExecutingCommand = true;
        if (_previousCommand != null && _previousCommand.type == CommandTypes.Ability)
        {
            yield return new WaitForSeconds(.5f);
        }

        isExecutingCommand = false;
        _isTurnEnding = false;
        _previousCommand = _currentCommand;
        _currentCommand = null;

        ChangeState<SelectUnitState>();
    }

    void ApplyAbility()
    {
        Ability ability = _currentCommand.data as Ability;
        ability.Perform(_currentCommand.targets);
    }

    void OnTileHover(Tile hoveredTile)
    {
        currentHoveredTile = hoveredTile;
    }

    void PlayBasicAttackAnimation()
    {
        Equipment equipment = currentTurn.actor.GetComponent<Equipment>();
        Equippable primaryWeapon = equipment.GetItem(EquipSlots.Primary);
        switch (primaryWeapon.itemType)
        {
            case ItemType.Bow:
                currentTurn.actor.Anim.SetTrigger("Longbow Shoot Attack 01");
                break;
            case ItemType.Staff:
                currentTurn.actor.Anim.SetTrigger("Melee Right Attack 02");
                break;
            default:
                currentTurn.actor.Anim.SetTrigger("Melee Right Attack 01");
                break;
        }
    }

    void PlayAbilityAnimation()
    {
        Equipment equipment = currentTurn.actor.GetComponent<Equipment>();
        Equippable primaryWeapon = equipment.GetItem(EquipSlots.Primary);
        switch (primaryWeapon.itemType)
        {
            case ItemType.Bow:
                currentTurn.actor.Anim.SetTrigger("Longbow Shoot Attack 01");
                break;
            case ItemType.Staff:
                currentTurn.actor.Anim.SetTrigger("Cast Spell 01");
                break;
            default:
                currentTurn.actor.Anim.SetTrigger("Melee Right Attack 02");
                break;
        }
    }

    public Tile SelectTile(Point p)
    {
        if (pos == p || !location.Grid.ContainsTileAt(p))
            return null;

        pos = p;
        tileSelectionIndicator.localPosition = location.Grid.Tiles[p].Center;
        return location.Grid.Tiles[p];
    }

    private IEnumerator MoveSequence()
    {
        isExecutingCommand = true;
        CurrentActor.IsBusy = true;

        Movement m = currentTurn.actor.GetComponent<Movement>();

        List<Tile> path = new List<Tile> { currentTurn.actor.CurrentTile };
        path.AddRange(m.GetWayPoints(SelectedTile));
        // Pay the move cost.
        PayMovementCost(path.Count - 1);

        yield return StartCoroutine(m.DoMove(path));
        yield return new WaitForSeconds(.25f);

        UpdateWayPoints();
        isExecutingCommand = false;
        CurrentActor.IsBusy = false;
        _canAddMoveCommand = true;
    }

    public void UpdateWayPoints()
    {
        Movement mover = currentTurn.actor.GetComponent<Movement>();

        movementTiles = mover.GetTilesInRange(location.Grid);
        waypoints = new List<Tile>();
    }

    void PayMovementCost(int tilesMoved)
    {
        Stats stats = currentTurn.actor.GetComponent<Stats>();
        stats[StatTypes.MP] -= tilesMoved;
    }

    void OnCurrentActorDisplaced(object sender, object args)
    {
        SelectTile(currentTurn.actor.CurrentTile.GridPosition);
        location.Grid.DeselectTiles(movementTiles);
        location.Grid.DeselectTiles(waypoints);
        UpdateWayPoints();
    }
}