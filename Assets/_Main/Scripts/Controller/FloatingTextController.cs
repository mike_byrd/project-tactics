﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MCB;

public class FloatingTextController : MonoBehaviour
{
  [SerializeField] FloatingText textPrefab;
  [SerializeField] Canvas mainCanvas;

  const float damageOffsetY = 65f;

  public static FloatingTextController instance { get; private set; }

  private void Awake()
  {
    if (!instance) {
      instance = this;
    } else if (instance != this) {
      Destroy(this.gameObject);
      return;
    }
  }

  public void CreateFloatingText(string text, Vector3 location, Color color)
  {
    FloatingText floatingText = Instantiate(instance.textPrefab, location, Quaternion.identity);
    Vector3 screenPos = Camera.main.WorldToScreenPoint(location);

    // The second argument allows the child element to maintain its scale.
    floatingText.transform.SetParent(instance.mainCanvas.transform, false);
    floatingText.transform.position = screenPos;

    floatingText.Show(text);
    floatingText.SetColor(color);
  }

  public void SpawnUnitDamageText(Unit unit, string text, Color color)
  {
    Vector3 pos = new Vector3(unit.transform.position.x, unit.transform.position.y, -50);
    FloatingText floatingText = Instantiate(instance.textPrefab, pos, Quaternion.identity);
    
    Canvas unitCanvas = unit.transform.Find("Unit Canvas").GetComponent<Canvas>();    

    // The second argument allows the child element to maintain its scale.
    floatingText.transform.SetParent(unitCanvas.transform, false);
    floatingText.transform.localPosition = Vector3.up * damageOffsetY;
    //floatingText.transform.localPosition += Vector3.up * damageOffsetY;
    //floatingText.transform.position = screenPos;

    floatingText.Show(text);
    floatingText.SetColor(color);
  }
}
