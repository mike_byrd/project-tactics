﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffectController : MonoBehaviour
{
  public static ParticleEffectController instance { get; private set; }

  [SerializeField]
  Vector3 particlePositionOffset;

  [SerializeField]
  GameObject defaultParticlePrefab;

  private void Awake()
  {
    if(!instance) {
      instance = this;
    } else if(instance != this) {
      Destroy(this.gameObject);
      return;
    }
  }

  public void PlayAbilityEffect(BaseAbilityEffect abilityEffect, Vector3 location)
  {

    Vector3 spawnLocation = location + particlePositionOffset;
    GameObject particleSystemPrefab;
    if(abilityEffect.particleEffectPrefab) {
      particleSystemPrefab = abilityEffect.particleEffectPrefab;
    } else {
      particleSystemPrefab = defaultParticlePrefab;
    }

    GameObject.Instantiate(particleSystemPrefab, spawnLocation, Quaternion.identity);  
  }
}
