﻿using UnityEngine;
using System;
using System.Collections;
using MCB.Common;

class Repeater
{
    const float threshold = 0.5f;
    const float rate = 0.25f;
    float _next;
    bool _hold;
    string _axis;

    public Repeater(string axisName)
    {
        _axis = axisName;
    }

    public int Update()
    {
        int retValue = 0;
        int value = Mathf.RoundToInt(Input.GetAxisRaw(_axis));

        if (value != 0)
        {
            if (Time.time > _next)
            {
                retValue = value;
                _next = Time.time + (_hold ? rate : threshold);
                _hold = true;
            }
        }
        else
        {
            _hold = false;
            _next = 0;
        }

        return retValue;
    }
}

public class InputController : MonoBehaviour
{
    public static event EventHandler<InfoEventArgs<Point>> moveEvent;
    public static event EventHandler<InfoEventArgs<string>> fireEvent;
    public static event EventHandler<InfoEventArgs<float>> scrollEvent;

    Repeater _hor = new Repeater("Horizontal");
    Repeater _ver = new Repeater("Vertical");

    // TODO: Do this better. Attack starts at index 7. Abilities start at index 8
    string[] _buttons = new string[]
    {
        GameConstants.INPUT_FIRE1, GameConstants.INPUT_FIRE2, GameConstants.INPUT_FIRE3,
        GameConstants.INPUT_UP, GameConstants.INPUT_DOWN, GameConstants.INPUT_LEFT, GameConstants.INPUT_RIGHT,
        GameConstants.INPUT_ACTION_BASIC_ATTACK, GameConstants.INPUT_ACTION_ABILITY1,
        GameConstants.INPUT_ACTION_ABILITY2, GameConstants.INPUT_ACTION_ABILITY3,
        GameConstants.INPUT_ACTION_ABILITY4, GameConstants.INPUT_ACTION_ABILITY5,
        GameConstants.INPUT_ACTION_ABILITY6, GameConstants.INPUT_ACTION_ABILITY7,
        GameConstants.INPUT_ACTION_ABILITY8
    };

    void Update()
    {
        int x = _hor.Update();
        int y = _ver.Update();
        if (x != 0 || y != 0)
        {
            if (moveEvent != null)
            {
                moveEvent(this, new InfoEventArgs<Point>(new Point(x, y)));
            }
        }

        for (int i = 0; i < _buttons.Length; ++i)
        {
            if (Input.GetButtonUp(_buttons[i]))
            {
                if (fireEvent != null)
                {
                    fireEvent(this, new InfoEventArgs<string>(_buttons[i]));
                }
            }
        }

        float scrollValue = Input.GetAxis("Mouse ScrollWheel");
        if (scrollValue != 0)
        {
            if (scrollEvent != null)
            {
                scrollEvent(this, new InfoEventArgs<float>(scrollValue));
            }
        }
    }
}