﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using MCB;
using UnityEngine.UI;

public class StatPanelController : MonoBehaviour
{
    #region Const

    const string ShowKey = "Show";
    const string HideKey = "Hide";

    public static string ON_ARROW_CLICKED = "StatPanelController.OnArrowClicked";

    #endregion

    #region Fields

    [SerializeField] StatPanel primaryPanel;
    [SerializeField] StatPanel secondaryPanel;

    Tweener primaryTransition;
    Tweener secondaryTransition;

    #endregion

    #region MonoBehaviour

    void Start()
    {
        //if (primaryPanel.panel.CurrentPosition == null)
        //  primaryPanel.panel.SetPosition(HideKey, false);
        //if (secondaryPanel.panel.CurrentPosition == null)
        //secondaryPanel.panel.SetPosition(HideKey, false);
    }

    #endregion

    #region Public

    public void ShowPrimary(GameObject obj)
    {
        Unit unit = obj.GetComponent<Unit>();
        primaryPanel.UpdateUI(unit);
    }

    public void UpdateFacingDirection(string direction)
    {
        // TODO: Update this to turn right or left.
        Direction dir;
        switch (direction)
        {
            case "Left":
                dir = Direction.North;
                break;
            case "Right":
                dir = Direction.South;
                break;
            default:
                return;
        }

        this.PostNotification(ON_ARROW_CLICKED, dir);
    }

    public void OnMouseEnterAP(RectTransform rectTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(rectTransform, "<b>AP</b>: Actions Points");
    }

    public void OnMouseEnterMP(RectTransform rectTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(rectTransform, "<b>MP</b>: Movement Points");
    }

    public void OnMouseEnterHP(RectTransform rectTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(rectTransform, "<b>HP</b>: Health Points");
    }

    public void OnMouseEnterLeftArrow(RectTransform rectTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(rectTransform, "Turn Left");
    }

    public void OnMouseEnterRightArrow(RectTransform rectTransform)
    {
        InfoCardController.instance.ShowSimpleInfo(rectTransform, "Turn Right");
    }

    public void OnMouseExitStat()
    {
        InfoCardController.instance.HideCard();
    }

    #endregion

    #region Private

    void MovePanel(StatPanel obj, bool visible)
    {
        float endPosition;
        if (visible)
        {
            endPosition = obj.showPosition.x;
        }
        else
        {
            endPosition = obj.hidePosition.x;
        }

        RectTransform rectTransform = obj.GetComponent<RectTransform>();
        rectTransform.DOAnchorPosX(endPosition, .5f, true);
    }

    #endregion
}