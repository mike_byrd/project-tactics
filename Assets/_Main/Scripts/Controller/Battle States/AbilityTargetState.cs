﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;

public class AbilityTargetState : BattleState
{
    List<Tile> abilityRangeTiles;
    AbilityRange abilityRange;
    AbilityArea abilityArea;

    List<Tile> selectedTiles;
    Tile tileHovered;

    protected override void Awake()
    {
        base.Awake();
        selectedTiles = new List<Tile>();
        abilityRangeTiles = new List<Tile>();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        TileSurface.MouseDown += OnTileSurfaceMouseDown;
        TileSurface.MouseEntered += OnTileHover;
        this.AddObserver(OnEndTurnClicked, AbilityMenuEntry.ON_END_TURN);
        this.AddObserver(OnAbilitySelected, AbilityMenuEntry.ON_SELECT);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        TileSurface.MouseDown -= OnTileSurfaceMouseDown;
        TileSurface.MouseEntered -= OnTileHover;
        this.RemoveObserver(OnEndTurnClicked, AbilityMenuEntry.ON_END_TURN);
        this.RemoveObserver(OnAbilitySelected, AbilityMenuEntry.ON_SELECT);
    }

    public override void Enter()
    {
        base.Enter();
        tileHovered = _owner.currentHoveredTile;

        UpdateAbility(currentTurn.ability);

        if (abilityRangeTiles.Contains(tileHovered))
        {
            UpdateSelectedTiles();
        }

        //statPanelController.ShowPrimary(currentTurn.actor.gameObject);
        if (abilityRange.directionOriented)
        {
            //RefreshSecondaryStatPanel(pos);
        }

        if (driver.Current == Drivers.Computer)
        {
            StartCoroutine(ComputerHighlightTarget());
        }
    }

    public override void Exit()
    {
        base.Exit();
        ClearAbilityTiles();

        // Hide cursor ability.
        _owner.cursorAbilityImage.gameObject.SetActive(false);

        //statPanelController.HidePrimary();
        //statPanelController.HideSecondary();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        if (abilityRange.directionOriented)
        {
            ChangeDirection(e.info);
        }
        else
        {
            //SelectNextTile(e.info);
            //RefreshSecondaryStatPanel(pos);
        }
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        if (e.info == GameConstants.INPUT_FIRE1)
        {
            if (abilityRange.directionOriented || abilityRangeTiles.Contains(board.GetTile(pos)))
            {
                _owner.ChangeState<ConfirmAbilityTargetState>();
            }
        }
        else if (e.info == GameConstants.INPUT_FIRE2)
        {
            _owner.ChangeState<CommandSelectionState>();
        }
        else if (e.info == GameConstants.INPUT_FIRE3)
        {
            EndCurrentTurn();
        }

        ResolveAbilityInput(e.info);
    }

    void ClearAbilityTiles()
    {
        board.DeselectTiles(abilityRangeTiles);
        board.DeselectTiles(selectedTiles);
        selectedTiles.Clear();
        abilityRangeTiles.Clear();

        if (abilityRange)
        {
            abilityRange.ClearOutOfSightTiles();
        }
    }

    void UpdateAbility(Ability ability)
    {
        if (!ability.CanPerform())
        {
            return;
        }

        currentTurn.ability = ability;

        ClearAbilityTiles();
        abilityRange = currentTurn.ability.GetComponent<AbilityRange>();
        abilityArea = currentTurn.ability.GetComponent<AbilityArea>();

        _owner.SetUnitCollidersEnabled(true);
        HighlightTiles();
        _owner.SetUnitCollidersEnabled(false);

        _owner.cursorAbilityImage.sprite = ability.sprite;
        _owner.cursorAbilityImage.gameObject.SetActive(true);
    }

    void OnEndTurnClicked(object sender, object args)
    {
        EndCurrentTurn();
    }

    void OnAbilitySelected(object sender, object args)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        AbilityMenuEntry abilityEntry = args as AbilityMenuEntry;
        if (abilityEntry && abilityEntry.ability != currentTurn.ability)
        {
            UpdateAbility(abilityEntry.ability);
        }
    }

    void ResolveAbilityInput(string input)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        Ability ability;
        switch (input)
        {
            case GameConstants.INPUT_ACTION_BASIC_ATTACK:
                ability = abilityMenuPanelController.GetAbilityAt(0);
                break;
            case GameConstants.INPUT_ACTION_ABILITY1:
                ability = abilityMenuPanelController.GetAbilityAt(0);
                break;
            case GameConstants.INPUT_ACTION_ABILITY2:
                ability = abilityMenuPanelController.GetAbilityAt(1);
                break;
            case GameConstants.INPUT_ACTION_ABILITY3:
                ability = abilityMenuPanelController.GetAbilityAt(2);
                break;
            case GameConstants.INPUT_ACTION_ABILITY4:
                ability = abilityMenuPanelController.GetAbilityAt(3);
                break;
            case GameConstants.INPUT_ACTION_ABILITY5:
                ability = abilityMenuPanelController.GetAbilityAt(4);
                break;
            case GameConstants.INPUT_ACTION_ABILITY6:
                ability = abilityMenuPanelController.GetAbilityAt(5);
                break;
            case GameConstants.INPUT_ACTION_ABILITY7:
                ability = abilityMenuPanelController.GetAbilityAt(6);
                break;
            case GameConstants.INPUT_ACTION_ABILITY8:
                ability = abilityMenuPanelController.GetAbilityAt(7);
                break;
            default:
                ability = null;
                break;
        }

        if (ability && ability != currentTurn.ability)
        {
            UpdateAbility(ability);
            if (abilityRangeTiles.Contains(tileHovered))
            {
                UpdateSelectedTiles();
            }
        }
    }

    void OnTileSurfaceMouseDown(Tile selectedTile)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        if (selectedTile)
        {
            //Debug.Log(string.Format("Tile Selected: [{0}, {1}]", selectedTile.pos.x, selectedTile.pos.y));
            if (abilityRange.directionOriented || abilityRangeTiles.Contains(board.GetTile(selectedTile.GridPosition)))
            {
                FindTargets(selectedTile.GridPosition);

                if (currentTurn.ability.requiresTarget && !HasAtLeastOneTarget())
                {
                    return;
                }

                SelectTile(selectedTile.GridPosition);

                // Pay the ability cost.
                AbilityMagicCost abilityCost = currentTurn.ability.GetComponent<AbilityMagicCost>();
                if (abilityCost)
                {
                    PayAbilityCost(abilityCost);
                }

                DurationAbilityCooldown cooldown = currentTurn.ability.GetComponent<DurationAbilityCooldown>();
                if (cooldown)
                {
                    cooldown.isActive = true;
                }

                _owner.AddCommandToQueue(new Command(CommandTypes.Ability, currentTurn.ability, currentTurn.targets));
                _owner.ChangeState<CommandSelectionState>();
            }
        }
    }

    bool HasAtLeastOneTarget()
    {
        bool valid = false;
        for (int i = 0; i < currentTurn.targets.Count; i++)
        {
            if (currentTurn.targets[i].content)
            {
                valid = true;
                break;
            }
        }

        return valid;
    }

    void OnTileHover(Tile selectedTile)
    {
        if (selectedTile)
        {
            tileHovered = selectedTile;
            if (abilityRangeTiles.Contains(selectedTile))
            {
                UpdateSelectedTiles();
            }
            else
            {
                for (int i = 0; i < selectedTiles.Count; i++)
                {
                    if (abilityRangeTiles.Contains(selectedTiles[i]))
                    {
                        board.HighlightTile(selectedTiles[i]);
                    }
                    else
                    {
                        if (abilityRange.outOfSightTiles.Contains(selectedTiles[i]))
                        {
                            selectedTiles[i].state = TileState.OutOfSight;
                        }
                        else
                        {
                            board.DeselectTile(selectedTiles[i]);
                        }
                    }
                }
            }
        }
    }

    void FindTargets(Point selectedPos)
    {
        List<Tile> abilityAreaTiles = abilityArea.GetTilesInArea(board, selectedPos);
        currentTurn.targets = new List<Tile>();
        for (int i = 0; i < abilityAreaTiles.Count; ++i)
        {
            if (currentTurn.ability.IsTarget(abilityAreaTiles[i]))
            {
                currentTurn.targets.Add(abilityAreaTiles[i]);
            }
        }
    }

    void ChangeDirection(Point p)
    {
        Direction dir = p.GetDirection();
        if (currentTurn.actor.Facing != dir)
        {
            board.DeselectTiles(abilityRangeTiles);
            currentTurn.actor.Facing = dir;
            currentTurn.actor.Match();
            HighlightTiles();
        }
    }

    void HighlightTiles()
    {
        abilityRangeTiles = abilityRange.GetTilesInRange(board);
        board.HighlightTiles(abilityRangeTiles);
    }

    void UpdateSelectedTiles()
    {
        List<Tile> abilityAreaTiles = abilityArea.GetTilesInArea(board, tileHovered.GridPosition);
        if (selectedTiles.Count > 0)
        {
            for (int i = 0; i < selectedTiles.Count; i++)
            {
                if (abilityRangeTiles.Contains(selectedTiles[i]))
                {
                    board.HighlightTile(selectedTiles[i]);
                }
                else
                {
                    if (abilityRange.outOfSightTiles.Contains(selectedTiles[i]))
                    {
                        selectedTiles[i].state = TileState.OutOfSight;
                    }
                    else
                    {
                        board.DeselectTile(selectedTiles[i]);
                    }
                }
            }

            selectedTiles.Clear();
        }

        selectedTiles.AddRange(abilityAreaTiles);
        board.SelectTiles(abilityAreaTiles);
    }

    void PayAbilityCost(AbilityMagicCost abilityCost)
    {
        Stats stats = currentTurn.actor.GetComponent<Stats>();
        stats[StatTypes.AP] -= abilityCost.amount;

        statPanelController.ShowPrimary(currentTurn.actor.gameObject);
        abilityMenuPanelController.UpdateLockedAbilities();
    }

    void ActivateAbilityCooldown(DurationAbilityCooldown cooldown)
    {
        cooldown.isActive = true;
    }

    IEnumerator ComputerHighlightTarget()
    {
        if (abilityRange.directionOriented)
        {
            ChangeDirection(currentTurn.plan.attackDirection.GetNormal());
            yield return new WaitForSeconds(0.25f);
        }
        else
        {
            Point cursorPos = pos;
            while (cursorPos != currentTurn.plan.fireLocation)
            {
                if (cursorPos.x < currentTurn.plan.fireLocation.x) cursorPos.x++;
                if (cursorPos.x > currentTurn.plan.fireLocation.x) cursorPos.x--;
                if (cursorPos.y < currentTurn.plan.fireLocation.y) cursorPos.y++;
                if (cursorPos.y > currentTurn.plan.fireLocation.y) cursorPos.y--;
                SelectTile(cursorPos);
                yield return new WaitForSeconds(0.25f);
            }
        }

        yield return new WaitForSeconds(0.5f);
        _owner.ChangeState<ConfirmAbilityTargetState>();
    }
}