﻿using UnityEngine;
using System.Collections;
using MCB;
using MCB.Networking;
using ProjectTactics;

public class CommandSelectionState : BaseAbilityMenuState
{
    AbilityCatalog catalog;

    protected override void AddListeners()
    {
        base.AddListeners();
        TileSurface.MouseDown += OnTileSurfaceMouseDown;
        TileSurface.MouseEntered += OnTileHover;

        _owner.location.Grid.MouseExited += OnMouseExitBoard;

        this.AddObserver(OnAbilitySelected, AbilityMenuEntry.ON_SELECT);
        this.AddObserver(OnEndTurnClicked, AbilityMenuEntry.ON_END_TURN);
        this.AddObserver(OnFacingArrowClicked, StatPanelController.ON_ARROW_CLICKED);
        this.AddObserver(OnMOVDidChange, Stats.DidChangeNotification(StatTypes.MP));
//        this.AddObserver(OnMouseExitBoard, CameraRig.OnMouseExitBoard);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        TileSurface.MouseDown -= OnTileSurfaceMouseDown;
        TileSurface.MouseEntered -= OnTileHover;

        _owner.location.Grid.MouseExited -= OnMouseExitBoard;

        this.RemoveObserver(OnAbilitySelected, AbilityMenuEntry.ON_SELECT);
        this.RemoveObserver(OnEndTurnClicked, AbilityMenuEntry.ON_END_TURN);
        this.RemoveObserver(OnFacingArrowClicked, StatPanelController.ON_ARROW_CLICKED);
        this.RemoveObserver(OnMOVDidChange, Stats.DidChangeNotification(StatTypes.MP));
//        this.RemoveObserver(OnMouseExitBoard, CameraRig.OnMouseExitBoard);
    }

    public override void Enter()
    {
        base.Enter();
        _owner.UpdateWayPoints();

        statPanelController.ShowPrimary(currentTurn.actor.gameObject);
        if (driver.Current == Drivers.Computer)
        {
            StartCoroutine(ComputerTurn());
        }
    }

    protected void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            _owner.ChangeState<BattleGameMenuState>();
        }
    }

    public override void Exit()
    {
        base.Exit();

        board.DeselectTiles(_owner.movementTiles);
        board.DeselectTiles(_owner.waypoints);
    }

    private void OnMouseExitBoard()
    {
        board.DeselectTiles(_owner.movementTiles);
        board.DeselectTiles(_owner.waypoints);
    }

    void OnEndTurnClicked(object sender, object args)
    {
        EndCurrentTurn();
    }

    void OnAbilitySelected(object sender, object args)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        AbilityMenuEntry abilityEntry = args as AbilityMenuEntry;
        if (abilityEntry)
        {
            SelectAbility(abilityEntry.ability);
        }
    }

    void SelectAbility(Ability ability)
    {
        if (!ability.CanPerform())
        {
            return;
        }

        if (ability)
        {
            currentTurn.ability = ability;
            _owner.ChangeState<AbilityTargetState>();
        }
    }

    private void OnTileSurfaceMouseDown(Tile selectedTile)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        if (!_owner.movementTiles.Contains(selectedTile) || !_owner.CanAddMoveCommand()) return;

        _owner.AddCommandToQueue(new Command(CommandTypes.Movement, selectedTile));
        board.DeselectTiles(_owner.waypoints);
    }

    private void OnTileHover(Tile selectedTile)
    {
        Unit currentActor = currentTurn.actor;
        if (!currentActor.Movement.CanUnitMove() || _owner.CurrentActor.IsBusy) return;

        board.DeselectTiles(_owner.movementTiles);
        board.DeselectTiles(_owner.waypoints);
        if (_owner.movementTiles.Contains(selectedTile))
        {
            _owner.waypoints = currentActor.Movement.GetWayPoints(selectedTile);
            board.HighlightTiles(_owner.waypoints);
        }
        else
        {
            if (selectedTile == _owner.currentTurn.actor.CurrentTile)
            {
                board.HighlightTiles(_owner.movementTiles);
            }
            else
            {
                board.DeselectTiles(_owner.movementTiles);
                board.DeselectTiles(_owner.waypoints);
            }
        }
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        switch (e.info)
        {
            case GameConstants.INPUT_FIRE3:
                EndCurrentTurn();
                break;

            default:
                break;
        }

        ResolveFacingInput(e.info);
        ResolveAbilityInput(e.info);
    }

    void ResolveAbilityInput(string input)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        Ability ability;
        switch (input)
        {
            case GameConstants.INPUT_ACTION_BASIC_ATTACK:
                ability = abilityMenuPanelController.GetAbilityAt(0);
                break;
            case GameConstants.INPUT_ACTION_ABILITY1:
                ability = abilityMenuPanelController.GetAbilityAt(0);
                break;
            case GameConstants.INPUT_ACTION_ABILITY2:
                ability = abilityMenuPanelController.GetAbilityAt(1);
                break;
            case GameConstants.INPUT_ACTION_ABILITY3:
                ability = abilityMenuPanelController.GetAbilityAt(2);
                break;
            case GameConstants.INPUT_ACTION_ABILITY4:
                ability = abilityMenuPanelController.GetAbilityAt(3);
                break;
            case GameConstants.INPUT_ACTION_ABILITY5:
                ability = abilityMenuPanelController.GetAbilityAt(4);
                break;
            case GameConstants.INPUT_ACTION_ABILITY6:
                ability = abilityMenuPanelController.GetAbilityAt(5);
                break;
            case GameConstants.INPUT_ACTION_ABILITY7:
                ability = abilityMenuPanelController.GetAbilityAt(6);
                break;
            case GameConstants.INPUT_ACTION_ABILITY8:
                ability = abilityMenuPanelController.GetAbilityAt(7);
                break;
            default:
                ability = null;
                break;
        }

        if (ability)
        {
            SelectAbility(ability);
        }
    }

    void ResolveFacingInput(string input)
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        switch (input)
        {
            case GameConstants.INPUT_UP:
                if (_owner.isExecutingCommand)
                {
                    return;
                }

                ChangeUnitFacing(Direction.North);
                break;
            case GameConstants.INPUT_DOWN:
                if (_owner.isExecutingCommand)
                {
                    return;
                }

                ChangeUnitFacing(Direction.South);
                break;
            case GameConstants.INPUT_LEFT:
                if (_owner.isExecutingCommand)
                {
                    return;
                }

                ChangeUnitFacing(Direction.West);
                break;
            case GameConstants.INPUT_RIGHT:
                if (_owner.isExecutingCommand)
                {
                    return;
                }

                ChangeUnitFacing(Direction.East);
                break;
        }
    }

    void OnFacingArrowClicked(object sender, object args)
    {
        if (_owner.isExecutingCommand)
        {
            return;
        }

        Direction direction = (Direction) args;
        ChangeUnitFacing(direction);
    }

    void ChangeUnitFacing(Direction direction)
    {
        currentTurn.actor.Facing = direction;
        currentTurn.actor.Match();
    }

    protected override void LoadMenu()
    {
        catalog = currentTurn.actor.GetComponentInChildren<AbilityCatalog>();
        GameObject container = catalog.GetCategory(0);
        //menuTitle = container.name;

        int count = catalog.AbilityCount(container);

        //if(menuOptions == null)
        //  menuOptions = new List<string>(count);
        //else
        //  menuOptions.Clear();

        //bool[] locks = new bool[count];
        //for (int i = 0; i < count; ++i) {
        //  Ability ability = catalog.GetAbility(category, i);
        //  AbilityMagicCost cost = ability.GetComponent<AbilityMagicCost>();
        //  if (cost)
        //    menuOptions.Add(string.Format("{0}: {1}", ability.name, cost.amount));
        //  else
        //    menuOptions.Add(ability.name);
        //  locks[i] = !ability.CanPerform();
        //}

        abilityMenuPanelController.UpdateUI(catalog);
        //for (int i = 0; i < count; ++i)
        //  abilityMenuPanelController.SetLocked(i, locks[i]);
    }

    protected override void Confirm()
    {
        //switch (abilityMenuPanelController.selection) {
        //  case 0: // Move
        //    owner.ChangeState<MoveTargetState>();
        //    break;
        //  case 1: // Action
        //    owner.ChangeState<CategorySelectionState>();
        //    break;
        //  case 2: // Wait
        //    owner.ChangeState<EndFacingState>();
        //    break;
        //}
    }

    protected override void Cancel()
    {
        //abilityMenuPanelController.Hide();
        //if (currentTurn.hasUnitMoved && !currentTurn.lockMove) {
        //  currentTurn.UndoMove();
        //  //abilityMenuPanelController.SetLocked(0, false);
        //  SelectTile(currentTurn.actor.tile.pos);
        //  UpdateWayPoints();

        //} else {
        //  //owner.ChangeState<ExploreState>();
        //}
    }

    void OnMOVDidChange(object sender, object args)
    {
        Stats stat = sender as Stats;
        statPanelController.ShowPrimary(currentTurn.actor.gameObject);
    }

    IEnumerator ComputerTurn()
    {
        if (currentTurn.plan == null)
        {
            currentTurn.plan = _owner.cpu.Evaluate();
            currentTurn.ability = currentTurn.plan.ability;
        }

        yield return new WaitForSeconds(1f);

        if (currentTurn.hasUnitMoved == false && currentTurn.plan.moveLocation != currentTurn.actor.CurrentTile.GridPosition)
            _owner.ChangeState<MoveTargetState>();
        else if (currentTurn.hasUnitActed == false && currentTurn.plan.ability != null)
            _owner.ChangeState<AbilityTargetState>();
        else
            _owner.ChangeState<EndFacingState>();
    }
}