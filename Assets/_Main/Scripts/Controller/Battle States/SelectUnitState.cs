﻿using UnityEngine;
using System.Collections;
using MCB;
using MCB.Cameras;

public class SelectUnitState : BattleState
{
    public override void Enter()
    {
        base.Enter();

//    owner.cameraRig.virtualCamera.enabled = false;
        StartCoroutine(ChangeCurrentUnit());

        abilityMenuPanelController.SetEndButtonVisible(true);
    }

    public override void Exit()
    {
        base.Exit();
        //statPanelController.HidePrimary();
    }

    IEnumerator ChangeCurrentUnit()
    {
        _owner.round.MoveNext();
        SelectTile(currentTurn.actor.CurrentTile.GridPosition);
        RefreshPrimaryStatPanel(pos);

        //owner.cameraRig.virtualCamera.Follow = currentTurn.actor.transform;
        yield return FreeStyleCamera.SetTargetSmooth(currentTurn.actor.transform);

        _owner.SetUnitCollidersEnabled(false);

        //owner.cameraRig.virtualCamera.enabled = true;
        _owner.ChangeState<CommandSelectionState>();
        //owner.ChangeState<MoveTargetState>();
    }
}