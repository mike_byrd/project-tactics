﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public class MoveSequenceState : BattleState
{
    public override void Enter()
    {
        base.Enter();

        StartCoroutine("Sequence");
    }

    public override void Exit()
    {
        base.Exit();
    }

    IEnumerator Sequence()
    {
        if (currentTurn.actor.Anim)
        {
            currentTurn.actor.Anim.SetBool("Run", true);
        }

        Movement m = currentTurn.actor.GetComponent<Movement>();
        List<Tile> waypoints = m.GetWayPoints(_owner.SelectedTile);

        yield return StartCoroutine(m.Traverse(waypoints));

        board.DeselectTiles(waypoints);

        // Pay the move cost.
        PayMovementCost(waypoints.Count - 1);

        if (currentTurn.actor.Anim)
        {
            currentTurn.actor.Anim.SetBool("Run", false);
        }

        //currentTurn.hasUnitMoved = true;
        _owner.ChangeState<CommandSelectionState>();
    }

    void PayMovementCost(int tilesMoved)
    {
        Stats stats = currentTurn.actor.GetComponent<Stats>();
        stats[StatTypes.MP] -= tilesMoved;
    }
}