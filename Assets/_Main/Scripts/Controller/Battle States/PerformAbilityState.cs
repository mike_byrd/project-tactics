﻿using UnityEngine;
using System.Collections;

public class PerformAbilityState : BattleState
{
  //bool isAnimating;
  //float turnDelay = 1f;

  public override void Enter()
  {
    base.Enter();

    currentTurn.hasUnitActed = true;
    if (currentTurn.hasUnitMoved) {
      currentTurn.lockMove = true;
    }

    // Pay the ability cost.
    AbilityMagicCost abilityCost = currentTurn.ability.GetComponent<AbilityMagicCost>();
    if (abilityCost) {
      PayAbilityCost(abilityCost);
    }

    DurationAbilityCooldown cooldown = currentTurn.ability.GetComponent<DurationAbilityCooldown>();
    if (cooldown) {
      cooldown.isActive = true;
    }

    //StartCoroutine(Animate());
  }

  //void OnEnable()
  //{
  //  this.AddObserver(OnAnimationEvent, AnimationEventHandler.AnimationEventNotification);
  //}

  //void OnDisable()
  //{
  //  this.RemoveObserver(OnAnimationEvent, AnimationEventHandler.AnimationEventNotification);
  //}

  //IEnumerator Animate()
  //{
  //  // Turn to face the first target in the list.
  //  Directions dir = currentTurn.actor.tile.GetDirection(currentTurn.targets[0]);
  //  currentTurn.actor.Turn(dir);

  //  // play animations, etc
  //  if (currentTurn.actor.anim) {
  //    if (currentTurn.ability.name.Contains("Basic Attack")) {
  //      PlayBasicAttackAnimation();
  //    } else {
  //      PlayAbilityAnimation();
  //    }
  //  }

  //  isAnimating = true;
  //  yield return new WaitUntil(() => !isAnimating);

  //  ApplyAbility();

  //  // Wait a little bit so the process doesn't seem so jarring.
  //  yield return new WaitForSeconds(turnDelay);

  //  if (IsBattleOver()) {
  //    owner.ChangeState<CutSceneState>();
  //  } else if (!UnitHasControl()) {
  //    owner.ChangeState<SelectUnitState>();
  //  } else if (currentTurn.hasUnitMoved) {
  //    //owner.ChangeState<EndFacingState>();
  //    owner.ChangeState<CommandSelectionState>();
  //  } else {
  //    owner.ChangeState<CommandSelectionState>();
  //  }
  //}

  //void PlayBasicAttackAnimation()
  //{
  //  Equipment equipment = currentTurn.actor.GetComponent<Equipment>();
  //  Equippable primaryWeapon = equipment.GetItem(EquipSlots.Primary);
  //  switch (primaryWeapon.itemType) {
  //    case ItemType.Bow:
  //      currentTurn.actor.anim.SetTrigger("Longbow Shoot Attack 01");
  //      break;
  //    case ItemType.Staff:
  //      currentTurn.actor.anim.SetTrigger("Melee Right Attack 02");
  //      break;
  //    default:
  //      currentTurn.actor.anim.SetTrigger("Melee Right Attack 01");
  //      break;
  //  }
  //}

  //void PlayAbilityAnimation()
  //{
  //  Equipment equipment = currentTurn.actor.GetComponent<Equipment>();
  //  Equippable primaryWeapon = equipment.GetItem(EquipSlots.Primary);
  //  switch (primaryWeapon.itemType) {
  //    case ItemType.Bow:
  //      currentTurn.actor.anim.SetTrigger("Longbow Shoot Attack 01");
  //      break;
  //    case ItemType.Staff:
  //      currentTurn.actor.anim.SetTrigger("Cast Spell 01");
  //      break;
  //    default:
  //      currentTurn.actor.anim.SetTrigger("Melee Right Attack 02");
  //      break;
  //  }
  //}

  //void OnAnimationEvent(object sender, object args)
  //{
  //  isAnimating = false;
  //}

  void PayAbilityCost(AbilityMagicCost abilityCost)
  {
    Stats stats = currentTurn.actor.GetComponent<Stats>();
    stats[StatTypes.AP] -= abilityCost.amount;

    statPanelController.ShowPrimary(currentTurn.actor.gameObject);
    abilityMenuPanelController.UpdateLockedAbilities();
  }

  //void ApplyAbility()
  //{
  //  currentTurn.ability.Perform(currentTurn.targets);
  //}

  bool UnitHasControl()
  {
    return currentTurn.actor.GetComponentInChildren<KnockOutStatusEffect>() == null;
  }
}