﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using MCB;
using MCB.Common;
using ProjectTactics;

public abstract class BattleState : State
{
    protected BattleScene _owner;

    protected Driver driver;

//  public CameraRig cameraRig { get { return owner.cameraRig; } }
    public WorldGrid board
    {
        get { return _owner.location.Grid; }
    }

    public Transform tileSelectionIndicator
    {
        get { return _owner.tileSelectionIndicator; }
    }

    public Point pos
    {
        get { return _owner.pos; }
        set { _owner.pos = value; }
    }

    public Tile currentTile
    {
        get { return _owner.SelectedTile; }
    }

    public AbilityMenuPanelController abilityMenuPanelController
    {
        get { return _owner.abilityMenuPanelController; }
    }

    public StatPanelController statPanelController
    {
        get { return _owner.statPanelController; }
    }

    public HitSuccessIndicator hitSuccessIndicator
    {
        get { return _owner.hitSuccessIndicator; }
    }

    public Turn currentTurn
    {
        get { return _owner.currentTurn; }
    }

    public List<Unit> units
    {
        get { return _owner.units; }
    }

    protected virtual void Awake()
    {
        _owner = GetComponent<BattleScene>();
    }

    protected override void AddListeners()
    {
        if (driver == null || driver.Current == Drivers.Human)
        {
            InputController.moveEvent += OnMove;
            InputController.fireEvent += OnFire;
            InputController.scrollEvent += OnScroll;
        }
    }

    protected override void RemoveListeners()
    {
        InputController.moveEvent -= OnMove;
        InputController.fireEvent -= OnFire;
        InputController.scrollEvent -= OnScroll;
    }

    public override void Enter()
    {
        driver = (currentTurn.actor != null) ? currentTurn.actor.GetComponent<Driver>() : null;
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    protected virtual void OnMove(object sender, InfoEventArgs<Point> e)
    {
    }

    protected virtual void OnFire(object sender, InfoEventArgs<string> e)
    {
    }

    protected virtual void OnScroll(object sender, InfoEventArgs<float> e)
    {
//    if (e.info < 0) {
//      // Scroll out
//      if (cameraRig.virtualCamera.m_Lens.OrthographicSize < 10) {
//        cameraRig.virtualCamera.m_Lens.OrthographicSize++;
//      }
//    } else if (e.info > 0) {
//      //Scroll in
//      if (cameraRig.virtualCamera.m_Lens.OrthographicSize > 3) {
//        cameraRig.virtualCamera.m_Lens.OrthographicSize--;
//      }
//    }
    }

    protected virtual Tile SelectTile(Point p)
    {
        if (pos == p || !board.ContainsTileAt(p))
            return null;

        pos = p;
        tileSelectionIndicator.localPosition = board.Tiles[p].Center;
        return board.Tiles[p];
    }

    protected virtual void SelectNextTile(Point directionVector)
    {
        Point nextTilePos = pos;
        Tile tile = null;
        int attempts = 0;
        while (tile == null && board.IsPointInBounds(nextTilePos))
        {
            tile = SelectTile(nextTilePos += directionVector);

            attempts++;
            if (attempts > 100)
            {
                Debug.Log("Max Attempts. Aborting.");
                break;
            }
        }
    }

    protected virtual Unit GetUnit(Point p)
    {
        Tile t = board.GetTile(p);
        GameObject content = t != null ? t.content : null;
        return content != null ? content.GetComponent<Unit>() : null;
    }

    protected virtual void RefreshPrimaryStatPanel(Point p)
    {
        Unit target = GetUnit(p);
        if (target != null)
        {
            statPanelController.ShowPrimary(target.gameObject);
        }
        else
        {
            //statPanelController.HidePrimary();
        }
    }

    protected virtual bool DidPlayerWin()
    {
        return _owner.GetComponent<BaseVictoryCondition>().Victor == Alliances.Ally;
    }

    protected virtual bool IsBattleOver()
    {
        return _owner.GetComponent<BaseVictoryCondition>().Victor != Alliances.None;
    }

    protected void EndCurrentTurn()
    {
        if (_owner.TurnIsEnding())
        {
            return;
        }

        _owner.AddCommandToQueue(new Command(CommandTypes.EndTurn));
        abilityMenuPanelController.SetEndButtonVisible(false);
    }
}