﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;

public class InitBattleState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {

        _owner.location.Initialize(GM.BattleLocationData);
//        Point p = new Point(board.WorldData.Tiles[0].Position.x, board.WorldData.Tiles[0].Position.y);
//        SelectTile(p);
        SpawnUnits();
        //SpawnTestUnits();
        AddVictoryCondition();
        _owner.round = _owner.gameObject.AddComponent<TurnOrderController>().Round();

//        _owner.settingsMenuPanel.SetupForGameBattle();
        yield return null;

        //owner.ChangeState<CutSceneState>();
        _owner.ChangeState<SelectUnitState>();
    }

    void SpawnUnits()
    {
        GameObject unitContainer = new GameObject("Units");
        unitContainer.transform.SetParent(_owner.transform);

        List<Tile> playerStartingTiles = new List<Tile>();
        List<Tile> enemyStartingTiles = new List<Tile>();
        for (int i = 0; i < board.PlayerStartingTiles.Count; i++)
        {
            playerStartingTiles.Add(board.GetTile(board.PlayerStartingTiles[i]));
        }

        for (int i = 0; i < board.EnemyStartingTiles.Count; i++)
        {
            enemyStartingTiles.Add(board.GetTile(board.EnemyStartingTiles[i]));
        }

        int count = 0;
        LocationData battleLocationData = NetworkManager.CurrentWorld.GetLocation("battle_field_01");
//        foreach (var kvp in GM.BattleLocationData.players)
//        {
//            PlayerHandle handle = kvp.Value;
//            _owner.Spawn(handle.PlayerData.LeaderData, playerStartingTiles[count].GridPosition, Alliances.Ally);
//            count++;
//        }

        count = 0;
        foreach (var enemyData in GM.Enemies)
        {
            _owner.Spawn(enemyData, enemyStartingTiles[count].GridPosition, Alliances.Enemy);
            count++;
        }

        SelectTile(units[0].CurrentTile.GridPosition);
    }

    void SpawnTestUnits()
    {
        GameObject unitContainer = new GameObject("Units");
        unitContainer.transform.SetParent(_owner.transform);

        JobType[] jobs = {JobType.Archer, JobType.Soldier, JobType.BlackMage, JobType.WhiteMage};
        int playerTeamCount = jobs.Length;

        //List<Tile> locations = new List<Tile>(board.tiles.Values);
        List<Tile> playerStartingTiles = new List<Tile>();
        List<Tile> enemyStartingTiles = new List<Tile>();
        for (int i = 0; i < board.PlayerStartingTiles.Count; i++)
        {
            playerStartingTiles.Add(board.GetTile(board.PlayerStartingTiles[i]));
        }

        for (int i = 0; i < board.EnemyStartingTiles.Count; i++)
        {
            enemyStartingTiles.Add(board.GetTile(board.EnemyStartingTiles[i]));
        }

        // Uncomment to see the starting tiles.
        //board.SelectTiles(playerStartingTiles);

        for (int i = 0; i < playerTeamCount * 2; ++i)
        {
            int level = Random.Range(9, 12);
            //int level = 1;

            Alliances alliances;
            bool human = true; // default this to true if you want the player to control the enemies.
            Tile tile;
            Direction dir;
            if (i < playerTeamCount)
            {
                alliances = Alliances.Ally;
                human = true;
                tile = playerStartingTiles[i];
                dir = Direction.North;
            }
            else
            {
                alliances = Alliances.Enemy;
                int random = Random.Range(0, enemyStartingTiles.Count);
                tile = enemyStartingTiles[random];
                enemyStartingTiles.RemoveAt(random);
                dir = Direction.South;
            }

            Unit unit = UnitFactory.Create(jobs[i % (jobs.Length)], level, alliances);
            unit.transform.SetParent(unitContainer.transform);

            tile.Place(unit);
            unit.Facing = dir;
            unit.Match();

            units.Add(unit);
        }

        SelectTile(units[0].CurrentTile.GridPosition);
    }

    void SpawnSpecificTestUnits()
    {
        string[] recipes =
        {
            "Alaois",
            "Hania",
            "Kamau",
            "Enemy Orc Rogue",
            "Enemy Orc Warrior",
            "Enemy Orc Wizard"
        };

        GameObject unitContainer = new GameObject("Units");
        unitContainer.transform.SetParent(_owner.transform);

        List<Tile> locations = new List<Tile>(board.Tiles.Values);
        for (int i = 0; i < recipes.Length; ++i)
        {
            int level = Random.Range(9, 12);
            GameObject instance = UnitFactory.Create(recipes[i], level);
            instance.transform.SetParent(unitContainer.transform);

            int random = Random.Range(0, locations.Count);
            Tile randomTile = locations[random];
            locations.RemoveAt(random);

            Unit unit = instance.GetComponent<Unit>();
            randomTile.Place(unit);
            unit.Facing = (Direction) Random.Range(0, 4);
            unit.Match();

            units.Add(unit);
        }

        SelectTile(units[0].CurrentTile.GridPosition);
    }

    void AddVictoryCondition()
    {
        //DefeatTargetVictoryCondition vc = owner.gameObject.AddComponent<DefeatTargetVictoryCondition>();
        //Unit enemy = units[units.Count - 1];
        //vc.target = enemy;
        //Health health = enemy.GetComponent<Health>();
        //health.MinHP = 10;

        _owner.gameObject.AddComponent<DefeatAllEnemiesVictoryCondition>();
    }
}