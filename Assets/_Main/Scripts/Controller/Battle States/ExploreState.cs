using UnityEngine;
using System.Collections;
using MCB.Common;

public class ExploreState : BattleState
{
    public override void Enter()
    {
        base.Enter();
        RefreshPrimaryStatPanel(pos);
    }

    public override void Exit()
    {
        base.Exit();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        SelectNextTile(e.info);
        RefreshPrimaryStatPanel(pos);
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        if (e.info == GameConstants.INPUT_FIRE1)
        {
            _owner.ChangeState<CommandSelectionState>();
        }
    }
}