﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;

public class ConfirmAbilityTargetState : BattleState
{
    List<Tile> tiles;
    AbilityArea aa;
    int index = 0;

    public override void Enter()
    {
        base.Enter();
        aa = currentTurn.ability.GetComponent<AbilityArea>();
        tiles = aa.GetTilesInArea(board, pos);
        board.HighlightTiles(tiles);
        FindTargets();
        RefreshPrimaryStatPanel(currentTurn.actor.CurrentTile.GridPosition);
        if (currentTurn.targets.Count > 0)
        {
            if (driver.Current == Drivers.Human)
            {
                //hitSuccessIndicator.Show();
            }

            SetTarget(0);
        }

        if (driver.Current == Drivers.Computer)
            StartCoroutine(ComputerDisplayAbilitySelection());
    }

    public override void Exit()
    {
        base.Exit();
        board.DeselectTiles(tiles);
        hitSuccessIndicator.Hide();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        if (e.info.y > 0 || e.info.x > 0)
            SetTarget(index + 1);
        else
            SetTarget(index - 1);
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        if (e.info == GameConstants.INPUT_FIRE1)
        {
            if (currentTurn.targets.Count > 0)
            {
                _owner.ChangeState<PerformAbilityState>();
            }
        }
        else
        {
            // TODO: Check for keyboard & controller input vs mouse input to
            // determine whether the current actor should be selected and the
            // secondary panel should be hidden.

            // For now default to mouse input case.
            SelectTile(currentTurn.actor.CurrentTile.GridPosition);

            _owner.ChangeState<AbilityTargetState>();
        }
    }

    void FindTargets()
    {
        currentTurn.targets = new List<Tile>();
        for (int i = 0; i < tiles.Count; ++i)
        {
            if (currentTurn.ability.IsTarget(tiles[i]))
            {
                currentTurn.targets.Add(tiles[i]);
            }
        }
    }

    void SetTarget(int target)
    {
        index = target;
        if (index < 0)
            index = currentTurn.targets.Count - 1;
        if (index >= currentTurn.targets.Count)
            index = 0;

        if (currentTurn.targets.Count > 0)
        {
            //RefreshSecondaryStatPanel(currentTurn.targets[index].pos);
            //UpdateHitSuccessIndicator();
            SelectTile(currentTurn.targets[index].GridPosition);
        }
    }

    void UpdateHitSuccessIndicator()
    {
        int chance = 0;
        int amount = 0;
        Tile target = currentTurn.targets[index];

        Transform obj = currentTurn.ability.transform;
        for (int i = 0; i < obj.childCount; ++i)
        {
            AbilityEffectTarget targeter = obj.GetChild(i).GetComponent<AbilityEffectTarget>();
            if (targeter.IsTarget(target))
            {
                HitRate hitRate = targeter.GetComponent<HitRate>();
                chance = hitRate.Calculate(target);

                BaseAbilityEffect effect = targeter.GetComponent<BaseAbilityEffect>();
                amount = effect.Predict(target);
                break;
            }
        }

        hitSuccessIndicator.SetStats(chance, amount);
    }

    IEnumerator ComputerDisplayAbilitySelection()
    {
        _owner.battleMessageController.Display(currentTurn.ability.name);
        yield return new WaitForSeconds(2f);
        _owner.ChangeState<PerformAbilityState>();
    }
}