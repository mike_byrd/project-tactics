using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;

public class MoveTargetState : BattleState
{
    private List<Tile> _tiles;
    private List<Tile> _waypoints;

    protected override void AddListeners()
    {
        base.AddListeners();
        TileSurface.MouseDown += OnTileSurfaceMouseDown;
        TileSurface.MouseEntered += OnTileHover;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        TileSurface.MouseDown -= OnTileSurfaceMouseDown;
        TileSurface.MouseEntered -= OnTileHover;
    }

    public override void Enter()
    {
        base.Enter();
        Movement mover = currentTurn.actor.GetComponent<Movement>();

        _tiles = mover.GetTilesInRange(board);
        _waypoints = new List<Tile>();
        //board.SelectTiles(tiles);
        RefreshPrimaryStatPanel(pos);

        if (driver.Current == Drivers.Computer)
        {
            StartCoroutine(ComputerHighlightMoveTarget());
        }
        else
        {
            //abilityMenuPanelController.Show();
        }
    }

    public override void Exit()
    {
        base.Exit();
        board.DeselectTiles(_tiles);
        board.DeselectTiles(_waypoints);
        _tiles = null;
        _waypoints = null;
        //statPanelController.HidePrimary();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        // Try to get the next tile, until you reach the end of the board.
        SelectNextTile(e.info);

        RefreshPrimaryStatPanel(pos);
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        if (e.info == GameConstants.INPUT_FIRE1)
        {
            if (_tiles.Contains(_owner.SelectedTile))
            {
                _owner.ChangeState<MoveSequenceState>();
            }
        }
        else
        {
            _owner.ChangeState<CommandSelectionState>();
        }
    }

    void OnTileSurfaceMouseDown(Tile selectedTile)
    {
        Debug.Log(string.Format("Tile Selected: [{0}, {1}]", selectedTile.GridPosition.x, selectedTile.GridPosition.y));
    }

    private void OnTileHover(Tile selectedTile)
    {
        Debug.Log($"Hovered: {selectedTile.GridPosition} | Tiles: {_tiles.Count}");

        board.DeselectTiles(_tiles);
        board.DeselectTiles(_waypoints);
        if (_tiles.Contains(selectedTile))
        {
            Movement m = currentTurn.actor.GetComponent<Movement>();
            _waypoints = m.GetWayPoints(selectedTile);
            board.HighlightTiles(_waypoints);
        }
        else
        {
            board.DeselectTiles(_tiles);
            board.DeselectTiles(_waypoints);
        }
    }

    IEnumerator ComputerHighlightMoveTarget()
    {
        Point cursorPos = pos;
        while (cursorPos != currentTurn.plan.moveLocation)
        {
            if (cursorPos.x < currentTurn.plan.moveLocation.x) cursorPos.x++;
            if (cursorPos.x > currentTurn.plan.moveLocation.x) cursorPos.x--;
            if (cursorPos.y < currentTurn.plan.moveLocation.y) cursorPos.y++;
            if (cursorPos.y > currentTurn.plan.moveLocation.y) cursorPos.y--;
            SelectTile(cursorPos);
            yield return new WaitForSeconds(0.25f);
        }

        yield return new WaitForSeconds(0.5f);
        _owner.ChangeState<MoveSequenceState>();
    }
}