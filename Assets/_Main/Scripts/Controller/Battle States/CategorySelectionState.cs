﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public class CategorySelectionState : BaseAbilityMenuState
{
  public override void Enter()
  {
    base.Enter();
    statPanelController.ShowPrimary(currentTurn.actor.gameObject);
  }

  public override void Exit()
  {
    base.Exit();
  }

  protected override void LoadMenu()
  {
    //if (menuOptions == null)
    //  menuOptions = new List<string>();
    //else
    //  menuOptions.Clear();

    //menuTitle = "Action";
    //menuOptions.Add("Attack");

    //AbilityCatalog catalog = currentTurn.actor.GetComponentInChildren<AbilityCatalog>();
    //for (int i = 0; i < catalog.CategoryCount(); ++i)
    //  menuOptions.Add(catalog.GetCategory(i).name);

    //abilityMenuPanelController.Show(menuTitle, menuOptions);
  }

  protected override void Confirm()
  {
    //if (abilityMenuPanelController.selection == 0)
    //  Attack();
    //else
    //  SetCategory(abilityMenuPanelController.selection - 1);
  }

  protected override void Cancel()
  {
    _owner.ChangeState<CommandSelectionState>();
  }

  void Attack()
  {
    currentTurn.ability = currentTurn.actor.GetComponentInChildren<Ability>();
    _owner.ChangeState<AbilityTargetState>();
  }

  void SetCategory(int index)
  {
    ActionSelectionState.category = index;
    _owner.ChangeState<ActionSelectionState>();
  }
}
