﻿using UnityEngine;
using System.Collections;
using MCB;
using MCB.Common;

public class EndFacingState : BattleState
{
    Direction startDir;

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(OnFacingArrowClicked, StatPanelController.ON_ARROW_CLICKED);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(OnFacingArrowClicked, StatPanelController.ON_ARROW_CLICKED);
    }

    public override void Enter()
    {
        base.Enter();

        startDir = currentTurn.actor.Facing;
        SelectTile(currentTurn.actor.CurrentTile.GridPosition);
        _owner.facingIndicator.gameObject.SetActive(true);
        _owner.facingIndicator.SetDirection(currentTurn.actor.Facing);
        if (driver.Current == Drivers.Computer)
        {
            StartCoroutine(ComputerControl());
        }
    }

    public override void Exit()
    {
        _owner.facingIndicator.gameObject.SetActive(false);
        base.Exit();
    }

    void OnFacingArrowClicked(object sender, object args)
    {
        Direction direction = (Direction) args;
        currentTurn.actor.Facing = direction;
        currentTurn.actor.Match();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        currentTurn.actor.Facing = e.info.GetDirection();
        currentTurn.actor.Match();
        _owner.facingIndicator.SetDirection(currentTurn.actor.Facing);
    }

    protected override void OnFire(object sender, InfoEventArgs<string> e)
    {
        switch (e.info)
        {
            case GameConstants.INPUT_FIRE1:
            case GameConstants.INPUT_FIRE3: // TODO: Temporary spacebar code.
                _owner.ChangeState<SelectUnitState>();
                break;
            case GameConstants.INPUT_FIRE2:
                // Don't allow the player to undo if they have acted.
                if (currentTurn.hasUnitActed)
                {
                    return;
                }

                currentTurn.actor.Facing = startDir;
                currentTurn.actor.Match();
                _owner.ChangeState<CommandSelectionState>();
                break;
        }
    }

    IEnumerator ComputerControl()
    {
        yield return new WaitForSeconds(0.5f);
        currentTurn.actor.Facing = _owner.cpu.DetermineEndFacingDirection();
        currentTurn.actor.Match();
        _owner.facingIndicator.SetDirection(currentTurn.actor.Facing);
        yield return new WaitForSeconds(0.5f);
        _owner.ChangeState<SelectUnitState>();
    }
}