﻿using System;
using UnityEngine;
using System.Collections;
using MCB.Common;
using MCB.MapEditor;
using ProjectTactics;
using TileData = ProjectTactics.DataModels.TileData;

namespace MCB
{
    public class Tile : MonoBehaviour
    {
        #region Const

        public const float StepHeight = 0.125f;
        public const float SurfaceOffsetY = 0.01f;

        #endregion

        #region Fields / Properties

        public Point GridPosition { get; set; }
        public int Height { get; private set; }

        public Vector3 Center => _surface.transform.position;

        public float WorldHeight => Height * StepHeight;

        public GameObject content;
        [SerializeField] Renderer display;
        [SerializeField] SpriteRenderer fill;
        [HideInInspector] public Tile prev;
        [HideInInspector] public int distance;

        [SerializeField] private Color selectedTileColor;
        [SerializeField] private Color defaultTileColor;
        [SerializeField] private Color attackHoverColor;
        [SerializeField] private Color outOfSightColor;
        [SerializeField] private Color worldHighlightColor;

        [SerializeField] private Color allyColor;
        [SerializeField] private Color enemyColor;

        #endregion

        TileState _state = TileState.None;
        private TileSurface _surface;

        public TileState state
        {
            get { return _state; }
            set
            {
                _state = value;
                UpdateHighlight();
            }
        }

        public bool IsWalkable { get; set; } = true;

        private void Awake()
        {
            _surface = GetComponentInChildren<TileSurface>();
        }

        private void Start()
        {
            state = TileState.None;
        }

        private void UpdateHighlight()
        {
            switch (_state)
            {
                case TileState.None:
                    fill.gameObject.SetActive(false);
                    break;
                case TileState.BattleHighlighted:
                    fill.gameObject.SetActive(true);
                    fill.color = selectedTileColor;
                    break;
                case TileState.WorldHighlighted:
                    fill.gameObject.SetActive(true);
                    fill.color = worldHighlightColor;
                    break;
                case TileState.Selected:
                    fill.gameObject.SetActive(true);
                    fill.color = attackHoverColor;
                    break;
                case TileState.OutOfSight:
                    fill.gameObject.SetActive(true);
                    fill.color = outOfSightColor;
                    break;
                case TileState.Enemy:
                    fill.gameObject.SetActive(true);
                    fill.color = enemyColor;
                    break;
                case TileState.Ally:
                    fill.gameObject.SetActive(true);
                    fill.color = allyColor;
                    break;
            }
        }

        #region Public

        public void Grow()
        {
            Height++;
            Match();
        }

        public void Shrink()
        {
            Height--;
            Match();
        }

        public void Load(Point p, int h)
        {
            GridPosition = p;
            Height = h;
            Match();
        }

        public void Load(Vector3 v)
        {
            Load(new Point((int) v.x, (int) v.z), (int) v.y);
        }

        public void Load(Point p)
        {
            display.material = GM.TileMaterialDefault;
            GridPosition = p;
            Match();
        }

        #endregion

        #region Private

        void Match()
        {
            transform.position = new Vector3(GridPosition.x, 0, GridPosition.y);
            _surface.transform.localPosition = new Vector3(0, WorldHeight + SurfaceOffsetY, 0);

            if (Height > 0)
            {
                display.transform.parent.localScale = new Vector3(1, WorldHeight, 1);
            }
            else
            {
                display.transform.parent.localScale = new Vector3(1, SurfaceOffsetY * 0.5f, 1);
            }
        }

        public void HighlightForInteract(bool highlight)
        {
            if (highlight)
            {
                state = TileState.WorldHighlighted;
            }
            else
            {
                state = TileState.None;
            }
        }

        public void SetHeight(int height)
        {
            Height = height;
            Match();
        }

        public void SetVisible(bool visible)
        {
            display.enabled = visible;
        }

        #endregion

        public void UpdateTileData(TileData data)
        {
            IsWalkable = data.IsWalkable;

            if (!data.IsWalkable)
            {
                display.material = GM.TileMaterialUnwalkable;
            }
            else
            {
                switch (data.Type)
                {
                    case TileType.StartAlly:
                        display.material = GM.TileMaterialAlly;
                        break;
                    case TileType.StartEnemy:
                        display.material = GM.TileMaterialEnemy;
                        break;
                    case TileType.Travel:
                        display.material = GM.TileMaterialTravel;
                        break;
                    default:
                        display.material = GM.TileMaterialDefault;
                        break;
                }
            }

            SetHeight(data.Height);

            _surface.gameObject.SetActive(IsWalkable);
        }

        public void Place(Unit unit, bool updateTransform = true)
        {
            // Clear the old tiles
            if (unit.CurrentTile)
            {
                unit.CurrentTile.content = null;
                unit.CurrentTile = null;
            }

            // Update the reference for both the unit and this tile.
            unit.CurrentTile = this;
            content = unit.gameObject;

            // Update the tile positions.
            unit.GridPosition = GridPosition;

            if (updateTransform)
            {
                unit.transform.position = Center;
            }
        }
    }
}