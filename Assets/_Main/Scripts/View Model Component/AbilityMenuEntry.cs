﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MCB;

public class AbilityMenuEntry : MonoBehaviour
{
  #region Enums
  [System.Flags]
  enum States
  {
    None = 0,
    Selected = 1 << 0,
    Locked = 1 << 1
  }
  #endregion

  public const string ON_SELECT = "AbilityMenuEntry.OnSelect";
  public const string ON_HOVER = "AbilityMenuEntry.OnHover";
  public const string ON_EXIT = "AbilityMenuEntry.OnExit";
  public const string ON_END_TURN = "AbilityMenuEntry.OnEndTurn";

  public Ability ability { get; private set; }
  DurationAbilityCooldown cooldown;
  public int entryIndex = 0;

  States state;

  [SerializeField] Image image;
  [SerializeField] Sprite normalSprite;
  [SerializeField] Sprite selectedSprite;
  [SerializeField] Sprite disabledSprite;
  [SerializeField] Text label;
  Outline outline;

  Color lockedColor = new Color(.39f, .39f, .39f); // #646464FF

  #region Properties
  public string Title {
    get { return label.text; }
    set { label.text = value; }
  }

  public bool IsLocked {
    get { return (State & States.Locked) != States.None; }
    set {
      if (value) {
        State |= States.Locked;
        image.color = lockedColor;
      } else {
        State &= ~States.Locked;
        image.color = Color.white;
      }
    }
  }

  public bool IsSelected {
    get { return (State & States.Selected) != States.None; }
    set {
      if (value)
        State |= States.Selected;
      else
        State &= ~States.Selected;
    }
  }

  States State {
    get { return state; }
    set {
      if (state == value) {
        return;
      }

      state = value;
    }
  }
  #endregion

  #region MonoBehaviour


  #endregion

  #region Public
  public void Reset()
  {
    State = States.None;
    ability = null;
    label.text = "";
    image.sprite = null;
    image.enabled = false;
  }

  public void AssignAbility(Ability ability)
  {
    this.ability = ability;
    label.text = ability.name;
    if (ability.sprite) {
      image.sprite = ability.sprite;
      image.enabled = true;
    }

    cooldown = ability.GetComponent<DurationAbilityCooldown>();

    UpdateLockedState();
  }

  public void UpdateLockedState()
  {
    if (!ability) {
      return;
    }

    if (ability.CanPerform()) {
      IsLocked = false;
      image.color = Color.white;
      label.text = "";
      label.gameObject.SetActive(false);
    } else {
      IsLocked = true;
      image.color = lockedColor;
      if (cooldown && cooldown.isActive) {
        label.text = cooldown.remainingTurns.ToString();
        label.gameObject.SetActive(true);
      }
    }
  }
  #endregion
}
