﻿using UnityEngine;
using System.Collections;
using Cinemachine;

public class CameraRig : MonoBehaviour
{
  public static string OnCameraPanActivated = "CameraRig.OnCameraPanActivated";  
  public static string OnMouseEnterBoard = "CameraRig.OnMouseEnterBoard";
  public static string OnMouseExitBoard = "CameraRig.OnMouseExitBoard";

  public float speed = 3f;
  public float maxPanDistance = 300f;  
  public bool reversePanDirection = false;
  public Transform follow;
  Transform _transform;

  public CameraMode mode { get; private set; }
  Vector3 followPos;

  Vector3 lastMousePos;
  Vector3 panStartPos;
  Vector3 startTransformPos;

  Ray ray;
  RaycastHit hit;
  Camera cam;

  [SerializeField]
  private CinemachineVirtualCamera _virtualCamera;
  public CinemachineVirtualCamera virtualCamera { get { return _virtualCamera; } }

  [SerializeField]
  private CinemachineExternalCamera _externalCamera;
  public CinemachineExternalCamera externalCamera { get { return _externalCamera; } }

  void Awake()
  {
    _transform = transform;
    mode = CameraMode.FollowingUnit;
    cam = Camera.main;
  }

  bool isMouseOnBoard = false;

  void Update()
  {
    bool isRightMouseDown = Input.GetMouseButton(1);
    Vector3 mousePos = Input.mousePosition;
    bool mousePositionChanged = mousePos != lastMousePos;
    bool isCameraReady = virtualCamera.Follow != null;

    if(isCameraReady && isRightMouseDown && (mode != CameraMode.PreparingForPan && mode != CameraMode.Panning)) {
      mode = CameraMode.PreparingForPan;
    } else if(!isRightMouseDown && (mode == CameraMode.Panning)) {      
      mode = CameraMode.FollowingUnit;

      virtualCamera.enabled = true;      
      this.PostNotification(OnCameraPanActivated, startTransformPos);      
    } else if(isRightMouseDown && mode == CameraMode.PreparingForPan && mousePositionChanged) {
      mode = CameraMode.Panning;      
      panStartPos = mousePos;
      startTransformPos = _transform.position;

      virtualCamera.enabled = false;
      this.PostNotification(OnCameraPanActivated, startTransformPos);
    }

    ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    LayerMask mask = 1 << LayerMask.NameToLayer("Tile");
    bool tileHit = Physics.Raycast(ray, out hit, 1000, mask);
    if(tileHit && !isMouseOnBoard) {
      isMouseOnBoard = true;
      this.PostNotification(OnMouseEnterBoard);
    } else if (!tileHit && isMouseOnBoard) {
      isMouseOnBoard = false;
      this.PostNotification(OnMouseExitBoard);
    }

    lastMousePos = mousePos;
  }

  void LateUpdate()
  {
    if(mode == CameraMode.Panning && Input.mousePosition != panStartPos) {
      Vector3 heading = (Input.mousePosition - panStartPos);
      float mouseDistance = Vector3.Distance(panStartPos, Input.mousePosition);
      float transformDistance = Vector3.Distance(startTransformPos, _transform.position);
      if(reversePanDirection) {
        Vector3 newPos = startTransformPos - (heading.normalized * speed * (mouseDistance / maxPanDistance));
        _transform.position = newPos;
      } else {
        Vector3 newPos = startTransformPos + (heading.normalized * (mouseDistance / maxPanDistance));
        _transform.position = newPos;
      }
    }
  }
}