﻿using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AbilityInfoCard : UIView
{
    [SerializeField] TMP_Text nameLabel;

    [SerializeField] TMP_Text descriptionLabel;

    [SerializeField] TMP_Text costLabel;

    [SerializeField] TMP_Text effectLabel;

    [SerializeField] RectTransform _rectTransform;

    public RectTransform rectTransform
    {
        get { return _rectTransform; }
    }

    public void UpdateInfo(AbilityMenuEntry abilityEntry)
    {
        string keyInput = GetKeyInput(abilityEntry.entryIndex);
        nameLabel.text = string.Format("{0}: <b>[{1}]</b>", abilityEntry.ability.name, keyInput);

        AbilityMagicCost cost = abilityEntry.ability.GetComponent<AbilityMagicCost>();
        if (cost)
        {
            // TODO: Change the color if the unit can't afford the ability.
            string abilityString = $"AP: <color=#48D662>{cost.amount}</color>";
            costLabel.text = abilityString;
        }
        else
        {
            costLabel.text = "No Cost";
        }

        descriptionLabel.text = abilityEntry.ability.description;
    }

    string GetKeyInput(int entryIndex)
    {
        return (entryIndex + 1).ToString();
    }

    public void SetVisible(bool visible)
    {
        if (visible)
        {
            Show(true);
        }
        else
        {
            Hide();
        }
    }

    public void Clear()
    {
        nameLabel.text = "";
        descriptionLabel.text = "";
        costLabel.text = "";
        effectLabel.text = "";
    }
}