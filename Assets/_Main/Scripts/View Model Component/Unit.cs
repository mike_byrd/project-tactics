﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using ProjectTactics;
using ProjectTactics.DataModels;
using TMPro;

namespace MCB
{
    public class Unit : MonoBehaviour
    {
        public static event Action<Unit> MouseEntered;
        public static event Action<Unit> MouseExited;
        public static event Action<Unit> Clicked;

        public Transform cameraTarget;

        public GenderType gender { get; set; }

        public Tile CurrentTile { get; set; }
        public Tile DestinationTile { get; set; }

        public Animator Anim => ModelVisual.Anim;
        public BoxCollider Collider => ModelVisual.Collider;

        public Direction Facing
        {
            get => _movement.Facing;
            set => _movement.SetFacing(value);
        }

        public bool IsBusy { get; set; }

        [SerializeField] private UnitHUD _hud;
        public UnitHUD Hud => _hud;

        private string _displayName;

        /// <summary>
        /// Also changes the name of the gameObject.
        /// </summary>
        public string DisplayName
        {
            get => _displayName;
            set
            {
                _displayName = value;
                name = value;
            }
        }

        public int Level => Stats[StatTypes.LVL];
        public UnitModelVisual ModelVisual { get; private set; }
        public PlayerHandle Owner { get; set; }
        public bool IsMouseHovering { get; private set; }

        private Movement _movement;
        public Movement Movement
        {
            get
            {
                if (!_movement)
                {
                    _movement = GetComponent<Movement>();
                }

                return _movement;
            }
        }

        private AbilityCatalog _abilityCatalog;
        public AbilityCatalog AbilityCatalog
        {
            get
            {
                if (!_abilityCatalog)
                {
                    _abilityCatalog = GetComponentInChildren<AbilityCatalog>();
                }

                return _abilityCatalog;
            }
        }

        private Alliance _alliance;
        public Alliance Alliance
        {
            get
            {
                if (!_alliance)
                {
                    _alliance = GetComponent<Alliance>();
                }

                return _alliance;
            }
        }

        private Job _job;
        public Job Job
        {
            get
            {
                if (!_job)
                {
                    _job = GetComponentInChildren<Job>();
                }

                return _job;
            }
        }

        private Stats _stats;
        public Stats Stats
        {
            get
            {
                if (!_stats)
                {
                    _stats = GetComponent<Stats>();
                }

                return _stats;
            }
        }

        public string ID { get; set; }

        public Point GridPosition;

        void OnEnable()
        {
            this.AddObserver(OnEquippedItem, Equipment.EquippedNotification);
            this.AddObserver(OnUnEquippedItem, Equipment.UnEquippedNotification);
        }

        void OnDisable()
        {
            this.RemoveObserver(OnEquippedItem, Equipment.EquippedNotification);
            this.RemoveObserver(OnUnEquippedItem, Equipment.UnEquippedNotification);
        }

        private void Awake()
        {
            ModelVisual = GetComponent<UnitModelVisual>();
        }

        private bool _isMouseDownOverUnit = false;
        private void Update()
        {
            if (!IsMouseHovering)
            {
                return;
            }

            if (Input.GetMouseButtonUp(1))
            {
                if (Util.IsPointerOverUI())
                {
                    return;
                }

                if (_isMouseDownOverUnit)
                {
                    Clicked?.Invoke(this);
                    _isMouseDownOverUnit = false;
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                if (Util.IsPointerOverUI())
                {
                    return;
                }

                _isMouseDownOverUnit = true;
            }
        }

        private void OnMouseEnter()
        {
            if (Util.IsPointerOverUI())
            {
                return;
            }

            MouseEntered?.Invoke(this);

            IsMouseHovering = true;
            Hud.namePlate.Show();
        }

        private void OnMouseExit()
        {
            MouseExited?.Invoke(this);
            _isMouseDownOverUnit = false;
            IsMouseHovering = false;
            Hud.namePlate.Hide();
        }

        public void Turn(Direction direction)
        {
            Facing = direction;
            Match();
        }

        public void Match()
        {
            transform.localPosition = CurrentTile.Center;
            transform.localEulerAngles = _movement.Facing.ToEuler();
        }

        void OnEquippedItem(object sender, object args)
        {
            Equipment equipment = sender as Equipment;
            if (equipment.gameObject != this.gameObject)
            {
                return;
            }

            Equippable item = args as Equippable;
            switch (item.defaultSlots)
            {
                case EquipSlots.Primary:
                    Transform mainHand;
                    // Bow animation is performed with the left hand.
                    if (item.itemType == ItemType.Bow)
                    {
                        mainHand = ModelVisual.LeftHand;
                    }
                    else
                    {
                        mainHand = ModelVisual.RightHand;
                    }

                    item.transform.SetParent(mainHand, false);
                    item.gameObject.SetActive(true);
                    break;
                case EquipSlots.Secondary:
                    item.transform.SetParent(ModelVisual.LeftHand, false);
                    item.gameObject.SetActive(true);
                    break;
                case EquipSlots.Head:
                    ModelVisual.HideHair();

                    item.transform.SetParent(ModelVisual.Head.transform, false);
                    item.gameObject.SetActive(true);
                    break;
                default:
                    break;
            }
        }

        void OnUnEquippedItem(object sender, object args)
        {
            Equipment equipment = sender as Equipment;
            if (equipment.gameObject != this.gameObject)
            {
                return;
            }

            Equippable item = args as Equippable;
            item.gameObject.SetActive(false);

            if (item.defaultSlots == EquipSlots.Head)
            {
                ModelVisual.ShowHair();
            }
        }

        public void SetColliderEnabled(bool enabled)
        {
            if (enabled)
            {
                // TODO: Do this better somehow...
                Collider.size = new Vector3(.95f, 1.1f, .95f);
            }
            else
            {
                Collider.size = Vector3.zero;
            }
        }
    }
}