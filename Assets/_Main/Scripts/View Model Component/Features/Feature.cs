﻿using UnityEngine;
using System.Collections;

/// <summary>
/// - A feature represents some ability to modifiy a thing. An item will have a feature, for example. 
/// 
/// - A feature can be activated for a time and then be deactivated. This would be the case with 
///   equipment – equip a sword for an attack boost but when you un-equip the sword your attack stat 
///   drops back down. Activate and Deactivate are exposed for this purpose.
///   
/// - A feature can have a one-shot(permanent) application.This would be the case when you consume 
///   a health potion – you get a boost to your hit point stat which doesn’t need to be un-done by 
///   the item. The method Apply is exposed for this purpose.
///   
/// - The base class applications are not virtual, so they will always work in a very specific way. 
///   Concrete subclasses implement what happens when the feature is activated in the OnApply and 
///   OnRemove methods which were left empty.
/// </summary>
public abstract class Feature : MonoBehaviour
{
	#region Fields / Properties
	protected GameObject _target { get; private set; }
	#endregion

	#region Public
	public void Activate (GameObject target)
	{
		if (_target == null)
		{
			_target = target;
			OnApply();
		}
	}

	public void Deactivate ()
	{
		if (_target != null)
		{
			OnRemove();
			_target = null;
		}
	}

	public void Apply (GameObject target)
	{
		_target = target;
		OnApply();
		_target = null;
	}
	#endregion

	#region Private
	protected abstract void OnApply ();
	protected virtual void OnRemove () {}
	#endregion
}