using UnityEngine;
using System.Collections;

/// <summary>
/// This simple component can be applied to any stat type, either as a good or bad modifier,
/// and is compatible both with consumable and equippable items. 
/// </summary>
public class StatModifierFeature : Feature
{
	#region Fields / Properties
	public StatTypes type;
	public int amount;

	Stats stats 
	{ 
		get 
		{ 
			return _target.GetComponentInParent<Stats>();
		}
	}
	#endregion

	#region Protected
	protected override void OnApply ()
	{
		stats[type] += amount;
	}

	protected override void OnRemove ()
	{
		stats[type] -= amount;
	}
	#endregion
}