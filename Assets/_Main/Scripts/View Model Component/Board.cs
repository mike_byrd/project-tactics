﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MCB;
using MCB.Common;
using MCB.MapEditor;
using ProjectTactics.DataModels;
using UnityEditor;
using GridTile = MCB.MapEditor.GridTile;
using Tile = MCB.Tile;
using TileData = MCB.MapEditor.TileData;

public class Board : MonoBehaviour
{
    #region Fields / Properties

    [SerializeField] GameObject tilePrefab;

    public Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();

    public LocationData LocationData { get; private set; }

    // TODO: Currently on by default for testing. Turn off by default and add to game settings.
    public bool IsShowingTacticalView { get; private set; } = true;

    public Point min
    {
        get { return _min; }
    }

    public Point max
    {
        get { return _max; }
    }

    public List<Point> PlayerStartingTiles { get; set; }
    public List<Point> EnemyStartingTiles { get; set; }

    Point _min;
    Point _max;

    Point[] dirs = new Point[4]
    {
        new Point(0, 1),
        new Point(0, -1),
        new Point(1, 0),
        new Point(-1, 0)
    };

    #endregion

    private List<Tile> selectedTiles = new List<Tile>();

    #region Public

    public void Load()
    {
        TextAsset mapFile = Resources.Load<TextAsset>("Json/Map Data/battle_field_01");
        if (!mapFile)
        {
            return;
        }

        LocationDataModel dataModel = JsonUtility.FromJson<LocationDataModel>(mapFile.text);
        LocationData = new LocationData(dataModel);

        _min = new Point(int.MaxValue, int.MaxValue);
        _max = new Point(int.MinValue, int.MinValue);

        PlayerStartingTiles = new List<Point>();
        EnemyStartingTiles = new List<Point>();

        foreach (ProjectTactics.DataModels.TileData td in LocationData.Tiles)
        {
            if (td.Type == TileType.StartAlly)
            {
                PlayerStartingTiles.Add(td.Position);
            }
            else if (td.Type == TileType.StartEnemy)
            {
                EnemyStartingTiles.Add(td.Position);
            }
        }

        for (int y = 0; y < LocationData.Height; y++)
        {
            for (int x = 0; x < LocationData.Width; x++)
            {
                Tile t = Instantiate(tilePrefab).GetComponent<Tile>();
                t.transform.SetParent(transform);
                t.Load(new Point(x, y));
                tiles.Add(t.GridPosition, t);

                _min.x = Mathf.Min(_min.x, t.GridPosition.x);
                _min.y = Mathf.Min(_min.y, t.GridPosition.y);
                _max.x = Mathf.Max(_max.x, t.GridPosition.x);
                _max.y = Mathf.Max(_max.y, t.GridPosition.y);
            }
        }

        foreach (ProjectTactics.DataModels.TileData td in LocationData.Tiles)
        {
            Tile t = tiles[td.Position];
            t.UpdateTileData(td);
        }
    }

    public Tile GetTile(Point p)
    {
        return tiles.ContainsKey(p) ? tiles[p] : null;
    }

    public List<Tile> Search(Tile start, Func<Tile, Tile, bool> addTile, bool ignoreStartTile = false)
    {
        List<Tile> retValue = new List<Tile>();
        if (!ignoreStartTile)
        {
            retValue.Add(start);
        }

        ClearSearch();
        Queue<Tile> checkNext = new Queue<Tile>();
        Queue<Tile> checkNow = new Queue<Tile>();

        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            Tile t = checkNow.Dequeue();
            for (int i = 0; i < 4; ++i)
            {
                Tile next = GetTile(t.GridPosition + dirs[i]);
                if (next == null || next.distance <= t.distance + 1)
                    continue;

                if (addTile(t, next))
                {
                    next.distance = t.distance + 1;
                    next.prev = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            if (checkNow.Count == 0)
            {
                SwapReference(ref checkNow, ref checkNext);
            }
        }

        return retValue;
    }

    public List<Tile> Search(Point start, Func<Point, bool> addPos)
    {
        List<Point> searchPositions = new List<Point>();
        List<Tile> retValues = new List<Tile>();

        Queue<Point> checkNext = new Queue<Point>();
        Queue<Point> checkNow = new Queue<Point>();

        checkNow.Enqueue(start);

        int attempts = 0;

        HashSet<Point> uniquePoints = new HashSet<Point>();

        while (checkNow.Count > 0)
        {
            if (attempts++ >= 1000)
            {
                Debug.Log("Max Attempts");
                break;
            }

            Point currentPos = checkNow.Dequeue();
            for (int i = 0; i < 4; ++i)
            {
                Point nextPos = currentPos + dirs[i];
                if (!IsPointInBounds(nextPos) || uniquePoints.Contains(nextPos))
                {
                    continue;
                }

                Tile nextTile;
                if (addPos(nextPos))
                {
                    checkNext.Enqueue(nextPos);
                    searchPositions.Add(nextPos);
                    uniquePoints.Add(nextPos);

                    nextTile = GetTile(nextPos);
                    if (nextTile)
                    {
                        retValues.Add(nextTile);
                    }
                }
            }

            if (checkNow.Count == 0)
            {
                SwapReference(ref checkNow, ref checkNext);
            }
        }

        return retValues;
    }

    public void HighlightTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            tiles[i].state = TileState.BattleHighlighted;
        }
    }

    public void SelectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            tiles[i].state = TileState.Selected;
        }
    }

    public void DeselectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            tiles[i].state = TileState.None;
        }
    }

    public void HighlightTile(Tile tile)
    {
        tile.state = TileState.BattleHighlighted;
    }

    public void SelectTile(Tile tile)
    {
        tile.state = TileState.Selected;
    }

    public void DeselectTile(Tile tile)
    {
        tile.state = TileState.None;
    }

    public bool ContainsTileAt(Point p)
    {
        return tiles.ContainsKey(p);
    }

    public bool IsPointInBounds(Point p)
    {
        return p.x >= min.x && p.x <= max.x && p.y >= min.y && p.y <= max.y;
    }

    #endregion

    #region Private

    void ClearSearch()
    {
        foreach (Tile t in tiles.Values)
        {
            t.prev = null;
            t.distance = int.MaxValue;
        }
    }

    void SwapReference(ref Queue<Tile> a, ref Queue<Tile> b)
    {
        Queue<Tile> temp = a;
        a = b;
        b = temp;
    }

    void SwapReference(ref Queue<Point> a, ref Queue<Point> b)
    {
        Queue<Point> temp = a;
        a = b;
        b = temp;
    }

    #endregion

    public void ToggleTacticalView()
    {
        IsShowingTacticalView = !IsShowingTacticalView;
        foreach (KeyValuePair<Point,Tile> kvp in tiles)
        {
            kvp.Value.SetVisible(IsShowingTacticalView);
        }
    }
}