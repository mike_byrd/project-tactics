﻿using UnityEngine;
using System.Collections;
using MCB;

public abstract class BaseAbilityPicker : MonoBehaviour
{
	#region Fields
	protected Unit owner;
	protected AbilityCatalog ac;
	#endregion

	#region MonoBehaviour
	void Start ()
	{
		owner = GetComponentInParent<Unit>();
		ac = owner.GetComponentInChildren<AbilityCatalog>();
	}
	#endregion

	#region Public
	public abstract void Pick (PlanOfAttack plan);
	#endregion
	
	#region Protected
	protected Ability Find (string abilityName)
	{
		for (int i = 0; i < ac.transform.childCount; ++i)
		{
			Transform category = ac.transform.GetChild(i);
			Transform child = category.Find(abilityName);
			if (child != null) {
				Ability ability = child.GetComponent<Ability>();
				AbilityMagicCost abilityMagicCost = ability.GetComponent<AbilityMagicCost>();
				if(!abilityMagicCost) {
					return ability;
				}

				Stats s = GetComponentInParent<Stats>();
				if(s[StatTypes.AP] < abilityMagicCost.amount) {
					Debug.Log("AI Unit did not have enough MP to perform ability: " + ability.name);
					return null;
				}
				
				return ability;
			}
				
		}
		return null;
	}

	protected Ability Default ()
	{
		return owner.GetComponentInChildren<Ability>();
	}
	#endregion
}