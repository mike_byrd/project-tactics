﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public class ComputerPlayer : MonoBehaviour
{
    #region Fields

    BattleScene bc;

    Unit actor
    {
        get { return bc.currentTurn.actor; }
    }

    Alliance alliance
    {
        get { return actor.GetComponent<Alliance>(); }
    }

    Unit nearestFoe;

    #endregion

    #region MonoBehaviour

    void Awake()
    {
        bc = GetComponent<BattleScene>();
    }

    #endregion

    #region Public

    public PlanOfAttack Evaluate()
    {
        PlanOfAttack poa = new PlanOfAttack();
        AttackPattern pattern = actor.GetComponentInChildren<AttackPattern>();
        if (pattern)
            pattern.Pick(poa);
        else
            DefaultAttackPattern(poa);

        if (IsPositionIndependent(poa))
            PlanPositionIndependent(poa);
        else if (IsDirectionIndependent(poa))
            PlanDirectionIndependent(poa);
        else
            PlanDirectionDependent(poa);

        if (poa.ability == null)
            MoveTowardOpponent(poa);

        return poa;
    }

    #endregion

    #region Private

    void DefaultAttackPattern(PlanOfAttack poa)
    {
        // Just get the first "Attack" ability
        poa.ability = actor.GetComponentInChildren<Ability>();
        poa.target = Targets.Foe;
    }

    bool IsPositionIndependent(PlanOfAttack poa)
    {
        AbilityRange range = poa.ability.GetComponent<AbilityRange>();
        return range.positionOriented == false;
    }

    bool IsDirectionIndependent(PlanOfAttack poa)
    {
        AbilityRange range = poa.ability.GetComponent<AbilityRange>();
        return !range.directionOriented;
    }

    void PlanPositionIndependent(PlanOfAttack poa)
    {
        List<Tile> moveOptions = GetMoveOptions();
        Tile tile = moveOptions[Random.Range(0, moveOptions.Count)];
        poa.moveLocation = poa.fireLocation = tile.GridPosition;
    }

    void PlanDirectionIndependent(PlanOfAttack poa)
    {
        Tile startTile = actor.CurrentTile;
        Dictionary<Tile, AttackOption> map = new Dictionary<Tile, AttackOption>();
        AbilityRange ar = poa.ability.GetComponent<AbilityRange>();
        List<Tile> moveOptions = GetMoveOptions();

        for (int i = 0; i < moveOptions.Count; ++i)
        {
            Tile moveTile = moveOptions[i];
            moveTile.Place(actor);
            List<Tile> fireOptions = ar.GetTilesInRange(bc.location.Grid);

            for (int j = 0; j < fireOptions.Count; ++j)
            {
                Tile fireTile = fireOptions[j];
                AttackOption ao = null;
                if (map.ContainsKey(fireTile))
                {
                    ao = map[fireTile];
                }
                else
                {
                    ao = new AttackOption();
                    map[fireTile] = ao;
                    ao.target = fireTile;
                    ao.direction = actor.Facing;
                    RateFireLocation(poa, ao);
                }

                ao.AddMoveTarget(moveTile);
            }
        }

        startTile.Place(actor);
        List<AttackOption> list = new List<AttackOption>(map.Values);
        PickBestOption(poa, list);
    }

    void PlanDirectionDependent(PlanOfAttack poa)
    {
        Tile startTile = actor.CurrentTile;
        Direction startDirection = actor.Facing;
        List<AttackOption> list = new List<AttackOption>();
        List<Tile> moveOptions = GetMoveOptions();

        for (int i = 0; i < moveOptions.Count; ++i)
        {
            Tile moveTile = moveOptions[i];
            moveTile.Place(actor);

            for (int j = 0; j < 4; ++j)
            {
                actor.Facing = (Direction) j;
                AttackOption ao = new AttackOption();
                ao.target = moveTile;
                ao.direction = actor.Facing;
                RateFireLocation(poa, ao);
                ao.AddMoveTarget(moveTile);
                list.Add(ao);
            }
        }

        startTile.Place(actor);
        actor.Facing = startDirection;
        PickBestOption(poa, list);
    }

    bool IsAbilityTargetMatch(PlanOfAttack poa, Tile tile)
    {
        bool isMatch = false;
        if (poa.target == Targets.Tile)
            isMatch = true;
        else if (poa.target != Targets.None)
        {
            Alliance other = tile.content.GetComponentInChildren<Alliance>();
            if (other != null && alliance.IsMatch(other, poa.target))
                isMatch = true;
        }

        return isMatch;
    }

    List<Tile> GetMoveOptions()
    {
        return actor.GetComponent<Movement>().GetTilesInRange(bc.location.Grid);
    }

    void RateFireLocation(PlanOfAttack poa, AttackOption option)
    {
        AbilityArea area = poa.ability.GetComponent<AbilityArea>();
        List<Tile> tiles = area.GetTilesInArea(bc.location.Grid, option.target.GridPosition);
        option.areaTargets = tiles;
        option.isCasterMatch = IsAbilityTargetMatch(poa, actor.CurrentTile);

        for (int i = 0; i < tiles.Count; ++i)
        {
            Tile tile = tiles[i];
            if (actor.CurrentTile == tiles[i] || !poa.ability.IsTarget(tile))
                continue;

            bool isMatch = IsAbilityTargetMatch(poa, tile);
            option.AddMark(tile, isMatch);
        }
    }

    void PickBestOption(PlanOfAttack poa, List<AttackOption> list)
    {
        int bestScore = 1;
        List<AttackOption> bestOptions = new List<AttackOption>();
        for (int i = 0; i < list.Count; ++i)
        {
            AttackOption option = list[i];
            int score = option.GetScore(actor, poa.ability);
            if (score > bestScore)
            {
                bestScore = score;
                bestOptions.Clear();
                bestOptions.Add(option);
            }
            else if (score == bestScore)
            {
                bestOptions.Add(option);
            }
        }

        if (bestOptions.Count == 0)
        {
            poa.ability = null; // Clear ability as a sign not to perform it
            return;
        }

        List<AttackOption> finalPicks = new List<AttackOption>();
        bestScore = 0;
        for (int i = 0; i < bestOptions.Count; ++i)
        {
            AttackOption option = bestOptions[i];
            int score = option.bestAngleBasedScore;
            if (score > bestScore)
            {
                bestScore = score;
                finalPicks.Clear();
                finalPicks.Add(option);
            }
            else if (score == bestScore)
            {
                finalPicks.Add(option);
            }
        }

        AttackOption choice = finalPicks[UnityEngine.Random.Range(0, finalPicks.Count)];
        poa.fireLocation = choice.target.GridPosition;
        poa.attackDirection = choice.direction;
        poa.moveLocation = choice.bestMoveTile.GridPosition;
    }

    void FindNearestFoe()
    {
        nearestFoe = null;
        bc.location.Grid.Search(actor.CurrentTile, delegate(Tile arg1, Tile arg2)
        {
            if (nearestFoe == null && arg2.content != null)
            {
                Alliance other = arg2.content.GetComponentInChildren<Alliance>();
                if (other != null && alliance.IsMatch(other, Targets.Foe))
                {
                    Unit unit = other.GetComponent<Unit>();
                    Stats stats = unit.GetComponent<Stats>();
                    if (stats[StatTypes.HP] > 0)
                    {
                        nearestFoe = unit;
                        return true;
                    }
                }
            }

            return nearestFoe == null;
        });
    }

    void MoveTowardOpponent(PlanOfAttack poa)
    {
        List<Tile> moveOptions = GetMoveOptions();
        FindNearestFoe();
        if (nearestFoe != null)
        {
            Tile toCheck = nearestFoe.CurrentTile;
            while (toCheck != null)
            {
                if (moveOptions.Contains(toCheck))
                {
                    poa.moveLocation = toCheck.GridPosition;
                    return;
                }

                toCheck = toCheck.prev;
            }
        }

        poa.moveLocation = actor.CurrentTile.GridPosition;
    }

    public Direction DetermineEndFacingDirection()
    {
        Direction dir = (Direction) UnityEngine.Random.Range(0, 4);
        FindNearestFoe();
        if (nearestFoe != null)
        {
            Direction start = actor.Facing;
            for (int i = 0; i < 4; ++i)
            {
                actor.Facing = (Direction) i;
                if (nearestFoe.GetFacing(actor) == Facings.Front)
                {
                    dir = actor.Facing;
                    break;
                }
            }

            actor.Facing = start;
        }

        return dir;
    }

    #endregion
}