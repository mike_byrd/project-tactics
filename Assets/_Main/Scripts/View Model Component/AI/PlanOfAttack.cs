﻿using UnityEngine;
using System.Collections;
using MCB;
using MCB.Common;

public class PlanOfAttack
{
    public Ability ability;
    public Targets target;
    public Point moveLocation;
    public Point fireLocation;
    public Direction attackDirection;
}