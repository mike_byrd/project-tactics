﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The only thing this script does is to know how to remove itself. That’s not 
/// much, but its really the sole purpose of this class. It is there as a sort 
/// of “lock” to keep a status effect applied, but it doesn’t care what the 
/// status effect is, it only needs to know about itself and how to remove 
/// itself. The Status component manages the relationship between these “locks” 
/// and the effects, but we will get to that later.
/// </summary>
public class StatusCondition : MonoBehaviour
{
  public virtual void Remove()
  {
    Status s = GetComponentInParent<Status>();
    if (s) {
      s.Remove(this);
    }
  }
}