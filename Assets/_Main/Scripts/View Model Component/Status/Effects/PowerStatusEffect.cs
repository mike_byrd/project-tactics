﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MCB;

public class PowerStatusEffect : StatusEffect
{
  public float percentAmount = 1.25f;

  void Awake() { statusName = "Power"; }

  void OnEnable()
  {
    this.AddObserver(OnGetAttack, BaseAbilityEffect.GetAttackNotification);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnGetAttack, BaseAbilityEffect.GetAttackNotification);
  }

  void OnGetAttack(object sender, object args)
  {
    var info = args as Info<Unit, Unit, List<ValueModifier>>;

    // If the attacker is the parent of this status effect add this modifier.
    if (info.arg0.transform == transform.parent) {
      info.arg2.Add(new MultValueModifier(1, percentAmount));
    }
  }
}
