﻿using UnityEngine;
using System.Collections;
using MCB;

public class KnockOutStatusEffect : StatusEffect
{
  Unit owner;
  Stats stats;

  void Awake()
  {
    owner = GetComponentInParent<Unit>();
    stats = owner.GetComponent<Stats>();
    statusName = "Knocked Out";
  }

  void OnEnable()
  {
    // Play Death Animation.
    owner.Anim.SetBool("IsAlive", false);
    
    this.AddObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, owner);
    this.AddObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.SPD), stats);
  }

  void OnDisable()
  {
    // Play Revive Animation/ Set back to alive.
    owner.Anim.SetBool("IsAlive", true);

    this.RemoveObserver(OnTurnCheck, TurnOrderController.TurnCheckNotification, owner);
    this.RemoveObserver(OnStatCounterWillChange, Stats.WillChangeNotification(StatTypes.SPD), stats);
  }

  void OnTurnCheck(object sender, object args)
  {
    // Dont allow a KO'd unit to take turns
    BaseException exc = args as BaseException;
    if (exc.defaultToggle == true)
      exc.FlipToggle();
  }

  void OnStatCounterWillChange(object sender, object args)
  {
    // Dont allow a KO'd unit to increment the turn order counter
    ValueChangeException exc = args as ValueChangeException;
    if (exc.toValue > exc.fromValue)
      exc.FlipToggle();
  }
}