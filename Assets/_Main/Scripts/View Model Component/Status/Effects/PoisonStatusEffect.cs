﻿using UnityEngine;
using System.Collections;
using MCB;

public class PoisonStatusEffect : StatusEffect
{
  Unit owner;

  void Awake() { statusName = "Poisoned"; }

  void OnEnable()
  {
    owner = GetComponentInParent<Unit>();
    if (owner) {
      this.AddObserver(OnNewTurn, TurnOrderController.TurnBeganNotification, owner);
    }      
  }

  void OnDisable()
  {
    this.RemoveObserver(OnNewTurn, TurnOrderController.TurnBeganNotification, owner);
  }

  void OnNewTurn(object sender, object args)
  {
    Stats s = GetComponentInParent<Stats>();
    int currentHP = s[StatTypes.HP];
    int maxHP = s[StatTypes.MHP];

    int value = Mathf.Clamp((int)(currentHP - maxHP * 0.1f), 0, maxHP);
    s.SetValue(StatTypes.HP, value, false);

    Vector3 textPos = s.transform.position;
    float offsetY = 1.5f;
    textPos.y += offsetY;

    Color color = Color.white;
    int damage = currentHP - value;
    //FloatingTextController.instance.CreateFloatingText(Mathf.Abs(damage).ToString(), textPos, color);
    FloatingTextController.instance.SpawnUnitDamageText(owner, Mathf.Abs(damage).ToString(), color);

    // Play ability particle effect.
    //ParticleEffectController.instance.PlayAbilityEffect(null, s.transform.position);
  }
}
