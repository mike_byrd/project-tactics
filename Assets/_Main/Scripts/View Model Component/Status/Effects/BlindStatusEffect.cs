﻿using UnityEngine;
using System.Collections.Generic;
using MCB;

public class BlindStatusEffect : StatusEffect
{
  void Awake() { statusName = "Blind"; }

  public int flatAmount = -20;

  void OnEnable()
  {
    this.AddObserver(OnGetRange, AbilityRange.GetRangeNotification);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnGetRange, AbilityRange.GetRangeNotification);
  }

  void OnGetRange(object sender, object args)
  {
    var info = args as Info<Unit, List<ValueModifier>>;

    // If the attacker is the parent of this status effect add this modifier.
    if(info.arg0.transform == transform.parent) {
      info.arg1.Add(new AddValueModifier(1, flatAmount));
    }
  }
}