﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MCB;

public class DefenseUpStatusEffect : StatusEffect
{
  public float percentAmount = 1.4f;

  void Awake() { statusName = "Def Up"; }

  void OnEnable()
  {
    this.AddObserver(OnGetDefense, BaseAbilityEffect.GetDefenseNotification);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnGetDefense, BaseAbilityEffect.GetDefenseNotification);
  }

  void OnGetDefense(object sender, object args)
  {
    var info = args as Info<Unit, Unit, List<ValueModifier>>;

    // If the attacker is the parent of this status effect add this modifier.
    if(info.arg1.transform == transform.parent) {
      info.arg2.Add(new MultValueModifier(1, percentAmount));
    }
  }
}
