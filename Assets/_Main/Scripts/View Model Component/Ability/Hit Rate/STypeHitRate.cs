﻿using UnityEngine;
using System.Collections;
using MCB;

/// <summary>
/// The second type of hit rate component is used for special abilities which 
/// focus on applying status effects. Its chances of success are partially 
/// determined by the defender’s RES (resistance) stat.
/// </summary>
public class STypeHitRate : HitRate
{
  public override int Calculate(Tile target)
  {
    if (!target.content) {
      return 0;
    }

    Unit defender = target.content.GetComponent<Unit>();
    if (AutomaticMiss(defender))
      return Final(100);

    if (AutomaticHit(defender))
      return Final(0);

    int res = GetResistance(defender);
    res = AdjustForStatusEffects(defender, res);
    res = AdjustForRelativeFacing(defender, res);
    res = Mathf.Clamp(res, 0, 100);
    return Final(res);
  }

  int GetResistance(Unit target)
  {
    Stats s = target.GetComponentInParent<Stats>();
    //return s[StatTypes.RES];
    return 0;
  }

  int AdjustForRelativeFacing(Unit target, int rate)
  {
    switch (attacker.GetFacing(target)) {
      case Facings.Front:
        return rate;
      case Facings.Side:
        return rate - 10;
      default:
        return rate - 20;
    }
  }
}