﻿using UnityEngine;
using System.Collections;
using MCB;

/// <summary>
/// Our final hit rate type is for special abilities which should normally hit 
/// without fail. There still may be exceptions to this rule, so I left a 
/// notification to allow a chance for misses.
/// </summary>
public class FullTypeHitRate : HitRate
{
  public override bool IsAngleBased { get { return false; } }

  public override int Calculate(Tile target)
  {
    if (!target.content) {
      return Final(0);
    }

    Unit defender = target.content.GetComponent<Unit>();
    if (AutomaticMiss(defender)) {
      return Final(100);
    }

    return Final(0);
  }
}