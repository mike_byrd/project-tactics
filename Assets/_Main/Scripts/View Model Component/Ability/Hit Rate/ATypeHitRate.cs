﻿using UnityEngine;
using System.Collections;
using MCB;

/// <summary>
/// The first concrete subclass of our HitRate is the default type which will be
///  used with most abilities such as a standard attack. Its chances of success 
/// are partially determined by the defender’s EVD (evade) stat.
/// </summary>
public class ATypeHitRate : HitRate
{
  public override int Calculate(Tile target)
  {
    if (!target.content) {
      return 0;
    }

    Unit defender = target.content.GetComponent<Unit>();
    if (AutomaticHit(defender)) {
      return Final(0);
    }

    if (AutomaticMiss(defender)) {
      return Final(100);
    }

    int evade = GetEvade(defender);
    evade = AdjustForRelativeFacing(defender, evade);
    evade = AdjustForStatusEffects(defender, evade);
    evade = Mathf.Clamp(evade, 5, 95);
    return Final(evade);
  }

  int GetEvade(Unit target)
  {
    Stats s = target.GetComponentInParent<Stats>();
    //return Mathf.Clamp(s[StatTypes.EVD], 0, 100);
    return 0;
  }

  int AdjustForRelativeFacing(Unit target, int rate)
  {
    switch (attacker.GetFacing(target)) {
      case Facings.Front:
        return rate;
      case Facings.Side:
        return rate / 2;
      default:
        return rate / 4;
    }
  }
}