﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MCB;

public class DurationAbilityCooldown : MonoBehaviour
{
  public int duration = 2;
  public int remainingTurns { get; private set; }
  Ability owner;

  bool _isActive;
  public bool isActive {
    get {
      return _isActive;
    }
    set {
      _isActive = value;
      remainingTurns = duration;
    }
  }

  void Awake()
  {
    owner = GetComponent<Ability>();
  }

  void OnEnable()
  {
    this.AddObserver(OnNewTurn, TurnOrderController.RoundBeganNotification);
    this.AddObserver(OnCanPerformCheck, Ability.CanPerformCheck, owner);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnNewTurn, TurnOrderController.RoundBeganNotification);
    this.RemoveObserver(OnCanPerformCheck, Ability.CanPerformCheck, owner);
  }

  void OnNewTurn(object sender, object args)
  {
    if (!_isActive) {
      return;
    }

    remainingTurns--;

    //Debug.Log(string.Format("Remaining Cooldown [{0}]: {1} Turns.", transform.name, remainingTurns));

    if (remainingTurns <= 0) {
      Remove();
    }
  }

  public virtual void Remove()
  {
    _isActive = false;
  }

  #region Notification Handlers
  void OnCanPerformCheck(object sender, object args)
  {
    BaseException exc = (BaseException)args;
    if (exc.toggle == true) {
      if (_isActive == true) {
        exc.FlipToggle();
      }
    }
  }
  #endregion
}
