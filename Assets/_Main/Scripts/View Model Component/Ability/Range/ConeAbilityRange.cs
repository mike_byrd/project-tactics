﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

/// <summary>
/// Uses the unit's direction. Attacks in a cone.
/// </summary>
public class ConeAbilityRange : AbilityRange
{
    public override bool directionOriented
    {
        get { return true; }
    }

    public override List<Tile> GetTilesInRange(WorldGrid board)
    {
        /*if the unit is facing north, then the outer loop will increment
         * on “Y” for as many tiles as is specified by the horizontal field.
         * Then an inner loop on “X” beginning with a lateral offset of 1 and
         * incrementing by 2 (so you get odd numbers like 1, 3, 5 etc) is used
         * to determine the sideways spread of the cone at each step away from
         * the user.
         */
        sourceTile = GetSourceTile();
        Point pos = sourceTile.GridPosition;
        List<Tile> retValue = new List<Tile>();
        int dir = (unit.Facing == Direction.North || unit.Facing == Direction.East) ? 1 : -1;
        int lateral = 1;

        if (unit.Facing == Direction.North || unit.Facing == Direction.South)
        {
            for (int y = 1; y <= maxHorizontal; ++y)
            {
                int min = -(lateral / 2);
                int max = (lateral / 2);
                for (int x = min; x <= max; ++x)
                {
                    Point next = new Point(pos.x + x, pos.y + (y * dir));
                    Tile tile = board.GetTile(next);
                    if (ValidTile(tile))
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }
        else
        {
            for (int x = 1; x <= maxHorizontal; ++x)
            {
                int min = -(lateral / 2);
                int max = (lateral / 2);
                for (int y = min; y <= max; ++y)
                {
                    Point next = new Point(pos.x + (x * dir), pos.y + y);
                    Tile tile = board.GetTile(next);
                    if (ValidTile(tile))
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }

        return retValue;
    }

    private bool ValidTile(Tile t)
    {
        return t != null && Mathf.Abs(t.Height - sourceTile.Height) <= vertical;
    }
}