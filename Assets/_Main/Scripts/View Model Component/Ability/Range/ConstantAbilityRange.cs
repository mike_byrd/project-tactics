﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

/// <summary>
/// The use case for this type of class is when an ability always has the
/// same pre-specified amount of range, i.e. Spell “X” has “Y” reach in
/// terms of tiles. This class would be similar to a class where the range
/// is based off of the range of an equipped weapon, except that the
/// horizontal and vertical fields would need to be adjusted first to match.
/// </summary>
public class ConstantAbilityRange : AbilityRange
{
    int unitRangeStat;

    public override List<Tile> GetTilesInRange(WorldGrid board)
    {
        outOfSightTiles = new List<Tile>();
        unitRangeStat = _rangeModifiable ? GetStat(unit, GetRangeNotification, 0) : 0;

        BattleScene.instance.SetUnitCollidersEnabled(true);

        sourceTile = GetSourceTile();
        List<Tile> tilesInRange = board.Search(sourceTile.GridPosition, ExpandSearch);
        Filter(tilesInRange);

        BattleScene.instance.SetUnitCollidersEnabled(false);

        sourceTile = null;
        return tilesInRange;
    }

    protected virtual void Filter(List<Tile> tiles)
    {
        sourceTile = GetSourceTile();
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            bool isLessThanMinRange = tiles[i].GridPosition.DistanceTo(sourceTile.GridPosition) < minHorizonal;
            int distance = sourceTile.GridPosition.DistanceTo(tiles[i].GridPosition);
            int finalRange = Mathf.Clamp(maxHorizontal + unitRangeStat, minHorizonal, 999);
            bool tileIsTooHigh = Mathf.Abs(tiles[i].Height - sourceTile.Height) > vertical;
            bool inLOS = InLOS(sourceTile, tiles[i]);
            bool passEmptyTileRequirement = CheckEmptyTileRequirement(tiles[i]);
            if (distance > finalRange || isLessThanMinRange || tileIsTooHigh || !inLOS || !passEmptyTileRequirement)
            {
                if (!inLOS)
                {
                    outOfSightTiles.Add(tiles[i]);
                    tiles[i].state = TileState.OutOfSight;
                }

                tiles.RemoveAt(i);
            }
        }
    }

    bool ExpandSearch(Point to)
    {
        int finalRange = Mathf.Clamp(maxHorizontal + unitRangeStat, minHorizonal, 999);
        int distance = to.DistanceTo(sourceTile.GridPosition);
        return distance <= finalRange;
    }
}