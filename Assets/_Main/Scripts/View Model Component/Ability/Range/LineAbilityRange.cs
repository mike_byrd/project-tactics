﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

/// <summary>
/// Uses the unit's direction. a line starting from the user and extending to the edge of the board.
/// </summary>
public class LineAbilityRange : AbilityRange
{
    public override bool directionOriented
    {
        get { return true; }
    }

    public override List<Tile> GetTilesInRange(WorldGrid board)
    {
        outOfSightTiles = new List<Tile>();

        BattleScene.instance.SetUnitCollidersEnabled(true);

        List<Tile> retValue = new List<Tile>();
        sourceTile = GetSourceTile();

        if (!ability.ignoreSelf)
        {
            retValue.Add(sourceTile);
        }

        retValue.AddRange(GetTiles(board, Direction.North));
        retValue.AddRange(GetTiles(board, Direction.South));
        retValue.AddRange(GetTiles(board, Direction.East));
        retValue.AddRange(GetTiles(board, Direction.West));

        Filter(retValue);

        BattleScene.instance.SetUnitCollidersEnabled(false);
        return retValue;
    }

    protected virtual void Filter(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            bool isLessThanMinRange = tiles[i].GridPosition.DistanceTo(sourceTile.GridPosition) < minHorizonal;
            bool isTooHigh = Mathf.Abs(tiles[i].Height - sourceTile.Height) > vertical;
            bool passEmptyTileRequirement = CheckEmptyTileRequirement(tiles[i]);
            bool inLOS = InLOS(sourceTile, tiles[i]);
            if (isTooHigh || isLessThanMinRange || !inLOS || !passEmptyTileRequirement)
            {
                if (!inLOS)
                {
                    outOfSightTiles.Add(tiles[i]);
                    tiles[i].state = TileState.OutOfSight;
                }

                tiles.RemoveAt(i);
            }
        }
    }

    private List<Tile> GetTiles(WorldGrid board, Direction dir)
    {
        List<Tile> retValue = new List<Tile>();

        Point startPos = sourceTile.GridPosition;
        Point endPos;

        switch (dir)
        {
            case Direction.North:
                endPos = new Point(startPos.x, board.Max.y);
                break;
            case Direction.East:
                endPos = new Point(board.Max.x, startPos.y);
                break;
            case Direction.South:
                endPos = new Point(startPos.x, board.Min.y);
                break;
            default: // West
                endPos = new Point(board.Min.x, startPos.y);
                break;
        }

        int dist = 0;
        int unitRangeStat = _rangeModifiable ? GetStat(unit, GetRangeNotification, 0) : 0;
        while (startPos != endPos)
        {
            if (startPos.x < endPos.x)
                startPos.x++;
            else if (startPos.x > endPos.x)
                startPos.x--;

            if (startPos.y < endPos.y)
                startPos.y++;
            else if (startPos.y > endPos.y)
                startPos.y--;

            Tile t = board.GetTile(startPos);
            if (t != null && Mathf.Abs(t.Height - sourceTile.Height) <= vertical)
            {
                retValue.Add(t);
            }

            dist++;
            if (dist >= Mathf.Clamp(maxHorizontal + unitRangeStat, minHorizonal, 999))
                break;
        }

        return retValue;
    }
}