﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics;

/// <summary>
/// This class is super simple – only two fields, two properties, and an abstract
/// method signature. The first field, horizontal, defines the number of tiles
/// away from the user which can be reached (think X & Z axis). The second field,
/// vertical, defines the height difference between the user’s tile and the target
/// tiles which are within reach (think Y axis).
/// </summary>
public abstract class AbilityRange : MonoBehaviour
{
    public static string GetRangeNotification = "AbilityRange.GetRangeNotification";

    public int minHorizonal = 1;
    public int maxHorizontal = 1;
    public int vertical = int.MaxValue;

    [HideInInspector] public List<Tile> outOfSightTiles;
    protected Tile sourceTile;

    public abstract List<Tile> GetTilesInRange(WorldGrid board);

    /// <summary>
    /// directionOriented should be true when the range is a pattern like a cone
    /// or line. When it is true, we will use the movement input buttons to change
    /// the user’s facing direction so that the effected tiles change. When the
    /// directionOriented property is false, you may move the cursor to select tiles
    /// within the highlighted range.
    /// </summary>
    public virtual bool directionOriented
    {
        get { return false; }
    }

    public virtual bool positionOriented
    {
        get { return true; }
    }

    protected Unit unit
    {
        get { return GetComponentInParent<Unit>(); }
    }

    protected Ability ability { get; private set; }

    [SerializeField] protected bool _rangeModifiable = true;

    public bool rangeModifiable
    {
        get { return _rangeModifiable; }
    }

    [SerializeField] protected bool _requiresLineOfSight = true;

    public bool requiresLineOfSight
    {
        get { return _requiresLineOfSight; }
    }


    // If the unit is moving and we want to queue up an ability we'll need to base it off of the destination tile.
    protected Tile GetSourceTile()
    {
        return unit.DestinationTile ? unit.DestinationTile : unit.CurrentTile;
    }

    void Awake()
    {
        outOfSightTiles = new List<Tile>();
        ability = GetComponentInParent<Ability>();
    }

    void OnEnable()
    {
        this.AddObserver(OnGetRange, GetRangeNotification);
    }

    void OnDisable()
    {
        this.RemoveObserver(OnGetRange, GetRangeNotification);
    }

    void OnGetRange(object sender, object args)
    {
        if (IsMyEffect(sender))
        {
            var info = args as Info<Unit, List<ValueModifier>>;
            info.arg1.Add(new AddValueModifier(0, GetBaseRange()));
        }
    }

    bool IsMyEffect(object sender)
    {
        MonoBehaviour obj = sender as MonoBehaviour;
        //TODO: restructure the sent object so that maybe we don't have to pay for ancestry.com...
        return (obj != null && obj.transform.parent == transform.parent);
    }

    int GetBaseRange()
    {
        return GetComponentInParent<Stats>()[StatTypes.RNG];
    }

    protected virtual int GetStat(Unit unit, string notification, int startValue)
    {
        var mods = new List<ValueModifier>();
        var info = new Info<Unit, List<ValueModifier>>(unit, mods);
        this.PostNotification(notification, info);
        mods.Sort(Compare);

        float value = startValue;
        for (int i = 0; i < mods.Count; ++i)
        {
            value = mods[i].Modify(startValue, value);
        }

        int retValue = Mathf.FloorToInt(value);
        retValue = Mathf.Clamp(retValue, -999, 999);
        return retValue;
    }

    int Compare(ValueModifier x, ValueModifier y)
    {
        return x.sortOrder.CompareTo(y.sortOrder);
    }

    protected bool CheckEmptyTileRequirement(Tile tile)
    {
        if (!ability.requiresEmptyTile)
        {
            return true;
        }

        return !tile.content;
    }

    protected bool InLOS(Tile tile1, Tile tile2, bool debugging = false)
    {
        // if you don't require a line of sight then just return true...
        if (!_requiresLineOfSight)
        {
            return true;
        }

        Vector3 pos1 = tile1.transform.position + (Vector3.up * 0.5f);
        Vector3 pos2 = tile2.transform.position + (Vector3.up * 0.5f);

        float dist = Vector3.Distance(pos2, pos1);
        Vector3 dir = (pos2 - pos1).normalized;
        LayerMask mask = 1 << LayerMask.NameToLayer("Unit");

        bool flag = false;

        RaycastHit[] hits;
        if (!LOSRaycast(pos1, dir, dist, mask, out hits, debugging))
        {
            if (debugging)
                flag = true;
            else
                return true;
        }

        if (hits.Length == 1)
        {
            Unit hitUnit = hits[0].transform.GetComponent<Unit>();
            if (hitUnit.CurrentTile == tile2)
            {
                return true;
            }
        }

        return flag;
    }

    protected bool LOSRaycast(Vector3 pos, Vector3 dir, float dist, LayerMask mask, out RaycastHit[] hits,
        bool debugging = false)
    {
        float debugDuration = 5f;
        hits = Physics.RaycastAll(pos, dir, dist, mask);

        if (hits.Length != 0)
        {
            if (debugging)
            {
                Debug.DrawLine(pos, pos + dir * dist, Color.red, debugDuration);
            }

            return true;
        }
        else
        {
            if (debugging)
            {
                Debug.DrawLine(pos, pos + dir * dist, Color.white, debugDuration);
            }

            return false;
        }
    }

    public void ClearOutOfSightTiles()
    {
        foreach (Tile tile in outOfSightTiles)
        {
            tile.state = TileState.None;
        }

        outOfSightTiles.Clear();
    }
}