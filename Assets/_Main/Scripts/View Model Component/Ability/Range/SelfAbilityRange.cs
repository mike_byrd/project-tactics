﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics;

/// <summary>
/// Some abilities may only effect the user. In this case
/// we don’t need to perform a search on the board, we simply
/// return the user’s tile.
/// </summary>
public class SelfAbilityRange : AbilityRange
{
    public override bool positionOriented
    {
        get { return false; }
    }

    public override List<Tile> GetTilesInRange(WorldGrid board)
    {
        List<Tile> retValue = new List<Tile>(1);
        retValue.Add(GetSourceTile());
        return retValue;
    }
}