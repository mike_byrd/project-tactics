﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using ProjectTactics;

/// <summary>
/// Reach any or all targets no matter where they are on the board.
/// </summary>
public class InfiniteAbilityRange : AbilityRange
{
    public override bool positionOriented
    {
        get { return false; }
    }

    public override List<Tile> GetTilesInRange(WorldGrid board)
    {
        return new List<Tile>(board.Tiles.Values);
    }
}