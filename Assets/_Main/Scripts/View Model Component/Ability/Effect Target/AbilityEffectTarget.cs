﻿using UnityEngine;
using System.Collections;
using MCB;

public abstract class AbilityEffectTarget : MonoBehaviour 
{
	public abstract bool IsTarget (Tile tile);
}