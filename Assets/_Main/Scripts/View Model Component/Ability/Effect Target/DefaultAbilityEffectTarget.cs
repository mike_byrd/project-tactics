using UnityEngine;
using System.Collections;
using MCB;

public class DefaultAbilityEffectTarget : AbilityEffectTarget
{
  public override bool IsTarget(Tile tile)
  {
    return tile != null;
  }
}