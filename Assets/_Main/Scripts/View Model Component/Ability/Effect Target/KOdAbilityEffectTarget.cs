using UnityEngine;
using System.Collections;
using MCB;

public class KOdAbilityEffectTarget : AbilityEffectTarget 
{
	public override bool IsTarget (Tile tile)
	{
		if (tile == null || tile.content == null)
			return false;

		Stats s = tile.content.GetComponent<Stats>();
		bool isDead = s != null && s[StatTypes.HP] <= 0;

		return isDead;
	}
}