﻿using UnityEngine;
using System.Collections;
using MCB;

public class AbilityMagicCost : MonoBehaviour
{
  #region Fields
  public int amount;
  Ability owner;
  #endregion

  #region MonoBehaviour
  void Awake()
  {
    owner = GetComponent<Ability>();
  }

  void OnEnable()
  {
    this.AddObserver(OnCanPerformCheck, Ability.CanPerformCheck, owner);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnCanPerformCheck, Ability.CanPerformCheck, owner);
  }
  #endregion

  #region Notification Handlers
  void OnCanPerformCheck(object sender, object args)
  {
    Stats s = GetComponentInParent<Stats>();
    if (s[StatTypes.AP] < amount) {
      BaseException exc = (BaseException)args;
      exc.FlipToggle();
    }
  }
  #endregion
}