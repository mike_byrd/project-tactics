﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

public abstract class AbilityArea : MonoBehaviour
{
    protected Unit unit
    {
        get { return GetComponentInParent<Unit>(); }
    }

    public abstract List<Tile> GetTilesInArea(WorldGrid board, Point pos);
}