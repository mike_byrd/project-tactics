﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

/// <summary>
/// Simply returns whatever Tile exists at the indicated position.
/// This could be used for implementing a single target attack like an archer.
/// </summary>
public class UnitAbilityArea : AbilityArea
{
    public override List<Tile> GetTilesInArea(WorldGrid board, Point pos)
    {
        List<Tile> retValue = new List<Tile>();
        Tile tile = board.GetTile(pos);
        if (tile != null)
            retValue.Add(tile);
        return retValue;
    }
}