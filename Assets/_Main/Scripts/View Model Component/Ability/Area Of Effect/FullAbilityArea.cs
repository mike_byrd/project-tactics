﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics;

public class FullAbilityArea : AbilityArea
{
    public override List<Tile> GetTilesInArea(WorldGrid board, Point pos)
    {
        AbilityRange ar = GetComponent<AbilityRange>();
        return ar.GetTilesInRange(board);
    }
}