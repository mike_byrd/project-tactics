﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MCB;
using MCB.Common;
using ProjectTactics;

public class ConeAbilityArea : AbilityArea
{
    public int horizontal = 1;
    public int vertical = int.MaxValue;

    public override List<Tile> GetTilesInArea(WorldGrid board, Point pos)
    {
        /*if the unit is facing north, then the outer loop will increment
             * on “Y” for as many tiles as is specified by the horizontal field.
             * Then an inner loop on “X” beginning with a lateral offset of 1 and
             * incrementing by 2 (so you get odd numbers like 1, 3, 5 etc) is used
             * to determine the sideways spread of the cone at each step away from
             * the user.
             */
        List<Tile> retValue = new List<Tile>();
        Direction tileDirection = pos.GetDirection(unit.CurrentTile.GridPosition);
        int dir = (tileDirection == Direction.North || tileDirection == Direction.East) ? -1 : 1;
        int lateral = 1;

        if (tileDirection == Direction.North || tileDirection == Direction.South)
        {
            for (int y = 0; y <= horizontal; ++y)
            {
                int min = -(lateral / 2);
                int max = (lateral / 2);
                for (int x = min; x <= max; ++x)
                {
                    Point next = new Point(pos.x + x, pos.y + (y * dir));
                    Tile tile = board.GetTile(next);
                    if (ValidTile(tile))
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }
        else
        {
            for (int x = 0; x <= horizontal; ++x)
            {
                int min = -(lateral / 2);
                int max = (lateral / 2);
                for (int y = min; y <= max; ++y)
                {
                    Point next = new Point(pos.x + (x * dir), pos.y + y);
                    Tile tile = board.GetTile(next);
                    if (ValidTile(tile))
                        retValue.Add(tile);
                }

                lateral += 2;
            }
        }

        return retValue;
    }

    bool ValidTile(Tile t)
    {
        return t != null && Mathf.Abs(t.Height - unit.CurrentTile.Height) <= vertical;
    }
}