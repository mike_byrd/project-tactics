﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

namespace MCB
{
  public class Ability : MonoBehaviour
  {
    public const string CanPerformCheck = "Ability.CanPerformCheck";
    public const string FailedNotification = "Ability.FailedNotification";
    public const string DidPerformNotification = "Ability.DidPerformNotification";

    [SerializeField]
    string _description; // TODO: Load this value from an external data source.
    public string description { get { return _description; } }

    [SerializeField]
    bool _requiresTarget = false;
    public bool requiresTarget { get { return _requiresTarget; } }

    [SerializeField]
    bool _requiresEmptyTile = false;
    public bool requiresEmptyTile { get { return _requiresEmptyTile; } }

    [SerializeField]
    protected bool _ignoreSelf = false;
    public bool ignoreSelf { get { return _ignoreSelf; } }

    protected AbilityRange _abilityRange;
    public AbilityRange abilityRange { get { return _abilityRange; } }

    public Sprite sprite;

    private void Awake()
    {
      _abilityRange = GetComponent<AbilityRange>();
    }

    public bool CanPerform()
    {
      BaseException exc = new BaseException(true);
      this.PostNotification(CanPerformCheck, exc);
      return exc.toggle;
    }

    public void Perform(List<Tile> targets)
    {
      for (int i = 0; i < targets.Count; ++i) {
        Perform(targets[i]);
      }

      this.PostNotification(DidPerformNotification);
    }

    public bool IsTarget(Tile tile)
    {
      Transform obj = transform;
      for (int i = 0; i < obj.childCount; ++i) {
        AbilityEffectTarget targeter = obj.GetChild(i).GetComponent<AbilityEffectTarget>();
        if (targeter.IsTarget(tile))
          return true;
      }
      return false;
    }

    void Perform(Tile target)
    {
      for (int i = 0; i < transform.childCount; ++i) {
        Transform child = transform.GetChild(i);
        BaseAbilityEffect effect = child.GetComponent<BaseAbilityEffect>();

        effect.Apply(target);
      }
    }
  }
}