﻿using UnityEngine;
using System.Collections;
using MCB;

public class HealAbilityEffect : BaseAbilityEffect
{
  public override int Predict(Tile target)
  {
    if (!target.content) {
      return 0;
    }

    Unit attacker = GetComponentInParent<Unit>();
    Unit defender = target.content.GetComponent<Unit>();
    return GetStat(attacker, defender, GetPowerNotification, 0);
  }

  protected override int OnApply(Tile target)
  {
    if (!target.content) {
      return 0;
    }

    Unit defender = target.content.GetComponent<Unit>();

    // Start with the predicted value
    int value = Predict(target);

    // Add some random variance
    value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

    // Clamp the amount to a range
    value = Mathf.Clamp(value, minDamage, maxDamage);

    // Apply the amount to the target
    Stats s = defender.GetComponent<Stats>();
    s[StatTypes.HP] += value;

    Vector3 textPos = defender.transform.position;
    textPos.y += POPUP_TEXT_OFFSET_Y;

    Color color = Color.green;
    //FloatingTextController.instance.CreateFloatingText(value.ToString(), textPos, color);
    FloatingTextController.instance.SpawnUnitDamageText(defender, value.ToString(), color);

    return value;
  }
}