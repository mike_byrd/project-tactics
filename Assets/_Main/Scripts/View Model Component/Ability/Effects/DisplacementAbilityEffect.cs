﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MCB;
using MCB.Common;

public enum DisplacementType
{
    Push,
    Pull
}

public class DisplacementAbilityEffect : BaseAbilityEffect
{
    [SerializeField] DisplacementType _type;

    public DisplacementType type
    {
        get { return _type; }
    }

    [SerializeField] int _distance;

    public int distance
    {
        get { return _distance; }
    }

    [SerializeField] bool displaceOwner = false;

    float displacementSpeed = .25f;

    public static string OnCurrentActorDisplaced = "DisplacementAbilityEffect.OnOwnerDisplaced";

    public override int Predict(Tile target)
    {
        return 0;
    }

    protected override int OnApply(Tile target)
    {
        if (ability.requiresTarget && !target.content)
        {
            return 0;
        }

        if (displaceOwner)
        {
            DisplaceOwner(target);
        }
        else
        {
            DisplaceTarget(target);
        }

        return 0;
    }

    void DisplaceTarget(Tile target)
    {
        Unit targetUnit = target.content.GetComponent<Unit>();
        Tile destination;
        if (targetUnit)
        {
            Direction direction;
            if (_type == DisplacementType.Push)
            {
                direction = owner.CurrentTile.GetDirection(targetUnit.CurrentTile);
            }
            else
            {
                direction = targetUnit.CurrentTile.GetDirection(owner.CurrentTile);
            }

            int tilesMoved;
            destination = GetDestinationTile(targetUnit.CurrentTile, direction, out tilesMoved);

            if (destination)
            {
                StartCoroutine(Move(targetUnit, destination, displacementSpeed * tilesMoved));
            }
        }
    }

    void DisplaceOwner(Tile target)
    {
        Direction direction;
        if (_type == DisplacementType.Push)
        {
            direction = target.GetDirection(owner.CurrentTile);
        }
        else
        {
            direction = owner.CurrentTile.GetDirection(target);
        }

        int tilesMoved;
        Tile destination = GetDestinationTile(owner.CurrentTile, direction, out tilesMoved);

        if (destination)
        {
            StartCoroutine(Move(owner, destination, displacementSpeed * tilesMoved));
            if (owner)
            {
                this.PostNotification(OnCurrentActorDisplaced);
            }
        }
    }

    IEnumerator Move(Unit target, Tile destination, float moveSpeed)
    {
        destination.Place(target);

        DG.Tweening.Tweener tweener = target.transform.DOMove(destination.Center, moveSpeed);
        tweener.SetEase(Ease.Linear);

        yield return tweener.WaitForCompletion();
    }

    Tile GetDestinationTile(Tile origin, Direction direction, out int tilesMoved)
    {
        if (distance == 0)
        {
            tilesMoved = 0;
            return origin;
        }

        Point dest;
        Tile validTile = null;
        Tile tile;
        for (int i = 1; i <= distance; i++)
        {
            dest = origin.GridPosition + (direction.GetNormal() * i);
            tile = BattleScene.instance.location.Grid.GetTile(dest);
            if (tile && !tile.content)
            {
                validTile = tile;
            }
            else
            {
                tilesMoved = i;
                return validTile;
            }
        }

        tilesMoved = distance;
        return validTile;
    }
}