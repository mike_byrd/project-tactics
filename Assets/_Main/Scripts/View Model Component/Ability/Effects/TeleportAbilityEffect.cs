﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MCB;
using DG.Tweening;

public class TeleportAbilityEffect : BaseAbilityEffect
{
    public static string OnCurrentActorTeleported = "TeleportAbilityEffect.OnCurrentActorTeleported";

    public override int Predict(Tile target)
    {
        return 0;
    }

    protected override int OnApply(Tile target)
    {
        if (ability.requiresEmptyTile)
        {
            StartCoroutine(Teleport(owner, target));
            this.PostNotification(OnCurrentActorTeleported);
        }
        else if (ability.requiresTarget)
        {
            if (target.content)
            {
                Swap(target.content);
            }
        }

        return 0;
    }

    IEnumerator Teleport(Unit unit, Tile tile)
    {
        tile.Place(unit);

        DG.Tweening.Tweener shrink = unit.transform.DOScale(Vector3.zero, 0.5f);
        shrink.SetEase(Ease.InBack);

        yield return shrink.WaitForCompletion();

        unit.transform.position = tile.Center;

        DG.Tweening.Tweener grow = unit.transform.DOScale(Vector3.one, 0.5f);
        grow.SetEase(Ease.OutBack);

        yield return grow.WaitForCompletion();
    }

    void Swap(GameObject content)
    {
        Unit unit = content.GetComponent<Unit>();
        if (unit)
        {
            Tile oldTargetTile = unit.CurrentTile;
            StartCoroutine(Teleport(unit, owner.CurrentTile));

            StartCoroutine(Teleport(owner, oldTargetTile));
            this.PostNotification(OnCurrentActorTeleported);
        }
    }
}