﻿using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using MCB;

public class ApplyAbilityEffect : BaseAbilityEffect
{
  public string statusName;
  public int duration;

  public override int Predict(Tile target)
  {
    return 0;
  }

  protected override int OnApply(Tile target)
  {
    string statusClassName = statusName.Contains("StatusEffect") ? statusName : statusName + "StatusEffect";
    Type statusType = Type.GetType(statusClassName);
    if (statusType == null || !statusType.IsSubclassOf(typeof(StatusEffect))) {
      Debug.LogError("Invalid Status Type");
      return 0;
    }

    if (!target.content) {
      return 0;
    }

    MethodInfo mi = typeof(Status).GetMethod("Add");
    Type[] types = { statusType, typeof(DurationStatusCondition) };
    MethodInfo constructed = mi.MakeGenericMethod(types);

    Status status = target.content.GetComponent<Status>();
    object retValue = constructed.Invoke(status, null);

    DurationStatusCondition condition = retValue as DurationStatusCondition;
    condition.duration = duration;

    Unit defender = target.content.GetComponent<Unit>();
    Vector3 textPos = defender.transform.position;
    textPos.y += POPUP_TEXT_OFFSET_Y;

    Color color = Color.white;
    //FloatingTextController.instance.CreateFloatingText(GetEffectDisplayName(statusClassName), textPos, color);
    FloatingTextController.instance.SpawnUnitDamageText(defender, GetEffectDisplayName(statusClassName), color);

    return 0;
  }

  string GetEffectDisplayName(string effectName)
  {
    switch (effectName) {
      case "BlindStatusEffect":
        return "Blind";
      case "HasteStatusEffect":
        return "Haste";
      case "PoisonStatusEffect":
        return "Poison";
      case "SlowStatusEffect":
        return "Slow";
      case "KnockOutStatusEffect":
        return "Knock Out";
      case "StopStatusEffect":
        return "Stop";
      case "PowerStatusEffect":
        return "Power";
      case "DefenseUpStatusEffect":
        return "Def Up";
      case "DefenseDownStatusEffect":
        return "Def Down";
      default:
        return "None";
    }
  }
}