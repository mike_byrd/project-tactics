﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;

public abstract class BaseAbilityEffect : MonoBehaviour
{
  #region Consts & Notifications
  protected const int minDamage = -999;
  protected const int maxDamage = 999;

  public const float POPUP_TEXT_OFFSET_Y = 1.5f;

  public const string GetAttackNotification = "BaseAbilityEffect.GetAttackNotification";
  public const string GetDefenseNotification = "BaseAbilityEffect.GetDefenseNotification";
  public const string GetPowerNotification = "BaseAbilityEffect.GetPowerNotification";
  public const string TweakDamageNotification = "BaseAbilityEffect.TweakDamageNotification";

  public const string MissedNotification = "BaseAbilityEffect.MissedNotification";
  public const string HitNotification = "BaseAbilityEffect.HitNotification";
  #endregion

  #region Public
  public GameObject particleEffectPrefab;
  protected Unit owner { get { return GetComponentInParent<Unit>(); } }
  protected Ability ability { get; private set; }

  [SerializeField]
  protected Alliances _targetAlliances = Alliances.None;
  public Alliances targetAlliances { get { return _targetAlliances; } }

  void Awake() {
    ability = GetComponentInParent<Ability>();
  }

  public abstract int Predict(Tile target);

  public void Apply(Tile target)
  {
    if(target.content) {
      Unit targetUnit = target.content.GetComponent<Unit>();
      if(ability.ignoreSelf && targetUnit == owner) {
        return;
      }

      if(_targetAlliances != Alliances.None) {
        Alliance targetAlliance = targetUnit.GetComponent<Alliance>();
        Alliance ownerAlliance = owner.GetComponent<Alliance>();
        if(_targetAlliances == Alliances.Ally) {
          if(targetAlliance.type != ownerAlliance.type) {
            return;
          }
        } else if(_targetAlliances == Alliances.Enemy) {
          if(targetAlliance.type == ownerAlliance.type) {
            return;
          }
        }        
      }      
    }    

    if (GetComponent<AbilityEffectTarget>().IsTarget(target) == false)
      return;

    if (GetComponent<HitRate>().RollForHit(target))
      this.PostNotification(HitNotification, OnApply(target));
    else
      this.PostNotification(MissedNotification, OnMiss(target));
  }
  #endregion

  #region Protected
  protected abstract int OnApply(Tile target);
  protected virtual int OnMiss(Tile target)
  {
    if (!target.content) {
      return 0;
    }

    Unit defender = target.content.GetComponent<Unit>();

    Vector3 textPos = defender.transform.position;
    textPos.y += POPUP_TEXT_OFFSET_Y;

    Color color = Color.white;

    //FloatingTextController.instance.CreateFloatingText("Miss", textPos, color);
    FloatingTextController.instance.SpawnUnitDamageText(defender, "Miss", color);

    return 0;
  }

  protected virtual int GetStat(Unit attacker, Unit target, string notification, int startValue)
  {
    var mods = new List<ValueModifier>();
    var info = new Info<Unit, Unit, List<ValueModifier>>(attacker, target, mods);
    this.PostNotification(notification, info);
    mods.Sort(Compare);

    float value = startValue;
    for (int i = 0; i < mods.Count; ++i) {
      value = mods[i].Modify(startValue, value);
    }

    int retValue = Mathf.FloorToInt(value);
    retValue = Mathf.Clamp(retValue, minDamage, maxDamage);
    return retValue;
  }

  int Compare(ValueModifier x, ValueModifier y)
  {
    return x.sortOrder.CompareTo(y.sortOrder);
  }
  #endregion
}