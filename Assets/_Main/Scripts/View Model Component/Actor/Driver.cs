﻿using UnityEngine;
using System.Collections;

public class Driver : MonoBehaviour 
{
	public Drivers normal;
	public Drivers special;

	public Drivers Current
	{
		get
		{
			return special != Drivers.None ? special : normal;
		}
	}

	public void UpdateAttackPattern(string name)
	{    
		AttackPattern ap = gameObject.GetComponentInChildren<AttackPattern>();
		if(ap) {
			Destroy(ap.gameObject);
		}

		if(string.IsNullOrEmpty(name)) {
			normal = Drivers.Human;
		} else {
			normal = Drivers.Computer;
			GameObject instance = Util.InstantiatePrefab("Attack Pattern/" + name);
			instance.transform.SetParent(gameObject.transform);
		}
	}
}