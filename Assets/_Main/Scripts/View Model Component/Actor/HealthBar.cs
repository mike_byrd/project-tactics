﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    Health unitHealth;

    [SerializeField] Slider healthSlider;

    [SerializeField] Image fill;

    [SerializeField] Text healthLabel;

    int lastHealth = 0;

    Color allyColor = new Color(0, 1, .6f); // #00FF9AFF
    Color enemyColor = new Color(1, .2f, .2f); // #FF3232FF

    private void Awake()
    {
        healthSlider = GetComponentInChildren<Slider>();
        unitHealth = GetComponentInParent<Health>();
    }

    public void InitializeHealthBar(Alliances alliances)
    {
        if (alliances == Alliances.Enemy)
        {
            fill.color = enemyColor;
        }
    }

    private void Update()
    {
        if (lastHealth != unitHealth.HP)
        {
            healthSlider.value = (float) unitHealth.HP / (float) unitHealth.MHP;
            healthLabel.text = string.Format("{0} / {1}", unitHealth.HP, unitHealth.MHP);
        }

        if (unitHealth.HP <= 0 && healthSlider.gameObject.activeSelf)
        {
            healthSlider.gameObject.SetActive(false);
        }
        else if (unitHealth.HP > 0 && !healthSlider.gameObject.activeSelf)
        {
            healthSlider.gameObject.SetActive(true);
        }
    }
}