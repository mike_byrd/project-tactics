using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// This component will post notifications when equipping and un-equipping
/// items so that other components or game systems can respond (like actually
/// updating a visual character model with the item). It exposes a readonly 
/// list of the items which are currently equipped, just in case another 
/// system needs to review them. Also, the system is somewhat smart in that
/// it will automatically un-equip any item which had been previously equipped
/// in an overlapping slot(s) before equipping a new item in the same slot(s).
/// 
/// NOTE: This class does not prevent equipping the same item more than once.
/// </summary>
public class Equipment : MonoBehaviour
{
  #region Notifications
  public const string EquippedNotification = "Equipment.EquippedNotification";
  public const string UnEquippedNotification = "Equipment.UnEquippedNotification";
  #endregion

  #region Fields / Properties
  public IList<Equippable> items { get { return _items.AsReadOnly(); } }
  List<Equippable> _items = new List<Equippable>();
  #endregion

  #region Public
  public void Equip(Equippable item, EquipSlots slots)
  {
    UnEquip(slots);

    _items.Add(item);
    item.transform.SetParent(transform, false);
    item.slots = slots;
    item.OnEquip();

    this.PostNotification(EquippedNotification, item);
  }

  public void UnEquip(Equippable item)
  {
    item.OnUnEquip();
    item.slots = EquipSlots.None;
    item.transform.SetParent(transform, false);
    _items.Remove(item);

    this.PostNotification(UnEquippedNotification, item);
  }

  public void UnEquip(EquipSlots slots)
  {
    for (int i = _items.Count - 1; i >= 0; --i) {
      Equippable item = _items[i];
      if ((item.slots & slots) != EquipSlots.None)
        UnEquip(item);
    }
  }

  public Equippable GetItem(EquipSlots slots)
  {
    for (int i = _items.Count - 1; i >= 0; --i) {
      Equippable item = _items[i];
      if ((item.slots & slots) != EquipSlots.None)
        return item;
    }
    return null;
  }
  #endregion
}