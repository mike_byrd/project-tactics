﻿using UnityEngine;
using System.Collections;
using MCB;

public class ActionPoints : MonoBehaviour
{
  #region Fields
  public int AP {
    get { return stats[StatTypes.AP]; }
    set { stats[StatTypes.AP] = value; }
  }

  public int MAP {
    get { return stats[StatTypes.MAP]; }
    set { stats[StatTypes.MAP] = value; }
  }

  Unit unit;
  Stats stats;
  #endregion

  #region MonoBehaviour
  void Awake()
  {
    stats = GetComponent<Stats>();
    unit = GetComponent<Unit>();
  }

  void OnEnable()
  {
    this.AddObserver(OnMPWillChange, Stats.WillChangeNotification(StatTypes.AP), stats);
    this.AddObserver(OnMMPDidChange, Stats.DidChangeNotification(StatTypes.MAP), stats);
    this.AddObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification, unit);
  }

  void OnDisable()
  {
    this.RemoveObserver(OnMPWillChange, Stats.WillChangeNotification(StatTypes.AP), stats);
    this.RemoveObserver(OnMMPDidChange, Stats.DidChangeNotification(StatTypes.MAP), stats);
    this.RemoveObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification, unit);
  }
  #endregion

  #region Event Handlers
  void OnMPWillChange(object sender, object args)
  {
    ValueChangeException vce = args as ValueChangeException;
    vce.AddModifier(new ClampValueModifier(int.MaxValue, 0, stats[StatTypes.MAP]));
  }

  void OnMMPDidChange(object sender, object args)
  {
    int oldMMP = (int)args;
    if (MAP > oldMMP)
      AP += MAP - oldMMP;
    else
      AP = Mathf.Clamp(AP, 0, MAP);
  }

  void OnTurnBegan(object sender, object args)
  {
    if (AP < MAP) {
      // MP += Mathf.Max(Mathf.FloorToInt(MMP * 0.1f), 1); // disable MP regen for now.
      AP = MAP;
    }
  }
  #endregion
}
