﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class HitSuccessIndicator : MonoBehaviour
{
  const string ShowKey = "Show";
  const string HideKey = "Hide";

  [SerializeField] Canvas canvas;
  [SerializeField] GameObject panel;
  [SerializeField] Image arrow;
  [SerializeField] Text label;
  Tweener transition;

  void Start()
  {
    //canvas.gameObject.SetActive(false);
  }

  public void SetStats(int chance, int amount)
  {
    arrow.fillAmount = (chance / 100f);
    label.text = string.Format("{0}% {1}pt(s)", chance, Mathf.Abs(amount));
    label.color = amount > 0 ? Color.green : Color.red;
  }

  public void Show()
  {
    //canvas.gameObject.SetActive(true);
    SetVisible(true);
  }

  public void Hide()
  {
    SetVisible(false);
    //transition.completedEvent += delegate (object sender, System.EventArgs e) {
    //  canvas.gameObject.SetActive(false);
    //};
  }

  void SetVisible(bool visible)
  {
    RectTransform rectTransform = panel.GetComponent<RectTransform>();
    float endPosition;
    if (visible) {
      endPosition = 130; // TODO: Store this value somewhere.
    } else {
      endPosition = -rectTransform.sizeDelta.y;
    }

    rectTransform.DOAnchorPosY(endPosition, .5f, true);
  }
}