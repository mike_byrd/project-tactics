﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FloatingText : MonoBehaviour
{
  [SerializeField] Animator anim;
  [SerializeField] TextMeshProUGUI textObj;

  float clipLength;

  public void SetColor(Color color)
  {
    textObj.color = color;
  }

  public void Show(string text)
  {
    textObj.text = text;

    AnimatorClipInfo[] clipInfo = anim.GetCurrentAnimatorClipInfo(0);
    clipLength = clipInfo[0].clip.length;

    Destroy(this.gameObject, clipLength);
  }
}
