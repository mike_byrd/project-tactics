﻿using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SimpleInfoCard : UIView
{
    [SerializeField] TMP_Text info;

    [SerializeField] RectTransform _rectTransform;

    public RectTransform rectTransform
    {
        get { return _rectTransform; }
    }

    public void UpdateInfo(string text)
    {
        info.text = text;
    }

    public void SetVisible(bool visible)
    {
        if (visible)
        {
            Show(true);
        }
        else
        {
            Hide();
        }
    }

    public void Clear()
    {
        info.text = "";
    }
}