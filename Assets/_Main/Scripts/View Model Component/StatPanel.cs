﻿using UnityEngine;
using UnityEngine.UI;
using MCB;
using System.Collections;
using TMPro;

public class StatPanel : MonoBehaviour
{
    public Sprite allyBackground;
    public Sprite enemyBackground;
    public Image background;

    public TMP_Text hpLabel;
    public TMP_Text maxHpLabel;
    public TMP_Text apLabel;
    public TMP_Text mpLabel;

    public GameObject unitInfoPanel;
    public TMP_Text nameLabel;
    public TMP_Text jobLabel;
    public TMP_Text lvLabel;

    public Vector3 showPosition;
    public Vector3 hidePosition;

    public void UpdateUI(Unit unit)
    {
        Alliance alliance = unit.Alliance;
        Job job = unit.Job;
        Stats stats = unit.Stats;

        background.sprite = alliance.type == Alliances.Enemy ? enemyBackground : allyBackground;
        // avatar.sprite = null; Need a component which provides this data
        nameLabel.text = unit.DisplayName;

        hpLabel.text = stats[StatTypes.HP].ToString();
        maxHpLabel.text = stats[StatTypes.MHP].ToString();
        apLabel.text = stats[StatTypes.AP].ToString();
        mpLabel.text = stats[StatTypes.MP].ToString();
        lvLabel.text = $"LV. {stats[StatTypes.LVL]}";
        jobLabel.text = Util.GetDisplayName(job.jobType);
    }
}