﻿using UnityEngine;
using System.Collections;

/// <summary>
/// If an item has a consumable component, then our systems which allow the use 
/// of items in battle will be able to show the item as an option for use. The 
/// consumption of said item would then apply whatever features were also on the
/// item, to whichever target is specified with the Consume method. Note that I 
/// want to specify a target because it shouldn’t be assumed that the consumable 
/// will be applied to the user – while the user might wish to use a health 
/// potion on himself, there might be times he wishes to use a health potion on
/// an ally, or perhaps the consumable is a bomb, and then the target would 
/// almost certainly be an opponent.
/// </summary>
public class Consumable : MonoBehaviour 
{
	public void Consume (GameObject target)
	{
		Feature[] features = GetComponentsInChildren<Feature>();
		for (int i = 0; i < features.Length; ++i) {
			features[i].Apply(target);
		}
	}
}