﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Items which are equippable have features which are activated for as long
/// as the item is still equipped. However, with equipment you are required
/// to manage what you choose to wear. For instance, there may only be a single
/// accessory slot and you will have to choose between the amulet of speed or
/// the buckler of defense. Perhaps different missions would justify swapping
/// out your equipment.
/// </summary>
public class Equippable : MonoBehaviour
{
    #region Fields

    /// <summary>
    /// The EquipSlots flag which is the default
    /// equip location(s) for this item.
    /// For example, a normal weapon would only specify
    /// primary, but a two-handed weapon would specify
    /// both primary and secondary.
    /// </summary>
    public EquipSlots defaultSlots;

    /// <summary>
    /// Some equipment may be allowed to be equipped in
    /// more than one slot location, such as when
    /// dual-wielding swords.
    /// </summary>
    public EquipSlots secondarySlots;

    /// <summary>
    /// The slot(s) where an item is currently equipped
    /// </summary>
    public EquipSlots slots;

    public ItemType itemType;

    [SerializeField] Material _allyMaterial;

    [SerializeField] Material _enemyMaterial;

    bool _isEquipped;

    #endregion

    #region Public

    public void OnEquip()
    {
        if (_isEquipped)
            return;

        _isEquipped = true;

        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
            features[i].Activate(gameObject);
    }

    public void OnUnEquip()
    {
        if (!_isEquipped)
            return;

        _isEquipped = false;

        Feature[] features = GetComponentsInChildren<Feature>();
        for (int i = 0; i < features.Length; ++i)
            features[i].Deactivate();
    }

    public void SetAllianceColor(Alliances alliance)
    {
        if (!_allyMaterial || !_enemyMaterial) return;

        MeshRenderer meshRenderer = transform.GetComponentInChildren<MeshRenderer>();
        if (alliance == Alliances.Ally)
        {
            meshRenderer.material = _allyMaterial;
        }
        else if (alliance == Alliances.Enemy)
        {
            meshRenderer.material = _enemyMaterial;
        }
    }

    #endregion
}