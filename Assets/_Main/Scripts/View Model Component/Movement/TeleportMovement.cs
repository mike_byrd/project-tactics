﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MCB;
using DG.Tweening;
using MCB.Common;

public class TeleportMovement : Movement
{
    public override IEnumerator DoMove(Tile position)
    {
        yield return null;
    }

    public override IEnumerator DoMove(List<Tile> waypoints)
    {
        throw new System.NotImplementedException();
    }

    public override IEnumerator Traverse(List<Tile> tiles)
    {
        DG.Tweening.Tweener spin = jumper.DOLocalRotate(new Vector3(0, 360, 0), 0.5f, RotateMode.LocalAxisAdd);
        spin.SetEase(Ease.InOutQuad);
        spin.SetLoops(2, LoopType.Yoyo);

        DG.Tweening.Tweener shrink = transform.DOScale(Vector3.zero, 0.5f);
        shrink.SetEase(Ease.InBack);

        yield return shrink.WaitForCompletion();

        transform.position = tiles[0].Center;

        DG.Tweening.Tweener grow = transform.DOScale(Vector3.one, 0.5f);
        grow.SetEase(Ease.OutBack);

        yield return grow.WaitForCompletion();
    }
}