﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MCB;

public class WalkMovement : Movement
{
    #region Protected

    protected override bool ExpandSearch(Tile from, Tile to)
    {
        // Skip if the distance in height between the two tiles is more than the unit can jump
        if ((Mathf.Abs(from.Height - to.Height) > jumpHeight))
            return false;

        if (to.content)
        {
            Unit u = to.content.GetComponent<Unit>();
            // Allow the search to include tiles occupied by allies. Units CAN walk through allies.
            if (u && u.GetComponent<Alliance>().IsMatch(unit.GetComponent<Alliance>(), Targets.Ally))
            {
                return true;
            }

            // Any other content, currently enemies, will be skipped. Units can't walk through enemies.
            return false;
        }

        return base.ExpandSearch(from, to);
    }

    public void Update()
    {
        if (_pathUpdated && !isMoving)
        {
            _pathUpdated = false;
            StartCoroutine(Traverse(_waypoints));
        }
    }

    public override IEnumerator Traverse(List<Tile> tiles)
    {
        isMoving = true;
        unit.DestinationTile = tiles[tiles.Count - 1];

        for (int i = 1; i < tiles.Count; i++)
        {
            if (_pathUpdated)
            {
                break;
            }

            Tile from = tiles[i - 1];
            Tile to = tiles[i];

            Direction dir = from.GetDirection(to);
            if (Facing != dir)
            {
                StartCoroutine(Turn(dir));
            }

            yield return Walk(to);
        }

        if (!_pathUpdated)
        {
            unit.Anim.SetBool("Run", false);
        }

        // TODO: create separate movement for battle.
        if (!inBattle && unit.Owner.IsMe)
        {
            // Remove highlight upon reaching the destination.
            if (!unit.IsMouseHovering)
            {
                // We don't want to remove the highlight is the unit has mouse focus.
                unit.CurrentTile.HighlightForInteract(false);
            }
        }

        isMoving = false;
        unit.DestinationTile = null;
    }

    public override IEnumerator DoMove(Tile tile)
    {
        if (!tile.IsWalkable)
        {
            yield break;
        }

        if (unit.CurrentTile == tile)
        {
            yield break;
        }

        // TODO: Assign an owner from the server.
        if (unit.Owner != null && unit.Owner.IsMe && _waypoints != null)
        {
            // Remove highlight from the old path if one exists.
            Tile endTile = _waypoints[_waypoints.Count - 1];
            endTile.HighlightForInteract(false);
        }

        _waypoints = new List<Tile> { unit.CurrentTile };

        List<Tile> path = GetWayPoints(tile);
        if (path == null)
        {
            Debug.LogWarning("No path found");
            yield break;
        }

        _waypoints.AddRange(path);

        if (!isMoving)
        {
            unit.Anim.SetBool("Run", true);
            StartCoroutine(Traverse(_waypoints));
        }
        else
        {
            _pathUpdated = true;
            unit.DestinationTile.HighlightForInteract(false);
            unit.DestinationTile = tile;
        }

        // TODO: Assign an owner from the server.
        if ( unit.Owner != null && unit.Owner.IsMe)
        {
            // Apply highlight to the new destination.
            tile.HighlightForInteract(true);
        }
    }

    public override IEnumerator DoMove(List<Tile> waypoints)
    {
        if (waypoints == null || waypoints.Count == 0)
        {
            yield break;
        }

        _waypoints = waypoints;
        Tile tile = _waypoints[0];
        if (!isMoving)
        {
            unit.Anim.SetBool("Run", true);
            yield return Traverse(_waypoints);
        }
        else
        {
            _pathUpdated = true;
            unit.DestinationTile.HighlightForInteract(false);
            unit.DestinationTile = tile;
        }

        // TODO: Assign an owner from the server.
        if ( unit.Owner != null && unit.Owner.IsMe)
        {
            // Apply highlight to the new destination.
            tile.HighlightForInteract(true);
        }
    }

    #endregion

    #region Private

    IEnumerator Walk(Tile to)
    {
        float distance = Vector3.Distance(transform.position, to.Center);
        float speedDelta = moveSpeed * distance;

        walkTween = transform.DOMove(to.Center, speedDelta);
        walkTween.SetEase(Ease.Linear);

        to.Place(unit, false);

        yield return walkTween.WaitForCompletion();
    }

    IEnumerator Jump(Tile to)
    {
        if (unit.Anim)
        {
            unit.Anim.SetBool("Run", true);
        }

        // Jump sequence
        Sequence s1 = transform.DOJump(to.Center, Tile.StepHeight * 2f, 1, .45f);
        s1.SetEase(Ease.Linear);

        yield return s1.WaitForCompletion();
    }

    #endregion
}