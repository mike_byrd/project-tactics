﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG;
using DG.Tweening;
using MCB;
using MCB.Common;
using MCB.MapEditor;
using ProjectTactics;
using Tile = MCB.Tile;

public abstract class Movement : MonoBehaviour
{
    public bool allowDiagonal;
    public bool inBattle;
    [SerializeField] protected float moveSpeed = .5f;
    [SerializeField] protected float turnSpeed = .15f;

    #region Properties

    public int movementRange
    {
        get { return stats[StatTypes.MP]; }
        set { stats[StatTypes.MP] = value; }
    }

    public int maxMovementRange
    {
        get { return stats[StatTypes.MMP]; }
        set { stats[StatTypes.MMP] = value; }
    }

    public int jumpHeight
    {
        get { return stats[StatTypes.JMP]; }
    }

    protected bool isMoving;
    public bool IsMoving => isMoving;

    [SerializeField]
    protected Direction _facing;
    public Direction Facing => _facing;

    protected Unit unit;
    public Transform jumper { get; private set; }
    protected Stats stats;

    protected List<Tile> _waypoints;
    protected bool _pathUpdated;
    protected Tween turnTween;
    protected Tween walkTween;
    protected WorldGrid worldGrid;

    #endregion

    #region MonoBehaviour

    protected virtual void Awake()
    {
        unit = GetComponent<Unit>();
        jumper = transform.Find("Jumper");
    }

    private void OnEnable()
    {
        this.AddObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification, unit);
    }

    private void OnDisable()
    {
        this.RemoveObserver(OnTurnBegan, TurnOrderController.TurnBeganNotification, unit);
    }

    protected virtual void Start()
    {
        stats = GetComponent<Stats>();
    }

    #endregion

    #region Public

    public abstract IEnumerator DoMove(Tile position);
    public abstract IEnumerator DoMove(List<Tile> waypoints);

    public virtual List<Tile> GetTilesInRange(WorldGrid board)
    {
        List<Tile> retValue = board.Search(unit.CurrentTile, ExpandSearch);
        Filter(retValue);
        return retValue;
    }

    public abstract IEnumerator Traverse(List<Tile> tiles);

    #endregion

    #region Protected

    protected virtual bool ExpandSearch(Tile from, Tile to)
    {
        return (from.distance + 1) <= movementRange;
    }

    protected virtual void Filter(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
        {
            if (tiles[i].content != null)
            {
                tiles.RemoveAt(i);
            }
        }
    }

    protected virtual IEnumerator Turn(Direction dir)
    {
        turnTween = transform.DORotate(dir.ToEuler(), turnSpeed);
        _facing = dir;

        yield return turnTween.WaitForCompletion();
    }

    public virtual void SetFacing(Direction dir)
    {
        transform.eulerAngles = dir.ToEuler();
        _facing = dir;
    }

    #endregion

    public void SetWorldGrid(WorldGrid grid, Tile tile)
    {
        worldGrid = grid;
        tile.Place(unit);
    }

    public List<Tile> GetWayPoints(Tile destination)
    {
        if (worldGrid == null)
        {
            Debug.Log("Grid null for " + name);
            return null;
        }

        return worldGrid.FindPath(unit.CurrentTile, destination, jumpHeight, allowDiagonal);
    }

    public bool CanUnitMove()
    {
        return stats[StatTypes.MP] > 0;
    }

    void OnTurnBegan(object sender, object args)
    {
        if (movementRange < maxMovementRange)
        {
            movementRange = maxMovementRange;
        }
    }
}