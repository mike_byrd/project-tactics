﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MCB;
using MCB.Networking;
using ProjectTactics.DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace ProjectTactics
{
    public class LoginScene : MonoBehaviour
    {
        [SerializeField] private TMP_InputField usernameInput;
        [SerializeField] private TMP_InputField emailInput;
        [SerializeField] private TMP_InputField passwordInput;
        [SerializeField] private Button registerButton;
        [SerializeField] private Button loginButton;
        [SerializeField] private Toggle rememberMeCheckbox;
        [SerializeField] private TMP_Text serverStatusLabel;
        [SerializeField] private TMP_Text buildTypeLabel;

        [SerializeField] private Button user1Button;
        [SerializeField] private Button user2Button;
        [SerializeField] private Button user3Button;
        [SerializeField] private Button serverConnectButton;
        [SerializeField] private TMP_Dropdown serverDropdown;

        private string _environment;

#if PRODUCTION
        private const string ServerUri = "https://mcb-project-revenant.herokuapp.com";
#else
        private const string ServerUri = "http://localhost:3000";
#endif

        private void OnEnable()
        {
            NetworkManager.ConnectionStatusChanged += OnServerStatusChanged;
        }

        private void OnDisable()
        {
            NetworkManager.ConnectionStatusChanged -= OnServerStatusChanged;
        }

        private void Start()
        {
            NetworkManager.ConnectToServer(ServerUri);

            DOTween.Init();
            Random.InitState(DateTime.Now.Millisecond);
            NameGenerator.Initialize();
            UnitFactory.Initialize();

            UpdateStatusLabel();
            SetMenuEnabled(NetworkManager.IsConnected);
#if PRODUCTION
            buildTypeLabel.text = "Production";
            user1Button.gameObject.SetActive(false);
            user2Button.gameObject.SetActive(false);
            user3Button.gameObject.SetActive(false);
            serverDropdown.gameObject.SetActive(false);
            serverConnectButton.gameObject.SetActive(false);
#else
            buildTypeLabel.text = "Development";
            user1Button.gameObject.SetActive(true);
            user2Button.gameObject.SetActive(true);
            user3Button.gameObject.SetActive(true);
            serverDropdown.gameObject.SetActive(true);
            serverDropdown.value = GM.ServerSelectionIndex;
            serverConnectButton.gameObject.SetActive(true);
#endif
        }

        private void UpdateStatusLabel()
        {
            serverStatusLabel.text = NetworkManager.IsConnected
                ? "Server Status:\n<color=green>Connected</color>"
                : "Server Status:\n<color=red>Disconnected</color>";
        }

        private void OnServerStatusChanged(bool connected)
        {
            UpdateStatusLabel();
            SetMenuEnabled(connected);
        }

        private void SetMenuEnabled(bool enabled)
        {
            usernameInput.enabled = enabled;
            emailInput.enabled = enabled;
            passwordInput.enabled = enabled;
            registerButton.enabled = enabled;
            loginButton.enabled = enabled;
            rememberMeCheckbox.enabled = enabled;
        }

        public void RegisterWithUsername()
        {
            if (string.IsNullOrWhiteSpace(usernameInput.text))
            {
                DBG.Log("invalid info");
                return;
            }

            string username = usernameInput.text;
            string password = passwordInput.text;
#if !PRODUCTION
            TrySetupUserButtons(ref username, ref password);
#endif
            NetworkManager.RegisterWithUsername(username, password, OnLogin);
        }

        public void LoginWithUsername()
        {
            if (string.IsNullOrWhiteSpace(usernameInput.text))
            {
                DBG.Log("invalid info");
                return;
            }

            string username = usernameInput.text;
            string password = passwordInput.text;
#if !PRODUCTION
            TrySetupUserButtons(ref username, ref password);
#endif
            NetworkManager.LoginWithUsername(username, password, OnLogin);
        }

        private void OnLogin(LoginUserResult result)
        {
            NetworkManager.LocalPlayer = new PlayerHandle
            {
                PlayerData = new PlayerData(result.player)
            };

            DBG.Log($"Welcome to new adventure, {NetworkManager.LocalPlayer.PlayerData.Username}!");
            SceneManager.LoadScene("_Main/Scenes/Character Select Scene");
        }

        public void TestUserLogin(string name)
        {
            NetworkManager.LoginWithUsername(name, "Test123!", OnLogin);
        }

#if !PRODUCTION
        private static void TrySetupUserButtons(ref string username, ref string password)
        {
            if (username.ToLower() == "user1" ||
                username.ToLower() == "user2" ||
                username.ToLower() == "user3")
            {
                password = "Test123!";
            }
        }

        public void OnServerConnectClicked()
        {
            string newUri;
            switch (GM.ServerSelectionIndex)
            {
                case 0:
                    newUri = "http://localhost:3000";
                    break;
                case 1:
                    newUri = "https://mcb-project-revenant.herokuapp.com";
                    break;
                default:
                    newUri = "http://localhost:3000";
                    break;
            }

            NetworkManager.DisconnectFromServer();
            NetworkManager.ConnectToServer(newUri);
        }

        public void OnServerDropdownValueChanged(TMP_Dropdown dropdown)
        {
            GM.ServerSelectionIndex = dropdown.value;
        }
#endif
    }
}