﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;
using UnityEngine;

namespace MCB
{
    public class CharacterSelectScene : StateMachine
    {
        [SerializeField] public CreateCharacterPanel createCharacterPanel;
        [SerializeField] public SelectCharacterPanel selectCharacterPanel;

        public GameObject modelContainer;
        public GameObject currentModel;
        public GameObject currentHair;

        public UnitData currentUnitData;
        private Dictionary<JobType, GameObject> skinTones;

        private Dictionary<string, GameObject> modelTable;
        private Dictionary<string, GameObject> hairTable;

        private void OnEnable()
        {
            SelectCharacterPanel.NewCharacterButtonClicked += OnNewCharacterButtonClicked;
            CreateCharacterPanel.BackButtonClicked += OnBackButtonClicked;
        }

        private void OnDisable()
        {
            SelectCharacterPanel.NewCharacterButtonClicked -= OnNewCharacterButtonClicked;
            CreateCharacterPanel.BackButtonClicked -= OnBackButtonClicked;
        }

        private void Awake()
        {
            modelTable = new Dictionary<string, GameObject>();
            hairTable = new Dictionary<string, GameObject>();
        }

        private void Start()
        {
            createCharacterPanel.Hide();
            selectCharacterPanel.Show();

            ChangeState<SelectingCharacterState>();
        }

        private void OnNewCharacterButtonClicked()
        {
            createCharacterPanel.Show();
            selectCharacterPanel.Hide();

            ChangeState<CreatingCharacterState>();
        }

        private void OnBackButtonClicked()
        {
            createCharacterPanel.Hide();
            selectCharacterPanel.Show();

            if (currentModel)
            {
                currentModel.SetActive(false);
            }

            ChangeState<SelectingCharacterState>();
        }

        public void GetCharacterInventory()
        {
            NetworkManager.GetUnitInventory(OnGetUnitInventory);
        }

        private void OnGetUnitInventory(UnitData[] unitDataList)
        {
            createCharacterPanel.Hide();
            selectCharacterPanel.Show();

            ChangeState<SelectingCharacterState>();
        }

        public void UpdateCharacterSpotlight(UnitData unitData)
        {
            currentUnitData = unitData;

            if (currentModel)
            {
                // A different base model may already be showing, so lets hide it.
                currentModel.SetActive(false);
            }

            JobType job = unitData.Job;
            GenderType gender = unitData.Gender;

            int modelIndex = unitData.BaseModelId;
            int hairIndex = unitData.HairModelId;

            currentModel = GetModel(job, gender, modelIndex);
            currentModel.SetActive(true);

            if (currentHair)
            {
                currentHair.SetActive(false);
            }

            currentHair = GetHair(gender, hairIndex);
            currentHair.SetActive(true);

            UnitModelVisual visual = currentModel.AddComponent<UnitModelVisual>();
            visual.Init(currentModel, currentHair);

            currentModel.name = unitData.Name;
            currentModel.SetActive(true);
        }

        private GameObject GetModel(JobType job, GenderType gender, int modelIndex)
        {
            var baseModelPrefab = UnitFactory.GetSkinTones(job, gender)[modelIndex];
            if (!modelTable.TryGetValue(baseModelPrefab.name, out GameObject model))
            {
                model = Util.InstantiatePrefab(baseModelPrefab, modelContainer.transform);
                modelTable[baseModelPrefab.name] = model;
            }

            return model;
        }

        private GameObject GetHair(GenderType gender, int hairIndex)
        {
            var hairPropPrefab = UnitFactory.GetHairstyles(gender)[hairIndex];
            if (!hairTable.TryGetValue(hairPropPrefab.name, out GameObject hair))
            {
                hair = Util.InstantiatePrefab(hairPropPrefab);
                hairTable[hairPropPrefab.name] = hair;
            }

            return hair;
        }
    }
}
