using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MCB;
using MCB.Cameras;
using MCB.Common;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class GameWorldScene : StateMachine
    {
        public Location townCenter;

        public MenuBar menuBar;
        public ZoneChangeMenuPanel zoneChangeMenu;
        public SettingsMenuPanel settingsMenu;
        public AbilityMenuPanelController abilityMenu;
        public UnitManagerViewDetailsPanel viewDetailsPanel;
        public Camera mainCamera;
        public Camera uiCamera;

        public Location TownCenter => townCenter;

        private void Start()
        {
            ChangeState<InitTownCenterState>();
        }

        public void SpawnInTown(PlayerHandle handle)
        {
            Unit unit = UnitFactory.Create(handle.PlayerData.LeaderData);
            handle.Unit = unit;
            unit.Owner = handle;
            unit.Hud.namePlate.UpdateUI(unit.Owner.PlayerData.NameplateDecorationId);
            unit.Hud.HealthBar.gameObject.SetActive(false);
            unit.Hud.Initialize(uiCamera);

            townCenter.Grid.Place(unit, handle.PlayerData.Position);
            unit.Movement.allowDiagonal = true;

            bool isMe = NetworkManager.LocalPlayer.PlayerData.ID == handle.PlayerData.ID;
            if (!isMe)
            {
                return;
            }

            handle.IsMe = true;
            NetworkManager.LocalPlayer = handle;
            FreeStyleCamera.SetTarget(unit.cameraTarget);
        }

        public void UpdateInbox(List<Mail> mailList)
        {
            menuBar.UpdateMailButton(mailList.Count);
        }
    }
}
