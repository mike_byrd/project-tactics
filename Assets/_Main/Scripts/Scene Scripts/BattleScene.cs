using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class BattleScene : StateMachine
    {
        public Location location;
        public Camera mainCamera;
        public Camera uiCamera;

        private Dictionary<string, Unit> _units;

        private void Start()
        {
            _units = new Dictionary<string, Unit>();
            ChangeState<InitBattleState>();
        }

        public void Spawn(UnitData unitData, Point position, Alliances alliances)
        {
            Unit unit = UnitFactory.Create(unitData);

            location.Grid.Place(unit, position);

            //TODO: add alliance to unitData?
            Alliance alliance = unit.GetComponent<Alliance>();
            if (alliances == Alliances.Enemy)
            {
                alliance.type = Alliances.Enemy;
                unit.Facing = Direction.South;
            }
            else
            {
                unit.Facing = Direction.North;
            }

            Debug.Log($"{unit.DisplayName} is facing {unit.Facing}");
            unit.Movement.allowDiagonal = false;
            unit.Movement.inBattle = true;
            unit.Match();

            unit.Hud.Initialize(uiCamera);

            _units[unit.ID] = unit;
        }
    }
}