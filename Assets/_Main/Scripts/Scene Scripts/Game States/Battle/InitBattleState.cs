using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class InitBattleState : BattleState
    {
        public override void Enter()
        {
            base.Enter();
            SetupBattle();
        }

        private void SetupBattle()
        {
            Debug.Log("Setup Battle");
            owner.location.Initialize(BattleManager.LocationData);

            WorldGrid grid = owner.location.Grid;
            UnitData[] enemies = BattleManager.GetEnemyUnits();
            for (int i = 0; i < enemies.Length; i++)
            {
                if (i >= grid.EnemyStartingTiles.Count)
                {
                    Debug.Log("Not enough enemy starting tiles.");
                    break;
                }

                owner.Spawn(enemies[i], grid.EnemyStartingTiles[i], Alliances.Enemy);
            }

            // TODO: Allow players to place units.
            foreach (Point position in grid.PlayerStartingTiles)
            {
                Tile tile = grid.GetTile(position);
                tile.SetVisible(true);
//                tile.state = TileState.Ally;
            }
        }
    }
}
