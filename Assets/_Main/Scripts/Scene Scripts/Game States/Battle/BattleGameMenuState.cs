using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics;
using UnityEngine;

namespace MCB
{
    public class BattleGameMenuState : BattleState
    {
        protected override void AddListeners()
        {

            SettingsMenuPanel.LogoutClicked += OnLogoutClicked;
            SettingsMenuPanel.ExitGameClicked += OnExitGameClicked;
        }

        protected override void RemoveListeners()
        {

            SettingsMenuPanel.LogoutClicked -= OnLogoutClicked;
            SettingsMenuPanel.ExitGameClicked -= OnExitGameClicked;
        }

        public override void Enter()
        {
            base.Enter();
            _owner.settingsMenuPanel.Show();

//            SettingsMenuPanel.LeaveCombat += GameMenuPanelOnLeaveCombat;
        }

        public override void Exit()
        {
            base.Exit();
            _owner.settingsMenuPanel.Hide();

//            SettingsMenuPanel.LeaveCombat -= GameMenuPanelOnLeaveCombat;
        }

        private void GameMenuPanelOnLeaveCombat()
        {
            Debug.Log("Leaving Combat");
            NetworkManager.LeaveBattle();
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                OnGameMenuCloseClicked();
            }
        }

        private void OnGameMenuCloseClicked()
        {
            _owner.ChangeToPreviousState();
        }

        private void OnLogoutClicked()
        {
            NetworkManager.Logout();
        }

        private void OnExitGameClicked()
        {
            GM.ExitGame();
        }
    }

}