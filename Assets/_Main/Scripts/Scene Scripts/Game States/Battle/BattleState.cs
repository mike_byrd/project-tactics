using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace ProjectTactics
{
    public class BattleState : State
    {
        protected BattleScene owner;

        private void Awake()
        {
            owner = GetComponent<BattleScene>();
        }
    }
}
