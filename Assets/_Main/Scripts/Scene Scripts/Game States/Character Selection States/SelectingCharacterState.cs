﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;
using UnityEngine.SceneManagement;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;

namespace MCB
{
    public class SelectingCharacterState : CharacterSelectState
    {
        public override void Enter()
        {
            base.Enter();
            owner.selectCharacterPanel.ShowCharacterSlots();

            UnitData[] units = NetworkManager.LocalPlayer.PlayerData.Inventory.GetUnits();
            owner.selectCharacterPanel.UpdateUI(units);

            if (units.Length > 0)
            {
                owner.currentUnitData = units[0];
                DBG.Log($"Selected unit: Name: {owner.currentUnitData.Name}. ID: {owner.currentUnitData.ID}");
            }
        }

        protected override void AddListeners()
        {
            SelectCharacterPanel.SelectionChanged += SelectCharacterPanelOnSelectionChanged;
            SelectCharacterPanel.BackClicked += OnBackClicked;
            SelectCharacterPanel.CreateClicked += OnCreateClicked;
            SelectCharacterPanel.JoinClicked += OnJoinClicked;
        }

        protected override void RemoveListeners()
        {
            SelectCharacterPanel.SelectionChanged -= SelectCharacterPanelOnSelectionChanged;
            SelectCharacterPanel.BackClicked -= OnBackClicked;
            SelectCharacterPanel.CreateClicked -= OnCreateClicked;
            SelectCharacterPanel.JoinClicked -= OnJoinClicked;
        }

        private void OnBackClicked(string panelId)
        {
            if (panelId == "root")
            {
                NetworkManager.Logout();
            }
        }

        private void OnJoinClicked()
        {
            NetworkManager.GetWorlds(OnGetWorlds);

            owner.ChangeState<JoiningWorldState>();
        }

        private void OnGetWorlds(GetWorldsResult[] results)
        {
            owner.selectCharacterPanel.UpdateJoinPanelUI(results);
        }

        private void OnCreateClicked()
        {
            owner.ChangeState<CreatingWorldState>();
        }

        private void SelectCharacterPanelOnSelectionChanged(UnitData selection)
        {
            owner.UpdateCharacterSpotlight(selection);
        }
    }
}
