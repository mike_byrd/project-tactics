using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MCB
{
    public class JoiningWorldState : CharacterSelectState
    {
        public override void Enter()
        {
            base.Enter();
            owner.selectCharacterPanel.ShowJoinRoomPanel();
        }

        protected override void AddListeners()
        {
            JoinWorldPanel.JoinClicked += OnJoinClicked;
            JoinWorldPanel.BackClicked += OnBackClicked;
        }

        protected override void RemoveListeners()
        {
            JoinWorldPanel.JoinClicked -= OnJoinClicked;
            JoinWorldPanel.BackClicked -= OnBackClicked;
        }

        private void OnJoinClicked(string worldID)
        {
            NetworkManager.JoinWorld(worldID, owner.currentUnitData.ID, OnJoinWorld);
        }

        private void OnJoinWorld(EnterWorldResult result)
        {
            NetworkManager.Party = new PartyData(result.party);
            SceneManager.LoadScene("Game World Scene");
        }

        private void OnBackClicked()
        {
            owner.ChangeState<SelectingCharacterState>();
        }
    }

}
