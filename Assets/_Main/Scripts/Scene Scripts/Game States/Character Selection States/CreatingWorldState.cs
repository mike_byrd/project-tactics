using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MCB
{
    public class CreatingWorldState : CharacterSelectState
    {
        public override void Enter()
        {
            base.Enter();
            owner.selectCharacterPanel.ShowCreateRoomPanel();
        }

        public override void Exit()
        {
            base.Exit();
            owner.createCharacterPanel.Hide();
        }

        protected override void AddListeners()
        {
            CreateWorldPanel.CreateClicked += OnCreateClicked;
            CreateWorldPanel.BackClicked += OnBackClicked;
        }

        protected override void RemoveListeners()
        {
            CreateWorldPanel.CreateClicked -= OnCreateClicked;
            CreateWorldPanel.BackClicked -= OnBackClicked;
        }

        private void OnCreateClicked(string roomName)
        {
            NetworkManager.CreateWorld(roomName, owner.currentUnitData.ID, OnCreateWorld);
        }

        private void OnCreateWorld(EnterWorldResult result)
        {
            NetworkManager.Party = new PartyData(result.party);
            SceneManager.LoadScene("Game World Scene");
        }

        private void OnBackClicked()
        {
            owner.ChangeState<SelectingCharacterState>();
        }
    }
}
