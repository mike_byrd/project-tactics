﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using UnityEngine;

namespace MCB
{
    public class CharacterSelectState : State
    {
        protected CharacterSelectScene owner;

        protected virtual void Awake()
        {
            owner = GetComponent<CharacterSelectScene>();
        }
    }
}
