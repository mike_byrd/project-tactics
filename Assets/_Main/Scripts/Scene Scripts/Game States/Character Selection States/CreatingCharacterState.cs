﻿using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;

namespace MCB
{
    public class CreatingCharacterState : CharacterSelectState
    {
        public static event Action CharacterCreated;

        private UnitData unitData;
        private Dictionary<string, int> currentProperties;

        protected override void Awake()
        {
            base.Awake();
            currentProperties = new Dictionary<string, int>
            {
                {"skin", 0},
                {"hair", 0},
            };
        }

        public override void Enter()
        {
            base.Enter();

            var unitDataModel = new UnitDataModel
            {
                gender = "Male",
                job = "Soldier",
                baseModelId = 0,
                hairModelId = 0,
                level = 1,
                alliance = "Ally"
            };

            unitData = new UnitData(unitDataModel);

            owner.UpdateCharacterSpotlight(unitData);
            owner.createCharacterPanel.UpdateUI(unitData);
        }

        protected override void AddListeners()
        {
            CharacterPropertySlot.LeftClicked += CharacterPropertySlotOnLeftClicked;
            CharacterPropertySlot.RightClicked += CharacterPropertySlotOnRightClicked;

            CreateCharacterPanel.GenderButtonClicked += OnGenderButtonClicked;
            CreateCharacterPanel.JobButtonClicked += OnJobButtonClicked;
            CreateCharacterPanel.CreateCharacterButtonClicked += OnCreateCharacterButtonClicked;
        }

        protected override void RemoveListeners()
        {
            CharacterPropertySlot.LeftClicked -= CharacterPropertySlotOnLeftClicked;
            CharacterPropertySlot.RightClicked -= CharacterPropertySlotOnRightClicked;

            CreateCharacterPanel.GenderButtonClicked -= OnGenderButtonClicked;
            CreateCharacterPanel.JobButtonClicked -= OnJobButtonClicked;
            CreateCharacterPanel.CreateCharacterButtonClicked -= OnCreateCharacterButtonClicked;
        }

        private void OnCreateCharacterButtonClicked()
        {
            owner.createCharacterPanel.SetCreateButtonActive(false);
            NetworkManager.CreateStartingUnit(unitData.ToDataModel(), OnCharacterCreated);
        }

        private void OnCharacterCreated()
        {
            owner.GetCharacterInventory();
        }

        private void OnGenderButtonClicked(GenderType gender)
        {
            unitData.Gender = gender;
            unitData.BaseModelId = 0;
            unitData.HairModelId = 0;

            currentProperties["skin"] = 0;
            currentProperties["hair"] = 0;

            owner.createCharacterPanel.ResetPropertySlots();
            owner.UpdateCharacterSpotlight(unitData);
        }

        private void OnJobButtonClicked(JobType job)
        {
            unitData.Job = job;
            unitData.BaseModelId = 0;
            unitData.HairModelId = 0;

            currentProperties["skin"] = 0;
            currentProperties["hair"] = 0;

            owner.createCharacterPanel.ResetPropertySlots();
            owner.UpdateCharacterSpotlight(unitData);
        }

        private int DecreasePropertyIndex(string propertyName, int totalOptions)
        {
            var propertyIndex = currentProperties[propertyName];
            if (propertyIndex <= 0)
            {
                propertyIndex = totalOptions - 1;
            }
            else
            {
                propertyIndex--;
            }

            currentProperties[propertyName] = propertyIndex;
            return propertyIndex;
        }

        private int IncreasePropertyIndex(string propertyName, int totalOptions)
        {
            var propertyIndex = currentProperties[propertyName];
            if (propertyIndex >= totalOptions - 1)
            {
                propertyIndex = 0;
            }
            else
            {
                propertyIndex++;
            }

            currentProperties[propertyName] = propertyIndex;
            return propertyIndex;
        }

        private void CharacterPropertySlotOnLeftClicked(CharacterPropertySlot propertySlot)
        {
            JobType job = unitData.Job;
            GenderType gender = unitData.Gender;

            switch (propertySlot.PropertyName)
            {
                case "skin":
                    var skinTones = UnitFactory.GetSkinTones(job, gender);
                    var newPropertyIndex = DecreasePropertyIndex(propertySlot.PropertyName, skinTones.Length);

                    unitData.BaseModelId = newPropertyIndex;
                    owner.UpdateCharacterSpotlight(unitData);
                    propertySlot.UpdateUI($"{newPropertyIndex + 1}");
                    break;
                case "hair":
                    var hairStyles = UnitFactory.GetHairstyles(gender);
                    newPropertyIndex = DecreasePropertyIndex(propertySlot.PropertyName, hairStyles.Length);

                    unitData.HairModelId = newPropertyIndex;
                    owner.UpdateCharacterSpotlight(unitData);
                    propertySlot.UpdateUI($"{newPropertyIndex + 1}");
                    break;
                default:
                    break;
            }
        }

        private void CharacterPropertySlotOnRightClicked(CharacterPropertySlot propertySlot)
        {
            JobType job = unitData.Job;
            GenderType gender = unitData.Gender;

            switch (propertySlot.PropertyName)
            {
                case "skin":
                    var skinTones = UnitFactory.GetSkinTones(job, gender);
                    var newPropertyIndex = IncreasePropertyIndex(propertySlot.PropertyName, skinTones.Length);

                    unitData.BaseModelId = newPropertyIndex;
                    owner.UpdateCharacterSpotlight(unitData);
                    propertySlot.UpdateUI($"{newPropertyIndex + 1}");
                    break;
                case "hair":
                    var hairStyles = UnitFactory.GetHairstyles(gender);
                    newPropertyIndex = IncreasePropertyIndex(propertySlot.PropertyName, hairStyles.Length);

                    unitData.HairModelId = newPropertyIndex;
                    owner.UpdateCharacterSpotlight(unitData);
                    propertySlot.UpdateUI($"{newPropertyIndex + 1}");
                    break;
                default:
                    break;
            }
        }
    }
}
