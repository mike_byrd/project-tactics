using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class InitTownCenterState : TownCenterWorldState
    {
        private void Start()
        {
            Physics.queriesHitTriggers = true;

            World currentWorld = NetworkManager.CurrentWorld;
            LocationData townCenterData = currentWorld.GetLocation("town_center");
            _owner.townCenter.Initialize(townCenterData);

            PlayerHandle[] players = currentWorld.GetPlayers();
            foreach (PlayerHandle p in players)
            {
                Debug.Log($"Player spawned: {p.PlayerData.Username}");
                _owner.SpawnInTown(p);
            }

            _owner.abilityMenu.SetupForTownCenter();
            _owner.abilityMenu.UpdateUI(NetworkManager.LocalPlayer.Unit.AbilityCatalog);
            _owner.menuBar.mainStatPanel.UpdateUI(NetworkManager.LocalPlayer.Unit);

            _owner.ChangeState<ExploreTownState>();
        }
    }
}
