using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using MCB.Networking;
using ProjectTactics.DataModels;
using UnityEngine;

namespace ProjectTactics
{
    public class TownCenterWorldState : State
    {
        protected GameWorldScene _owner;

        private void Awake()
        {
            _owner = GetComponent<GameWorldScene>();
        }

        protected override void AddListeners()
        {
            NetworkManager.PlayerJoined += OnPlayerJoined;
            NetworkManager.PlayerLeft += OnPlayerLeft;
            NetworkManager.PlayerMoved += OnPlayerMoved;

            NetworkManager.MailReceived += OnMailReceived;

            NetworkManager.PartyUpdated += OnPartyUpdated;
        }

        protected override void RemoveListeners()
        {
            NetworkManager.PlayerJoined -= OnPlayerJoined;
            NetworkManager.PlayerLeft -= OnPlayerLeft;
            NetworkManager.PlayerMoved -= OnPlayerMoved;

            NetworkManager.MailReceived -= OnMailReceived;

            NetworkManager.PartyUpdated -= OnPartyUpdated;
        }

        private void OnPartyUpdated(PartyData party)
        {
            UpdatePartyIcons();
        }

        private void UpdatePartyIcons()
        {
            PartyData party = NetworkManager.Party;
            PlayerHandle[] players = NetworkManager.CurrentWorld.GetPlayers();
            foreach (PlayerHandle player in players)
            {
                string playerId = player.PlayerData.ID;
                if (party.HasPlayer(playerId) && party.units.Count > 1)
                {
                    player.Unit.Hud.ShowPartyIcon(party.hostId == playerId);
                }
                else
                {
                    player.Unit.Hud.HidePartyIcon();
                }
            }
        }

        protected virtual void OnMailReceived(List<Mail> mailList)
        {
            _owner.UpdateInbox(mailList);
        }

        protected virtual void OnPlayerJoined(PlayerHandle player)
        {
            _owner.SpawnInTown(player);
        }

        protected virtual void OnPlayerLeft(PlayerHandle player)
        {
            Destroy(player.Unit.gameObject);
        }

        private void OnPlayerMoved(PlayerHandle player, Point position)
        {
            Tile tile = _owner.TownCenter.Grid.GetTile(position);
            StartCoroutine(player.Unit.Movement.DoMove(tile));
        }
    }
}
