using System;
using System.Collections;
using System.Collections.Generic;
using MCB;
using MCB.Common;
using MCB.Networking;
using MCB.UI;
using ProjectTactics.DataModels;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace ProjectTactics
{
    public class ExploreTownState : TownCenterWorldState
    {
        protected override void AddListeners()
        {
            base.AddListeners();
            _owner.menuBar.SettingsClicked += MenuBarOnSettingsClicked;

            Unit.Clicked += UnitOnClicked;

            ContextMenu.UnitInspected += ContextMenuOnUnitInspected;
            PartyMenuPanelStateRoot.UnitInspected += PartyMenuPanelStateRootOnUnitInspected;
            UnitManagerViewDetailsPanel.DoneButtonClicked += ViewDetailsPanelOnDoneButtonClicked;

            SettingsMenuPanel.LogoutClicked += OnLogoutClicked;
            SettingsMenuPanel.ExitGameClicked += OnExitGameClicked;
            SettingsMenuPanel.LeaveWorldClicked += OnLeaveWorldClicked;

            // Modal UI listeners
            ZoneChangePoint.Clicked += ZoneChangePointOnClicked;
            ZoneChangeMenuPanel.CloseClicked += ZoneChangeMenuPanelOnCloseClicked;
            ZoneChangeMenuPanel.BattleClicked += ZoneChangeMenuPanelOnBattleClicked;

            NetworkManager.BattleRequestComplete += NetworkManagerOnBattleRequestComplete;
        }

        protected override void RemoveListeners()
        {
            base.RemoveListeners();
            _owner.menuBar.SettingsClicked -= MenuBarOnSettingsClicked;

            Unit.Clicked -= UnitOnClicked;

            ContextMenu.UnitInspected -= ContextMenuOnUnitInspected;
            PartyMenuPanelStateRoot.UnitInspected -= PartyMenuPanelStateRootOnUnitInspected;
            UnitManagerViewDetailsPanel.DoneButtonClicked -= ViewDetailsPanelOnDoneButtonClicked;

            SettingsMenuPanel.LogoutClicked -= OnLogoutClicked;
            SettingsMenuPanel.ExitGameClicked -= OnExitGameClicked;
            SettingsMenuPanel.LeaveWorldClicked -= OnLeaveWorldClicked;

            // Modal UI listeners
            ZoneChangePoint.Clicked -= ZoneChangePointOnClicked;
            ZoneChangeMenuPanel.CloseClicked -= ZoneChangeMenuPanelOnCloseClicked;
            ZoneChangeMenuPanel.BattleClicked -= ZoneChangeMenuPanelOnBattleClicked;

            NetworkManager.BattleRequestComplete -= NetworkManagerOnBattleRequestComplete;
        }

        private void OnLeaveWorldClicked()
        {
            NetworkManager.LeaveWorld();
        }

        private void OnLogoutClicked()
        {
            NetworkManager.Logout();
        }

        private void OnExitGameClicked()
        {
            GM.ExitGame();
        }

        private void ZoneChangePointOnClicked(string zoneName)
        {
            switch (zoneName)
            {
                case K.ZoneName.TownCenter:
                    _owner.zoneChangeMenu.Show(true);
                    break;
            }
        }

        private void ZoneChangeMenuPanelOnCloseClicked()
        {
            _owner.zoneChangeMenu.Hide();
        }

        private void ZoneChangeMenuPanelOnBattleClicked()
        {
            // TODO: Load this based on current selected mission
            NetworkManager.RequestBattle(NetworkManager.LocalPlayer.PlayerData.ID, "battle01");
        }

        private void NetworkManagerOnBattleRequestComplete(CreateBattleResult result)
        {
            Debug.Log($"Battle started for: {result.battleId}");
            BattleManager.SetupBattleData(result);

            SceneManager.LoadScene("_Main/Scenes/Battle");
        }

        private void ViewDetailsPanelOnDoneButtonClicked()
        {
            _owner.viewDetailsPanel.Hide();
        }

        protected void ContextMenuOnUnitInspected(Unit unit)
        {
            Unit clone = RenderTextureManager.LoadForUnit(unit, out RenderTextureTarget textureTarget);
            _owner.viewDetailsPanel.InspectUnit(clone, textureTarget);
        }

        private void PartyMenuPanelStateRootOnUnitInspected(UnitData unitData)
        {
            Unit clone = RenderTextureManager.LoadForUnit(unitData, out RenderTextureTarget textureTarget);
            _owner.viewDetailsPanel.InspectUnit(clone, textureTarget);
        }

        private void MenuBarOnSettingsClicked()
        {
            _owner.settingsMenu.Show(true);
        }

        private void UnitOnClicked(Unit unit)
        {
            ContextMenu.Show(unit, Input.mousePosition);
        }

        Ray ray;
        RaycastHit hit;
        private bool _isMouseOnGrid = false;

        private void Update()
        {
            // TODO: Add input settings instead of hardcoded keys.
            if (Input.GetButtonDown("Cancel"))
            {
                // Hide the model if it's showing.
                UIModalView modal = UIModalView.CurrentModelView;
                if (modal)
                {
                    modal.Hide();
                    return;
                }

                // Otherwise get the current ui view with focus and dismiss it.
                UIPanel currentPanel = UIView.CurrentFocus as UIPanel;
                if (currentPanel != null)
                {
                    currentPanel.Controller.Dismiss();
                    return;
                }

                if (UIView.CurrentFocus is IDismissible dismissible)
                {
                    dismissible.Dismiss();
                    return;
                }

                // Finally, check to see if the settings menu is showing. If not, show it.
                if (!_owner.settingsMenu.IsVisible)
                {
                    _owner.settingsMenu.Show(true);
                }
                else
                {
                    _owner.settingsMenu.Hide();
                }
            }
            else if (Input.GetKeyDown(KeyCode.T))
            {
                _owner.TownCenter.Grid.ToggleTacticalView();
            }

            if (!Util.IsMouseOverGameWindow)
            {
                return;
            }

            if (!Input.GetMouseButtonDown(0))
            {
                return;
            }

            // Bit shift the index of the layer to get a bit mask
            ray = _owner.mainCamera.ScreenPointToRay(Input.mousePosition);
            int displayLayerMask = LayerMask.NameToLayer("TileDisplay");
            int surfaceBitMask = 1 << LayerMask.NameToLayer("TileSurface");
            int displayBitMask = 1 << displayLayerMask;
            int layerMask = displayBitMask | surfaceBitMask;
            bool tileHit = Physics.Raycast(ray, out hit, 1000, layerMask);
            if (!tileHit)
            {
                return;
            }

            if (hit.collider.gameObject.layer == displayLayerMask)
            {
                // We don't want to check for surfaces behind tile displays. This prevents clicking through a tile display.
                return;
            }

            Tile tile = hit.collider.GetComponentInParent<Tile>();
            if (!tile)
            {
                Debug.LogError("Tile not found.");
                return;
            }

            if (!tile.IsWalkable || Util.IsPointerOverUI())
            {
                return;
            }

            TryMove(tile.GridPosition);
        }

        private static void TryMove(Point position)
        {
            if (NetworkManager.LocalPlayer.Unit.GridPosition == position)
            {
                return;
            }

            NetworkManager.RequestMove(NetworkManager.LocalPlayer.PlayerData.ID, position);
        }
    }
}