using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class Cursor : MonoBehaviour
    {
        private void Start()
        {
            if (!Application.isEditor)
            {
                UnityEngine.Cursor.visible = false;
            }
        }

        private void Update()
        {
            transform.position = Input.mousePosition;
        }
    }
}
