﻿using UnityEngine;
using UnityEditor;

public class YourClassAsset
{
  [MenuItem("Assets/Create/Conversation Data")]
  public static void CreateConversationData()
  {
    ScriptableObjectUtility.CreateAsset<ConversationData>();
  }

  [MenuItem("Assets/Create/Unit Recipe")]
  public static void CreateUnitRecipe()
  {
    ScriptableObjectUtility.CreateAsset<UnitRecipe>();
  }

  [MenuItem("Assets/Create/Ability Catalog Recipe")]
  public static void CreateAbilityCatalogRecipe()
  {
    ScriptableObjectUtility.CreateAsset<AbilityCatalogRecipe>();
  }

  [MenuItem("Assets/Create/Item Recipe")]
  public static void CreateItemRecipe()
  {
    ScriptableObjectUtility.CreateAsset<ItemRecipe>();
  }

  [MenuItem("Assets/Create/Inventory Catalog")]
  public static void CreateInventoryCatalog()
  {
    ScriptableObjectUtility.CreateAsset<InventoryCatalog>();
  }
}