﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MCB;

[CustomEditor(typeof(Tile))]
public class TileInspector : Editor
{
  public Tile current {
    get {
      return (Tile)target;
    }
  }

  public override void OnInspectorGUI()
  {
    DrawDefaultInspector();

    if(GUILayout.Button("Grow"))
      current.Grow();
    if(GUILayout.Button("Shrink"))
      current.Shrink();
    //if(GUI.changed)
    //  current.UpdateMarker();
  }
}
