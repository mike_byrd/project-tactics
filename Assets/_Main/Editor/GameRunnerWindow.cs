using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace MCB
{
    public class GameRunnerWindow : OdinEditorWindow
    {
        [MenuItem("MCB/Game Runner")]
        private static void OpenWindow()
        {
            GameRunnerWindow window = GetWindow<GameRunnerWindow>();
            window.titleContent = new GUIContent
            {
                text = "Game Runner"
            };

            window.minSize = new Vector2(240, 110);
            window.Show();
        }

//        [Title("Account Info")] public string username;
//        public string password;

        [Button(ButtonSizes.Large), LabelText("Run Game"), GUIColor(0, 1, 0), PropertySpace(SpaceBefore = 5)]
        public void RunGame()
        {
            try
            {
                SceneAsset scene = AssetDatabase.LoadAssetAtPath<SceneAsset>("Assets/_Main/Scenes/Login Scene.unity");
                EditorSceneManager.playModeStartScene = scene;
                EditorApplication.EnterPlaymode();
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        [Button(ButtonSizes.Large), LabelText("Run Map Editor"), PropertySpace(SpaceBefore = 5)]
        public void RunMapEditor()
        {
            try
            {
                SceneAsset scene = AssetDatabase.LoadAssetAtPath<SceneAsset>("Assets/MCB/Map Editor/Map Editor Scene.unity");
                EditorSceneManager.playModeStartScene = scene;
                EditorApplication.EnterPlaymode();
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }
        }

        [Button(ButtonSizes.Large), LabelText("Clear Current Scene"), PropertySpace(SpaceBefore = 5)]
        public void ClearCurrentScene()
        {
            EditorSceneManager.playModeStartScene = null;
        }
    }
}
