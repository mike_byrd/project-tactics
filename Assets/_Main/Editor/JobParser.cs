﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using Newtonsoft.Json;
using ProjectTactics.DataModels;

public static class JobParser
{
    [MenuItem("MCB/Export Job Data")]
    public static void ExportJobData()
    {
        Job[] jobs = Resources.LoadAll<Job>("Jobs");
        foreach (Job job in jobs)
        {
            JobDataModel jobDataModel = new JobDataModel
            {
                job = job.jobType.ToString(),
                baseStats = new StatsDataModel
                {
                    maxHp = job.baseStats[0],
                    attack = job.baseStats[1],
                    defense = job.baseStats[2],
                    magicAttack = job.baseStats[3],
                    magicDefense = job.baseStats[4],
                    speed = job.baseStats[5],
                    maxAp = job.baseStats[6],
                    maxMp = job.baseStats[7],
                    range  = job.baseStats[8],
                    jumpHeight = job.baseStats[9],
                    criticalHitRate = job.baseStats[10]
                },
                growthStats = new StatsDataModel
                {
                    maxHp = job.growStats[0],
                    attack = job.growStats[1],
                    defense = job.growStats[2],
                    magicAttack = job.growStats[3],
                    magicDefense = job.growStats[4],
                    speed = job.growStats[5],
                    maxAp = job.growStats[6],
                    maxMp = job.growStats[7],
                    range  = job.growStats[8],
                    jumpHeight = job.growStats[9],
                    criticalHitRate = job.growStats[10]
                }
            };

            string json = JsonConvert.SerializeObject(jobDataModel);
//            string json = JsonUtility.ToJson(jobDataModel);
            string savePath = $"{Application.dataPath}/_Main/Resources/Jobs/Json/{jobDataModel.job}.json";
            FileStream stream = new FileStream(savePath, FileMode.Create);
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(json);
            }

#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
        }
    }

    [MenuItem("Pre Production/Parse Jobs")]
    public static void Parse()
    {
        CreateDirectories();
        ParseStartingStats();
        ParseGrowthStats();
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    static void CreateDirectories()
    {
        if (!AssetDatabase.IsValidFolder("Assets/_Main/Resources/Jobs"))
            AssetDatabase.CreateFolder("Assets/_Main/Resources", "Jobs");
    }

    static void ParseStartingStats()
    {
        string readPath = string.Format("{0}/_Main/Settings/JobStartingStats.csv", Application.dataPath);
        string[] readText = File.ReadAllLines(readPath);
        for (int i = 1; i < readText.Length; ++i)
            ParseStartingStats(readText[i]);
    }

    static void ParseStartingStats(string line)
    {
        string[] elements = line.Split(',');
        GameObject obj = GetOrCreate(elements[0]);
        Job job = obj.GetComponent<Job>();
        for (int i = 1; i < Job.statOrder.Length + 1; ++i)
        {
            job.baseStats[i - 1] = Convert.ToInt32(elements[i]);
        }

        StatModifierFeature move = GetFeature(obj, StatTypes.MMP);
        move.amount = Convert.ToInt32(elements[8]);

        StatModifierFeature jump = GetFeature(obj, StatTypes.JMP);
        jump.amount = Convert.ToInt32(elements[10]);
    }

    static void ParseGrowthStats()
    {
        string readPath = string.Format("{0}/_Main/Settings/JobGrowthStats.csv", Application.dataPath);
        string[] readText = File.ReadAllLines(readPath);
        for (int i = 1; i < readText.Length; ++i)
        {
            ParseGrowthStats(readText[i]);
        }
    }

    static void ParseGrowthStats(string line)
    {
        string[] elements = line.Split(',');
        GameObject obj = GetOrCreate(elements[0]);
        Job job = obj.GetComponent<Job>();
        for (int i = 1; i < elements.Length; ++i)
        {
            job.growStats[i - 1] = Convert.ToSingle(elements[i]);
        }
    }

    static StatModifierFeature GetFeature(GameObject obj, StatTypes type)
    {
        StatModifierFeature[] smf = obj.GetComponents<StatModifierFeature>();
        for (int i = 0; i < smf.Length; ++i)
        {
            if (smf[i].type == type)
                return smf[i];
        }

        StatModifierFeature feature = obj.AddComponent<StatModifierFeature>();
        feature.type = type;
        return feature;
    }

    static GameObject GetOrCreate(string jobName)
    {
        string fullPath = string.Format("Assets/_Main/Resources/Jobs/{0}.prefab", jobName);
        GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(fullPath);
        if (obj == null)
            obj = Create(fullPath);
        return obj;
    }

    static GameObject Create(string fullPath)
    {
        GameObject instance = new GameObject("temp");
        instance.AddComponent<Job>();
        GameObject prefab = PrefabUtility.CreatePrefab(fullPath, instance);
        GameObject.DestroyImmediate(instance);
        return prefab;
    }
}