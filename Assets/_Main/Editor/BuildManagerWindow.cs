using System.Collections;
using System.Collections.Generic;
using System.IO;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace MCB.BuildManagement
{
    public enum ServerEnvironmentType
    {
        Development,
        Production
    }

    public enum PlatformType
    {
        Windows,
        Mac,
        Android,
        IOS
    }

    public class BuildManagerWindow : OdinEditorWindow
    {
        [MenuItem("Build/Build Manager")]
        private static void OpenWindow()
        {
            BuildManagerWindow window = GetWindow<BuildManagerWindow>();
            window.titleContent = new GUIContent
            {
                text = "Build Manager"
            };

            window.minSize = new Vector2(240, 215);
            window.Show();
        }

        [EnumToggleButtons, Title("Server Environment"), HideLabel, PropertySpace(SpaceBefore = 5)]
        public ServerEnvironmentType environment;

        [EnumToggleButtons, Title("Platform"), HideLabel, PropertySpace(SpaceBefore = 5)]
        public PlatformType platform;

        [LabelText("Build and Run")]
        public bool autoRun;

        [LabelText("Output Path"), FolderPath(RequireExistingPath = true), HideLabel, PropertySpace(SpaceBefore = 5)]
        public string outputPath;

        [LabelText("App Name"), HideLabel, PropertySpace(SpaceBefore = 5)]
        public string appName;

        [Button(ButtonSizes.Large), LabelText("Build"), PropertySpace(SpaceBefore = 5)]
        public void BuildGame()
        {
            switch (platform)
            {
                case PlatformType.Windows:
                    BuildWindowsClient();
                    break;
                case PlatformType.Mac:
                    BuildMacClient();
                    break;
                default:
                    Debug.LogWarning($"{platform} builds are not available.");
                    break;
            }
        }

        private void BuildMacClient(bool switchToPreviousTarget = true)
        {
            if (!Directory.Exists(outputPath))
            {
                Debug.LogError($"The path does not exist: {outputPath}");
                return;
            }

            if (string.IsNullOrEmpty(appName))
            {
                Debug.LogError($"App name can't be empty.");
                return;
            }

            Debug.Log($"Building for ${platform} with {environment} server...");
            string currentSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(
                EditorUserBuildSettings.selectedBuildTargetGroup);
            string finalSymbols = currentSymbols.Replace(";PRODUCTION", "");
            switch (environment)
            {
                case ServerEnvironmentType.Production:
                    finalSymbols = $"{currentSymbols};PRODUCTION";
                    break;
            }

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
                finalSymbols);

            Debug.Log($"Build path: {outputPath}/{appName}");

            // Build player.
            var currentBuildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            var currentActiveBuildTarget = EditorUserBuildSettings.activeBuildTarget;

            BuildPipeline.BuildPlayer(new BuildPlayerOptions
            {
                locationPathName = $"{outputPath}/{appName}",
                target = BuildTarget.StandaloneOSX,
                scenes = GetSceneNames(),
                options = autoRun ? BuildOptions.AutoRunPlayer : BuildOptions.None
            });

            if (switchToPreviousTarget)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(currentBuildTargetGroup, currentActiveBuildTarget);
            }
        }

        private void BuildWindowsClient(bool switchToPreviousTarget = true)
        {
            if (!Directory.Exists(outputPath))
            {
                Debug.LogError($"The path does not exist: {outputPath}");
                return;
            }

            if (string.IsNullOrEmpty(appName))
            {
                Debug.LogError($"App name can't be empty.");
                return;
            }

            Debug.Log($"Building for ${platform} with {environment} server...");
            string currentSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(
                EditorUserBuildSettings.selectedBuildTargetGroup);
            string finalSymbols = currentSymbols.Replace(";PRODUCTION", "");
            switch (environment)
            {
                case ServerEnvironmentType.Production:
                    finalSymbols = $"{currentSymbols};PRODUCTION";
                    break;
            }

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
                finalSymbols);

            string buildPath = $"{outputPath}/{environment.ToString()}/{platform.ToString()}/{appName}.exe";
            Debug.Log($"Build path: {buildPath}");

            // Build player.
            var currentBuildTargetGroup = EditorUserBuildSettings.selectedBuildTargetGroup;
            var currentActiveBuildTarget = EditorUserBuildSettings.activeBuildTarget;

            BuildPipeline.BuildPlayer(new BuildPlayerOptions
            {
                locationPathName = buildPath,
                target = BuildTarget.StandaloneWindows64,
                scenes = GetSceneNames(),
                options = autoRun ? BuildOptions.AutoRunPlayer : BuildOptions.None
            });

            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup,
                currentSymbols);

            if (switchToPreviousTarget)
            {
                EditorUserBuildSettings.SwitchActiveBuildTarget(currentBuildTargetGroup, currentActiveBuildTarget);
            }
        }

        private static string[] GetSceneNames()
        {
            string[] scenes = new string[EditorBuildSettings.scenes.Length];

            for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
            {
                var scene = EditorBuildSettings.scenes[i];
                scenes[i] = scene.path;
            }

            return scenes;
        }
    }
}
