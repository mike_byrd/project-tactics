using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.MapEditor
{
    [Serializable]
    public class MapData : ScriptableObject
    {
        public string id;
        public string mapName;
        public int width;
        public int height;
        public TileData[] tiles;

        public void Copy(MapData from)
        {
            id = from.id;
            mapName = from.mapName;
            width = from.width;
            height = from.height;
            tiles = new TileData[from.tiles.Length];

            for (int i = 0; i < tiles.Length; i++)
            {
                TileData copy = from.tiles[i];
                if (copy != null)
                {
                    TileData td = new TileData
                    {
                        type = copy.type,
                        height = copy.height,
                        position = copy.position,
                        isWalkable = copy.isWalkable
                    };

                    tiles[i] = td;
                }
            }
        }
    }
}
