using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;
using UnityEngine.UI;

namespace MCB.MapEditor
{
    public class Tile : MonoBehaviour
    {
        [SerializeField] private string tileName;
        [SerializeField] private TileType type;

        // The amount to multiply height by.
        [SerializeField] private float heightStep = 0.125f;

        // The amount to increase/decrease height by using the scrollwheel.
        [SerializeField] private int heightDelta = 1;
        [SerializeField] private Sprite icon;

        public Point GridPosition { get; private set; }
        public int Height { get; private set; }
        public Sprite Icon => icon;
        public TileType Type => type;
        public string TileName => tileName;

        public bool isWalkable;

        public void SetGridPosition(Point position)
        {
            GridPosition = position;
            Transform t = transform;
            Vector3 worldPosition = t.position;
            worldPosition.x = position.x;
            worldPosition.z = position.y;
            t.position = worldPosition;
        }

        private const float MinVisibleHeight = 0.01f;
        private const int MinHeight = 0;
        private const int MaxHeight = 10;

        public void SetHeight(int height)
        {
            Vector3 newScale = transform.localScale;
            int newWorldHeight = Mathf.Clamp(height, MinHeight, MaxHeight);
            Height = newWorldHeight;

            if (newWorldHeight == 0)
            {
                newScale.y = MinVisibleHeight;
            }
            else
            {
                newScale.y = newWorldHeight * heightStep;
            }

            transform.localScale = newScale;
        }

        public void IncreaseHeight()
        {
            SetHeight(Height + heightDelta);
        }

        public void DecreaseHeight()
        {
            SetHeight(Height - heightDelta);
        }
    }
}
