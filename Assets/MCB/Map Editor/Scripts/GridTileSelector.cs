using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MCB.MapEditor
{
    public class GridTileSelector : MonoBehaviour
    {
        public static event Action<Rect> SelectionRectAdded;
        public static event Action<Rect> SelectionRectRemoved;
        public static event Action<GridTile, float> ScrolledOverTile;

        private Point _startPosition;
        private Rect _selectionRect;
        private bool _isSelectingRect;
        private GridTile _hoveredTile;

        // Used for detecting if the mouse has moved off the grid.
        private Camera _mainCamera;

        private static bool IsMouseOverUiElement => EventSystem.current.IsPointerOverGameObject();

        private void Awake()
        {
            _selectionRect = new Rect();
            _mainCamera = Camera.main;
        }

        private void OnEnable()
        {
            GridTile.MouseDown += GridTileOnMouseDown;
            GridTile.MouseEnter += GridTileOnMouseEnter;
            GridTile.MouseUp += GridTileOnMouseUp;
        }

        private void OnDisable()
        {
            GridTile.MouseDown -= GridTileOnMouseDown;
            GridTile.MouseEnter -= GridTileOnMouseEnter;
            GridTile.MouseUp -= GridTileOnMouseUp;
        }

        private void Update()
        {
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity))
            {
                if (_hoveredTile)
                {
                    // Clear hover tile if we moved the mouse off grid.
                    _hoveredTile = null;
                }
            }

            if (!_hoveredTile) return;

            if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                float scrollWheelMovement = Input.GetAxis("Mouse ScrollWheel");
                if (scrollWheelMovement > 0 || scrollWheelMovement < 0)
                {
                    ScrolledOverTile?.Invoke(_hoveredTile, scrollWheelMovement);
                }
            }
        }

        private static bool IsHoldingShift()
        {
            return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftShift);
        }

        private void SendAddedOrRemovedEvent()
        {
            if (!IsHoldingShift())
            {
                SelectionRectAdded?.Invoke(_selectionRect);
            }
            else
            {
                SelectionRectRemoved?.Invoke(_selectionRect);
            }
        }

        private void UpdateSelectionRect(Point end)
        {
            int lowX;
            int highX;
            int lowY;
            int highY;

            if (_startPosition.x < end.x)
            {
                lowX = _startPosition.x;
                highX = end.x;
            }
            else if (_startPosition.x > end.x)
            {
                highX = _startPosition.x;
                lowX = end.x;
            }
            else
            {
                lowX = highX = _startPosition.x;
            }

            if (_startPosition.y < end.y)
            {
                lowY = _startPosition.y;
                highY = end.y;
            }
            else if (_startPosition.y > end.y)
            {
                highY = _startPosition.y;
                lowY = end.y;
            }
            else
            {
                lowY = highY = _startPosition.y;
            }

            _selectionRect = new Rect(lowX, lowY, highX, highY);
        }

        private void GridTileOnMouseDown(GridTile gridTile)
        {
            if (IsMouseOverUiElement)
            {
                return;
            }

            _startPosition = gridTile.GridPosition;
            _isSelectingRect = true;

            _selectionRect.x = _startPosition.x;
            _selectionRect.y = _startPosition.y;
            _selectionRect.width = _startPosition.x;
            _selectionRect.height = _startPosition.y;

            SendAddedOrRemovedEvent();
        }

        private void GridTileOnMouseEnter(GridTile gridTile)
        {
            _hoveredTile = gridTile;
            if (!_isSelectingRect) return;

            UpdateSelectionRect(gridTile.GridPosition);
            SendAddedOrRemovedEvent();
        }

        private void GridTileOnMouseUp(GridTile gridTile)
        {
            _isSelectingRect = false;
        }
    }
}
