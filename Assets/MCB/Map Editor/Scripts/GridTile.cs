using MCB;
using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;

namespace MCB.MapEditor
{
    public class GridTile : MonoBehaviour
    {
        public static event Action<GridTile> MouseDown;
        public static event Action<GridTile> MouseEnter;
        public static event Action<GridTile> MouseUp;

        public const float MinHeight = 0.125f;
        public const float MaxHeight = 5f;

        public Point GridPosition { get; private set; }
        public float Height { get; private set; }

        public void SetGridPosition(Point gridPosition)
        {
            GridPosition = gridPosition;
            transform.localPosition = new Vector3(gridPosition.x, Height, gridPosition.y);
        }

        public void SetHeight(float height)
        {
            Height = height;
            var t = transform;
            var oldPosition = t.position;
            t.localPosition = new Vector3(oldPosition.x, height, oldPosition.z);
        }

        private void OnMouseDown()
        {
            MouseDown?.Invoke(this);
        }

        private void OnMouseEnter()
        {
            MouseEnter?.Invoke(this);
        }

        private void OnMouseUp()
        {
            MouseUp?.Invoke(this);
        }
    }
}
