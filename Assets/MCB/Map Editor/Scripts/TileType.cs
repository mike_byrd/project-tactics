using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.MapEditor
{
    public enum TileType
    {
        None = 0,
        Height,
        Unwalkable,
        StartAlly,
        StartEnemy,
        Travel
    }
}
