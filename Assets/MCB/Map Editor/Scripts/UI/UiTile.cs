using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB.MapEditor
{
    public class UiTile : MonoBehaviour
    {
        public static event Action<UiTile> TileClicked;

        [SerializeField] private Image icon;
        [SerializeField] private Image highlight;
        [SerializeField] private TMP_Text label;
        [SerializeField] private Color highlightColor;

        public TileType Type { get; private set; }

        public void OnPointerDown()
        {
            TileClicked?.Invoke(this);
        }

        public void UpdateUi(Tile tilePrefab)
        {
            name = tilePrefab.name;
            label.text = tilePrefab.TileName;
            icon.sprite = tilePrefab.Icon;
            Type = tilePrefab.Type;
        }

        public void SetSelected(bool selected)
        {
            highlight.gameObject.SetActive(selected);
        }
    }

}
