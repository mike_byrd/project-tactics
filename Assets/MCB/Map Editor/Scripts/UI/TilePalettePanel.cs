using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.MapEditor
{
    public class TilePalettePanel : MonoBehaviour
    {
        public static event Action<TileType> UiTileSelected;

        [SerializeField] private UiTile uiTilePrefab;
        [SerializeField] private Tile[] mapTilePrefabs;

        private List<UiTile> uiTiles;
        private UiTile selectedTile;

        private void Awake()
        {
            uiTiles = new List<UiTile>();
        }

        private void OnEnable()
        {
            MapEditor.TilePrefabsLoaded += OnTilePrefabsLoaded;
            MapEditor.TileBrushCleared += OnTileBrushCleared;
            UiTile.TileClicked += OnUiTileClicked;
        }

        private void OnDisable()
        {
            MapEditor.TilePrefabsLoaded -= OnTilePrefabsLoaded;
            MapEditor.TileBrushCleared -= OnTileBrushCleared;
            UiTile.TileClicked -= OnUiTileClicked;
        }

        private void SelectUiTile(UiTile uiTile)
        {
            if (!uiTile)
            {
                foreach (var tile in uiTiles)
                {
                    tile.SetSelected(false);
                }
                return;
            }

            foreach (var tile in uiTiles)
            {
                tile.SetSelected(tile.Type == uiTile.Type);
            }
        }

        private void OnTileBrushCleared()
        {
            SelectUiTile(null);
        }

        private void OnUiTileClicked(UiTile uiTile)
        {
            SelectUiTile(uiTile);
            UiTileSelected?.Invoke(uiTile.Type);
        }

        private void OnTilePrefabsLoaded(Tile[] tiles)
        {
            if (tiles == null) return;

            for (int i = 0; i < tiles.Length; i++)
            {
                var tilePrefab = tiles[i];
                var uiTile = Instantiate(uiTilePrefab, transform);
                uiTile.UpdateUi(tilePrefab);
                uiTiles.Add(uiTile);
            }
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
