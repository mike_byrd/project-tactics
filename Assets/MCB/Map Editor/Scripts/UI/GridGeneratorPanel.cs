using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace MCB
{
    public class GridGeneratorPanel : MonoBehaviour
    {
        public static event Action GenerateButtonClick;
        public static event Action ClearButtonClick;

        [SerializeField] private TMP_InputField widthInputField;
        [SerializeField] private TMP_InputField heightInputField;
        [SerializeField] private Button generateButton;
        [SerializeField] private Button clearButton;

        [SerializeField] private int defaultGridWidth = 10;
        [SerializeField] private int defaultGridHeight = 10;

        public int GridWidth { get; private set; }
        public int GridHeight { get; private set; }

        private void OnEnable()
        {
            generateButton.onClick.AddListener(OnGenerateButtonClicked);
            clearButton.onClick.AddListener(OnClearButtonClicked);
            widthInputField.onValueChanged.AddListener(OnWidthValueChanged);
            heightInputField.onValueChanged.AddListener(OnHeightValueChanged);
        }

        private void Start()
        {
            GridWidth = defaultGridWidth;
            GridHeight = defaultGridHeight;
        }

        private void OnDisable()
        {
            generateButton.onClick.RemoveListener(OnGenerateButtonClicked);
            clearButton.onClick.RemoveListener(OnClearButtonClicked);
            widthInputField.onValueChanged.RemoveListener(OnWidthValueChanged);
            heightInputField.onValueChanged.RemoveListener(OnHeightValueChanged);
        }

        private void OnWidthValueChanged(string widthText)
        {
            GridWidth = int.TryParse(widthText, out int width) ? width : defaultGridWidth;
        }

        private void OnHeightValueChanged(string heightText)
        {
            GridHeight = int.TryParse(heightText, out int height) ? height : defaultGridHeight;
        }

        private void OnGenerateButtonClicked()
        {
            GenerateButtonClick?.Invoke();
        }

        private void OnClearButtonClicked()
        {
            ClearButtonClick?.Invoke();
        }
    }
}