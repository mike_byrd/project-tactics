using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB.MapEditor
{
    public class MapLoaderPanel : MonoBehaviour
    {
        public static event Action<string, string> SaveButtonClicked;
        public static event Action LoadButtonClicked;
        public static event Action<int> MapSelectionChanged;

        [SerializeField] private TMP_InputField mapDataNameInput;
        [SerializeField] private TMP_InputField idInput;
        [SerializeField] private TMP_Dropdown mapDataDropdown;
        [SerializeField] private Button saveButton;
        [SerializeField] private Button loadButton;

        private void OnEnable()
        {
            mapDataDropdown.onValueChanged.AddListener(OnValueChanged);
            saveButton.onClick.AddListener(OnSaveButtonClicked);
            loadButton.onClick.AddListener(OnLoadButtonClicked);
        }

        private void OnDisable()
        {
            mapDataDropdown.onValueChanged.RemoveListener(OnValueChanged);
            saveButton.onClick.RemoveListener(OnSaveButtonClicked);
            loadButton.onClick.RemoveListener(OnLoadButtonClicked);
        }

        private static void OnValueChanged(int selection)
        {
            MapSelectionChanged?.Invoke(selection);
        }

        private void OnSaveButtonClicked()
        {
            SaveButtonClicked?.Invoke(mapDataNameInput.text, idInput.text);
        }

        private static void OnLoadButtonClicked()
        {
            LoadButtonClicked?.Invoke();
        }

        public void UpdateUi(MapData[] mapDataList, int currentSelection)
        {
            List<TMP_Dropdown.OptionData> optionDataList = new List<TMP_Dropdown.OptionData>(mapDataList.Length);
            foreach (MapData gridData in mapDataList)
            {
                optionDataList.Add(new TMP_Dropdown.OptionData
                {
                    text = gridData.mapName,
                });
            }

            mapDataDropdown.options = optionDataList;
            if (mapDataDropdown.options.Count <= 0) return;

            MapData currentMap = mapDataList[currentSelection];
            //var mapName = mapDataDropdown.options[currentSelection].text;
            mapDataDropdown.captionText.text = currentMap.mapName;
            mapDataNameInput.text = currentMap.mapName;
            idInput.text = currentMap.id;
        }
    }
}
