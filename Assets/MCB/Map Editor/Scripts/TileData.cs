using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;

namespace MCB.MapEditor
{
    [Serializable]
    public class TileData
    {
        public TileType type;
        public Point position;
        public int height;
        public bool isWalkable;

        public TileData()
        {
            position = new Point();
        }
    }
}
