using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using MCB.Common;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MCB.MapEditor
{
    /// <summary>
    /// This controls the main functions of the Map Editor feature.
    /// </summary>
    public class MapEditor : MonoBehaviour
    {
        public static event Action<Tile[]> TilePrefabsLoaded;
        public static event Action TileBrushCleared;

        #region Unity Serialized Fields
        [SerializeField] private GridGeneratorPanel gridGeneratorPanel;
        [SerializeField] private Transform mapEditorTilesContainer;
        [SerializeField] private MapLoaderPanel mapLoaderPanel;
        #endregion

        #region Private Fields

        private const string MapDataFilePath = "Json/Map Data";
        private const string MapTilesPath = "Prefabs/Tiles";

        private GridGenerator _gridGenerator;
        private Dictionary<TileType, Tile> _mapTilePrefabs;
        private Dictionary<Point, Tile> _mapEditorTiles;

        private MapData[] _mapDataList;
        private MapData _currentMapData;

        private Tile _currentTileBrush;
        private string _lastSavedMapName;
        #endregion

        #region Unity Events
        private void Awake()
        {
            _gridGenerator = GetComponent<GridGenerator>();
            _mapEditorTiles = new Dictionary<Point, Tile>();
        }

        private void Start()
        {
            if (!EventSystem.current)
            {
                var eventSystem = new GameObject("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
            }

            var prefabs = Resources.LoadAll<Tile>(MapTilesPath);
            if (prefabs == null)
            {
                Debug.Log("No map tile prefabs.");
                return;
            }

            TilePrefabsLoaded?.Invoke(prefabs);

            _mapTilePrefabs = new Dictionary<TileType, Tile>(prefabs.Length);
            foreach (var prefab in prefabs)
            {
                _mapTilePrefabs.Add(prefab.Type, prefab);
            }

            // Load all map json
            LoadMapDataAssets();
        }

        private void OnEnable()
        {
            GridGeneratorPanel.GenerateButtonClick += OnGenerateButtonClicked;
            GridGeneratorPanel.ClearButtonClick += OnClearButtonClicked;

            GridTileSelector.SelectionRectAdded += OnSelectionRectAdded;
            GridTileSelector.SelectionRectRemoved += OnSelectionRectRemoved;
            GridTileSelector.ScrolledOverTile += OnScrolledOverTile;

            MapLoaderPanel.SaveButtonClicked += OnSaveButtonClicked;
            MapLoaderPanel.LoadButtonClicked += OnLoadButtonClicked;
            MapLoaderPanel.MapSelectionChanged += OnMapSelectionChanged;

            TilePalettePanel.UiTileSelected += OnUiTileSelected;
        }

        private void OnDisable()
        {
            GridGeneratorPanel.GenerateButtonClick -= OnGenerateButtonClicked;
            GridGeneratorPanel.ClearButtonClick -= OnClearButtonClicked;

            GridTileSelector.SelectionRectAdded -= OnSelectionRectAdded;
            GridTileSelector.SelectionRectRemoved -= OnSelectionRectRemoved;
            GridTileSelector.ScrolledOverTile -= OnScrolledOverTile;

            MapLoaderPanel.SaveButtonClicked -= OnSaveButtonClicked;
            MapLoaderPanel.LoadButtonClicked -= OnLoadButtonClicked;
            MapLoaderPanel.MapSelectionChanged -= OnMapSelectionChanged;

            TilePalettePanel.UiTileSelected -= OnUiTileSelected;
        }

        private void Update()
        {
            if (Input.GetAxis("Cancel") > 0)
            {
                ClearTileBrush();
            }
        }
        #endregion

        #region Private Methods
        private void ClearTileBrush()
        {
            _currentTileBrush = null;
            TileBrushCleared?.Invoke();
        }
        private Tile GetMapEditorTile(Point gridPosition)
        {
            return _mapEditorTiles.TryGetValue(gridPosition, out Tile tile) ? tile : null;
        }

        private void LoadMapDataAssets()
        {
            int lastSelectedIndex = 0;
//            var mapDataFiles = Directory.GetFiles(MapDataFilePath, "*.json");

            TextAsset[] mapDataFiles = Resources.LoadAll<TextAsset>(MapDataFilePath);

            _mapDataList = new MapData[mapDataFiles.Length];
            for (int i = 0; i < mapDataFiles.Length; i++)
            {
                var file = mapDataFiles[i];
                string json = file.text;

                var mapData = ScriptableObject.CreateInstance<MapData>();
                JsonUtility.FromJsonOverwrite(json, mapData);
                _mapDataList[i] = mapData;

                if (_lastSavedMapName == mapData.mapName)
                {
                    _currentMapData = mapData;
                    lastSelectedIndex = i;
                }
            }

            _currentMapData = _mapDataList[lastSelectedIndex];
            mapLoaderPanel.UpdateUi(_mapDataList, lastSelectedIndex);
        }

        private void ClearMapEditorTiles()
        {
            foreach (var kvp in _mapEditorTiles)
            {
                Destroy(kvp.Value.gameObject);
            }

            _mapEditorTiles.Clear();
        }

        private void SpawnMapTile(Point position)
        {
            var mapTile = GetMapEditorTile(position);
            if (mapTile)
            {
                // A tile exists here so don't add one
                // TODO: Consider overwriting the tile
                return;
            }

            mapTile = Instantiate(_currentTileBrush, mapEditorTilesContainer);
            mapTile.SetGridPosition(position);
            mapTile.SetHeight(0);
            _mapEditorTiles.Add(position, mapTile);
        }

        private void SpawnMapTile(TileData data)
        {
            var tilePrefab = _mapTilePrefabs[data.type];

            var mapTile = Instantiate(tilePrefab, mapEditorTilesContainer);
            mapTile.SetGridPosition(data.position);
            mapTile.SetHeight(data.height);
            mapTile.isWalkable = data.isWalkable;
            _mapEditorTiles.Add(data.position, mapTile);
        }

        private void RemoveMapTile(Point position)
        {
            var tile = GetMapEditorTile(position);
            if (!tile) return;

            _mapEditorTiles.Remove(position);
            Destroy(tile.gameObject);
        }

        private void UpdateSelectedTiles(Rect selectionRect, Action<Point> callback)
        {
            for (int y = (int)selectionRect.y; y <= selectionRect.height; y++)
            {
                for (int x = (int)selectionRect.x; x <= selectionRect.width; x++)
                {
                    callback?.Invoke(new Point(x, y));
                }
            }

            // TODO: Update tile properties panel.
//            if (gridTile)
//            {
//                tilePropertiesPanel.UpdateUI(gridTile);
//            }
        }
        #endregion

        #region GridGeneratorPanel Events
        private void OnGenerateButtonClicked()
        {
            ClearMapEditorTiles();
            _gridGenerator.GenerateGrid(gridGeneratorPanel.GridWidth, gridGeneratorPanel.GridHeight);
        }

        private void OnClearButtonClicked()
        {
            ClearMapEditorTiles();
            _gridGenerator.ClearGrid();
        }
        #endregion

        #region GridTileSelector Events
        private void OnSelectionRectAdded(Rect selection)
        {
            if (!_currentTileBrush)
            {
                return;
            }

            UpdateSelectedTiles(selection, SpawnMapTile);
        }

        private void OnSelectionRectRemoved(Rect selection)
        {
            UpdateSelectedTiles(selection, RemoveMapTile);
        }

        private void OnScrolledOverTile(GridTile gridTile, float scrollWheelMovement)
        {
            var hoveredTile = GetMapEditorTile(gridTile.GridPosition);
            if (!hoveredTile)
            {
                return;
            }

            if (scrollWheelMovement < 0)
            {
                hoveredTile.DecreaseHeight();
                //tilePropertiesPanel.UpdateUI(hoveredTile);
            }
            else if (scrollWheelMovement > 0)
            {
                hoveredTile.IncreaseHeight();
                //tilePropertiesPanel.UpdateUI(hoveredTile);
            }
        }

        private void OnMapSelectionChanged(int selection)
        {
            _currentMapData = _mapDataList[selection];
            mapLoaderPanel.UpdateUi(_mapDataList, selection);
        }

        private void OnSaveButtonClicked(string mapName, string mapId)
        {
#if UNITY_EDITOR
            if (!Directory.Exists(MapDataFilePath))
            {
                Directory.CreateDirectory(MapDataFilePath);
            }
#endif
            MapData savedMapData = ScriptableObject.CreateInstance<MapData>();
            savedMapData.id = mapId;
            savedMapData.mapName = mapName;
            savedMapData.width = _gridGenerator.Width;
            savedMapData.height = _gridGenerator.Height;
            savedMapData.tiles = new TileData[_mapEditorTiles.Count];

            int count = 0;
            foreach (var kvp in _mapEditorTiles)
            {
                TileData td = new TileData
                {
                    type = kvp.Value.Type,
                    position = kvp.Value.GridPosition,
                    height = kvp.Value.Height,
                    isWalkable = kvp.Value.isWalkable
                };

                savedMapData.tiles[count] = td;
                count++;
            }

            // Save to json
            var json = JsonUtility.ToJson(savedMapData);
            string savePath = $"{Application.dataPath}/MCB/Resources/{MapDataFilePath}/{mapId}.json";
            FileStream stream = new FileStream(savePath, FileMode.Create);
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.Write(json);
            }

#if UNITY_EDITOR
            AssetDatabase.Refresh();
#endif
            Debug.Log($"Map Saved!: {mapName}");
            _lastSavedMapName = mapName;
            LoadMapDataAssets();
        }

        private void OnLoadButtonClicked()
        {
            if (_currentMapData == null)
            {
                Debug.LogError("No selected map data.");
                return;
            }

            ClearMapEditorTiles();
            _gridGenerator.GenerateGrid(_currentMapData.width, _currentMapData.height);

            foreach (var tile in _currentMapData.tiles)
            {
                SpawnMapTile(tile);
            }
        }
        #endregion

        #region TilePalettePanel Events

        private void OnUiTileSelected(TileType type)
        {
            _currentTileBrush = _mapTilePrefabs[type];
        }

        #endregion
    }
}
