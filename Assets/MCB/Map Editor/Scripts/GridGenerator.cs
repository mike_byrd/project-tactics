using System;
using System.Collections;
using System.Collections.Generic;
using MCB.Common;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MCB.MapEditor
{
    public class GridGenerator : MonoBehaviour
    {
        [SerializeField] private Transform tileContainer;
        [SerializeField] private GridTile gridTilePrefab;
        private GridTile[] _gridTiles;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public void GenerateGrid(int width = 10, int height = 10)
        {
            ClearGrid();

            Width = width;
            Height = height;
            _gridTiles = new GridTile[width * height];

            int count = 0;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var gridGo = Instantiate(gridTilePrefab, tileContainer);
                    if (gridGo == null)
                    {
                        Debug.LogError("gridGo isn't a game object for some reason");
                        return;
                    }

                    var gridTile = gridGo.GetComponent<GridTile>();

                    var pos = new Point(x, y);
                    gridTile.SetGridPosition(pos);
                    gridTile.name = $"({x}, {y})";
                    _gridTiles[count] = gridTile;
                    count++;
                }
            }
        }

        public void ClearGrid()
        {
            if (_gridTiles == null)
            {
                return;
            }

            var temp = tileContainer.GetComponentsInChildren<GridTile>();
            foreach (var child in temp)
            {
                Destroy(child.gameObject);
            }

            _gridTiles = null;
        }
    }
}