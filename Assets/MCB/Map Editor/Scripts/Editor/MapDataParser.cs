using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace MCB.MapEditor
{
    public static class MapDataParser
    {
        private const string MapJsonPath = "Assets/MCB/Json/Map Data";
        private const string MapDataPath = "Assets/MCB/Resources/Map Data";

        [MenuItem("MCB/Parse Map Data")]
        public static void ParseGridData()
        {
            if (!Directory.Exists(MapJsonPath))
            {
                Debug.Log("You must save a map using the Runtime Map Editor first.");
                return;
            }

            if (!Directory.Exists(MapDataPath))
            {
                Directory.CreateDirectory(MapDataPath);
            }

            ParseMapDataJson();
        }

        private static void ParseMapDataJson()
        {
            var mapDataFiles = Directory.GetFiles(MapJsonPath, "*.json");
            MapData mapData = null;
            foreach (var filePath in mapDataFiles)
            {
                using StreamReader reader = new StreamReader(filePath);
                string json = reader.ReadToEnd();

                var fileName = filePath.Replace(MapJsonPath, "")
                    .Replace(".json", "").Remove(0, 1);

                mapData = Resources.Load<MapData>($"Map Data/{fileName}");
                if (mapData)
                {
                    OverwriteAsset(json, mapData);
                }
                else
                {
                    CreateNewAsset(json, fileName);
                }
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = mapData;
        }

        private static void OverwriteAsset(string json, MapData asset)
        {
            var temp = ScriptableObject.CreateInstance<MapData>();
            JsonUtility.FromJsonOverwrite(json, temp);
            asset.Copy(temp);
            EditorUtility.SetDirty(asset);
        }

        private static void CreateNewAsset(string json, string fileName)
        {
            MapData mapData = ScriptableObject.CreateInstance<MapData>();
            JsonUtility.FromJsonOverwrite(json, mapData);

            AssetDatabase.CreateAsset(mapData, $"{MapDataPath}/{fileName}.asset");
        }
    }
}
