﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace MCB
{
    public class DebugPanel : MonoBehaviour
    {
        [SerializeField] GameObject _textPrefab;
        [SerializeField] GameObject _devCommandBtnPrefab;
        [SerializeField] RectTransform _consoleContent;
        [SerializeField] RectTransform _devCommandContent;
        [SerializeField] int maxEntries;
        [SerializeField] Button _devCommandBtn;
        [SerializeField] Button _clearLogsBtn;
        [SerializeField] GameObject _devCommandPanel;
        [SerializeField] DevCommandPopup _devCommandPopup;

        List<GameObject> entries = new List<GameObject>();

        Terminal terminal;

        private void OnEnable()
        {
            DevCommandPopup.ClosePopup += OnClosePopup;
        }

        private void OnDisable()
        {
            DevCommandPopup.ClosePopup -= OnClosePopup;
        }

        private void OnClosePopup(string buttonClicked)
        {
            _devCommandPanel.SetActive(false);
        }

        public void Initialize(Terminal terminal)
        {
            this.terminal = terminal;

            foreach (var item in terminal.GetCommands())
            {
                Button btn = Instantiate(_devCommandBtnPrefab).GetComponent<Button>();
                btn.transform.SetParent(_devCommandContent, false);
                btn.GetComponentInChildren<TMP_Text>().text = item.name;
                btn.onClick.AddListener(() => { _devCommandPopup.ShowPopup(item.name, item.description); });
            }

            _devCommandPopup.Initialize(terminal);
        }

        public void Log(string text, Color color)
        {
            TMP_Text newText = Instantiate(_textPrefab).GetComponent<TMP_Text>();

            newText.text = text;
            newText.color = color;

            newText.transform.SetParent(_consoleContent, false);

            entries.Add(newText.gameObject);

            if (entries.Count > maxEntries)
            {
                Destroy(entries[0]);
                entries.RemoveAt(0);
            }
        }

        public void Toggle()
        {
            SetActive(!IsActive);
        }

        public bool IsActive
        {
            get { return gameObject.activeSelf; }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }

        public void ClearLogs()
        {
            foreach (var item in entries)
            {
                Destroy(item);
            }

            entries.Clear();
        }

        public void ToggleDevCommandPanel()
        {
            _devCommandPanel.SetActive(!_devCommandPanel.activeSelf);
        }

        void OnLog(Terminal terminal, string[] args)
        {
            Debug.Log("This test worked!");
        }
    }
}