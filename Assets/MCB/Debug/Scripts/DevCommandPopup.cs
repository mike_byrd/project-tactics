﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MCB
{
    public class DevCommandPopup : MonoBehaviour
    {
        public static event Action<string> ClosePopup;
        
        [SerializeField] TMP_Text titleLabel;
        [SerializeField] TMP_Text descriptionLabel;
        [SerializeField] Button yesButton;
        [SerializeField] Button noButton;
        [SerializeField] TMP_InputField argInputField;

        string commandName;
        Terminal terminal;

        private void OnEnable()
        {
            yesButton.onClick.AddListener(OnYesClicked);
            noButton.onClick.AddListener(OnNoClicked);
        }

        private void OnDisable()
        {
            yesButton.onClick.RemoveListener(OnYesClicked);
            noButton.onClick.RemoveListener(OnNoClicked);
        }

        public void Initialize(Terminal terminal)
        {
            this.terminal = terminal;
        }

        public void ShowPopup(string title, string description)
        {
            commandName = title;

            titleLabel.text = string.Format("Execute: {0}?", title);
            descriptionLabel.text = description;
            argInputField.text = "";

            gameObject.SetActive(true);
        }

        void OnYesClicked()
        {
            gameObject.SetActive(false);

            string[] args;
            if (commandName.ToLower() == "log")
            {
                args = new []{argInputField.text};
            }
            else
            {
                args = argInputField.text.Split(new[] {";"}, System.StringSplitOptions.RemoveEmptyEntries);
            }
            
            terminal.ExecuteCommand(commandName, args);
            ClosePopup?.Invoke("yes-clicked");
        }

        void OnNoClicked()
        {
            gameObject.SetActive(false);
            ClosePopup?.Invoke("no-clicked");
        }
    }
}