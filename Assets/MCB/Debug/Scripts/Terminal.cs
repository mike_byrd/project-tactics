﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB
{
    public class Terminal
    {
        public struct Command
        {
            public string name;
            public string description;
            public Action<Terminal, string[]> callback;
        }

        private List<Command> _commands = new List<Command>();
        private string _currentCommand;
        private List<string> _commandHistory = new List<string>();

        public void AddCommand(string name, string helptext, Action<Terminal, string[]> callback)
        {
            Command command;
            command.name = name.ToLower();
            command.description = helptext;
            command.callback = callback;
            _commands.Add(command);
        }

        public void ExecuteCommand(string command)
        {
            _currentCommand = command;
            ExecuteNextCommand();
        }

        public void ExecuteCommand(string command, string[] args)
        {
            foreach (Command c in _commands)
            {
                if (c.name.Equals(command.ToLower()))
                {
                    c.callback(this, args);
                }
            }
        }

        private void ExecuteNextCommand()
        {
            _commandHistory.Add(_currentCommand);
            string[] args = _currentCommand.Split(';');
            foreach (Command com in _commands)
            {
                if (com.name.Equals(args[0].ToLower()))
                {
                    com.callback(this, args);
                }
            }

            _currentCommand = "";
        }

        public Command[] GetCommands()
        {
            List<Command> commands = new List<Command>();
            foreach (var command in _commands)
            {
                commands.Add(command);
            }

            return commands.ToArray();
        }
    }
}