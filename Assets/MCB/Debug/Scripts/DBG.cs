﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MCB
{
    public class DBG : MonoBehaviour
    {
        public static event Action<bool> DebugModeSet;
        public const string SET_DEBUG_MODE = "DBG.MonoBehaviour";

        [SerializeField] DebugPanel debugPanel;

        [SerializeField] bool showPanelAtStart;

        private Canvas _canvas;

        public static DBG instance { get; set; }

        int lastTouchCount = 0;

        Terminal terminal;

        private void Awake()
        {
            if (!instance)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(this.gameObject);
                return;
            }

            _canvas = GetComponent<Canvas>();
            terminal = new Terminal();
            DontDestroyOnLoad(instance.gameObject);
        }

        private void Start()
        {
            _canvas.worldCamera = Camera.main;

            InitializeDevCommands();

            debugPanel.SetActive(showPanelAtStart);
            debugPanel.Initialize(terminal);
        }

        private void Update()
        {
            int touchCount = Input.touchCount;
            bool justTapped3Fingers = lastTouchCount != touchCount && touchCount == 3;
            bool shouldTogglePanel = Input.GetKeyDown(KeyCode.BackQuote) || justTapped3Fingers;

            if (shouldTogglePanel)
            {
                debugPanel.Toggle();
            }

            lastTouchCount = touchCount;
        }

        public static void Log(string text)
        {
            if (!instance) return;
            instance.debugPanel.Log(text, Color.white);
            Debug.Log(text);
        }

        public static void LogWarning(string text)
        {
            if (!instance) return;
            instance.debugPanel.Log(text, Color.yellow);
            Debug.LogWarning(text);
        }

        public static void LogError(string text)
        {
            if (!instance) return;
            instance.debugPanel.Log(text, Color.red);
            Debug.LogError(text);
        }

        private void InitializeDevCommands()
        {
            terminal.AddCommand("log", "Logs text", OnLog);
        }

        private void OnLog(Terminal terminal, string[] args)
        {
            if (args.Length < 1) return;

            Log(args[0]);
        }
    }
}