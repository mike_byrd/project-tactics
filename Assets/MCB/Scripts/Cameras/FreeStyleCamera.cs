﻿// reference: http://www.unifycommunity.com/wiki/index.php?title=MouseOrbitZoom

using System;
using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.EventSystems;

namespace MCB.Cameras
{
    [AddComponentMenu("MCB/Cameras/Free Style Camera")]
    public class FreeStyleCamera : MonoBehaviour
    {
        public static FreeStyleCamera Instance { get; private set; }

        public Transform target;
        public Vector3 targetOffset;
        public float distance = 5.0f;
        public float maxDistance = 20;
        public float minDistance = .6f;
        public float xSpeed = 200.0f;
        public float ySpeed = 200.0f;
        public int yMinLimit = -80;
        public int yMaxLimit = 80;
        public int zoomRate = 40;
        public int zoomRateIncreaseIncrement = 5;
        public int zoomRateMin = 40;
        public int zoomRateMax = 180;
        public float panSpeed = 0.3f;
        public float zoomDampening = 5.0f;

        public bool enableWASDMovement;

        private float _xDeg;
        private float _yDeg;
        private float _currentDistance;
        private float _desiredDistance;
        private Quaternion _desiredRotation;
        private Quaternion _rotation;
        private Vector3 _position;

        private bool _isRotating = false;

        private bool _isMovingToTarget;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            else if (Instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void OnDisable()
        {
            _isRotating = false;
        }

        private void Start()
        {
            Init();
        }

        public static void SetTarget(Transform target)
        {
            if (!target)
            {
                return;
            }

            Instance._isMovingToTarget = false;
            Instance.target = target;
            Instance.Init();
        }

        public static IEnumerator SetTargetSmooth(Transform target)
        {
            if (!target)
            {
                yield break;
            }

            Instance._isMovingToTarget = true;

            Instance.target = target;
            Instance.UpdateDesiredPosition();

            DG.Tweening.Tweener moveTween = Instance.transform.DOMove(Instance._position, 1);
            yield return moveTween.WaitForCompletion();

            Instance._isMovingToTarget = false;
        }

        private void Init()
        {
            //If there is no target, create a temporary target at 'distance' from the cameras current viewpoint
            if (!target)
            {
                GameObject go = new GameObject("Cam Target");
                go.transform.position = transform.position + (transform.forward * distance);
                target = go.transform;
            }

            _currentDistance = distance;
            _desiredDistance = distance;

            //be sure to grab the current rotations as starting points.
            _position = transform.position;
            _rotation = transform.rotation;
            _desiredRotation = transform.rotation;

            _xDeg = Vector3.Angle(Vector3.right, transform.right);
            _yDeg = Vector3.Angle(Vector3.up, transform.up);

            UpdateRotation();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                Debug.Log("Disabling rotation");
                _isRotating = false;
            }
        }

        private void Update()
        {
            if (!target)
            {
                return;
            }

            if (Instance._isMovingToTarget)
            {
                return;
            }

            if (!_isRotating)
            {
                if (Input.GetMouseButtonDown(1) && !Util.IsPointerOverUI())
                {
                    _isRotating = true;
                }
                return;
            }

            if (_isRotating)
            {
                if (Input.GetMouseButtonUp(1))
                {
                    _isRotating = false;
                    return;
                }
            }

            if (_isRotating)
            {
                UpdateRotation();
            }
        }

        private void LateUpdate()
        {
            if (!target)
            {
                return;
            }

            if (Instance._isMovingToTarget)
            {
                return;
            }

            UpdateCameraZoom();
            UpdateCameraMove();
        }

        private void UpdateCameraMove()
        {
            if (!enableWASDMovement || !Input.GetMouseButton(1))
            {
                return;
            }

            float verticalSpeed = Input.GetAxis("Vertical");
            float horizontalSpeed = Input.GetAxis("Horizontal");
            float panVerticalSpeed = Input.GetAxis("Pan Vertical");

            //grab the rotation of the camera so we can move in a psuedo local XY space
            target.rotation = transform.rotation;
            target.Translate(Vector3.right * horizontalSpeed * panSpeed);
            target.Translate(Vector3.up * panVerticalSpeed * panSpeed, Space.World);
            target.Translate(Vector3.forward * verticalSpeed * panSpeed);
        }

        private void UpdateRotation()
        {
            _xDeg += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            _yDeg -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            //Clamp the vertical axis for the orbit
            _yDeg = ClampAngle(_yDeg, yMinLimit, yMaxLimit);

            // set camera rotation
            _desiredRotation = Quaternion.Euler(_yDeg, _xDeg, 0);

            _rotation = _desiredRotation;
            transform.rotation = _rotation;
        }

        private void UpdateCameraZoom()
        {
            float scrollWheelMovement = Input.GetAxis("Mouse ScrollWheel");
            if (Util.IsMouseOverGameWindow && !Util.IsPointerOverUI())
            {
                if (Input.GetMouseButton(1))
                {
                    if (scrollWheelMovement > 0)
                    {
                        int newZoomRate = Mathf.Clamp(zoomRate + zoomRateIncreaseIncrement, zoomRateMin, zoomRateMax);
                        zoomRate = newZoomRate;
                    }
                    else if (scrollWheelMovement < 0)
                    {
                        int newZoomRate = Mathf.Clamp(zoomRate - zoomRateIncreaseIncrement, zoomRateMin, zoomRateMax);
                        zoomRate = newZoomRate;
                    }
                }
                else
                {
                    // disable scroll zooming when holding alt so alt can be used for something else.
                    bool holdingAlt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
                    if (!holdingAlt)
                    {
                        // affect the desired Zoom distance if we roll the scrollwheel
                        _desiredDistance -= scrollWheelMovement * Time.deltaTime * zoomRate * Mathf.Abs(_desiredDistance);
                    }
                }
            }

            UpdateDesiredPosition();
            transform.position = _position;
        }

        private void UpdateDesiredPosition()
        {
            //clamp the zoom min/max
            _desiredDistance = Mathf.Clamp(_desiredDistance, minDistance, maxDistance);
            // For smoothing of the zoom, lerp distance
            _currentDistance = Mathf.Lerp(_currentDistance, _desiredDistance, Time.deltaTime * zoomDampening);

            // calculate position based on the new currentDistance
            _position = target.position - (_rotation * Vector3.forward * _currentDistance + targetOffset);
        }

        private static float ClampAngle(float angle, float min, float max)
        {
            if (angle < -360)
                angle += 360;
            if (angle > 360)
                angle -= 360;
            return Mathf.Clamp(angle, min, max);
        }
    }
}