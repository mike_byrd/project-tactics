using System;
using System.Collections;
using System.Collections.Generic;
using MCB.UI;
using UnityEngine;

namespace MCB.UI
{
    public class UIModalView : UIView
    {
        public static bool IsModalViewShowing { get; private set; }
        public static UIModalView CurrentModelView { get; private set; }

        /// <summary>
        /// Requires call to base.
        /// </summary>
        /// <param name="setFocused"></param>
        public override void Show(bool setFocused = false)
        {
            base.Show(setFocused);
            IsModalViewShowing = true;
            CurrentModelView = this;
        }

        /// <summary>
        /// Requires call to base.
        /// </summary>
        public override void Hide()
        {
            base.Hide();
            IsModalViewShowing = false;
            CurrentModelView = null;
        }
    }
}
