using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.UI
{
    public abstract class UIPanel : UIView, IDismissible
    {
        public UIController Controller { get; private set; }

        public void Init(UIController controller)
        {
            Controller = controller;
        }

        public void Dismiss()
        {
            Controller.Dismiss();
        }
    }
}
