using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MCB.UI
{
    public interface IDismissible
    {
        void Dismiss();
    }

    public class UIView : MonoBehaviour
    {
        public static UIView CurrentFocus => _focusList.Count > 0 ? _focusList[_focusList.Count - 1] : null;
        private static readonly List<UIView> _focusList = new List<UIView>();

        /// <summary>
        /// Shows the view.
        /// </summary>
        /// <param name="setFocused">Moves the view to the botton of it's parent so it appears in front.</param>
        public virtual void Show(bool setFocused = false)
        {
            gameObject.SetActive(true);
            if (setFocused)
            {
                SetCurrentFocus(this);
            }
        }

        public virtual void Hide()
        {
            _focusList.Remove(this);
            gameObject.SetActive(false);
        }

        public virtual void Toggle()
        {
            gameObject.SetActive(!IsVisible);
        }

        public bool IsVisible => gameObject.activeSelf;

        public static void SetCurrentFocus(UIView view)
        {
            if (!view || view.transform.parent == null)
            {
                return;
            }

            int currentViewIndex = _focusList.IndexOf(view);

            // See if we have this view
            if (currentViewIndex >= 0)
            {
                // If we do, move it to the top of the list.
                UIView removed = _focusList[currentViewIndex];
                _focusList.RemoveAt(currentViewIndex);
                _focusList.Add(removed);
            }
            else
            {
                _focusList.Add(view);
            }

            Transform focusTransform = CurrentFocus.transform;
            int siblingIndex = focusTransform.GetSiblingIndex();
            int lastSiblingIndex = focusTransform.parent.childCount - 1;

            if (lastSiblingIndex < 0 || siblingIndex == lastSiblingIndex)
            {
                return;
            }

            focusTransform.SetAsLastSibling();
        }
    }
}