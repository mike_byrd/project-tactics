using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCB.UI
{
    public abstract class UIController : StateMachine
    {
        public virtual void Dismiss()
        {

        }
    }
}
