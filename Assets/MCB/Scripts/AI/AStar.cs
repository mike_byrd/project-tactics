﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Vector2 = System.Numerics.Vector2;

namespace MCB.AI
{
    public class Node
    {
        public Node Parent;
        public Vector2 Position;

        public float DistanceToTarget;
        public float Cost;
        public float Weight;

        // Total cost of the node (F)
        public float TotalCost
        {
            get
            {
                if (DistanceToTarget != -1 && Cost != -1)
                    return DistanceToTarget + Cost;
                else
                    return -1;
            }
        }
        public bool Walkable;

        public Node(Vector2 pos, bool walkable, float weight = 1)
        {
            Parent = null;
            Position = pos;
            DistanceToTarget = -1;
            Cost = 1;
            Weight = weight;
            Walkable = walkable;
        }
    }

    /// <summary>
    /// F = G + H
    /// </summary>
    public class Astar
    {
        Node[,] Grid;
        int GridRows
        {
            get
            {
               return Grid.GetLength(0);
            }
        }
        int GridCols
        {
            get
            {
                return Grid.GetLength(1);
            }
        }

        public Astar(Node[,] grid)
        {
            Grid = grid;
        }

        public Stack<Node> FindPath(Vector2 from, Vector2 to, float jumpHeight = 999, bool allowDiagonal = false)
        {
            if (from == to) return null;

            Node start = Grid[(int)from.X, (int)from.Y];
            Node end = Grid[(int)to.X, (int)to.Y];

            Stack<Node> Path = new Stack<Node>();
            List<Node> OpenList = new List<Node>();
            List<Node> ClosedList = new List<Node>();
            List<Node> adjacencies;
            Node current = start;

            // add start node to Open List
            OpenList.Add(start);

            while(OpenList.Count != 0 && !ClosedList.Exists(closedNode => closedNode.Position == end.Position))
            {
                current = OpenList[0];
                OpenList.Remove(current);
                ClosedList.Add(current);
                adjacencies = GetAdjacentNodes(current, allowDiagonal);

                foreach(Node n in adjacencies)
                {
                    float currentDistance = Mathf.Abs(current.Weight - n.Weight);
                    if (!ClosedList.Contains(n) && n.Walkable && currentDistance <= jumpHeight)
                    {
                        if (!OpenList.Contains(n))
                        {
                            n.Parent = current;
                            n.DistanceToTarget = Math.Abs(n.Position.X - end.Position.X) + Math.Abs(n.Position.Y - end.Position.Y);
                            n.Cost = n.Weight + n.Parent.Cost + (start.Weight - n.Weight);
                            OpenList.Add(n);
                            OpenList = OpenList.OrderBy(node => node.TotalCost).ToList<Node>();
                        }
                    }
                }
            }

            // construct path, if end was not closed return null
            if(!ClosedList.Exists(x => x.Position == end.Position))
            {
                return null;
            }

            // if all good, return path
            Node temp = ClosedList[ClosedList.IndexOf(current)];
            if (temp == null) return null;
            do
            {
                Path.Push(temp);
                temp = temp.Parent;
            } while (temp != start && temp != null) ;
            return Path;
        }

        private List<Node> GetAdjacentNodes(Node n, bool allowDiagonal = false)
        {
            List<Node> temp = new List<Node>();

            int row = (int)n.Position.Y;
            int col = (int)n.Position.X;

            if(row + 1 < GridRows)
            {
                temp.Add(Grid[col, row + 1]);
                if (allowDiagonal)
                {
                    if (col + 1 < GridCols)
                    {
                        temp.Add(Grid[col + 1, row + 1]);
                    }

                    if (col - 1 >= 0)
                    {
                        temp.Add(Grid[col - 1, row + 1]);
                    }
                }
            }
            if(row - 1 >= 0)
            {
                temp.Add(Grid[col, row - 1]);
                if (allowDiagonal)
                {
                    if (col + 1 < GridCols)
                    {
                        temp.Add(Grid[col + 1, row - 1]);
                    }

                    if (col - 1 >= 0)
                    {
                        temp.Add(Grid[col - 1, row - 1]);
                    }
                }
            }
            if(col - 1 >= 0)
            {
                temp.Add(Grid[col - 1, row]);
            }
            if(col + 1 < GridCols)
            {
                temp.Add(Grid[col + 1, row]);
            }

            return temp;
        }
    }
}