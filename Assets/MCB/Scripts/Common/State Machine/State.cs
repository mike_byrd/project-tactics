using UnityEngine;
using System.Collections;

namespace MCB
{
    public abstract class State : MonoBehaviour
    {
        /// <summary>
        /// When overriding this, always call the base function, or implement the code from State.cs
        /// </summary>
        public virtual void Enter ()
        {
            enabled = true;
            AddListeners();
        }

        /// <summary>
        /// When overriding this, always call the base function, or implement the code from State.cs
        /// </summary>
        public virtual void Exit ()
        {
            enabled = false;
            RemoveListeners();
        }

        /// <summary>
        /// When overriding this, always call the base function, or implement the code from State.cs
        /// </summary>
        protected virtual void OnDestroy ()
        {
            RemoveListeners();
        }

        protected virtual void AddListeners ()
        {

        }

        protected virtual void RemoveListeners ()
        {

        }
    }
}