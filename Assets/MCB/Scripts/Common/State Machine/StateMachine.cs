﻿using UnityEngine;
using System.Collections;

namespace MCB
{
    public class StateMachine : MonoBehaviour
    {
        public virtual State CurrentState
        {
            get => _currentState;
            set => Transition (value);
        }

        public virtual State PreviousState
        {
            get => _previousState;
            private set => _previousState = value;
        }

        protected State _previousState;
        protected State _currentState;
        protected bool _inTransition;

        public virtual T GetState<T> () where T : State
        {
            T target = GetComponent<T>();
            if (target == null)
            {
                target = gameObject.AddComponent<T>();
            }

            return target;
        }

        /// <summary>
        /// NOTE: OnEnter gets called before Start due to CurrentState calling Transition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public virtual void ChangeState<T> () where T : State
        {
            CurrentState = GetState<T>();
        }

        public virtual void ChangeToPreviousState()
        {
            if (!PreviousState)
            {
                return;
            }

            Transition(PreviousState);
        }

        protected virtual void Transition (State nextState)
        {
            if (_currentState == nextState)
            {
                Debug.LogWarning($"Current state is already: {nextState}");
                return;
            }

            if (_inTransition)
            {
                Debug.LogWarning($"Already transitioning to: {nextState}");
                return;
            }

            _inTransition = true;

            if (_currentState != null)
            {
                _currentState.Exit();
            }

            Debug.Log($"MCB: Changing state from {_currentState} to {nextState}");
            _previousState = _currentState;
            _currentState = nextState;

            if (_currentState != null)
            {
                _currentState.Enter();
            }

            _inTransition = false;
        }
    }
}